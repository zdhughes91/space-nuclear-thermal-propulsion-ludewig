# Space Nuclear Thermal Propulsion - Ludewig et al. 

Author:     Zach Hughes, Texas A&M University (zhughes@tamu.edu)

### Personal Notes
1. Drag models for inner/outer need a citation. 


### To do:

## Introduction

This repository contains the transient coupled thermal-hydraulic/neutronic solution of a particle bed reactor (PBR) core using the OpenFOAM-based multi-physics solver GeN-Foam. This core was designed for use in a Nuclear Thermal Propulsion (NTP) system. The PBR design was chosen using the paper by ([Ludewig, 1993](https://www.osti.gov/biblio/10150090)). 

<img src="images/ludewigImages/sntpCoreDiagram.png" alt="Reactor" width="450"/>

*Fig 1: PBR Core Diagram*

The core design includes 37 fuel assemblies arranged in a hexagonal lattice. This core is 63.92cm in diameter as well as in height, and has an operating power of 1000 MW. Fig 2 is a more detailed diagram of a single assembly. 

<img src="images/ludewigImages/assemblyDiagram.png" alt="Assembly" width="450"/>

*Fig 2: PBR Assembly Diagram*

The moderator material was chosen to be Beryllium, while the hot frit, cold frit, were chosen to be porous ZrC and porous Beryllium respectively. The fuel bed is comprised of particle fuel which creates a porosity in the fuel region of 38%. This particle is comprised of a fuel kernel surrounded by a ZrC shell. The fuel material is (U0.1Zr0.9)C0.829 and has a 475$\mu m$ diameter. The shell has a thickness of 60$\mu m$. An axial diagram of the fuel assembly is shown below in Fig. 3. 

<img src="images/ludewigImages/sevenElementDiagram.png" alt="Assembly" width="450"/>

*Fig 3: Axial PBR Assembly Diagram*

In this assembly, the coolant enters through the outer propellant region (the space in between the cold frit and the moderator) and exits only out of the inner propellant region. This flow path ensures that all coolant passes through the fuel bed before exiting the assembly, however the diagonal flow through a porous region can create complications in modeling. 

Below is an illustration of the flow path. 
<img src="images/explainFlow.png" alt="Assembly" width="450"/>

*Fig 4: Flow Path Explaination*


# Model
## Thermal Hydraulic Model

### Heat Transfer Correlations
To determine the rate of heat transfer between a moving fluid and solid, heat transfer correlations are often used. The two following heat transfer correlations are based on experimental data on flow in packed beds, and they are both valid for the 100 < Re < 250 range that occurs in the PBR flow. 

([Achenbach, 1994](https://www.sciencedirect.com/science/article/pii/089417779400077L#aep-bibliography-id6)) **Eqn34**: Valid for:  $50 < Re < 2\times 10^{4}$

$$
    Nu = (1-\frac{d}{D})Pr^{\frac{1}{3}}Re^{0.61}
$$
d is pebble diameter and D is the equivalent diameter. 

([Nield & Bejan, 2006](https://link.springer.com/book/10.1007/978-1-4614-5541-7)) **Eqn2.16**: Valid for: 100 < Re

*Currently using this one*
$$
    Nu = (\frac{0.255}{\epsilon})Pr^{\frac{1}{3}}Re^{\frac{2}{3}}
$$

### Heat Conductances for Sub-scale Structure
To predict fuel particle temperatures from the fluid surface temperature, heat conductances are needed. These heat conductances were found between four nodes: the fuel maximum, the fuel average, the shell average, and the shell surface temperature. The analytical temperature distribution from the fuel particle surface to the kernel center was found by integrating the steady-state heat equation in spherical coordinates. 

$$
    \frac{1}{r^{2}}\frac{\partial}{\partial r}(k r^{2} \frac{\partial T}{\partial r}) = -q_{v}
$$

Along with the following boundary conditions:
$$
    T(R) = T_{s}
$$
$$
    \frac{\partial T(0)}{\partial r} = 0
$$

The numerical solution was then found using laplacianFoam in order to verify the analytical solution was done correct.y Both solutions to this problem are shown below in Fig. xxa. 

<img src="images/THimages/particleTemperatures.png" alt="Assembly" width="450" class="center"/>

*Fig: xxa*

> Since we have an analytical solution (which was further verified by a numerical solution), any error in the heat conductance model must be in the thermal conductivity/geometry of the particle.

### Friction Factor Correlation 
The following two friction factor correlations are based on experimental data on flow in packed beds, and they are both valid for the 100 < Re < 250 range that occurs in the PBR flow. 

([Nield & Bejan, 2006](https://link.springer.com/book/10.1007/978-1-4614-5541-7)) **Fig1.3**:

$$
    f_{K} = 0.55 + \frac{1}{Re}
$$

([Achenbach, 1994](https://www.sciencedirect.com/science/article/pii/089417779400077L#aep-bibliography-id6)) **Eqn21**: Valid for: $ Re < 10^{5} $

$$
    f_{K} = \frac{320(1-\epsilon)}{Re}
$$

### Turbulence Model
To model the turbulence, the k-$\epsilon$ turbulence model was used. This turbulence model creates two new partial differential equations to find the turublent kinetic energy and turbulent dissipation energy. These PDEs require boundary and initial conditions, which are were solved for using the equations below. 

The initial and boundary conditions for this model were found using the equations for turbulent kinetic energy:

$$
    k = \frac{3}{2}(IU)^{2}
$$

Where I is the turbulence intensity and U is the inlet velocity. The folowing equation for the turbulent dissipation energy was also used:
$$
    \epsilon = \frac{C_{\mu}^{0.75} k^{1.5}}{L}
$$

Where $C_{\mu}$ was taken to be 0.09. 

### Optimization of Cold Frit via Porosity Distribution
Due to axial power peaking in each fuel assembly, the maximum temperature in each assembly exceeded the melting temperature of the fuel. To address this, a porosity distribution implemented in the cold frit with the goal of flattening the temperature profile while minimizing additional pressure drop. Four analytical methods were developed in order to solve for the porosity distribution, and each of them require splitting the cold frit into axial zones and finding the power generated beneath each zone so the required mass flow rate can be found.

1. Pressure drop through cold frit constant
2. Pressure drop through all porous regions is constant
3. Porosity is inversely proportional to power, with mean porosity = 0.4
4. Enthalpy rise of coolant through all porous regions is constant

> In all of the work done, only five axial zones were created. While this made for an extremely coarse porosity distribution, it provides a starting point for evaluating each of the methods developed. 

## Neutronic Model

### Deterministic
Deterministic neutronics is handled natively within the GeN-Foam solver. Diffusion neutronics was solved using group cross sections generated by the Serpent2 model, and the result gave a cosine-like shape that leaned towards the inlet side of the assembly. This is likely due to a significantly ($x10^{2}$) higher hydrogen density in the volume closer to the inlet than at the outlet. 

### Monte Carlo
A Serpent2 monte carlo model was made of the SNTP core for neutronic analysis. The image below shows a cross section of the core. 

<div style="text-align: center;">
    <img src="images/serpentImages/fullCore.png" alt="serpentReactor" width="450"/>
</div>


## Uncertainty Quantification

### Grid Convergence
To determine the impact of the mesh on the final solution, the grid convergence index (GCI) ([Roache, 1994](https://www3.nd.edu/~coast/jjwteach/www/www/60130/CourseLectureNotes/Roache_1994.pdf)) was used. The GCI quantifies the percentage difference between the computed value and the asymptotic numerical value. It is found by first calculating the order of accuracy,p, for a solution variable f. 

$$
    p_{obs} = \frac{ln(\frac{f_{3}-f_{2}}{f_{2}-f_{1}})}{ln(r)}
$$
*Where r is the grid refinement factor and r12=r23*

Then, the GCI is solved for each of the meshes by employing the following equations. 

$$
    GCI_{12} = \frac{F_{s}}{r^{p}-1} \left | f_{2}-f_{1} \right |
$$

These were solved for various meshes where the solution variables were inlet pressure, outlet bulk velocity, outlet bulk temperature, and maximum fuel temperature. The results are shown below in Fig. xxb. The GCI was found individually for axial and radial mesh refinement. In the axial cases, the radial mesh was set to 2. For the radial cases, the axial mesh was set to 300. 

<img src="images/uncertaintyImages/bothGCIconvergence.png" alt="Assembly" width="450"/>

*Fig xxb: Grid Convergence in 2D Assembly Wedge*

While a radial mesh of 2 cells results in an almost 10% GCI in the outlet Tbulk, refining the radial mesh comes at a large computational cost. This is because the inlet flow rate is around 100 m/s, and the cold frit has a thickness of 2mm. Thus, if 4 radial mesh cells exist in the cold frit, each have a thickness of 0.5mm. Using an initial time step of 2e-5 seconds, a maximum Courant number of 0.4 would be allowable in order to prevent large amounts of mass loss in the simulation. Since velocity in the case reaches as high as 650 m/s, a coarser radial mesh was accepted with the cost of decreased accuracy in order to provide reasonable computational times. 

# Results

## Thermal Hydraulics
Using a power distribution generated from the diffusion solver with fixed gradient boundary conditions (shown & explained below), the steady state solution of the fuel assembly was found. This is shown below in Fig xx.

<img src="images/THimages/steadyStateUniformPorosity.png" alt="Assembly" width="450"/>

*Fig xxc: Deterministic vs Stochastic Axial Power Distribution in Assembly 15*

### Cold Frit Optimization

<img src="images/THimages/comparePorosityMethods.png" alt="Assembly" width="450"/>

*Fig xxc: Deterministic vs Stochastic Axial Power Distribution in Assembly 15*

## Neutronics
Deterministic neutronics was solved using the neutron diffusion equations. The boundary conditions for these equations had a significant impact on the solution, as shown in Figure xxc and Figure xxd below. 

<img src="images/compareNeutronics/assembly15_power.png" alt="Assembly" width="450"/>

*Fig xxc: Deterministic vs Stochastic Axial Power Distribution in Assembly 15*

<img src="images/compareNeutronics/assembly22_power.png" alt="Assembly" width="450"/>

*Fig xxd: Deterministic vs Stochastic Axial Power Distribution in Assembly 22*

The boundary conditions in both methods were found by finding the surface flux at the boundaries using Serpent. For the fixed value case, the surface flux was imposed for both the thermal (<0.625eV) and fast (>0.625eV) energy groups using the Serpent results. It is evident from this comparision that the diffusion equations do not describe the particle bed NTP. With that said, the solution may be utilized in certain instances to improve computational time, such as with point kinetics, however this has not yet been studied. 

An image of the power density generated by diffusion with fixed value BC is shown below in Fig xxe. 

<img src="images/diffusionImages/slicePowerDensity.png" alt="Assembly" width="450"/>
*Fig xxe: Power Density from Diffusion w/ Fixed Value BC*

It can be seen in Fig xxe that significant assembly-wise power peaking occurs as well as axial & azimuthal peaking within each assembly. 

