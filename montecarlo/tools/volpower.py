"""
    this script confirms that the serpent output is power density and not power

"""

import numpy as np

q = np.genfromtxt('../coupledCore/powerDensity',skip_header=14,skip_footer=2)
v = np.genfromtxt('cellVolumes',skip_header=23,skip_footer=25287-25226)

power = [q[i]*v[i] for i in range(len(q))]

print(f"Total Power = {np.sum(power):0.7e} W")