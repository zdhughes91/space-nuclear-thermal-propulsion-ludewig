"""
    This is a script for generating a materials file from an OpenFOAM polyMesh folder. 
    The materials file relates each cell to a specific material. This is necessary for the
    Serpent2 coupling because Serpent needs to know whih material to put in each cell
    when reading the unstructured mesh.

    Author: Zach Hughes - zhughes@tamu.edu

    To prepare for this script:
        1. create the polyMesh folder with all cellZones representing different materials
        2. type: splitMeshRegions -overwrite -cellZones
        3. This code will need the 'cellToRegion' file as well as the order of cellZones, 
           which can be found at the top of the regions' polyMesh folder in the cellZones 
           file

Link to definition of materials file:
    https://serpent.vtt.fi/mediawiki/index.php/Unstructured_mesh_based_input#Material_file

"""
import numpy as np

# --- user entered information

cellZoneOrder = ['o1','o2','fuel','outerPropellant','o3','hotFrit','innerPropellant','o4','o5']

# ---


celltoregion = np.genfromtxt('cellToRegion',skip_header=21,skip_footer=25223-25221)
print(celltoregion,len(celltoregion))


with open('materials','w') as file:
    for i,region in enumerate(celltoregion):
        if i == 0: file.write(f"{len(celltoregion)} "+'\n')

        file.write(f"{cellZoneOrder[int(region)]}mat"+'\n')

