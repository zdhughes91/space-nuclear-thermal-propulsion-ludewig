"""

"""
def perfectGas(pressure,temperature):
    """ pressure in MPA, temperature in K
        returns density in g/cc
    """
    pressure = pressure * 9.869 # MPa to atm conversion
    return 1e-3*(pressure * 2.01568)/ (0.0821 * temperature) # g/mol to kg/mol, then using L to m^3

nominalDensity = 2 # what is nominal density defined as in type9.ifc

cellZoneOrder = ['o1','o2','fuel','outerPropellant','o3','hotFrit','innerPropellant','o4','o5']

import numpy as np

#celltoregion = np.genfromtxt('cellToRegion',skip_header=21,skip_footer=25223-25221)

fuelDens = (0.006171 *0.41999993) + (6.73*0.29222371) + (5.94719911*0.28777637)

packingFractionHotFrit = 0.3 #both found in fluidRegion/phaseProperties
packingFractionColdFrit = 0.6
hfDens = (3.365*packingFractionHotFrit) + (perfectGas(8.0,2701.0)*(1-packingFractionHotFrit)) # 8MPa chosen roughtly, 2701 taken from USNC m5
cfDens = (1.85*packingFractionColdFrit) + (perfectGas(7.7,600.0)*(1-packingFractionColdFrit)) # temp taken from USNC m10 in serpent file

densityList = [1.85,1.85,fuelDens,0.0004922,1.85,3.365,0.01023,1.85,1.85]

print('hf hydro',perfectGas(8.0,2701.0))
print('cf hydro',perfectGas(7.7,600.0))

print(f'baseCoolInletDensity= {perfectGas(9.1,200)} g/cc')
print(f'perturbedCoolInletDensity= {perfectGas(7,3200)} g/cc')
# with open('density','w') as file:
#     for i,region in enumerate(celltoregion):
#         if i == 0: file.write(f"{len(celltoregion)} "+'\n')

#         file.write(f"{densityList[int(region)]/nominalDensity} "+'\n')