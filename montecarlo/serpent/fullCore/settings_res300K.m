
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  62]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/serpent/fullCore' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu May 23 12:03:56 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu May 23 12:35:54 2024' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 300 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1716483836755 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.15542E+00  9.47101E-01  9.56137E-01  9.69222E-01  1.05162E+00  9.80043E-01  9.64740E-01  9.75715E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.3E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  3.98914E-01 0.00013  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  6.01086E-01 8.8E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.75184E-01 6.8E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.49108E-01 7.0E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  3.96067E+00 0.00011  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.34384E-01 5.2E-06  6.31951E-02 7.3E-05  2.42063E-03 0.00031  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.77239E+01 0.00016  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.74273E+01 0.00016  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.07329E+01 0.00016  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.40121E+01 0.00019  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 300 ;
SIMULATED_HISTORIES       (idx, 1)        = 30000227 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00001E+05 0.00030 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00001E+05 0.00030 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.46195E+02 ;
RUNNING_TIME              (idx, 1)        =  3.19648E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  4.11317E-01  4.11317E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.74167E-02  1.74167E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  3.15361E+01  3.15361E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  3.19318E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.70206 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.79132E+00 0.00166 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.53094E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 4879.71 ;
MEMSIZE                   (idx, 1)        = 4750.92 ;
XS_MEMSIZE                (idx, 1)        = 3194.76 ;
MAT_MEMSIZE               (idx, 1)        = 593.51 ;
RES_MEMSIZE               (idx, 1)        = 294.61 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.03 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 128.80 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 53 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 948066 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 20 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 53 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 53 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 1698 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  7.02876E+14 0.00024  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.26204E-03 0.00398 ];
U235_FISS                 (idx, [1:   4]) = [  3.08678E+19 0.00020  9.99890E-01 2.6E-06 ];
U238_FISS                 (idx, [1:   4]) = [  3.39272E+15 0.02408  1.09892E-04 0.02407 ];
U235_CAPT                 (idx, [1:   4]) = [  6.05511E+18 0.00059  3.07179E-01 0.00052 ];
U238_CAPT                 (idx, [1:   4]) = [  1.56727E+17 0.00400  7.95061E-03 0.00396 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 30000227 3.00000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 8.90897E+05 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 30000227 3.08909E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 8183000 8.41354E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 12920815 1.31765E+07 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 8896412 9.30085E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 30000227 3.08909E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 6.63102E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.00000E+09 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.13657E+02 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  7.52155E+19 3.4E-07 ];
TOT_FISSRATE              (idx, [1:   2]) = [  3.08572E+19 2.4E-09 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  1.97162E+19 0.00024 ];
TOT_ABSRATE               (idx, [1:   2]) = [  5.05735E+19 9.5E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  7.02876E+19 0.00024 ];
TOT_FLUX                  (idx, [1:   2]) = [  4.71957E+21 0.00025 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.17914E+19 0.00049 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  7.23649E+19 0.00018 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  2.71453E+21 0.00025 ];
INI_FMASS                 (idx, 1)        =  8.79843E+00 ;
TOT_FMASS                 (idx, 1)        =  8.79843E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06073E+00 0.00011 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.61473E-01 0.00014 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.05217E-01 1.0E-04 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09238E+00 8.8E-05 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.41184E-01 0.00013 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.30904E-01 5.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.55167E+00 0.00018 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07061E+00 0.00021 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43753E+00 3.4E-07 ];
FISSE                     (idx, [1:   2]) = [  2.02271E+02 1.6E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07059E+00 0.00022  1.06234E+00 0.00021  8.26977E-03 0.00323 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07026E+00 0.00018 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07013E+00 0.00024 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07026E+00 0.00018 ];
ABS_KINF                  (idx, [1:   2]) = [  1.55126E+00 9.6E-05 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.94089E+01 3.8E-05 ];
IMP_ALF                   (idx, [1:   2]) = [  1.94103E+01 1.6E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.44514E-08 0.00074 ];
IMP_EALF                  (idx, [1:   2]) = [  7.43411E-08 0.00031 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.50133E-02 0.00387 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.49852E-02 0.00052 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.46561E-03 0.00240  2.16203E-04 0.01336  9.83608E-04 0.00595  5.99216E-04 0.00725  1.27536E-03 0.00498  2.12987E-03 0.00417  5.82181E-04 0.00772  5.30193E-04 0.00770  1.48978E-04 0.01541 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.07310E-01 0.00353  1.24667E-02 0.0E+00  2.82917E-02 4.9E-09  4.25244E-02 7.1E-09  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 5.2E-09  3.55460E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.71910E-03 0.00346  2.69824E-04 0.01881  1.14487E-03 0.00872  7.41212E-04 0.01030  1.52606E-03 0.00738  2.54083E-03 0.00611  6.88790E-04 0.01100  6.26381E-04 0.01141  1.81124E-04 0.02245 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.06902E-01 0.00523  1.24667E-02 0.0E+00  2.82917E-02 4.9E-09  4.25244E-02 7.2E-09  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 5.2E-09  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  7.56305E-05 0.00054  7.56347E-05 0.00054  7.51181E-05 0.00460 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  8.09677E-05 0.00049  8.09722E-05 0.00049  8.04210E-05 0.00461 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.71105E-03 0.00340  2.69157E-04 0.01751  1.13239E-03 0.00840  7.40625E-04 0.00993  1.51998E-03 0.00746  2.54459E-03 0.00578  6.96462E-04 0.01137  6.31983E-04 0.01103  1.75863E-04 0.02177 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.06757E-01 0.00500  1.24667E-02 0.0E+00  2.82917E-02 4.9E-09  4.25244E-02 7.1E-09  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 5.2E-09  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  7.39563E-05 0.00763  7.39607E-05 0.00763  7.28118E-05 0.01574 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  7.91730E-05 0.00762  7.91776E-05 0.00762  7.79462E-05 0.01573 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  7.65497E-03 0.01356  2.64521E-04 0.05412  1.09845E-03 0.02996  7.51773E-04 0.03412  1.51697E-03 0.02533  2.52412E-03 0.01967  6.74275E-04 0.03497  6.53503E-04 0.03639  1.71364E-04 0.07031 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.08051E-01 0.01632  1.24667E-02 0.0E+00  2.82917E-02 5.4E-09  4.25244E-02 7.4E-09  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 5.3E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  7.65479E-03 0.01336  2.62832E-04 0.05275  1.09563E-03 0.02856  7.52610E-04 0.03309  1.50500E-03 0.02504  2.53676E-03 0.01956  6.74997E-04 0.03385  6.58184E-04 0.03571  1.68766E-04 0.06654 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.08240E-01 0.01583  1.24667E-02 0.0E+00  2.82917E-02 5.4E-09  4.25244E-02 7.1E-09  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 5.4E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.03583E+02 0.01141 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  7.54425E-05 0.00033 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  8.07666E-05 0.00024 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.81156E-03 0.00202 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.03548E+02 0.00207 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.04247E-06 0.00021 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.06079E-06 0.00019  4.06202E-06 0.00019  3.90123E-06 0.00227 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  9.90593E-05 0.00028  9.90727E-05 0.00028  9.73230E-05 0.00309 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.75878E-01 0.00014  6.75077E-01 0.00014  8.00278E-01 0.00363 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.31223E+01 0.00473 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.74273E+01 0.00016  4.26942E+01 0.00019 ];

