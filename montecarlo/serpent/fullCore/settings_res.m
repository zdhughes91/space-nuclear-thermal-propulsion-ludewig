
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  62]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/serpent/fullCore' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Sat May 25 12:00:10 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Sat May 25 12:16:09 2024' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 300 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1716656410164 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.20498E+00  1.04141E+00  1.01603E+00  9.94119E-01  8.91605E-01  9.66769E-01  9.06662E-01  9.78431E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.00152E-01 0.00014  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.99848E-01 9.7E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.75306E-01 6.4E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.49901E-01 7.0E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  3.96453E+00 0.00012  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.34411E-01 5.5E-06  6.31693E-02 7.5E-05  2.41938E-03 0.00031  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.78124E+01 0.00016  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.75156E+01 0.00017  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.07068E+01 0.00016  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.40663E+01 0.00021  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 300 ;
SIMULATED_HISTORIES       (idx, 1)        = 30000356 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00001E+05 0.00035 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00001E+05 0.00035 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.21474E+02 ;
RUNNING_TIME              (idx, 1)        =  1.59940E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  3.19117E-01  3.19117E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.01000E-02  1.01000E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.56647E+01  1.56647E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.59786E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.59499 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.72754E+00 0.00200 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.34847E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 5348.61 ;
MEMSIZE                   (idx, 1)        = 5219.98 ;
XS_MEMSIZE                (idx, 1)        = 3663.83 ;
MAT_MEMSIZE               (idx, 1)        = 593.51 ;
RES_MEMSIZE               (idx, 1)        = 294.61 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.03 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 128.63 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 53 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 948066 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 24 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 62 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 62 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 2036 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 1 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  7.05536E+14 0.00025  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.71894E-03 0.00386 ];
U235_FISS                 (idx, [1:   4]) = [  3.08532E+19 0.00022  9.99875E-01 3.3E-06 ];
U238_FISS                 (idx, [1:   4]) = [  3.82647E+15 0.02632  1.23991E-04 0.02631 ];
U235_CAPT                 (idx, [1:   4]) = [  6.11300E+18 0.00061  3.07223E-01 0.00053 ];
U238_CAPT                 (idx, [1:   4]) = [  1.73917E+17 0.00389  8.74036E-03 0.00386 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 30000356 3.00000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 8.91544E+05 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 30000356 3.08915E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 8229159 8.46070E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 12866304 1.31208E+07 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 8904893 9.31000E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 30000356 3.08915E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.55717E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.00000E+09 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.13746E+02 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  7.52156E+19 3.5E-07 ];
TOT_FISSRATE              (idx, [1:   2]) = [  3.08572E+19 1.3E-09 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  1.99046E+19 0.00024 ];
TOT_ABSRATE               (idx, [1:   2]) = [  5.07618E+19 9.5E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  7.05536E+19 0.00025 ];
TOT_FLUX                  (idx, [1:   2]) = [  4.74368E+21 0.00025 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.18954E+19 0.00050 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  7.26572E+19 0.00019 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  2.73122E+21 0.00025 ];
INI_FMASS                 (idx, 1)        =  8.79149E+00 ;
TOT_FMASS                 (idx, 1)        =  8.79149E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06019E+00 0.00012 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.59558E-01 0.00014 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.02451E-01 0.00011 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09462E+00 9.3E-05 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.41103E-01 0.00013 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.30595E-01 5.9E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.54580E+00 0.00019 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06608E+00 0.00023 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43754E+00 3.5E-07 ];
FISSE                     (idx, [1:   2]) = [  2.02271E+02 1.7E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06594E+00 0.00024  1.05783E+00 0.00023  8.25035E-03 0.00311 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06601E+00 0.00018 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06610E+00 0.00025 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06601E+00 0.00018 ];
ABS_KINF                  (idx, [1:   2]) = [  1.54564E+00 9.5E-05 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93773E+01 3.9E-05 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93764E+01 1.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.68438E-08 0.00076 ];
IMP_EALF                  (idx, [1:   2]) = [  7.69097E-08 0.00034 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.50417E-02 0.00386 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.50277E-02 0.00053 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.49004E-03 0.00220  2.13869E-04 0.01175  9.99144E-04 0.00619  5.97578E-04 0.00739  1.27868E-03 0.00545  2.13128E-03 0.00381  5.87376E-04 0.00746  5.36075E-04 0.00740  1.46034E-04 0.01546 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.06308E-01 0.00365  1.24667E-02 0.0E+00  2.82917E-02 4.9E-09  4.25244E-02 7.0E-09  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 5.2E-09  3.55460E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.73852E-03 0.00311  2.60000E-04 0.01683  1.16685E-03 0.00887  7.36260E-04 0.00987  1.53836E-03 0.00762  2.53017E-03 0.00562  7.05744E-04 0.01094  6.27195E-04 0.01096  1.73941E-04 0.02175 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.04143E-01 0.00518  1.24667E-02 0.0E+00  2.82917E-02 4.9E-09  4.25244E-02 7.2E-09  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 5.2E-09  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  7.62325E-05 0.00049  7.62418E-05 0.00049  7.50416E-05 0.00465 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  8.12575E-05 0.00042  8.12674E-05 0.00042  7.99874E-05 0.00463 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.72872E-03 0.00321  2.62753E-04 0.01706  1.16489E-03 0.00840  7.34080E-04 0.01021  1.53684E-03 0.00763  2.52053E-03 0.00544  7.04171E-04 0.01048  6.35839E-04 0.01057  1.69631E-04 0.02196 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.03886E-01 0.00500  1.24667E-02 0.0E+00  2.82917E-02 4.9E-09  4.25244E-02 7.0E-09  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 5.2E-09  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  7.45381E-05 0.00763  7.45420E-05 0.00763  7.37392E-05 0.01512 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  7.94519E-05 0.00762  7.94560E-05 0.00762  7.86028E-05 0.01512 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  7.56745E-03 0.01359  2.71118E-04 0.06296  1.12681E-03 0.02954  7.41579E-04 0.03448  1.50455E-03 0.02556  2.42269E-03 0.02050  6.99804E-04 0.03808  6.33280E-04 0.03930  1.67618E-04 0.07253 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.05208E-01 0.01729  1.24667E-02 0.0E+00  2.82917E-02 5.4E-09  4.25244E-02 7.5E-09  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 5.4E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  7.60088E-03 0.01334  2.72904E-04 0.05776  1.12935E-03 0.02863  7.32682E-04 0.03376  1.52407E-03 0.02468  2.45130E-03 0.02014  6.88400E-04 0.03612  6.31462E-04 0.03738  1.70708E-04 0.07073 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.05161E-01 0.01696  1.24667E-02 0.0E+00  2.82917E-02 5.4E-09  4.25244E-02 7.2E-09  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 5.3E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.01483E+02 0.01115 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  7.60481E-05 0.00032 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  8.10611E-05 0.00022 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.73970E-03 0.00192 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.01776E+02 0.00193 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.04492E-06 0.00023 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.05902E-06 0.00021  4.06009E-06 0.00022  3.92002E-06 0.00226 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  9.97339E-05 0.00029  9.97510E-05 0.00029  9.75138E-05 0.00320 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.74003E-01 0.00015  6.73224E-01 0.00015  7.94417E-01 0.00330 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.31227E+01 0.00492 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.75156E+01 0.00017  4.28118E+01 0.00018 ];

