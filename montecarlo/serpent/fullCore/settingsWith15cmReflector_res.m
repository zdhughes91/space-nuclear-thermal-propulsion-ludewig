
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  62]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/serpent/fullCore' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu May 23 11:25:31 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu May 23 12:02:35 2024' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 300 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1716481531578 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.20941E+00  9.24967E-01  9.70940E-01  1.12302E+00  1.00461E+00  9.20210E-01  9.59680E-01  8.87164E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.11122E-01 0.00013  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.88878E-01 9.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.76564E-01 6.1E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.65951E-01 6.7E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  3.88998E+00 0.00011  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.37669E-01 5.3E-06  6.03491E-02 7.9E-05  1.98146E-03 0.00028  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.60524E+01 0.00016  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.58167E+01 0.00016  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.51385E+01 0.00016  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.49964E+01 0.00018  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 300 ;
SIMULATED_HISTORIES       (idx, 1)        = 30000462 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00002E+05 0.00028 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00002E+05 0.00028 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.86438E+02 ;
RUNNING_TIME              (idx, 1)        =  3.70621E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  4.88550E-01  4.88550E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.36167E-02  1.36167E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  3.65600E+01  3.65600E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  3.70291E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.72861 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.81599E+00 0.00160 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.57464E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 4879.72 ;
MEMSIZE                   (idx, 1)        = 4750.92 ;
XS_MEMSIZE                (idx, 1)        = 3194.76 ;
MAT_MEMSIZE               (idx, 1)        = 593.51 ;
RES_MEMSIZE               (idx, 1)        = 294.61 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.03 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 128.80 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 54 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 948066 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 20 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 53 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 53 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 1698 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  6.52580E+14 0.00021  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.02990E-03 0.00397 ];
U235_FISS                 (idx, [1:   4]) = [  3.08504E+19 0.00021  9.99885E-01 2.9E-06 ];
U238_FISS                 (idx, [1:   4]) = [  3.52176E+15 0.02552  1.14120E-04 0.02550 ];
U235_CAPT                 (idx, [1:   4]) = [  6.01202E+18 0.00060  2.95086E-01 0.00049 ];
U238_CAPT                 (idx, [1:   4]) = [  1.47942E+17 0.00396  7.26115E-03 0.00392 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 30000462 3.00000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 1.10181E+06 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 30000462 3.11018E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 9075453 9.36611E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 13855523 1.41841E+07 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 7069486 7.55164E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 30000462 3.11018E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 3.36766E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.00000E+09 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.13918E+02 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  7.52119E+19 2.9E-07 ];
TOT_FISSRATE              (idx, [1:   2]) = [  3.08572E+19 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.03772E+19 0.00023 ];
TOT_ABSRATE               (idx, [1:   2]) = [  5.12344E+19 9.1E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  6.52580E+19 0.00021 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.10857E+21 0.00023 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.64270E+19 0.00047 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  6.76614E+19 0.00015 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.10821E+21 0.00024 ];
INI_FMASS                 (idx, 1)        =  8.77822E+00 ;
TOT_FMASS                 (idx, 1)        =  8.77822E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06071E+00 0.00011 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.50389E-01 0.00013 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.16511E-01 9.5E-05 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.08670E+00 7.5E-05 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.09492E-01 0.00010 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.24380E-01 6.2E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.54010E+00 0.00018 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.15242E+00 0.00020 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43742E+00 2.9E-07 ];
FISSE                     (idx, [1:   2]) = [  2.02271E+02 1.3E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.15237E+00 0.00020  1.14383E+00 0.00020  8.58947E-03 0.00295 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.15244E+00 0.00015 ];
COL_KEFF                  (idx, [1:   2]) = [  1.15255E+00 0.00021 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.15244E+00 0.00015 ];
ABS_KINF                  (idx, [1:   2]) = [  1.54007E+00 9.7E-05 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.94537E+01 3.4E-05 ];
IMP_ALF                   (idx, [1:   2]) = [  1.94534E+01 1.6E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.11897E-08 0.00066 ];
IMP_EALF                  (idx, [1:   2]) = [  7.12079E-08 0.00032 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.38230E-02 0.00403 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.37305E-02 0.00047 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.04613E-03 0.00226  1.99232E-04 0.01264  9.29227E-04 0.00618  5.65619E-04 0.00798  1.19483E-03 0.00495  1.98152E-03 0.00413  5.46981E-04 0.00762  4.92029E-04 0.00838  1.36685E-04 0.01589 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.04514E-01 0.00371  1.24667E-02 0.0E+00  2.82917E-02 4.9E-09  4.25244E-02 7.1E-09  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 5.2E-09  3.55460E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.47937E-03 0.00307  2.53484E-04 0.01901  1.12339E-03 0.00865  7.22985E-04 0.01108  1.48054E-03 0.00701  2.44797E-03 0.00552  6.84790E-04 0.01013  6.00278E-04 0.01152  1.65936E-04 0.02163 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.01920E-01 0.00506  1.24667E-02 0.0E+00  2.82917E-02 4.9E-09  4.25244E-02 7.2E-09  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 5.2E-09  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  7.91399E-05 0.00049  7.91503E-05 0.00049  7.77512E-05 0.00464 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  9.11970E-05 0.00045  9.12090E-05 0.00045  8.95953E-05 0.00462 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.45222E-03 0.00295  2.52260E-04 0.01807  1.11413E-03 0.00804  7.20093E-04 0.01080  1.47269E-03 0.00669  2.43333E-03 0.00534  6.86115E-04 0.00992  6.03891E-04 0.01196  1.69717E-04 0.02198 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.05290E-01 0.00521  1.24667E-02 0.0E+00  2.82917E-02 4.9E-09  4.25244E-02 7.2E-09  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 5.2E-09  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  7.78754E-05 0.00764  7.78816E-05 0.00764  7.67609E-05 0.01578 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  8.97416E-05 0.00764  8.97488E-05 0.00764  8.84561E-05 0.01576 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  7.13740E-03 0.01251  2.56438E-04 0.05460  1.09686E-03 0.02596  7.28222E-04 0.03488  1.35245E-03 0.02460  2.36245E-03 0.01893  6.55728E-04 0.03624  5.24783E-04 0.03607  1.60485E-04 0.06340 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.90816E-01 0.01552  1.24667E-02 0.0E+00  2.82917E-02 5.3E-09  4.25244E-02 7.4E-09  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 5.3E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  7.16969E-03 0.01237  2.55929E-04 0.05365  1.10061E-03 0.02488  7.23752E-04 0.03314  1.36282E-03 0.02370  2.37745E-03 0.01853  6.56542E-04 0.03539  5.26182E-04 0.03496  1.66404E-04 0.06282 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.93467E-01 0.01524  1.24667E-02 0.0E+00  2.82917E-02 5.4E-09  4.25244E-02 7.2E-09  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 5.3E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -9.16779E+01 0.01001 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  7.92739E-05 0.00032 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  9.13515E-05 0.00027 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.41181E-03 0.00164 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -9.34967E+01 0.00161 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.20141E-06 0.00021 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.36565E-06 0.00019  4.36737E-06 0.00019  4.13348E-06 0.00210 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.21105E-04 0.00029  1.21139E-04 0.00029  1.16504E-04 0.00326 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.47115E-01 0.00012  7.46131E-01 0.00012  9.10341E-01 0.00323 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30676E+01 0.00466 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.58167E+01 0.00016  4.64576E+01 0.00020 ];

