
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Jan 25 2024 11:36:28' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  77]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/serpent/burnupAnalysis/fullCore' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Feb 15 13:23:38 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Feb 15 13:24:49 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 50 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1708025018261 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.22231E+00  7.84055E-01  7.78341E-01  7.83442E-01  7.81750E-01  1.22390E+00  1.21792E+00  1.20829E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.47249E-01 0.00100  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.52751E-01 0.00081  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.79500E-01 0.00049  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.64024E-01 0.00061  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.61166E+00 0.00090  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.02822E-01 8.0E-05  9.31982E-02 0.00075  3.97955E-03 0.00165  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.12842E+01 0.00131  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.09592E+01 0.00133  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.16599E+01 0.00117  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.95389E+01 0.00156  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 50 ;
SIMULATED_HISTORIES       (idx, 1)        = 500184 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00037E+04 0.00283 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00037E+04 0.00283 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  6.38689E+00 ;
RUNNING_TIME              (idx, 1)        =  1.17967E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  6.83883E-01  6.83883E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.34833E-02  1.34833E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  4.82300E-01  4.82300E-01  0.00000E+00 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.17960E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 5.41415 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99968E+00 0.00039 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  4.72520E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.07 ;
ALLOC_MEMSIZE             (idx, 1)        = 3397.46 ;
MEMSIZE                   (idx, 1)        = 3222.01 ;
XS_MEMSIZE                (idx, 1)        = 3152.18 ;
MAT_MEMSIZE               (idx, 1)        = 1.88 ;
RES_MEMSIZE               (idx, 1)        = 0.49 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 67.46 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 175.45 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 48 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 3410525 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 195 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 591 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 323 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 268 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 10883 ;
TOT_TRANSMU_REA           (idx, 1)        = 1901 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 1 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  7.74305E+08 ;
TOT_DECAY_HEAT            (idx, 1)        =  5.76780E-04 ;
TOT_SF_RATE               (idx, 1)        =  3.92943E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  7.74305E+08 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  5.76780E-04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  6.67649E+03 ;
INGESTION_TOXICITY        (idx, 1)        =  3.65969E+01 ;
ACTINIDE_INH_TOX          (idx, 1)        =  6.67649E+03 ;
ACTINIDE_ING_TOX          (idx, 1)        =  3.65969E+01 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  8.60831E+08  1.73360E-05 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  7.66312E+08 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.76069E+09 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  8.09526E+15 0.00182  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 1 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 0 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
FIMA                      (idx, [1:   3]) = [  0.00000E+00  0.00000E+00  2.25250E+25 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.89844E-03 0.03031 ];
U235_FISS                 (idx, [1:   4]) = [  3.39379E+19 2.5E-05  9.99852E-01 2.5E-05 ];
U238_FISS                 (idx, [1:   4]) = [  5.01353E+15 0.16631  1.47705E-04 0.16631 ];
U235_CAPT                 (idx, [1:   4]) = [  7.00362E+18 0.00469  3.16782E-01 0.00399 ];
U238_CAPT                 (idx, [1:   4]) = [  2.00093E+17 0.03051  9.05463E-03 0.03064 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 500184 5.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 1.61364E+04 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 500184 5.16136E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 132498 1.36562E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 205149 2.09681E+05 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 162537 1.69894E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 500184 5.16136E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -6.81030E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  1.10000E+09 0.0E+00  1.10000E+09 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  1.25016E+02 0.0E+00  1.25016E+02 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  8.27428E+19 2.4E-05  8.27428E+19 2.4E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  3.39429E+19 5.8E-07  3.39429E+19 5.8E-07  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.21128E+19 0.00372  7.73998E+18 0.00474  1.43728E+19 0.00482 ];
TOT_ABSRATE               (idx, [1:   6]) = [  5.60557E+19 0.00147  4.16829E+19 0.00088  1.43728E+19 0.00482 ];
TOT_SRCRATE               (idx, [1:   6]) = [  8.09526E+19 0.00182  8.09526E+19 0.00182  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  6.01725E+21 0.00208  6.48268E+20 0.00201  5.36899E+21 0.00215 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.75094E+19 0.00332 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  8.35651E+19 0.00182 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.42655E+21 0.00219 ];
INI_FMASS                 (idx, 1)        =  8.79885E+00 ;
TOT_FMASS                 (idx, 1)        =  8.79885E+00 ;
INI_BURN_FMASS            (idx, 1)        =  8.79885E+00 ;
TOT_BURN_FMASS            (idx, 1)        =  8.79885E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05336E+00 0.00075 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.63388E-01 0.00123 ];
SIX_FF_P                  (idx, [1:   2]) = [  8.93339E-01 0.00091 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.10578E+00 0.00072 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.22922E-01 0.00089 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.13258E-01 0.00053 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.54841E+00 0.00149 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.02228E+00 0.00181 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43770E+00 2.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02271E+02 5.8E-07 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.02242E+00 0.00187  1.01448E+00 0.00182  7.80036E-03 0.02430 ];
COL_KEFF                  (idx, [1:   2]) = [  1.02228E+00 0.00181 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.87400E+01 0.00033 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  1.45458E-07 0.00609 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.68067E-02 0.03286 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.77739E-03 0.01833  2.45874E-04 0.10334  1.05709E-03 0.04558  6.52434E-04 0.06019  1.31308E-03 0.03587  2.16911E-03 0.02875  6.96338E-04 0.05740  4.86916E-04 0.06404  1.56552E-04 0.11006 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  3.98442E-01 0.03045  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.8E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  7.41808E-05 0.00351  7.41974E-05 0.00353  7.21481E-05 0.03573 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  7.58322E-05 0.00308  7.58492E-05 0.00310  7.37664E-05 0.03565 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.61482E-03 0.02449  3.02236E-04 0.14530  1.12692E-03 0.05930  7.99027E-04 0.06836  1.53912E-03 0.05326  2.34663E-03 0.03855  7.91221E-04 0.08444  5.29815E-04 0.08408  1.79846E-04 0.15138 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  3.96140E-01 0.04184  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.6E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  6.68667E-05 0.04828  6.68073E-05 0.04829  7.29737E-05 0.12670 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  6.82801E-05 0.04835  6.82206E-05 0.04837  7.43574E-05 0.12622 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.45940E-03 0.12047  3.71316E-04 0.41378  7.89078E-04 0.30625  1.13482E-03 0.27971  9.58879E-04 0.22957  2.20133E-03 0.17417  4.27272E-04 0.25008  5.47846E-04 0.29603  2.88557E-05 0.63231 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.67888E-01 0.13366  1.24667E-02 5.6E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 1.3E-08 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  7.42171E-05 0.00209 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  7.58687E-05 0.00110 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.35672E-03 0.01378 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -9.91391E+01 0.01391 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  8.52895E-07 0.00172 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.40117E-06 0.00150  4.40288E-06 0.00151  4.19239E-06 0.01613 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  9.20529E-05 0.00222  9.20747E-05 0.00220  8.89842E-05 0.01964 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.53613E-01 0.00110  6.52986E-01 0.00113  7.55651E-01 0.02345 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.29925E+01 0.03390 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.09592E+01 0.00133  4.80962E+01 0.00147 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Jan 25 2024 11:36:28' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  77]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/serpent/burnupAnalysis/fullCore' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Feb 15 13:23:38 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Feb 15 13:25:46 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 50 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1708025018261 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  7.68394E-01  7.85508E-01  7.75072E-01  1.22517E+00  7.72220E-01  1.22945E+00  1.23089E+00  1.21330E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.65299E-01 0.00108  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.34701E-01 0.00094  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.78329E-01 0.00059  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.72769E-01 0.00061  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.57965E+00 0.00090  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.03022E-01 8.0E-05  9.31573E-02 0.00074  3.82056E-03 0.00201  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.32156E+01 0.00125  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.28871E+01 0.00127  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.19897E+01 0.00143  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  2.11728E+01 0.00166  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 50 ;
SIMULATED_HISTORIES       (idx, 1)        = 500458 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00092E+04 0.00295 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00092E+04 0.00295 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.39439E+01 ;
RUNNING_TIME              (idx, 1)        =  2.13925E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  6.83883E-01  6.83883E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.17667E-02  1.42667E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.41327E+00  5.31967E-01  3.99000E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  2.66667E-04  1.16666E-04 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.13917E+00  3.04923E+00 ] ;
CPU_USAGE                 (idx, 1)        = 6.51810 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  8.00002E+00 0.00038 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  7.04297E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.07 ;
ALLOC_MEMSIZE             (idx, 1)        = 3397.46 ;
MEMSIZE                   (idx, 1)        = 3222.01 ;
XS_MEMSIZE                (idx, 1)        = 3152.18 ;
MAT_MEMSIZE               (idx, 1)        = 1.88 ;
RES_MEMSIZE               (idx, 1)        = 0.49 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 67.46 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 175.45 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 48 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 3410525 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 195 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 591 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 323 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 268 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 10883 ;
TOT_TRANSMU_REA           (idx, 1)        = 1901 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 1 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  3.33349E+17 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.93355E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.25323E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.71047E+17 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  2.02253E+04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  9.03137E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  9.78393E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  6.81064E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  5.10895E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  3.59495E+07 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  3.01786E+17  9.04017E+03 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.53437E-19 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  8.34658E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  8.37681E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  8.37907E+15 0.00197  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 1 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 1 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  1.25016E+02         -NAN ] ;
BURN_DAYS                 (idx, [1:   2]) = [  1.00000E+00  1.00000E+00 ] ;
FIMA                      (idx, [1:   3]) = [  1.35772E-01  3.05826E+24  1.94667E+25 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.15197E-03 0.02944 ];
U235_FISS                 (idx, [1:   4]) = [  3.39241E+19 6.0E-05  9.99452E-01 5.9E-05 ];
U238_FISS                 (idx, [1:   4]) = [  4.20172E+15 0.23810  1.23790E-04 0.23810 ];
PU239_FISS                (idx, [1:   4]) = [  7.73884E+15 0.14620  2.27999E-04 0.14620 ];
U235_CAPT                 (idx, [1:   4]) = [  6.86867E+18 0.00504  2.89274E-01 0.00389 ];
U238_CAPT                 (idx, [1:   4]) = [  2.06301E+17 0.02905  8.68196E-03 0.02838 ];
PU239_CAPT                (idx, [1:   4]) = [  5.21352E+15 0.18433  2.17916E-04 0.18439 ];
PU240_CAPT                (idx, [1:   4]) = [  3.53709E+15 0.22707  1.47360E-04 0.22541 ];
PU241_CAPT                (idx, [1:   4]) = [  3.33372E+14 0.70002  1.40118E-05 0.69989 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 500458 5.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 1.59704E+04 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 500458 5.15970E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 137651 1.41687E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 198392 2.02583E+05 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 164415 1.71700E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 500458 5.15970E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.46683E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  1.10000E+09 0.0E+00  1.10000E+09 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  1.25016E+02 0.0E+00  1.25016E+02 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  8.27384E+19 2.3E-05  8.27384E+19 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  3.39427E+19 1.3E-06  3.39427E+19 1.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.37469E+19 0.00379  7.82648E+18 0.00502  1.59204E+19 0.00445 ];
TOT_ABSRATE               (idx, [1:   6]) = [  5.76896E+19 0.00156  4.17692E+19 0.00094  1.59204E+19 0.00445 ];
TOT_SRCRATE               (idx, [1:   6]) = [  8.37907E+19 0.00197  8.37907E+19 0.00197  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  6.44102E+21 0.00186  6.96564E+20 0.00211  5.74445E+21 0.00191 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.87770E+19 0.00358 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  8.64666E+19 0.00195 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.71133E+21 0.00187 ];
INI_FMASS                 (idx, 1)        =  8.79885E+00 ;
TOT_FMASS                 (idx, 1)        =  7.60621E+00 ;
INI_BURN_FMASS            (idx, 1)        =  8.79885E+00 ;
TOT_BURN_FMASS            (idx, 1)        =  7.60621E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05019E+00 0.00084 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.44567E-01 0.00114 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.01616E-01 0.00087 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09290E+00 0.00070 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.24331E-01 0.00087 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.06491E-01 0.00062 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50415E+00 0.00161 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.87630E-01 0.00196 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43759E+00 2.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02272E+02 1.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.88300E-01 0.00197  9.79580E-01 0.00199  8.04998E-03 0.02296 ];
COL_KEFF                  (idx, [1:   2]) = [  9.87630E-01 0.00196 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.88379E+01 0.00029 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  1.31874E-07 0.00550 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.46165E-02 0.03417 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  7.43439E-03 0.01754  2.48153E-04 0.10289  1.14023E-03 0.04195  6.65443E-04 0.05020  1.52579E-03 0.04545  2.39902E-03 0.03409  6.70910E-04 0.05045  6.20992E-04 0.05490  1.63848E-04 0.10079 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.05063E-01 0.02149  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  8.56003E-05 0.00412  8.56195E-05 0.00417  8.26684E-05 0.03408 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  8.45805E-05 0.00347  8.45997E-05 0.00354  8.16597E-05 0.03365 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  8.12202E-03 0.02349  2.32878E-04 0.14233  1.24109E-03 0.07178  7.33494E-04 0.07139  1.65278E-03 0.05674  2.59664E-03 0.04852  8.23653E-04 0.08494  6.44759E-04 0.08956  1.96729E-04 0.16836 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.12444E-01 0.03896  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 1.9E-09  3.55460E+00 6.6E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  7.66592E-05 0.04832  7.66700E-05 0.04834  7.52537E-05 0.09719 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  7.58029E-05 0.04833  7.58141E-05 0.04836  7.44220E-05 0.09722 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  7.41042E-03 0.09496  1.74129E-04 0.43766  9.83283E-04 0.23729  4.69546E-04 0.27281  1.51960E-03 0.19611  2.66458E-03 0.15780  7.64368E-04 0.24909  6.50650E-04 0.25821  1.84270E-04 0.49136 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.29779E-01 0.12887  1.24667E-02 5.6E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 6.6E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  8.55622E-05 0.00276 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  8.45458E-05 0.00203 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.95601E-03 0.01842 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -9.30319E+01 0.01866 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  9.06820E-07 0.00153 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.42963E-06 0.00185  4.43039E-06 0.00186  4.33010E-06 0.01945 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.00556E-04 0.00189  1.00578E-04 0.00194  9.78907E-05 0.02391 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.60849E-01 0.00113  6.60421E-01 0.00116  7.28994E-01 0.02495 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30382E+01 0.03525 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.28871E+01 0.00127  5.06243E+01 0.00160 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Jan 25 2024 11:36:28' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  77]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/serpent/burnupAnalysis/fullCore' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Feb 15 13:23:38 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Feb 15 13:26:39 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 50 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1708025018261 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.24014E+00  1.23814E+00  7.56276E-01  7.55249E-01  7.52529E-01  1.24196E+00  1.24479E+00  7.70918E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.89629E-01 0.00095  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.10371E-01 0.00091  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.78437E-01 0.00061  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.85838E-01 0.00060  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.52006E+00 0.00096  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.02773E-01 8.4E-05  9.35462E-02 0.00078  3.68058E-03 0.00183  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.55462E+01 0.00139  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.52077E+01 0.00141  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.19598E+01 0.00150  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  2.33537E+01 0.00161  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 50 ;
SIMULATED_HISTORIES       (idx, 1)        = 499902 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99804E+03 0.00283 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99804E+03 0.00283 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.08275E+01 ;
RUNNING_TIME              (idx, 1)        =  3.01467E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  6.83883E-01  6.83883E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.01833E-02  1.41167E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.25988E+00  4.31717E-01  4.14900E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  4.66665E-04  9.99967E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  3.01460E+00  3.01460E+00 ] ;
CPU_USAGE                 (idx, 1)        = 6.90873 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99984E+00 0.00041 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  7.86792E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.07 ;
ALLOC_MEMSIZE             (idx, 1)        = 3397.46 ;
MEMSIZE                   (idx, 1)        = 3222.01 ;
XS_MEMSIZE                (idx, 1)        = 3152.18 ;
MAT_MEMSIZE               (idx, 1)        = 1.88 ;
RES_MEMSIZE               (idx, 1)        = 0.49 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 67.46 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 175.45 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 48 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 3410525 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 195 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 591 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 323 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 268 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 10883 ;
TOT_TRANSMU_REA           (idx, 1)        = 1901 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 1 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  4.40565E+17 ;
TOT_DECAY_HEAT            (idx, 1)        =  3.92679E+04 ;
TOT_SF_RATE               (idx, 1)        =  4.04831E+02 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  3.53820E+17 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  2.65558E+04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.74938E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  1.61343E+08 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.42749E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.67666E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  5.55992E+07 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  4.43503E+17  1.35084E+04 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  2.16690E-25 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.11584E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.25385E+18 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  8.87061E+15 0.00190  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 1 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 2 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.50033E+02         -NAN ] ;
BURN_DAYS                 (idx, [1:   2]) = [  2.00000E+00  1.00000E+00 ] ;
FIMA                      (idx, [1:   3]) = [  2.72016E-01  6.12716E+24  1.63978E+25 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.81536E-03 0.02651 ];
U235_FISS                 (idx, [1:   4]) = [  3.38905E+19 8.9E-05  9.98488E-01 8.7E-05 ];
U238_FISS                 (idx, [1:   4]) = [  5.33272E+15 0.17018  1.57114E-04 0.17018 ];
PU239_FISS                (idx, [1:   4]) = [  3.19821E+16 0.07930  9.42267E-04 0.07930 ];
PU241_FISS                (idx, [1:   4]) = [  2.66206E+15 0.34803  7.84312E-05 0.34804 ];
U235_CAPT                 (idx, [1:   4]) = [  6.70069E+18 0.00515  2.55417E-01 0.00470 ];
U238_CAPT                 (idx, [1:   4]) = [  2.21809E+17 0.02746  8.44608E-03 0.02656 ];
PU239_CAPT                (idx, [1:   4]) = [  1.86599E+16 0.11203  7.11133E-04 0.11185 ];
PU240_CAPT                (idx, [1:   4]) = [  1.33534E+16 0.10202  5.09993E-04 0.10234 ];
PU241_CAPT                (idx, [1:   4]) = [  5.34508E+14 0.56549  2.01486E-05 0.56549 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 499902 5.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 1.60899E+04 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 499902 5.16090E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 143516 1.47890E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 187181 1.91350E+05 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 169205 1.76850E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 499902 5.16090E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.24564E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  1.10000E+09 0.0E+00  1.10000E+09 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  1.25016E+02 0.0E+00  1.25016E+02 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  8.27423E+19 2.5E-05  8.27423E+19 2.5E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  3.39419E+19 2.4E-06  3.39419E+19 2.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.62392E+19 0.00347  7.98149E+18 0.00477  1.82577E+19 0.00397 ];
TOT_ABSRATE               (idx, [1:   6]) = [  6.01811E+19 0.00151  4.19233E+19 0.00091  1.82577E+19 0.00397 ];
TOT_SRCRATE               (idx, [1:   6]) = [  8.87061E+19 0.00190  8.87061E+19 0.00190  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  7.09330E+21 0.00187  7.74104E+20 0.00185  6.31919E+21 0.00193 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.13791E+19 0.00370 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  9.15601E+19 0.00185 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  4.13932E+21 0.00206 ];
INI_FMASS                 (idx, 1)        =  8.79885E+00 ;
TOT_FMASS                 (idx, 1)        =  6.40941E+00 ;
INI_BURN_FMASS            (idx, 1)        =  8.79885E+00 ;
TOT_BURN_FMASS            (idx, 1)        =  6.40941E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.04425E+00 0.00089 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.17459E-01 0.00122 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.13982E-01 0.00084 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.07686E+00 0.00059 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.22226E-01 0.00111 ];
SIX_FF_LT                 (idx, [1:   2]) = [  8.94871E-01 0.00054 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.44352E+00 0.00159 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.32935E-01 0.00191 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43777E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02277E+02 2.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.32315E-01 0.00200  9.25577E-01 0.00190  7.35716E-03 0.02588 ];
COL_KEFF                  (idx, [1:   2]) = [  9.32935E-01 0.00191 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.89591E+01 0.00025 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  1.16800E-07 0.00471 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.26409E-02 0.03505 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  7.57315E-03 0.01760  2.53437E-04 0.08836  1.12855E-03 0.04440  7.24033E-04 0.05374  1.46451E-03 0.03589  2.50663E-03 0.03944  7.13166E-04 0.05440  6.34560E-04 0.04965  1.48261E-04 0.12083 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.00650E-01 0.02572  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.3E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  1.03373E-04 0.00424  1.03360E-04 0.00428  1.04961E-04 0.03972 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  9.63522E-05 0.00345  9.63409E-05 0.00351  9.77802E-05 0.03936 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.82863E-03 0.02430  3.14654E-04 0.11488  1.06794E-03 0.06777  7.45176E-04 0.08165  1.58115E-03 0.05784  2.58929E-03 0.05093  7.22810E-04 0.07840  6.19771E-04 0.07324  1.87826E-04 0.17539 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.12063E-01 0.04124  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 2.7E-09  3.55460E+00 4.7E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  9.29685E-05 0.04860  9.29527E-05 0.04858  9.83562E-05 0.11981 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  8.66921E-05 0.04863  8.66766E-05 0.04860  9.18245E-05 0.12093 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  7.21896E-03 0.09706  2.65420E-04 0.34029  1.08517E-03 0.22981  5.90632E-04 0.28343  1.07744E-03 0.20920  2.84644E-03 0.14612  7.66690E-04 0.29350  5.51847E-04 0.26968  3.53207E-05 0.73944 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.59988E-01 0.13068  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 5.5E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  1.03800E-04 0.00240 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  9.67566E-05 0.00156 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.97101E-03 0.01263 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -7.68272E+01 0.01313 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  9.75898E-07 0.00140 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.43838E-06 0.00186  4.43941E-06 0.00190  4.28914E-06 0.01910 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.12014E-04 0.00184  1.12023E-04 0.00182  1.11135E-04 0.02138 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.67693E-01 0.00133  6.67562E-01 0.00129  6.94088E-01 0.02555 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.41592E+01 0.03501 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.52077E+01 0.00141  5.38805E+01 0.00161 ];

