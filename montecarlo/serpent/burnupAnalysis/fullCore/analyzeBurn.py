"""

"""

# --- importing libraries
import numpy as np
import os
from oct2py import octave as oct
from scipy.io import loadmat
import matplotlib.pyplot as plt
# ---

# --- needed constants
cwd = os.getcwd()
# ---

# --- reading octave output scripts
oct.eval(f"cd {cwd}")
oct.eval("analyzeBurn")
oct.eval("save -v7 myworkspace.mat")
m = loadmat(f"{cwd}/myworkspace.mat")
# ---

print(m)
