
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  77]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/serpent/burnupAnalysis/rpvStuff' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Mon Feb 26 19:54:39 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Mon Feb 26 19:55:05 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 20 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1708998879625 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  7.66671E-01  7.67044E-01  7.58497E-01  1.23437E+00  1.21171E+00  1.24202E+00  1.23903E+00  7.80650E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 2.7E-09 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.46553E-01 0.00115  3.23182E-01 0.00099 ];
DT_FRAC                   (idx, [1:   4]) = [  5.53447E-01 0.00093  6.76818E-01 0.00047 ];
DT_EFF                    (idx, [1:   4]) = [  3.79348E-01 0.00087  2.61025E-01 0.00058 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.63713E-01 0.00075  3.25683E-01 0.00081 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.60765E+00 0.00179  4.41334E+00 0.00065  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  8.80495E-01 0.00012  1.10498E-01 0.00085  9.00641E-03 0.00154  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.13496E+01 0.00176  1.77423E+01 0.00278 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.10256E+01 0.00176  1.68868E+01 0.00281 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.17521E+01 0.00204  3.49642E+01 0.00302 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.95052E+01 0.00150  1.84886E+01 0.00305 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 20 ;
SIMULATED_HISTORIES       (idx, 1)        = 200140 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00070E+04 0.00551 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00070E+04 0.00551 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.41905E+00 ;
RUNNING_TIME              (idx, 1)        =  4.23400E-01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.20467E-01  2.20467E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.71667E-03  4.71667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.98200E-01  1.98200E-01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  4.23367E-01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 5.71339 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99760E+00 0.00077 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  4.95512E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.09 ;
ALLOC_MEMSIZE             (idx, 1)        = 951.42 ;
MEMSIZE                   (idx, 1)        = 861.65 ;
XS_MEMSIZE                (idx, 1)        = 775.40 ;
MAT_MEMSIZE               (idx, 1)        = 15.77 ;
RES_MEMSIZE               (idx, 1)        = 0.24 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.24 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 89.77 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 48 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1032602 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 8081 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 27 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 102 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 18 ;
TOT_REA_CHANNELS          (idx, 1)        = 2630 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 1 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  1.56886E-05 0.31018 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  8.07653E+15 0.00327  8.07653E+15 0.00327 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 156109 1.59200E+05 6.84845E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 13388 1.36399E+04 6.96997E+03 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 557960 5.69546E+05 1.98135E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 640921 6.54869E+05 6.56833E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1368378 1.39725E+06 6.90465E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1197167 1.22205E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 171211 1.75208E+05 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1368378 1.39725E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 4.77303E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  7.07435E+19 0.00243 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  4.90670E+20 0.00216 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  2.75358E+18 0.01266 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  5.64167E+20 0.00199 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  1.39455E+21 0.00215 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  6.79376E+21 0.00223 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  3.23529E+07 0.00244 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  7.09434E-10 0.00096 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.18210E-03 0.04102 ];
U235_FISS                 (idx, [1:   4]) = [  3.39343E+19 4.8E-05  9.99748E-01 4.7E-05 ];
U238_FISS                 (idx, [1:   4]) = [  8.53807E+15 0.18829  2.51543E-04 0.18829 ];
U235_CAPT                 (idx, [1:   4]) = [  6.93886E+18 0.00936  3.14649E-01 0.00814 ];
U238_CAPT                 (idx, [1:   4]) = [  2.11000E+17 0.04177  9.56066E-03 0.04143 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 200140 2.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 6.43771E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 200140 2.06438E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 53008 5.46107E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 82278 8.40702E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 64854 6.77568E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 200140 2.06438E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -7.07223E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.10000E+09 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.25403E+02 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  8.27415E+19 3.9E-05 ];
TOT_FISSRATE              (idx, [1:   2]) = [  3.39428E+19 1.1E-06 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.20569E+19 0.00640 ];
TOT_ABSRATE               (idx, [1:   2]) = [  5.59998E+19 0.00252 ];
TOT_SRCRATE               (idx, [1:   2]) = [  8.07653E+19 0.00327 ];
TOT_FLUX                  (idx, [1:   2]) = [  6.00289E+21 0.00396 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  2.66766E+20 0.00293  2.66348E+20 0.00194 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.73655E+19 0.00583 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  8.33653E+19 0.00334 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.42420E+21 0.00441 ];
INI_FMASS                 (idx, 1)        =  8.77173E+00 ;
TOT_FMASS                 (idx, 1)        =  8.77173E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05632E+00 0.00150 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.63984E-01 0.00209 ];
SIX_FF_P                  (idx, [1:   2]) = [  8.92726E-01 0.00107 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.10497E+00 0.00083 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.25220E-01 0.00125 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.11742E-01 0.00075 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.54966E+00 0.00252 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.02468E+00 0.00328 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43767E+00 4.0E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02271E+02 1.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.02403E+00 0.00340  1.01618E+00 0.00326  8.50071E-03 0.04806 ];
COL_KEFF                  (idx, [1:   2]) = [  1.02468E+00 0.00328 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.87352E+01 0.00043 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  1.46117E-07 0.00800 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.63388E-02 0.05489 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  7.11070E-03 0.03963  2.63369E-04 0.13250  1.07718E-03 0.09479  6.64422E-04 0.11553  1.41891E-03 0.05731  2.31723E-03 0.05870  6.00613E-04 0.08408  6.21435E-04 0.08921  1.47539E-04 0.20220 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.02005E-01 0.04164  1.24667E-02 5.4E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 2.7E-09  6.66488E-01 6.0E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  7.38505E-05 0.00683  7.38450E-05 0.00684  7.53711E-05 0.05579 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  7.56024E-05 0.00522  7.55959E-05 0.00512  7.72548E-05 0.05666 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  8.53046E-03 0.05017  4.14820E-04 0.17335  1.23708E-03 0.12360  7.23871E-04 0.14890  1.70246E-03 0.09753  2.69068E-03 0.06965  8.16968E-04 0.08510  7.57250E-04 0.08096  1.87335E-04 0.33141 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.14570E-01 0.04353  1.24667E-02 5.4E-09  2.82917E-02 2.7E-09  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 2.7E-09  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  5.46419E-05 0.13373  5.45756E-05 0.13371  5.02289E-05 0.20052 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  5.58609E-05 0.13357  5.57935E-05 0.13355  5.13906E-05 0.20055 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.53934E-03 0.20316  3.63901E-04 0.51862  8.09659E-04 0.40732  1.05118E-03 0.39108  1.43156E-03 0.29220  1.80396E-03 0.28357  4.91340E-04 0.43742  2.15288E-04 0.42092  3.72460E-04 0.56421 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.60599E-01 0.19801  1.24667E-02 8.6E-09  2.82917E-02 0.0E+00  4.25244E-02 5.7E-09  1.33042E-01 0.0E+00  2.92467E-01 5.5E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 9.1E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  7.40426E-05 0.00360 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  7.58067E-05 0.00197 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  8.21534E-03 0.01679 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.10977E+02 0.01701 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  8.54511E-07 0.00225 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.39692E-06 0.00323  4.39947E-06 0.00324  4.07892E-06 0.02352 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  9.19082E-05 0.00396  9.19588E-05 0.00403  8.58498E-05 0.03454 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.55337E-01 0.00157  6.54608E-01 0.00176  7.82329E-01 0.05950 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.36742E+01 0.07820 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  8.93914E+00 0.00199  4.81067E+01 0.00265 ];

