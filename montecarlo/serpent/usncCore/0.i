% Case: 2_templateSettings
% User: C.Garza
% Created with RAPTOR 0.12.1.dev20+g07e7bbab
% Created with ARMI 0.2.7
surf 1 inf
cell 1 1 m0 -1

% Reactor pressure vessel
surf 2 cylz 0 0 40.6425 0.0 63.920001
surf 3 cylz 0 0 40.96 0.0 63.920001
cell 2 2 m1 2 -3

% infinite universe of filler domain
cell inf00 inf00 m0 -1

% block <pbrAssemBlock B0015-000 at 004-013-000 XS: N BU GP: A>
surf 4 cone 0 0 0 2.219 113.10883572567784
surf 5 cone 0 0 0 2.429 133.38632302405497

mix mix0
rgb 252 90 80
m2 0.42000013
m5 0.28777627
m6 0.2922236
surf 6 cone 0 0 0 2.7365 196.64652051714447

mix mix1
rgb 252 90 80
m2 0.41999993
m5 0.28777637
m6 0.29222371
surf 7 cone 0 0 0 3.044 225.72213457076566
surf 8 cone 0 0 0 3.244 240.5527610208817
surf 9 cone 0 0 0 3.544 262.7987006960557
surf 10 cylz 0 0 4.5655
surf 11 hexyc 0 0 4.565500000005
pin 4p1
m2 0.10605
m3
surf 12 cylz 4.747854 0 0.13105
cell 3 4 fill 4p1 -12
trans F 3 4.747854 0 0.0
surf 13 cylz 2.373927 4.1117622 0.13105
cell 4 4 fill 4p1 -13
trans F 4 2.373927 4.1117622 0.0
surf 14 cylz -2.373927 4.1117622 0.13105
cell 5 4 fill 4p1 -14
trans F 5 -2.373927 4.1117622 0.0
surf 15 cylz -4.747854 5.8144442e-16 0.13105
cell 6 4 fill 4p1 -15
trans F 6 -4.747854 5.8144442e-16 0.0
surf 16 cylz -2.373927 -4.1117622 0.13105
cell 7 4 fill 4p1 -16
trans F 7 -2.373927 -4.1117622 0.0
surf 17 cylz 2.373927 -4.1117622 0.13105
cell 8 4 fill 4p1 -17
trans F 8 2.373927 -4.1117622 0.0
cell 9 4 m2 -4 12 13 14 15 16 17
cell 10 4 m4 4 -5 12 13 14 15 16 17
cell 11 4 mix0 5 -6 12 13 14 15 16 17
cell 12 4 mix1 6 -7 12 13 14 15 16 17
cell 13 4 m7 7 -8 12 13 14 15 16 17
cell 14 4 m2 8 -9 12 13 14 15 16 17
cell 15 4 m8 10 -11 12 13 14 15 16 17
cell 16 4 m8 12 13 14 15 16 17 4 ( -4 : 5 ) ( -5 : 6 ) ( -6 : 7 ) ( -7 : 8 ) ( -8 : 9 ) ( -10 : 11 )
cell 17 3 fill 4 -1
% block <axialReflectorFuelForwardBottom B0015-001 at 004-013-001 XS: Z BU GP: A>
surf 18 cylz 0 0 0.5749999929
surf 19 cylz 0 0 3.3039999999999994
surf 20 cylz 0 0 3.304003
surf 21 cylz 0 0 3.304004
surf 22 cylz 0 0 3.504004
surf 23 cylz 0 0 3.804004
surf 24 cylz 0 0 4.429004
surf 25 hexyc 0 0 4.42900404429004
pin 6p1
m2 0.10605
m3
cell 18 6 fill 6p1 -12
trans F 18 4.747854 0 0.0
cell 19 6 fill 6p1 -13
trans F 19 2.373927 4.1117622 0.0
cell 20 6 fill 6p1 -14
trans F 20 -2.373927 4.1117622 0.0
cell 21 6 fill 6p1 -15
trans F 21 -4.747854 5.8144442e-16 0.0
cell 22 6 fill 6p1 -16
trans F 22 -2.373927 -4.1117622 0.0
cell 23 6 fill 6p1 -17
trans F 23 2.373927 -4.1117622 0.0
cell 24 6 m9 -18 12 13 14 15 16 17
cell 25 6 m10 18 -19 12 13 14 15 16 17
cell 26 6 m11 19 -20 12 13 14 15 16 17
cell 27 6 m12 20 -21 12 13 14 15 16 17
cell 28 6 m7 21 -22 12 13 14 15 16 17
cell 29 6 m2 22 -23 12 13 14 15 16 17
cell 30 6 m13 24 -25 12 13 14 15 16 17
cell 31 6 m8 12 13 14 15 16 17 18 ( -18 : 19 ) ( -19 : 20 ) ( -20 : 21 ) ( -21 : 22 ) ( -22 : 23 ) ( -24 : 25 )
cell 32 5 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0015-002 at 004-013-002 XS: ZZ BU GP: A>
pin 8p1
m2 0.10605
m3
cell 33 8 fill 8p1 -12
trans F 33 4.747854 0 0.0
cell 34 8 fill 8p1 -13
trans F 34 2.373927 4.1117622 0.0
cell 35 8 fill 8p1 -14
trans F 35 -2.373927 4.1117622 0.0
cell 36 8 fill 8p1 -15
trans F 36 -4.747854 5.8144442e-16 0.0
cell 37 8 fill 8p1 -16
trans F 37 -2.373927 -4.1117622 0.0
cell 38 8 fill 8p1 -17
trans F 38 2.373927 -4.1117622 0.0
cell 39 8 void -18 12 13 14 15 16 17
cell 40 8 m10 18 -19 12 13 14 15 16 17
cell 41 8 m11 19 -20 12 13 14 15 16 17
cell 42 8 m12 20 -21 12 13 14 15 16 17
cell 43 8 m7 21 -22 12 13 14 15 16 17
cell 44 8 m2 22 -23 12 13 14 15 16 17
cell 45 8 m14 24 -25 12 13 14 15 16 17
cell 46 8 m0 12 13 14 15 16 17 18 ( -18 : 19 ) ( -19 : 20 ) ( -20 : 21 ) ( -21 : 22 ) ( -22 : 23 ) ( -24 : 25 )
cell 47 7 fill 8 -1
% block <axialReflectorFuelForwardTop B0015-003 at 004-013-003 XS: S BU GP: A>
pin 10p1
m2 0.10605
m3
cell 48 10 fill 10p1 -12
trans F 48 4.747854 0 0.0
cell 49 10 fill 10p1 -13
trans F 49 2.373927 4.1117622 0.0
cell 50 10 fill 10p1 -14
trans F 50 -2.373927 4.1117622 0.0
cell 51 10 fill 10p1 -15
trans F 51 -4.747854 5.8144442e-16 0.0
cell 52 10 fill 10p1 -16
trans F 52 -2.373927 -4.1117622 0.0
cell 53 10 fill 10p1 -17
trans F 53 2.373927 -4.1117622 0.0
cell 54 10 m10 -19 12 13 14 15 16 17
cell 55 10 m11 19 -20 12 13 14 15 16 17
cell 56 10 m12 20 -21 12 13 14 15 16 17
cell 57 10 m7 21 -22 12 13 14 15 16 17
cell 58 10 m2 22 -23 12 13 14 15 16 17
cell 59 10 m14 24 -25 12 13 14 15 16 17
cell 60 10 m0 12 13 14 15 16 17 19 ( -19 : 20 ) ( -20 : 21 ) ( -21 : 22 ) ( -22 : 23 ) ( -24 : 25 )
cell 61 9 fill 10 -1

% Assembly A0015
lat 11 9 0.0 0.0 4
0 3
63.92 5
63.92 7
63.92 9
% block <pbrAssemBlock B0017-000 at 004-014-000 XS: N BU GP: A>
cell 62 12 fill 4 -1
% block <axialReflectorFuelForwardBottom B0017-001 at 004-014-001 XS: Z BU GP: A>
cell 63 13 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0017-002 at 004-014-002 XS: ZZ BU GP: A>
cell 64 14 fill 8 -1
% block <axialReflectorFuelForwardTop B0017-003 at 004-014-003 XS: S BU GP: A>
cell 65 15 fill 10 -1

% Assembly A0017
lat 16 9 0.0 0.0 4
0 12
63.92 13
63.92 14
63.92 15
% block <pbrAssemBlock B0020-000 at 004-015-000 XS: N BU GP: A>
cell 66 17 fill 4 -1
% block <axialReflectorFuelForwardBottom B0020-001 at 004-015-001 XS: Z BU GP: A>
cell 67 18 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0020-002 at 004-015-002 XS: ZZ BU GP: A>
cell 68 19 fill 8 -1
% block <axialReflectorFuelForwardTop B0020-003 at 004-015-003 XS: S BU GP: A>
cell 69 20 fill 10 -1

% Assembly A0020
lat 21 9 0.0 0.0 4
0 17
63.92 18
63.92 19
63.92 20
% block <pbrAssemBlock B0024-000 at 004-016-000 XS: N BU GP: A>
cell 70 22 fill 4 -1
% block <axialReflectorFuelForwardBottom B0024-001 at 004-016-001 XS: Z BU GP: A>
cell 71 23 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0024-002 at 004-016-002 XS: ZZ BU GP: A>
cell 72 24 fill 8 -1
% block <axialReflectorFuelForwardTop B0024-003 at 004-016-003 XS: S BU GP: A>
cell 73 25 fill 10 -1

% Assembly A0024
lat 26 9 0.0 0.0 4
0 22
63.92 23
63.92 24
63.92 25
% block <pbrAssemBlock B0016-000 at 004-012-000 XS: N BU GP: A>
cell 74 27 fill 4 -1
% block <axialReflectorFuelForwardBottom B0016-001 at 004-012-001 XS: Z BU GP: A>
cell 75 28 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0016-002 at 004-012-002 XS: ZZ BU GP: A>
cell 76 29 fill 8 -1
% block <axialReflectorFuelForwardTop B0016-003 at 004-012-003 XS: S BU GP: A>
cell 77 30 fill 10 -1

% Assembly A0016
lat 31 9 0.0 0.0 4
0 27
63.92 28
63.92 29
63.92 30
% block <pbrAssemBlock B0019-000 at 003-009-000 XS: N BU GP: A>
cell 78 32 fill 4 -1
% block <axialReflectorFuelForwardBottom B0019-001 at 003-009-001 XS: Z BU GP: A>
cell 79 33 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0019-002 at 003-009-002 XS: ZZ BU GP: A>
cell 80 34 fill 8 -1
% block <axialReflectorFuelForwardTop B0019-003 at 003-009-003 XS: S BU GP: A>
cell 81 35 fill 10 -1

% Assembly A0019
lat 36 9 0.0 0.0 4
0 32
63.92 33
63.92 34
63.92 35
% block <pbrAssemBlock B0023-000 at 003-010-000 XS: N BU GP: A>
cell 82 37 fill 4 -1
% block <axialReflectorFuelForwardBottom B0023-001 at 003-010-001 XS: Z BU GP: A>
cell 83 38 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0023-002 at 003-010-002 XS: ZZ BU GP: A>
cell 84 39 fill 8 -1
% block <axialReflectorFuelForwardTop B0023-003 at 003-010-003 XS: S BU GP: A>
cell 85 40 fill 10 -1

% Assembly A0023
lat 41 9 0.0 0.0 4
0 37
63.92 38
63.92 39
63.92 40
% block <pbrAssemBlock B0027-000 at 003-011-000 XS: N BU GP: A>
cell 86 42 fill 4 -1
% block <axialReflectorFuelForwardBottom B0027-001 at 003-011-001 XS: Z BU GP: A>
cell 87 43 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0027-002 at 003-011-002 XS: ZZ BU GP: A>
cell 88 44 fill 8 -1
% block <axialReflectorFuelForwardTop B0027-003 at 003-011-003 XS: S BU GP: A>
cell 89 45 fill 10 -1

% Assembly A0027
lat 46 9 0.0 0.0 4
0 42
63.92 43
63.92 44
63.92 45
% block <pbrAssemBlock B0031-000 at 004-017-000 XS: N BU GP: A>
cell 90 47 fill 4 -1
% block <axialReflectorFuelForwardBottom B0031-001 at 004-017-001 XS: Z BU GP: A>
cell 91 48 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0031-002 at 004-017-002 XS: ZZ BU GP: A>
cell 92 49 fill 8 -1
% block <axialReflectorFuelForwardTop B0031-003 at 004-017-003 XS: S BU GP: A>
cell 93 50 fill 10 -1

% Assembly A0031
lat 51 9 0.0 0.0 4
0 47
63.92 48
63.92 49
63.92 50
% block <pbrAssemBlock B0018-000 at 004-011-000 XS: N BU GP: A>
cell 94 52 fill 4 -1
% block <axialReflectorFuelForwardBottom B0018-001 at 004-011-001 XS: Z BU GP: A>
cell 95 53 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0018-002 at 004-011-002 XS: ZZ BU GP: A>
cell 96 54 fill 8 -1
% block <axialReflectorFuelForwardTop B0018-003 at 004-011-003 XS: S BU GP: A>
cell 97 55 fill 10 -1

% Assembly A0018
lat 56 9 0.0 0.0 4
0 52
63.92 53
63.92 54
63.92 55
% block <pbrAssemBlock B0022-000 at 003-008-000 XS: N BU GP: A>
cell 98 57 fill 4 -1
% block <axialReflectorFuelForwardBottom B0022-001 at 003-008-001 XS: Z BU GP: A>
cell 99 58 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0022-002 at 003-008-002 XS: ZZ BU GP: A>
cell 100 59 fill 8 -1
% block <axialReflectorFuelForwardTop B0022-003 at 003-008-003 XS: S BU GP: A>
cell 101 60 fill 10 -1

% Assembly A0022
lat 61 9 0.0 0.0 4
0 57
63.92 58
63.92 59
63.92 60
% block <pbrAssemBlock B0026-000 at 002-005-000 XS: N BU GP: A>
cell 102 62 fill 4 -1
% block <axialReflectorFuelForwardBottom B0026-001 at 002-005-001 XS: Z BU GP: A>
cell 103 63 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0026-002 at 002-005-002 XS: ZZ BU GP: A>
cell 104 64 fill 8 -1
% block <axialReflectorFuelForwardTop B0026-003 at 002-005-003 XS: S BU GP: A>
cell 105 65 fill 10 -1

% Assembly A0026
lat 66 9 0.0 0.0 4
0 62
63.92 63
63.92 64
63.92 65
% block <pbrAssemBlock B0030-000 at 002-006-000 XS: N BU GP: A>
cell 106 67 fill 4 -1
% block <axialReflectorFuelForwardBottom B0030-001 at 002-006-001 XS: Z BU GP: A>
cell 107 68 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0030-002 at 002-006-002 XS: ZZ BU GP: A>
cell 108 69 fill 8 -1
% block <axialReflectorFuelForwardTop B0030-003 at 002-006-003 XS: S BU GP: A>
cell 109 70 fill 10 -1

% Assembly A0030
lat 71 9 0.0 0.0 4
0 67
63.92 68
63.92 69
63.92 70
% block <pbrAssemBlock B0034-000 at 003-012-000 XS: N BU GP: A>
cell 110 72 fill 4 -1
% block <axialReflectorFuelForwardBottom B0034-001 at 003-012-001 XS: Z BU GP: A>
cell 111 73 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0034-002 at 003-012-002 XS: ZZ BU GP: A>
cell 112 74 fill 8 -1
% block <axialReflectorFuelForwardTop B0034-003 at 003-012-003 XS: S BU GP: A>
cell 113 75 fill 10 -1

% Assembly A0034
lat 76 9 0.0 0.0 4
0 72
63.92 73
63.92 74
63.92 75
% block <pbrAssemBlock B0038-000 at 004-018-000 XS: N BU GP: A>
cell 114 77 fill 4 -1
% block <axialReflectorFuelForwardBottom B0038-001 at 004-018-001 XS: Z BU GP: A>
cell 115 78 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0038-002 at 004-018-002 XS: ZZ BU GP: A>
cell 116 79 fill 8 -1
% block <axialReflectorFuelForwardTop B0038-003 at 004-018-003 XS: S BU GP: A>
cell 117 80 fill 10 -1

% Assembly A0038
lat 81 9 0.0 0.0 4
0 77
63.92 78
63.92 79
63.92 80
% block <pbrAssemBlock B0021-000 at 004-010-000 XS: N BU GP: A>
cell 118 82 fill 4 -1
% block <axialReflectorFuelForwardBottom B0021-001 at 004-010-001 XS: Z BU GP: A>
cell 119 83 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0021-002 at 004-010-002 XS: ZZ BU GP: A>
cell 120 84 fill 8 -1
% block <axialReflectorFuelForwardTop B0021-003 at 004-010-003 XS: S BU GP: A>
cell 121 85 fill 10 -1

% Assembly A0021
lat 86 9 0.0 0.0 4
0 82
63.92 83
63.92 84
63.92 85
% block <pbrAssemBlock B0025-000 at 003-007-000 XS: N BU GP: A>
cell 122 87 fill 4 -1
% block <axialReflectorFuelForwardBottom B0025-001 at 003-007-001 XS: Z BU GP: A>
cell 123 88 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0025-002 at 003-007-002 XS: ZZ BU GP: A>
cell 124 89 fill 8 -1
% block <axialReflectorFuelForwardTop B0025-003 at 003-007-003 XS: S BU GP: A>
cell 125 90 fill 10 -1

% Assembly A0025
lat 91 9 0.0 0.0 4
0 87
63.92 88
63.92 89
63.92 90
% block <pbrAssemBlock B0029-000 at 002-004-000 XS: N BU GP: A>
cell 126 92 fill 4 -1
% block <axialReflectorFuelForwardBottom B0029-001 at 002-004-001 XS: Z BU GP: A>
cell 127 93 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0029-002 at 002-004-002 XS: ZZ BU GP: A>
cell 128 94 fill 8 -1
% block <axialReflectorFuelForwardTop B0029-003 at 002-004-003 XS: S BU GP: A>
cell 129 95 fill 10 -1

% Assembly A0029
lat 96 9 0.0 0.0 4
0 92
63.92 93
63.92 94
63.92 95
% block <pbrAssemBlock B0033-000 at 001-001-000 XS: N BU GP: A>
cell 130 97 fill 4 -1
% block <axialReflectorFuelForwardBottom B0033-001 at 001-001-001 XS: Z BU GP: A>
cell 131 98 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0033-002 at 001-001-002 XS: ZZ BU GP: A>
cell 132 99 fill 8 -1
% block <axialReflectorFuelForwardTop B0033-003 at 001-001-003 XS: S BU GP: A>
cell 133 100 fill 10 -1

% Assembly A0033
lat 101 9 0.0 0.0 4
0 97
63.92 98
63.92 99
63.92 100
% block <pbrAssemBlock B0037-000 at 002-001-000 XS: N BU GP: A>
cell 134 102 fill 4 -1
% block <axialReflectorFuelForwardBottom B0037-001 at 002-001-001 XS: Z BU GP: A>
cell 135 103 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0037-002 at 002-001-002 XS: ZZ BU GP: A>
cell 136 104 fill 8 -1
% block <axialReflectorFuelForwardTop B0037-003 at 002-001-003 XS: S BU GP: A>
cell 137 105 fill 10 -1

% Assembly A0037
lat 106 9 0.0 0.0 4
0 102
63.92 103
63.92 104
63.92 105
% block <pbrAssemBlock B0041-000 at 003-001-000 XS: N BU GP: A>
cell 138 107 fill 4 -1
% block <axialReflectorFuelForwardBottom B0041-001 at 003-001-001 XS: Z BU GP: A>
cell 139 108 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0041-002 at 003-001-002 XS: ZZ BU GP: A>
cell 140 109 fill 8 -1
% block <axialReflectorFuelForwardTop B0041-003 at 003-001-003 XS: S BU GP: A>
cell 141 110 fill 10 -1

% Assembly A0041
lat 111 9 0.0 0.0 4
0 107
63.92 108
63.92 109
63.92 110
% block <pbrAssemBlock B0045-000 at 004-001-000 XS: N BU GP: A>
cell 142 112 fill 4 -1
% block <axialReflectorFuelForwardBottom B0045-001 at 004-001-001 XS: Z BU GP: A>
cell 143 113 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0045-002 at 004-001-002 XS: ZZ BU GP: A>
cell 144 114 fill 8 -1
% block <axialReflectorFuelForwardTop B0045-003 at 004-001-003 XS: S BU GP: A>
cell 145 115 fill 10 -1

% Assembly A0045
lat 116 9 0.0 0.0 4
0 112
63.92 113
63.92 114
63.92 115
% block <pbrAssemBlock B0028-000 at 004-009-000 XS: N BU GP: A>
cell 146 117 fill 4 -1
% block <axialReflectorFuelForwardBottom B0028-001 at 004-009-001 XS: Z BU GP: A>
cell 147 118 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0028-002 at 004-009-002 XS: ZZ BU GP: A>
cell 148 119 fill 8 -1
% block <axialReflectorFuelForwardTop B0028-003 at 004-009-003 XS: S BU GP: A>
cell 149 120 fill 10 -1

% Assembly A0028
lat 121 9 0.0 0.0 4
0 117
63.92 118
63.92 119
63.92 120
% block <pbrAssemBlock B0032-000 at 003-006-000 XS: N BU GP: A>
cell 150 122 fill 4 -1
% block <axialReflectorFuelForwardBottom B0032-001 at 003-006-001 XS: Z BU GP: A>
cell 151 123 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0032-002 at 003-006-002 XS: ZZ BU GP: A>
cell 152 124 fill 8 -1
% block <axialReflectorFuelForwardTop B0032-003 at 003-006-003 XS: S BU GP: A>
cell 153 125 fill 10 -1

% Assembly A0032
lat 126 9 0.0 0.0 4
0 122
63.92 123
63.92 124
63.92 125
% block <pbrAssemBlock B0036-000 at 002-003-000 XS: N BU GP: A>
cell 154 127 fill 4 -1
% block <axialReflectorFuelForwardBottom B0036-001 at 002-003-001 XS: Z BU GP: A>
cell 155 128 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0036-002 at 002-003-002 XS: ZZ BU GP: A>
cell 156 129 fill 8 -1
% block <axialReflectorFuelForwardTop B0036-003 at 002-003-003 XS: S BU GP: A>
cell 157 130 fill 10 -1

% Assembly A0036
lat 131 9 0.0 0.0 4
0 127
63.92 128
63.92 129
63.92 130
% block <pbrAssemBlock B0040-000 at 002-002-000 XS: N BU GP: A>
cell 158 132 fill 4 -1
% block <axialReflectorFuelForwardBottom B0040-001 at 002-002-001 XS: Z BU GP: A>
cell 159 133 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0040-002 at 002-002-002 XS: ZZ BU GP: A>
cell 160 134 fill 8 -1
% block <axialReflectorFuelForwardTop B0040-003 at 002-002-003 XS: S BU GP: A>
cell 161 135 fill 10 -1

% Assembly A0040
lat 136 9 0.0 0.0 4
0 132
63.92 133
63.92 134
63.92 135
% block <pbrAssemBlock B0044-000 at 003-002-000 XS: N BU GP: A>
cell 162 137 fill 4 -1
% block <axialReflectorFuelForwardBottom B0044-001 at 003-002-001 XS: Z BU GP: A>
cell 163 138 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0044-002 at 003-002-002 XS: ZZ BU GP: A>
cell 164 139 fill 8 -1
% block <axialReflectorFuelForwardTop B0044-003 at 003-002-003 XS: S BU GP: A>
cell 165 140 fill 10 -1

% Assembly A0044
lat 141 9 0.0 0.0 4
0 137
63.92 138
63.92 139
63.92 140
% block <pbrAssemBlock B0048-000 at 004-002-000 XS: N BU GP: A>
cell 166 142 fill 4 -1
% block <axialReflectorFuelForwardBottom B0048-001 at 004-002-001 XS: Z BU GP: A>
cell 167 143 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0048-002 at 004-002-002 XS: ZZ BU GP: A>
cell 168 144 fill 8 -1
% block <axialReflectorFuelForwardTop B0048-003 at 004-002-003 XS: S BU GP: A>
cell 169 145 fill 10 -1

% Assembly A0048
lat 146 9 0.0 0.0 4
0 142
63.92 143
63.92 144
63.92 145
% block <pbrAssemBlock B0035-000 at 004-008-000 XS: N BU GP: A>
cell 170 147 fill 4 -1
% block <axialReflectorFuelForwardBottom B0035-001 at 004-008-001 XS: Z BU GP: A>
cell 171 148 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0035-002 at 004-008-002 XS: ZZ BU GP: A>
cell 172 149 fill 8 -1
% block <axialReflectorFuelForwardTop B0035-003 at 004-008-003 XS: S BU GP: A>
cell 173 150 fill 10 -1

% Assembly A0035
lat 151 9 0.0 0.0 4
0 147
63.92 148
63.92 149
63.92 150
% block <pbrAssemBlock B0039-000 at 003-005-000 XS: N BU GP: A>
cell 174 152 fill 4 -1
% block <axialReflectorFuelForwardBottom B0039-001 at 003-005-001 XS: Z BU GP: A>
cell 175 153 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0039-002 at 003-005-002 XS: ZZ BU GP: A>
cell 176 154 fill 8 -1
% block <axialReflectorFuelForwardTop B0039-003 at 003-005-003 XS: S BU GP: A>
cell 177 155 fill 10 -1

% Assembly A0039
lat 156 9 0.0 0.0 4
0 152
63.92 153
63.92 154
63.92 155
% block <pbrAssemBlock B0043-000 at 003-004-000 XS: N BU GP: A>
cell 178 157 fill 4 -1
% block <axialReflectorFuelForwardBottom B0043-001 at 003-004-001 XS: Z BU GP: A>
cell 179 158 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0043-002 at 003-004-002 XS: ZZ BU GP: A>
cell 180 159 fill 8 -1
% block <axialReflectorFuelForwardTop B0043-003 at 003-004-003 XS: S BU GP: A>
cell 181 160 fill 10 -1

% Assembly A0043
lat 161 9 0.0 0.0 4
0 157
63.92 158
63.92 159
63.92 160
% block <pbrAssemBlock B0047-000 at 003-003-000 XS: N BU GP: A>
cell 182 162 fill 4 -1
% block <axialReflectorFuelForwardBottom B0047-001 at 003-003-001 XS: Z BU GP: A>
cell 183 163 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0047-002 at 003-003-002 XS: ZZ BU GP: A>
cell 184 164 fill 8 -1
% block <axialReflectorFuelForwardTop B0047-003 at 003-003-003 XS: S BU GP: A>
cell 185 165 fill 10 -1

% Assembly A0047
lat 166 9 0.0 0.0 4
0 162
63.92 163
63.92 164
63.92 165
% block <pbrAssemBlock B0050-000 at 004-003-000 XS: N BU GP: A>
cell 186 167 fill 4 -1
% block <axialReflectorFuelForwardBottom B0050-001 at 004-003-001 XS: Z BU GP: A>
cell 187 168 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0050-002 at 004-003-002 XS: ZZ BU GP: A>
cell 188 169 fill 8 -1
% block <axialReflectorFuelForwardTop B0050-003 at 004-003-003 XS: S BU GP: A>
cell 189 170 fill 10 -1

% Assembly A0050
lat 171 9 0.0 0.0 4
0 167
63.92 168
63.92 169
63.92 170
% block <pbrAssemBlock B0042-000 at 004-007-000 XS: N BU GP: A>
cell 190 172 fill 4 -1
% block <axialReflectorFuelForwardBottom B0042-001 at 004-007-001 XS: Z BU GP: A>
cell 191 173 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0042-002 at 004-007-002 XS: ZZ BU GP: A>
cell 192 174 fill 8 -1
% block <axialReflectorFuelForwardTop B0042-003 at 004-007-003 XS: S BU GP: A>
cell 193 175 fill 10 -1

% Assembly A0042
lat 176 9 0.0 0.0 4
0 172
63.92 173
63.92 174
63.92 175
% block <pbrAssemBlock B0046-000 at 004-006-000 XS: N BU GP: A>
cell 194 177 fill 4 -1
% block <axialReflectorFuelForwardBottom B0046-001 at 004-006-001 XS: Z BU GP: A>
cell 195 178 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0046-002 at 004-006-002 XS: ZZ BU GP: A>
cell 196 179 fill 8 -1
% block <axialReflectorFuelForwardTop B0046-003 at 004-006-003 XS: S BU GP: A>
cell 197 180 fill 10 -1

% Assembly A0046
lat 181 9 0.0 0.0 4
0 177
63.92 178
63.92 179
63.92 180
% block <pbrAssemBlock B0049-000 at 004-005-000 XS: N BU GP: A>
cell 198 182 fill 4 -1
% block <axialReflectorFuelForwardBottom B0049-001 at 004-005-001 XS: Z BU GP: A>
cell 199 183 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0049-002 at 004-005-002 XS: ZZ BU GP: A>
cell 200 184 fill 8 -1
% block <axialReflectorFuelForwardTop B0049-003 at 004-005-003 XS: S BU GP: A>
cell 201 185 fill 10 -1

% Assembly A0049
lat 186 9 0.0 0.0 4
0 182
63.92 183
63.92 184
63.92 185
% block <pbrAssemBlock B0051-000 at 004-004-000 XS: N BU GP: A>
cell 202 187 fill 4 -1
% block <axialReflectorFuelForwardBottom B0051-001 at 004-004-001 XS: Z BU GP: A>
cell 203 188 fill 6 -1
% block <axialReflectorFuelForwardMiddle B0051-002 at 004-004-002 XS: ZZ BU GP: A>
cell 204 189 fill 8 -1
% block <axialReflectorFuelForwardTop B0051-003 at 004-004-003 XS: S BU GP: A>
cell 205 190 fill 10 -1

% Assembly A0051
lat 191 9 0.0 0.0 4
0 187
63.92 188
63.92 189
63.92 190
lat 193 3 0 0 9 9 9.13100000001
inf00 inf00 inf00 inf00 inf00 inf00 inf00 inf00 inf00
inf00 inf00 inf00 inf00 86 121 151 176 inf00
inf00 inf00 inf00 56 91 126 156 181 inf00
inf00 inf00 31 61 96 131 161 186 inf00
inf00 11 36 66 101 136 166 191 inf00
inf00 16 41 71 106 141 171 inf00 inf00
inf00 21 46 76 111 146 inf00 inf00 inf00
inf00 26 51 81 116 inf00 inf00 inf00 inf00
inf00 inf00 inf00 inf00 inf00 inf00 inf00 inf00 inf00

surf 26 hexxprism 0 0 31.63071184785748 0.0 63.920001

cell 206 192 fill 193 -26
% block <drum0 B0052-000 at ExCore XS: B BU GP: A>
surf 27 cylz 0 0 4.2
surf 28 plane 0.00000000e+00 0.00000000e+00 0.00000000e+00 -4.50000000e+00 -7.79422863e+00 0.00000000e+00 -2.25000000e+00 -3.89711432e+00 -1.00000000e+00
surf 29 plane 0.00000000e+00 0.00000000e+00 0.00000000e+00 -4.50000000e+00 7.79422863e+00 0.00000000e+00 -2.25000000e+00 3.89711432e+00 -1.00000000e+00
surf 30 cylz 0 0 4.5
surf 31 cylz 0 0 4.7
surf 32 hexyc 0 0 4.7
cell 207 195 m0 -27
cell 208 195 m15 -28 29 27 -30
cell 209 195 m0 ( 28 : -29 ) 27 -30
cell 210 195 m2 30 -31
cell 211 195 m0 31 -32
cell 212 195 void 27 ( 28 : -29 : -27 : 30 ) ( ( -28 29 ) : -27 : 30 ) ( -30 : 31 ) ( -31 : 32 )
cell 213 194 fill 195 -1
% block <drum0 B0052-001 at ExCore XS: B BU GP: A>
cell 214 196 fill 195 -1
% block <drum0 B0052-002 at ExCore XS: B BU GP: A>
cell 215 197 fill 195 -1
% block <drum0 B0052-003 at ExCore XS: B BU GP: A>
cell 216 198 fill 195 -1
% block <drum0 B0052-004 at ExCore XS: B BU GP: A>
cell 217 199 fill 195 -1
% block <axialReflectorControlDrum B0052-005 at ExCore XS: R BU GP: A>
surf 33 cylz 0 0 0.0
cell 218 201 m0 33 -32
cell 219 201 void ( -33 : 32 )
cell 220 200 fill 201 -1

% Assembly A0052
lat 202 9 0.0 0.0 6
0 194
12.784 196
25.568 197
38.352 198
51.136 199
63.92 200
% block <drum_1 B0053-000 at ExCore XS: C BU GP: A>
surf 34 plane 0.00000000e+00 0.00000000e+00 0.00000000e+00 -7.79422863e+00 4.50000000e+00 0.00000000e+00 -3.89711432e+00 2.25000000e+00 -1.00000000e+00
surf 35 plane 0.00000000e+00 0.00000000e+00 0.00000000e+00 1.10218212e-15 -9.00000000e+00 0.00000000e+00 5.51091060e-16 -4.50000000e+00 -1.00000000e+00
cell 221 204 m0 -27
cell 222 204 m0 ( 35 : -34 ) 27 -30
cell 223 204 m15 -35 34 27 -30
cell 224 204 m2 30 -31
cell 225 204 m0 31 -32
cell 226 204 void 27 ( ( -35 34 ) : -27 : 30 ) ( 35 : -34 : -27 : 30 ) ( -30 : 31 ) ( -31 : 32 )
cell 227 203 fill 204 -1
% block <drum_1 B0053-001 at ExCore XS: C BU GP: A>
cell 228 205 fill 204 -1
% block <drum_1 B0053-002 at ExCore XS: C BU GP: A>
cell 229 206 fill 204 -1
% block <drum_1 B0053-003 at ExCore XS: C BU GP: A>
cell 230 207 fill 204 -1
% block <drum_1 B0053-004 at ExCore XS: C BU GP: A>
cell 231 208 fill 204 -1
% block <axialReflectorControlDrum B0053-005 at ExCore XS: R BU GP: A>
cell 232 209 fill 201 -1

% Assembly A0053
lat 210 9 0.0 0.0 6
0 203
12.784 205
25.568 206
38.352 207
51.136 208
63.92 209
% block <drum_2 B0054-000 at ExCore XS: D BU GP: A>
surf 36 plane 0.00000000e+00 0.00000000e+00 0.00000000e+00 -9.00000000e+00 -1.65327318e-15 0.00000000e+00 -4.50000000e+00 -8.26636589e-16 -1.00000000e+00
surf 37 plane 0.00000000e+00 0.00000000e+00 0.00000000e+00 4.50000000e+00 -7.79422863e+00 0.00000000e+00 2.25000000e+00 -3.89711432e+00 -1.00000000e+00
cell 233 212 m0 -27
cell 234 212 m0 ( 37 : -36 ) 27 -30
cell 235 212 m15 -37 36 27 -30
cell 236 212 m2 30 -31
cell 237 212 m0 31 -32
cell 238 212 void 27 ( ( -37 36 ) : -27 : 30 ) ( 37 : -36 : -27 : 30 ) ( -30 : 31 ) ( -31 : 32 )
cell 239 211 fill 212 -1
% block <drum_2 B0054-001 at ExCore XS: D BU GP: A>
cell 240 213 fill 212 -1
% block <drum_2 B0054-002 at ExCore XS: D BU GP: A>
cell 241 214 fill 212 -1
% block <drum_2 B0054-003 at ExCore XS: D BU GP: A>
cell 242 215 fill 212 -1
% block <drum_2 B0054-004 at ExCore XS: D BU GP: A>
cell 243 216 fill 212 -1
% block <axialReflectorControlDrum B0054-005 at ExCore XS: R BU GP: A>
cell 244 217 fill 201 -1

% Assembly A0054
lat 218 9 0.0 0.0 6
0 211
12.784 213
25.568 214
38.352 215
51.136 216
63.92 217
% block <drum_3 B0055-000 at ExCore XS: E BU GP: A>
surf 38 plane 0.00000000e+00 0.00000000e+00 0.00000000e+00 -7.79422863e+00 -4.50000000e+00 0.00000000e+00 -3.89711432e+00 -2.25000000e+00 -1.00000000e+00
surf 39 plane 0.00000000e+00 0.00000000e+00 0.00000000e+00 7.79422863e+00 -4.50000000e+00 0.00000000e+00 3.89711432e+00 -2.25000000e+00 -1.00000000e+00
cell 245 220 m0 -27
cell 246 220 m0 ( 39 : -38 ) 27 -30
cell 247 220 m15 -39 38 27 -30
cell 248 220 m2 30 -31
cell 249 220 m0 31 -32
cell 250 220 void 27 ( ( -39 38 ) : -27 : 30 ) ( 39 : -38 : -27 : 30 ) ( -30 : 31 ) ( -31 : 32 )
cell 251 219 fill 220 -1
% block <drum_3 B0055-001 at ExCore XS: E BU GP: A>
cell 252 221 fill 220 -1
% block <drum_3 B0055-002 at ExCore XS: E BU GP: A>
cell 253 222 fill 220 -1
% block <drum_3 B0055-003 at ExCore XS: E BU GP: A>
cell 254 223 fill 220 -1
% block <drum_3 B0055-004 at ExCore XS: E BU GP: A>
cell 255 224 fill 220 -1
% block <axialReflectorControlDrum B0055-005 at ExCore XS: R BU GP: A>
cell 256 225 fill 201 -1

% Assembly A0055
lat 226 9 0.0 0.0 6
0 219
12.784 221
25.568 222
38.352 223
51.136 224
63.92 225
% block <drum_4 B0056-000 at ExCore XS: F BU GP: A>
surf 40 plane 0.00000000e+00 0.00000000e+00 0.00000000e+00 9.00000000e+00 2.75545530e-15 0.00000000e+00 4.50000000e+00 1.37772765e-15 -1.00000000e+00
surf 41 plane 0.00000000e+00 0.00000000e+00 0.00000000e+00 9.00000000e+00 5.51091060e-16 0.00000000e+00 4.50000000e+00 2.75545530e-16 -1.00000000e+00
cell 257 228 m0 -27
cell 258 228 m0 ( 40 : -28 ) 27 -30
cell 259 228 m15 -41 28 27 -30
cell 260 228 m2 30 -31
cell 261 228 m0 31 -32
cell 262 228 void 27 ( ( -40 28 ) : -27 : 30 ) ( 41 : -28 : -27 : 30 ) ( -30 : 31 ) ( -31 : 32 )
cell 263 227 fill 228 -1
% block <drum_4 B0056-001 at ExCore XS: F BU GP: A>
cell 264 229 fill 228 -1
% block <drum_4 B0056-002 at ExCore XS: F BU GP: A>
cell 265 230 fill 228 -1
% block <drum_4 B0056-003 at ExCore XS: F BU GP: A>
cell 266 231 fill 228 -1
% block <drum_4 B0056-004 at ExCore XS: F BU GP: A>
cell 267 232 fill 228 -1
% block <axialReflectorControlDrum B0056-005 at ExCore XS: R BU GP: A>
cell 268 233 fill 201 -1

% Assembly A0056
lat 234 9 0.0 0.0 6
0 227
12.784 229
25.568 230
38.352 231
51.136 232
63.92 233
% block <drum_5 B0057-000 at ExCore XS: G BU GP: A>
surf 42 plane 0.00000000e+00 0.00000000e+00 0.00000000e+00 7.79422863e+00 4.50000000e+00 0.00000000e+00 3.89711432e+00 2.25000000e+00 -1.00000000e+00
cell 269 236 m0 -27
cell 270 236 m0 ( 42 : -35 ) 27 -30
cell 271 236 m15 -42 35 27 -30
cell 272 236 m2 30 -31
cell 273 236 m0 31 -32
cell 274 236 void 27 ( ( -42 35 ) : -27 : 30 ) ( 42 : -35 : -27 : 30 ) ( -30 : 31 ) ( -31 : 32 )
cell 275 235 fill 236 -1
% block <drum_5 B0057-001 at ExCore XS: G BU GP: A>
cell 276 237 fill 236 -1
% block <drum_5 B0057-002 at ExCore XS: G BU GP: A>
cell 277 238 fill 236 -1
% block <drum_5 B0057-003 at ExCore XS: G BU GP: A>
cell 278 239 fill 236 -1
% block <drum_5 B0057-004 at ExCore XS: G BU GP: A>
cell 279 240 fill 236 -1
% block <axialReflectorControlDrum B0057-005 at ExCore XS: R BU GP: A>
cell 280 241 fill 201 -1

% Assembly A0057
lat 242 9 0.0 0.0 6
0 235
12.784 237
25.568 238
38.352 239
51.136 240
63.92 241
% block <drum_6 B0058-000 at ExCore XS: H BU GP: A>
surf 43 plane 0.00000000e+00 0.00000000e+00 0.00000000e+00 4.50000000e+00 7.79422863e+00 0.00000000e+00 2.25000000e+00 3.89711432e+00 -1.00000000e+00
cell 281 244 m0 -27
cell 282 244 m0 ( 43 : -37 ) 27 -30
cell 283 244 m15 -43 37 27 -30
cell 284 244 m2 30 -31
cell 285 244 m0 31 -32
cell 286 244 void 27 ( ( -43 37 ) : -27 : 30 ) ( 43 : -37 : -27 : 30 ) ( -30 : 31 ) ( -31 : 32 )
cell 287 243 fill 244 -1
% block <drum_6 B0058-001 at ExCore XS: H BU GP: A>
cell 288 245 fill 244 -1
% block <drum_6 B0058-002 at ExCore XS: H BU GP: A>
cell 289 246 fill 244 -1
% block <drum_6 B0058-003 at ExCore XS: H BU GP: A>
cell 290 247 fill 244 -1
% block <drum_6 B0058-004 at ExCore XS: H BU GP: A>
cell 291 248 fill 244 -1
% block <axialReflectorControlDrum B0058-005 at ExCore XS: R BU GP: A>
cell 292 249 fill 201 -1

% Assembly A0058
lat 250 9 0.0 0.0 6
0 243
12.784 245
25.568 246
38.352 247
51.136 248
63.92 249
% block <drum_7 B0059-000 at ExCore XS: I BU GP: A>
surf 44 plane 0.00000000e+00 0.00000000e+00 0.00000000e+00 -2.20436424e-15 9.00000000e+00 0.00000000e+00 -1.10218212e-15 4.50000000e+00 -1.00000000e+00
cell 293 252 m0 -27
cell 294 252 m0 ( 44 : -39 ) 27 -30
cell 295 252 m15 -44 39 27 -30
cell 296 252 m2 30 -31
cell 297 252 m0 31 -32
cell 298 252 void 27 ( ( -44 39 ) : -27 : 30 ) ( 44 : -39 : -27 : 30 ) ( -30 : 31 ) ( -31 : 32 )
cell 299 251 fill 252 -1
% block <drum_7 B0059-001 at ExCore XS: I BU GP: A>
cell 300 253 fill 252 -1
% block <drum_7 B0059-002 at ExCore XS: I BU GP: A>
cell 301 254 fill 252 -1
% block <drum_7 B0059-003 at ExCore XS: I BU GP: A>
cell 302 255 fill 252 -1
% block <drum_7 B0059-004 at ExCore XS: I BU GP: A>
cell 303 256 fill 252 -1
% block <axialReflectorControlDrum B0059-005 at ExCore XS: R BU GP: A>
cell 304 257 fill 201 -1

% Assembly A0059
lat 258 9 0.0 0.0 6
0 251
12.784 253
25.568 254
38.352 255
51.136 256
63.92 257
% block <drum_8 B0060-000 at ExCore XS: J BU GP: A>
cell 305 260 m0 -27
cell 306 260 m0 ( 29 : -40 ) 27 -30
cell 307 260 m15 -29 40 27 -30
cell 308 260 m2 30 -31
cell 309 260 m0 31 -32
cell 310 260 void 27 ( ( -29 40 ) : -27 : 30 ) ( 29 : -40 : -27 : 30 ) ( -30 : 31 ) ( -31 : 32 )
cell 311 259 fill 260 -1
% block <drum_8 B0060-001 at ExCore XS: J BU GP: A>
cell 312 261 fill 260 -1
% block <drum_8 B0060-002 at ExCore XS: J BU GP: A>
cell 313 262 fill 260 -1
% block <drum_8 B0060-003 at ExCore XS: J BU GP: A>
cell 314 263 fill 260 -1
% block <drum_8 B0060-004 at ExCore XS: J BU GP: A>
cell 315 264 fill 260 -1
% block <axialReflectorControlDrum B0060-005 at ExCore XS: R BU GP: A>
cell 316 265 fill 201 -1

% Assembly A0060
lat 266 9 0.0 0.0 6
0 259
12.784 261
25.568 262
38.352 263
51.136 264
63.92 265
% block <drum_9 B0061-000 at ExCore XS: K BU GP: A>
cell 317 268 m0 -27
cell 318 268 m0 ( 34 : -42 ) 27 -30
cell 319 268 m15 -34 42 27 -30
cell 320 268 m2 30 -31
cell 321 268 m0 31 -32
cell 322 268 void 27 ( ( -34 42 ) : -27 : 30 ) ( 34 : -42 : -27 : 30 ) ( -30 : 31 ) ( -31 : 32 )
cell 323 267 fill 268 -1
% block <drum_9 B0061-001 at ExCore XS: K BU GP: A>
cell 324 269 fill 268 -1
% block <drum_9 B0061-002 at ExCore XS: K BU GP: A>
cell 325 270 fill 268 -1
% block <drum_9 B0061-003 at ExCore XS: K BU GP: A>
cell 326 271 fill 268 -1
% block <drum_9 B0061-004 at ExCore XS: K BU GP: A>
cell 327 272 fill 268 -1
% block <axialReflectorControlDrum B0061-005 at ExCore XS: R BU GP: A>
cell 328 273 fill 201 -1

% Assembly A0061
lat 274 9 0.0 0.0 6
0 267
12.784 269
25.568 270
38.352 271
51.136 272
63.92 273
% block <drum_10 B0062-000 at ExCore XS: L BU GP: A>
cell 329 276 m0 -27
cell 330 276 m0 ( 36 : -43 ) 27 -30
cell 331 276 m15 -36 43 27 -30
cell 332 276 m2 30 -31
cell 333 276 m0 31 -32
cell 334 276 void 27 ( ( -36 43 ) : -27 : 30 ) ( 36 : -43 : -27 : 30 ) ( -30 : 31 ) ( -31 : 32 )
cell 335 275 fill 276 -1
% block <drum_10 B0062-001 at ExCore XS: L BU GP: A>
cell 336 277 fill 276 -1
% block <drum_10 B0062-002 at ExCore XS: L BU GP: A>
cell 337 278 fill 276 -1
% block <drum_10 B0062-003 at ExCore XS: L BU GP: A>
cell 338 279 fill 276 -1
% block <drum_10 B0062-004 at ExCore XS: L BU GP: A>
cell 339 280 fill 276 -1
% block <axialReflectorControlDrum B0062-005 at ExCore XS: R BU GP: A>
cell 340 281 fill 201 -1

% Assembly A0062
lat 282 9 0.0 0.0 6
0 275
12.784 277
25.568 278
38.352 279
51.136 280
63.92 281
% block <drum_11 B0063-000 at ExCore XS: M BU GP: A>
cell 341 284 m0 -27
cell 342 284 m0 ( 38 : -44 ) 27 -30
cell 343 284 m15 -38 44 27 -30
cell 344 284 m2 30 -31
cell 345 284 m0 31 -32
cell 346 284 void 27 ( ( -38 44 ) : -27 : 30 ) ( 38 : -44 : -27 : 30 ) ( -30 : 31 ) ( -31 : 32 )
cell 347 283 fill 284 -1
% block <drum_11 B0063-001 at ExCore XS: M BU GP: A>
cell 348 285 fill 284 -1
% block <drum_11 B0063-002 at ExCore XS: M BU GP: A>
cell 349 286 fill 284 -1
% block <drum_11 B0063-003 at ExCore XS: M BU GP: A>
cell 350 287 fill 284 -1
% block <drum_11 B0063-004 at ExCore XS: M BU GP: A>
cell 351 288 fill 284 -1
% block <axialReflectorControlDrum B0063-005 at ExCore XS: R BU GP: A>
cell 352 289 fill 201 -1

% Assembly A0063
lat 290 9 0.0 0.0 6
0 283
12.784 285
25.568 286
38.352 287
51.136 288
63.92 289

trans U 202 35.6925 0 0.0
surf 45 hexyprism 35.6925 0.0 4.7 0.0 63.920001
cell 353 291 fill 202 -45

trans U 210 30.910612 17.84625 0.0
surf 46 hexyprism 30.91061172 17.84625 4.7 0.0 63.920001
cell 354 291 fill 210 -46

trans U 218 17.84625 30.910612 0.0
surf 47 hexyprism 17.84625 30.91061172 4.7 0.0 63.920001
cell 355 291 fill 218 -47

trans U 226 0 35.6925 0.0
surf 48 hexyprism 0.0 35.6925 4.7 0.0 63.920001
cell 356 291 fill 226 -48

trans U 234 -17.84625 30.910612 0.0
surf 49 hexyprism -17.84625 30.91061172 4.7 0.0 63.920001
cell 357 291 fill 234 -49

trans U 242 -30.910612 17.84625 0.0
surf 50 hexyprism -30.91061172 17.84625 4.7 0.0 63.920001
cell 358 291 fill 242 -50

trans U 250 -35.6925 0 0.0
surf 51 hexyprism -35.6925 0.0 4.7 0.0 63.920001
cell 359 291 fill 250 -51

trans U 258 -30.910612 -17.84625 0.0
surf 52 hexyprism -30.91061172 -17.84625 4.7 0.0 63.920001
cell 360 291 fill 258 -52

trans U 266 -17.84625 -30.910612 0.0
surf 53 hexyprism -17.84625 -30.91061172 4.7 0.0 63.920001
cell 361 291 fill 266 -53

trans U 274 -0 -35.6925 0.0
surf 54 hexyprism -0.0 -35.6925 4.7 0.0 63.920001
cell 362 291 fill 274 -54

trans U 282 17.84625 -30.910612 0.0
surf 55 hexyprism 17.84625 -30.91061172 4.7 0.0 63.920001
cell 363 291 fill 282 -55

trans U 290 30.910612 -17.84625 0.0
surf 56 hexyprism 30.91061172 -17.84625 4.7 0.0 63.920001
cell 364 291 fill 290 -56

cell 365 0 fill 2 2 -3

cell 366 0 m0 26 45 46 47 48 49 50 51 52 53 54 55 56 -2
cell 367 0 fill 192 -26 45 46 47 48 49 50 51 52 53 54 55 56 -2
cell 368 0 fill 291 ( -45 : -46 : -47 : -48 : -49 : -50 : -51 : -52 : -53 : -54 : -55 : -56 ) -2

cell 369 0 outside 3
% Be-9
mat m0 -1.85 tmp 300.0
rgb 23 190 207
moder be-met.40t 4009
Be-9.00c 0.123621
% Inconel 617
mat m1 -8.258 tmp 300.0
rgb 127 127 127
Mo-100.00c 0.000449298
B-10.00c 5.46431e-06
B-11.00c 2.21332e-05
C-12.00c 0.000409444
C-13.00c 4.59584e-06
Al-27.00c 0.00211961
Si-28.00c 0.000816559
Si-29.00c 4.1461e-05
Si-30.00c 2.73308e-05
P-31.00c 2.40837e-05
S-32.00c 2.21059e-05
S-33.00c 1.74484e-07
S-34.00c 9.79436e-07
S-36.00c 4.6529e-09
Ti-46.00c 5.14277e-05
Ti-47.00c 4.63784e-05
Ti-48.00c 0.000459545
Ti-49.00c 3.37241e-05
Ti-50.00c 3.22904e-05
Cr-50.00c 0.000914255
Cr-52.00c 0.0176305
Cr-53.00c 0.00199916
Cr-54.00c 0.000497632
Fe-54.00c 0.000156152
Mn-55.00c 0.000452608
Fe-56.00c 0.00245124
Fe-57.00c 5.66099e-05
Fe-58.00c 7.53374e-06
Ni-58.00c 0.0289066
Co-59.00c 0.0105481
Ni-60.00c 0.0111347
Ni-61.00c 0.000484062
Ni-62.00c 0.00154305
Cu-63.00c 0.000270662
Ni-64.00c 0.000393194
Cu-65.00c 0.000120638
Mo-92.00c 0.000692376
Mo-94.00c 0.000431569
Mo-95.00c 0.000742764
Mo-96.00c 0.000778223
Mo-97.00c 0.000445565
Mo-98.00c 0.00112581
% Custom
mat m2 -1e-05 tmp 300.0
rgb 31 119 180
H-1.00c 5.97359e-06
H-2.00c 8.96173e-10
% ZrC
mat m3 -6.73 tmp 300.0
rgb 255 127 14
C-12.00c 0.0388233
C-13.00c 0.000435776
Zr-90.00c 0.0201988
Zr-91.00c 0.00440487
Zr-92.00c 0.00673293
Zr-94.00c 0.00682323
Zr-96.00c 0.00109925
% Custom
mat m4 -3.365 tmp 300.0
rgb 255 127 14
C-12.00c 0.0194116
C-13.00c 0.000217888
Zr-90.00c 0.0100994
Zr-91.00c 0.00220243
Zr-92.00c 0.00336646
Zr-94.00c 0.00341161
Zr-96.00c 0.000549627
% HighEnriched_U01Zr9_C829
mat m5 -5.94719911 tmp 300.0
rgb 214 39 40
B-10.00c 5.63397e-09
B-11.00c 2.28204e-08
C-12.00c 0.0253407
C-13.00c 0.000284439
U-234.00c 1.69769e-07
U-235.00c 0.00292697
U-238.00c 0.000200742
Zr-90.00c 0.0143138
Zr-91.00c 0.00312148
Zr-92.00c 0.00477125
Zr-94.00c 0.00483524
Zr-96.00c 0.00077898
% Custom
mat m6 -6.73 tmp 300.0
rgb 127 127 127
C-12.00c 0.0388233
C-13.00c 0.000435776
Zr-90.00c 0.0201988
Zr-91.00c 0.00440487
Zr-92.00c 0.00673293
Zr-94.00c 0.00682323
Zr-96.00c 0.00109925
% Custom
mat m7 -0.925 tmp 300.0
rgb 23 190 207
Be-9.00c 0.0618106
% AlPe
mat m8 -0.74856442 tmp 300.0
rgb 128 128 128 
H-1.00c 0.0407272
C-12.00c 0.020141
C-13.00c 0.000226074
H-2.00c 6.10999e-06
Al-27.00c 0.00611924
% Custom
mat m9 -3.365 tmp 700.0
rgb 255 127 14
C-12.01c 0.0194116
C-13.01c 0.000217888
Zr-90.01c 0.0100994
Zr-91.01c 0.00220243
Zr-92.01c 0.00336646
Zr-94.01c 0.00341161
Zr-96.01c 0.000549627
% Custom
mat m10 -1.7 tmp 300.0
rgb 214 39 40
C-12.00c 0.0842885
C-13.00c 0.000946104
% Custom
mat m11 -1.12 tmp 300.0
rgb 211 211 211
C-12.00c 0.0555312
C-13.00c 0.000623315
% Custom
mat m12 -1.13 tmp 300.0
rgb 255 127 14
C-12.00c 0.056027
C-13.00c 0.000628881
% AlPe
mat m13 -0.74856442 tmp 600.0
rgb 128 128 128
H-1.01c 0.0407272
C-12.01c 0.020141
C-13.01c 0.000226074
H-2.01c 6.10999e-06
Al-27.01c 0.00611924
% Be-9
mat m14 -1.85 tmp 600.0
rgb 23 190 207
moder be-met.43t 4009
Be-9.01c 0.123621
% PNNLBoronCarbide
mat m15 -2.52 tmp 300.0
rgb 44 160 44
B-10.00c 0.0217498
B-11.00c 0.0880976
C-12.00c 0.0271618
C-13.00c 0.000304881
therm be-met.40t be-met.40t
therm be-met.43t be-met.43t
% Sets parameters for simulated neutron population in criticality source mode
set pop 10000 150 50

% Switches off the group constant generation
set gcu -1
% Vacuum boundary condition
set bc 1
set mcvol 3000000


set opti 1

set power 1100000000.0

% Flux in all blocks
det block_flux
du 3 du 5 du 7 du 9 du 12 du 13 du 14 du 15 du 17 du 18 du 19 du 20 du 22 du 23 du 24 du 25 du 27 du 28 du 29 du 30 du 32 du 33 du 34 du 35 du 37 du 38 du 39 du 40 du 42 du 43 du 44 du 45 du 47 du 48 du 49 du 50 du 52 du 53 du 54 du 55 du 57 du 58 du 59 du 60 du 62 du 63 du 64 du 65 du 67 du 68 du 69 du 70 du 72 du 73 du 74 du 75 du 77 du 78 du 79 du 80 du 82 du 83 du 84 du 85 du 87 du 88 du 89 du 90 du 92 du 93 du 94 du 95 du 97 du 98 du 99 du 100 du 102 du 103 du 104 du 105 du 107 du 108 du 109 du 110 du 112 du 113 du 114 du 115 du 117 du 118 du 119 du 120 du 122 du 123 du 124 du 125 du 127 du 128 du 129 du 130 du 132 du 133 du 134 du 135 du 137 du 138 du 139 du 140 du 142 du 143 du 144 du 145 du 147 du 148 du 149 du 150 du 152 du 153 du 154 du 155 du 157 du 158 du 159 du 160 du 162 du 163 du 164 du 165 du 167 du 168 du 169 du 170 du 172 du 173 du 174 du 175 du 177 du 178 du 179 du 180 du 182 du 183 du 184 du 185 du 187 du 188 du 189 du 190 du 194 du 196 du 197 du 198 du 199 du 200 du 203 du 205 du 206 du 207 du 208 du 209 du 211 du 213 du 214 du 215 du 216 du 217 du 219 du 221 du 222 du 223 du 224 du 225 du 227 du 229 du 230 du 231 du 232 du 233 du 235 du 237 du 238 du 239 du 240 du 241 du 243 du 245 du 246 du 247 du 248 du 249 du 251 du 253 du 254 du 255 du 256 du 257 du 259 du 261 du 262 du 263 du 264 du 265 du 267 du 269 du 270 du 271 du 272 du 273 du 275 du 277 du 278 du 279 du 280 du 281 du 283 du 285 du 286 du 287 du 288 du 289

% Reaction rates
det block_rxnrates
du 3 du 5 du 7 du 9 du 12 du 13 du 14 du 15 du 17 du 18 du 19 du 20 du 22 du 23 du 24 du 25 du 27 du 28 du 29 du 30 du 32 du 33 du 34 du 35 du 37 du 38 du 39 du 40 du 42 du 43 du 44 du 45 du 47 du 48 du 49 du 50 du 52 du 53 du 54 du 55 du 57 du 58 du 59 du 60 du 62 du 63 du 64 du 65 du 67 du 68 du 69 du 70 du 72 du 73 du 74 du 75 du 77 du 78 du 79 du 80 du 82 du 83 du 84 du 85 du 87 du 88 du 89 du 90 du 92 du 93 du 94 du 95 du 97 du 98 du 99 du 100 du 102 du 103 du 104 du 105 du 107 du 108 du 109 du 110 du 112 du 113 du 114 du 115 du 117 du 118 du 119 du 120 du 122 du 123 du 124 du 125 du 127 du 128 du 129 du 130 du 132 du 133 du 134 du 135 du 137 du 138 du 139 du 140 du 142 du 143 du 144 du 145 du 147 du 148 du 149 du 150 du 152 du 153 du 154 du 155 du 157 du 158 du 159 du 160 du 162 du 163 du 164 du 165 du 167 du 168 du 169 du 170 du 172 du 173 du 174 du 175 du 177 du 178 du 179 du 180 du 182 du 183 du 184 du 185 du 187 du 188 du 189 du 190 du 194 du 196 du 197 du 198 du 199 du 200 du 203 du 205 du 206 du 207 du 208 du 209 du 211 du 213 du 214 du 215 du 216 du 217 du 219 du 221 du 222 du 223 du 224 du 225 du 227 du 229 du 230 du 231 du 232 du 233 du 235 du 237 du 238 du 239 du 240 du 241 du 243 du 245 du 246 du 247 du 248 du 249 du 251 du 253 du 254 du 255 du 256 du 257 du 259 du 261 du 262 du 263 du 264 du 265 du 267 du 269 du 270 du 271 du 272 du 273 du 275 du 277 du 278 du 279 du 280 du 281 du 283 du 285 du 286 du 287 du 288 du 289
% ['CAPTURE', 'FISSION', 'NU_SIGF', 'FISSION_POWER']
dr -2 void dr -6 void dr -7 void dr -8 void

% Total energy deposition tally
det block_power
du 3 du 5 du 7 du 9 du 12 du 13 du 14 du 15 du 17 du 18 du 19 du 20 du 22 du 23 du 24 du 25 du 27 du 28 du 29 du 30 du 32 du 33 du 34 du 35 du 37 du 38 du 39 du 40 du 42 du 43 du 44 du 45 du 47 du 48 du 49 du 50 du 52 du 53 du 54 du 55 du 57 du 58 du 59 du 60 du 62 du 63 du 64 du 65 du 67 du 68 du 69 du 70 du 72 du 73 du 74 du 75 du 77 du 78 du 79 du 80 du 82 du 83 du 84 du 85 du 87 du 88 du 89 du 90 du 92 du 93 du 94 du 95 du 97 du 98 du 99 du 100 du 102 du 103 du 104 du 105 du 107 du 108 du 109 du 110 du 112 du 113 du 114 du 115 du 117 du 118 du 119 du 120 du 122 du 123 du 124 du 125 du 127 du 128 du 129 du 130 du 132 du 133 du 134 du 135 du 137 du 138 du 139 du 140 du 142 du 143 du 144 du 145 du 147 du 148 du 149 du 150 du 152 du 153 du 154 du 155 du 157 du 158 du 159 du 160 du 162 du 163 du 164 du 165 du 167 du 168 du 169 du 170 du 172 du 173 du 174 du 175 du 177 du 178 du 179 du 180 du 182 du 183 du 184 du 185 du 187 du 188 du 189 du 190 du 194 du 196 du 197 du 198 du 199 du 200 du 203 du 205 du 206 du 207 du 208 du 209 du 211 du 213 du 214 du 215 du 216 du 217 du 219 du 221 du 222 du 223 du 224 du 225 du 227 du 229 du 230 du 231 du 232 du 233 du 235 du 237 du 238 du 239 du 240 du 241 du 243 du 245 du 246 du 247 du 248 du 249 du 251 du 253 du 254 du 255 du 256 du 257 du 259 du 261 du 262 du 263 du 264 du 265 du 267 du 269 du 270 du 271 du 272 du 273 du 275 du 277 du 278 du 279 du 280 du 281 du 283 du 285 du 286 du 287 du 288 du 289
dr -80 void
ene 1000 1 1e-11 1.02843e-11 1.05767e-11 1.08775e-11 1.11868e-11 1.15048e-11 1.1832e-11
1.21684e-11 1.25144e-11 1.28702e-11 1.32361e-11 1.36125e-11 1.39995e-11 1.43976e-11
1.48069e-11 1.52279e-11 1.56609e-11 1.61062e-11 1.65642e-11 1.70351e-11 1.75195e-11
1.80176e-11 1.85299e-11 1.90568e-11 1.95987e-11 2.01559e-11 2.0729e-11 2.13184e-11
2.19245e-11 2.25479e-11 2.3189e-11 2.38484e-11 2.45265e-11 2.52238e-11 2.5941e-11
2.66786e-11 2.74372e-11 2.82173e-11 2.90196e-11 2.98447e-11 3.06933e-11 3.1566e-11
3.24635e-11 3.33866e-11 3.43359e-11 3.53121e-11 3.63162e-11 3.73488e-11 3.84107e-11
3.95029e-11 4.0626e-11 4.17812e-11 4.29691e-11 4.41909e-11 4.54474e-11 4.67396e-11
4.80686e-11 4.94353e-11 5.08409e-11 5.22865e-11 5.37731e-11 5.53021e-11 5.68745e-11
5.84916e-11 6.01547e-11 6.18651e-11 6.36242e-11 6.54332e-11 6.72937e-11 6.9207e-11
7.11748e-11 7.31986e-11 7.52798e-11 7.74203e-11 7.96216e-11 8.18855e-11 8.42137e-11
8.66082e-11 8.90708e-11 9.16033e-11 9.42079e-11 9.68865e-11 9.96413e-11 1.02474e-10
1.05388e-10 1.08385e-10 1.11466e-10 1.14636e-10 1.17895e-10 1.21247e-10 1.24695e-10
1.2824e-10 1.31887e-10 1.35636e-10 1.39493e-10 1.43459e-10 1.47538e-10 1.51733e-10
1.56048e-10 1.60484e-10 1.65048e-10 1.6974e-10 1.74567e-10 1.7953e-10 1.84635e-10
1.89885e-10 1.95284e-10 2.00836e-10 2.06547e-10 2.12419e-10 2.18459e-10 2.24671e-10
2.31059e-10 2.37628e-10 2.44385e-10 2.51334e-10 2.5848e-10 2.65829e-10 2.73388e-10
2.81161e-10 2.89155e-10 2.97377e-10 3.05832e-10 3.14528e-10 3.23471e-10 3.32668e-10
3.42127e-10 3.51855e-10 3.61859e-10 3.72148e-10 3.82729e-10 3.93612e-10 4.04803e-10
4.16313e-10 4.2815e-10 4.40324e-10 4.52844e-10 4.6572e-10 4.78961e-10 4.9258e-10
5.06586e-10 5.20989e-10 5.35803e-10 5.51037e-10 5.66705e-10 5.82818e-10 5.9939e-10
6.16432e-10 6.33959e-10 6.51985e-10 6.70523e-10 6.89588e-10 7.09195e-10 7.2936e-10
7.50098e-10 7.71426e-10 7.9336e-10 8.15918e-10 8.39117e-10 8.62976e-10 8.87513e-10
9.12748e-10 9.387e-10 9.6539e-10 9.92839e-10 1.02107e-09 1.0501e-09 1.07996e-09
1.11067e-09 1.14225e-09 1.17472e-09 1.20812e-09 1.24248e-09 1.2778e-09 1.31413e-09
1.3515e-09 1.38993e-09 1.42945e-09 1.47009e-09 1.51189e-09 1.55488e-09 1.59909e-09
1.64456e-09 1.69132e-09 1.73941e-09 1.78886e-09 1.83973e-09 1.89203e-09 1.94583e-09
2.00116e-09 2.05806e-09 2.11657e-09 2.17675e-09 2.23865e-09 2.3023e-09 2.36776e-09
2.43508e-09 2.50432e-09 2.57553e-09 2.64876e-09 2.72407e-09 2.80152e-09 2.88118e-09
2.9631e-09 3.04735e-09 3.134e-09 3.22311e-09 3.31475e-09 3.409e-09 3.50593e-09 3.60561e-09
3.70813e-09 3.81357e-09 3.922e-09 4.03351e-09 4.1482e-09 4.26615e-09 4.38745e-09
4.5122e-09 4.64049e-09 4.77244e-09 4.90813e-09 5.04769e-09 5.19121e-09 5.33881e-09
5.49061e-09 5.64672e-09 5.80728e-09 5.9724e-09 6.14221e-09 6.31686e-09 6.49646e-09
6.68118e-09 6.87115e-09 7.06652e-09 7.26744e-09 7.47408e-09 7.68659e-09 7.90514e-09
8.12991e-09 8.36107e-09 8.5988e-09 8.84329e-09 9.09474e-09 9.35333e-09 9.61928e-09
9.89278e-09 1.01741e-08 1.04633e-08 1.07609e-08 1.10668e-08 1.13815e-08 1.17051e-08
1.20379e-08 1.23802e-08 1.27322e-08 1.30942e-08 1.34665e-08 1.38494e-08 1.42432e-08
1.46482e-08 1.50647e-08 1.5493e-08 1.59335e-08 1.63866e-08 1.68525e-08 1.73317e-08
1.78245e-08 1.83313e-08 1.88525e-08 1.93885e-08 1.99398e-08 2.05067e-08 2.10898e-08
2.16895e-08 2.23062e-08 2.29404e-08 2.35927e-08 2.42635e-08 2.49534e-08 2.56629e-08
2.63926e-08 2.7143e-08 2.79148e-08 2.87085e-08 2.95247e-08 3.03642e-08 3.12276e-08
3.21155e-08 3.30286e-08 3.39677e-08 3.49335e-08 3.59268e-08 3.69483e-08 3.79989e-08
3.90793e-08 4.01905e-08 4.13332e-08 4.25084e-08 4.37171e-08 4.49601e-08 4.62385e-08
4.75532e-08 4.89053e-08 5.02958e-08 5.17259e-08 5.31966e-08 5.47092e-08 5.62647e-08
5.78645e-08 5.95098e-08 6.12018e-08 6.2942e-08 6.47316e-08 6.65722e-08 6.8465e-08
7.04117e-08 7.24137e-08 7.44727e-08 7.65902e-08 7.87679e-08 8.10075e-08 8.33108e-08
8.56796e-08 8.81158e-08 9.06212e-08 9.31978e-08 9.58477e-08 9.8573e-08 1.01376e-07
1.04258e-07 1.07223e-07 1.10271e-07 1.13407e-07 1.16631e-07 1.19947e-07 1.23358e-07
1.26865e-07 1.30472e-07 1.34182e-07 1.37997e-07 1.41921e-07 1.45956e-07 1.50106e-07
1.54374e-07 1.58764e-07 1.63278e-07 1.6792e-07 1.72695e-07 1.77605e-07 1.82655e-07
1.87849e-07 1.9319e-07 1.98683e-07 2.04332e-07 2.10142e-07 2.16117e-07 2.22262e-07
2.28581e-07 2.35081e-07 2.41765e-07 2.48639e-07 2.55708e-07 2.62979e-07 2.70456e-07
2.78146e-07 2.86055e-07 2.94188e-07 3.02553e-07 3.11156e-07 3.20003e-07 3.29101e-07
3.38459e-07 3.48082e-07 3.57979e-07 3.68158e-07 3.78626e-07 3.89391e-07 4.00463e-07
4.1185e-07 4.2356e-07 4.35603e-07 4.47988e-07 4.60726e-07 4.73826e-07 4.87299e-07
5.01154e-07 5.15403e-07 5.30058e-07 5.45129e-07 5.60629e-07 5.7657e-07 5.92963e-07
6.09823e-07 6.27162e-07 6.44995e-07 6.63334e-07 6.82195e-07 7.01592e-07 7.2154e-07
7.42056e-07 7.63155e-07 7.84854e-07 8.0717e-07 8.3012e-07 8.53723e-07 8.77997e-07
9.02961e-07 9.28635e-07 9.55039e-07 9.82194e-07 1.01012e-06 1.03884e-06 1.06838e-06
1.09876e-06 1.13e-06 1.16213e-06 1.19517e-06 1.22915e-06 1.2641e-06 1.30004e-06
1.33701e-06 1.37502e-06 1.41412e-06 1.45433e-06 1.49568e-06 1.53821e-06 1.58194e-06
1.62692e-06 1.67318e-06 1.72076e-06 1.76968e-06 1.82e-06 1.87175e-06 1.92497e-06
1.9797e-06 2.03599e-06 2.09388e-06 2.15342e-06 2.21464e-06 2.27761e-06 2.34237e-06
2.40898e-06 2.47747e-06 2.54791e-06 2.62036e-06 2.69486e-06 2.77149e-06 2.85029e-06
2.93133e-06 3.01468e-06 3.1004e-06 3.18855e-06 3.27921e-06 3.37245e-06 3.46834e-06
3.56695e-06 3.66837e-06 3.77268e-06 3.87995e-06 3.99027e-06 4.10372e-06 4.22041e-06
4.3404e-06 4.46382e-06 4.59074e-06 4.72127e-06 4.85551e-06 4.99357e-06 5.13555e-06
5.28157e-06 5.43174e-06 5.58618e-06 5.74501e-06 5.90836e-06 6.07636e-06 6.24913e-06
6.42681e-06 6.60955e-06 6.79748e-06 6.99075e-06 7.18952e-06 7.39394e-06 7.60417e-06
7.82039e-06 8.04274e-06 8.27143e-06 8.50661e-06 8.74848e-06 8.99723e-06 9.25305e-06
9.51614e-06 9.78671e-06 1.0065e-05 1.03512e-05 1.06455e-05 1.09482e-05 1.12595e-05
1.15796e-05 1.19088e-05 1.22474e-05 1.25957e-05 1.29538e-05 1.33221e-05 1.37009e-05
1.40905e-05 1.44911e-05 1.49032e-05 1.53269e-05 1.57627e-05 1.62109e-05 1.66718e-05
1.71458e-05 1.76333e-05 1.81347e-05 1.86504e-05 1.91806e-05 1.9726e-05 2.02869e-05
2.08637e-05 2.14569e-05 2.2067e-05 2.26944e-05 2.33397e-05 2.40033e-05 2.46858e-05
2.53877e-05 2.61096e-05 2.6852e-05 2.76155e-05 2.84007e-05 2.92082e-05 3.00387e-05
3.08928e-05 3.17711e-05 3.26745e-05 3.36035e-05 3.4559e-05 3.55416e-05 3.65522e-05
3.75915e-05 3.86603e-05 3.97596e-05 4.089e-05 4.20527e-05 4.32484e-05 4.44781e-05
4.57427e-05 4.70433e-05 4.83809e-05 4.97565e-05 5.11713e-05 5.26262e-05 5.41226e-05
5.56615e-05 5.72441e-05 5.88717e-05 6.05456e-05 6.22671e-05 6.40376e-05 6.58584e-05
6.7731e-05 6.96568e-05 7.16373e-05 7.36742e-05 7.5769e-05 7.79234e-05 8.0139e-05
8.24176e-05 8.4761e-05 8.7171e-05 8.96495e-05 9.21986e-05 9.48201e-05 9.75161e-05
0.000100289 0.00010314 0.000106073 0.000109089 0.000112191 0.000115381 0.000118661
0.000122035 0.000125505 0.000129074 0.000132744 0.000136518 0.0001404 0.000144392
0.000148497 0.000152719 0.000157062 0.000161527 0.00016612 0.000170843 0.000175701
0.000180697 0.000185835 0.000191118 0.000196553 0.000202141 0.000207889 0.0002138
0.000219879 0.00022613 0.00023256 0.000239173 0.000245973 0.000252967 0.000260159
0.000267557 0.000275164 0.000282988 0.000291034 0.000299309 0.000307819 0.000316572
0.000325573 0.00033483 0.00034435 0.000354141 0.000364211 0.000374566 0.000385216
0.000396169 0.000407434 0.000419018 0.000430932 0.000443185 0.000455786 0.000468746
0.000482074 0.000495781 0.000509877 0.000524375 0.000539285 0.000554618 0.000570388
0.000586606 0.000603285 0.000620438 0.000638079 0.000656222 0.00067488 0.000694069
0.000713804 0.0007341 0.000754972 0.000776439 0.000798515 0.00082122 0.00084457
0.000868583 0.00089328 0.000918679 0.0009448 0.000971663 0.000999291 0.0010277 0.00105692
0.00108698 0.00111788 0.00114967 0.00118236 0.00121597 0.00125055 0.00128611 0.00132267
0.00136028 0.00139896 0.00143874 0.00147964 0.00152172 0.00156498 0.00160948 0.00165524
0.00170231 0.00175071 0.00180049 0.00185168 0.00190433 0.00195848 0.00201416 0.00207143
0.00213033 0.0021909 0.00225319 0.00231726 0.00238315 0.00245091 0.00252059 0.00259226
0.00266597 0.00274177 0.00281973 0.0028999 0.00298236 0.00306715 0.00315436 0.00324405
0.00333629 0.00343115 0.00352871 0.00362904 0.00373223 0.00383835 0.00394748 0.00405972
0.00417516 0.00429387 0.00441596 0.00454152 0.00467065 0.00480345 0.00494003 0.00508049
0.00522494 0.0053735 0.00552629 0.00568342 0.00584502 0.00601121 0.00618213 0.0063579
0.00653868 0.0067246 0.0069158 0.00711244 0.00731466 0.00752264 0.00773654 0.00795651
0.00818274 0.0084154 0.00865468 0.00890076 0.00915384 0.00941411 0.00968178 0.00995707
0.0102402 0.0105313 0.0108308 0.0111387 0.0114554 0.0117812 0.0121161 0.0124606 0.0128149
0.0131793 0.013554 0.0139394 0.0143358 0.0147434 0.0151626 0.0155937 0.0160371 0.0164931
0.016962 0.0174443 0.0179403 0.0184504 0.018975 0.0195145 0.0200694 0.02064 0.0212269
0.0218304 0.0224511 0.0230895 0.023746 0.0244212 0.0251155 0.0258297 0.0265641 0.0273194
0.0280962 0.028895 0.0297166 0.0305615 0.0314305 0.0323242 0.0332432 0.0341885 0.0351605
0.0361603 0.0371884 0.0382458 0.0393333 0.0404516 0.0416018 0.0427847 0.0440012 0.0452523
0.0465389 0.0478622 0.0492231 0.0506226 0.052062 0.0535423 0.0550647 0.0566303 0.0582405
0.0598965 0.0615995 0.063351 0.0651523 0.0670048 0.0689099 0.0708692 0.0728843 0.0749566
0.0770879 0.0792797 0.0815339 0.0838522 0.0862364 0.0886883 0.09121 0.0938034 0.0964706
0.0992135 0.102034 0.104936 0.107919 0.110988 0.114144 0.117389 0.120727 0.124159 0.12769
0.13132 0.135054 0.138894 0.142843 0.146905 0.151082 0.155378 0.159795 0.164339 0.169012
0.173817 0.178759 0.183842 0.189069 0.194445 0.199974 0.20566 0.211507 0.217521 0.223706
0.230067 0.236608 0.243336 0.250255 0.25737 0.264688 0.272214 0.279954 0.287914 0.2961
0.304519 0.313178 0.322082 0.33124 0.340658 0.350344 0.360306 0.37055 0.381086 0.391922
0.403065 0.414526 0.426312 0.438434 0.4509 0.46372 0.476905 0.490465 0.504411 0.518753
0.533502 0.548672 0.564272 0.580316 0.596816 0.613786 0.631238 0.649186 0.667644 0.686628
0.706151 0.726229 0.746878 0.768114 0.789954 0.812415 0.835514 0.859271 0.883702 0.908829
0.93467 0.961245 0.988577 1.01669 1.04559 1.07532 1.1059 1.13734 1.16968 1.20294 1.23714
1.27232 1.30849 1.3457 1.38396 1.42331 1.46378 1.5054 1.5482 1.59222 1.6375 1.68405
1.73194 1.78118 1.83183 1.88391 1.93748 1.99257 2.04922 2.10749 2.16741 2.22904 2.29241
2.3576 2.42463 2.49357 2.56447 2.63739 2.71237 2.7895 2.86881 2.95038 3.03427 3.12054
3.20927 3.30052 3.39436 3.49088 3.59013 3.69221 3.79719 3.90516 4.0162 4.13039 4.24783
4.36861 4.49282 4.62057 4.75195 4.88706 5.02601 5.16892 5.31589 5.46704 5.62248 5.78235
5.94676 6.11584 6.28974 6.46857 6.6525 6.84165 7.03618 7.23624 7.44199 7.65359 7.8712
8.09501 8.32517 8.56189 8.80533 9.05569 9.31317 9.57798 9.85031 10.1304 10.4184 10.7147
11.0193 11.3326 11.6548 11.9862 12.327 12.6775 13.038 13.4087 13.79 14.1821 14.5853 15
% Core spectrum
det core_spectrum de 1000
