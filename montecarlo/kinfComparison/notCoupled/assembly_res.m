
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Jan 25 2024 16:02:48' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'assembly' ;
WORKING_DIRECTORY         (idx, [1:  93]) = '/home/grads/z/zhughes/space-nuclear-thermal-propulsion-ludewig/montecarlo/serpent/hexAssembly' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-mj0jdqvh.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  36]) = '12th Gen Intel(R) Core(TM) i7-12700T' ;
CPU_MHZ                   (idx, 1)        = 50.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Mar 27 11:03:35 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Mar 27 11:05:58 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 150 ;
SKIP                      (idx, 1)        = 50 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711555415988 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 1 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
SHARE_BUF_ARRAY           (idx, 1)        = 1 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  54]) = '/home/grads/z/zhughes/Serpent2/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  9.23449E-02 0.00043  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.07655E-01 4.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.26154E-01 0.00015  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.23766E-01 0.00015  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  3.64010E+00 0.00043  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  7.59477E-01 6.0E-05  2.37147E-01 0.00019  3.37601E-03 0.00086  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.31919E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.31919E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.64845E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  5.60647E+00 0.00056  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 150 ;
SIMULATED_HISTORIES       (idx, 1)        = 1500136 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00009E+04 0.00090 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00009E+04 0.00090 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.36876E+00 ;
RUNNING_TIME              (idx, 1)        =  2.37138E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  2.26800E-01  2.26800E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.85000E-03  1.85000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.14262E+00  2.14262E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.37123E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.99889 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  9.99960E-01 5.2E-05 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.00431E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31765.09 ;
ALLOC_MEMSIZE             (idx, 1)        = 1408.08 ;
MEMSIZE                   (idx, 1)        = 1354.15 ;
XS_MEMSIZE                (idx, 1)        = 1117.70 ;
MAT_MEMSIZE               (idx, 1)        = 169.42 ;
RES_MEMSIZE               (idx, 1)        = 0.12 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 66.90 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 53.92 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 15 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 443739 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 19 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 47 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 47 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 1506 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 1 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.20562E+14 0.00064  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.72032E-03 0.01318 ];
U235_FISS                 (idx, [1:   4]) = [  8.35539E+17 0.00071  9.99898E-01 9.5E-06 ];
U238_FISS                 (idx, [1:   4]) = [  8.51162E+13 0.09343  1.01915E-04 0.09339 ];
U235_CAPT                 (idx, [1:   4]) = [  1.69888E+17 0.00215  4.47797E-01 0.00160 ];
U238_CAPT                 (idx, [1:   4]) = [  4.72595E+15 0.01316  1.24545E-02 0.01297 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 1500136 1.50000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 1.16948E+04 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 1500136 1.51169E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 468444 4.72014E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1031692 1.03968E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 1500136 1.51169E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.00117E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  2.70707E+07 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  2.03600E+18 1.2E-06 ];
TOT_FISSRATE              (idx, [1:   2]) = [  8.35327E+17 5.9E-09 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  3.78889E+17 0.00047 ];
TOT_ABSRATE               (idx, [1:   2]) = [  1.21422E+18 0.00015 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.20562E+18 0.00064 ];
TOT_FLUX                  (idx, [1:   2]) = [  9.65253E+19 0.00059 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.21422E+18 0.00015 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.24582E+19 0.00060 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05449E+00 0.00043 ];
SIX_FF_F                  (idx, [1:   2]) = [  8.35292E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  8.98616E-01 0.00027 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09550E+00 0.00026 ];
SIX_FF_LF                 (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
SIX_FF_LT                 (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.68937E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.68937E+00 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43737E+00 1.2E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02270E+02 5.8E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.68962E+00 0.00062  1.67842E+00 0.00055  1.09538E-02 0.01159 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.68989E+00 0.00015 ];
COL_KEFF                  (idx, [1:   2]) = [  1.68887E+00 0.00064 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.68989E+00 0.00015 ];
ABS_KINF                  (idx, [1:   2]) = [  1.68989E+00 0.00015 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.88127E+01 0.00012 ];
IMP_ALF                   (idx, [1:   2]) = [  1.88099E+01 5.2E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  1.35182E-07 0.00220 ];
IMP_EALF                  (idx, [1:   2]) = [  1.35529E-07 0.00097 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.29284E-02 0.01459 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.32380E-02 0.00196 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  3.95330E-03 0.00986  1.22404E-04 0.05033  6.27121E-04 0.02476  3.66021E-04 0.03563  7.81448E-04 0.02221  1.30809E-03 0.01756  3.48770E-04 0.03088  3.12095E-04 0.03592  8.73507E-05 0.06522 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  3.98916E-01 0.01682  1.24667E-02 0.0E+00  2.82917E-02 4.2E-09  4.25244E-02 7.9E-09  1.33042E-01 5.0E-09  2.92467E-01 4.6E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  6.50041E-03 0.01262  1.97306E-04 0.06974  1.02397E-03 0.03217  5.96583E-04 0.04021  1.30309E-03 0.02859  2.17109E-03 0.02165  5.61203E-04 0.04133  4.98156E-04 0.04859  1.49015E-04 0.08433 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  3.97677E-01 0.02297  1.24667E-02 0.0E+00  2.82917E-02 4.1E-09  4.25244E-02 7.8E-09  1.33042E-01 4.9E-09  2.92467E-01 4.6E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  4.15905E-05 0.00144  4.15882E-05 0.00143  4.19416E-05 0.01461 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  7.02686E-05 0.00131  7.02646E-05 0.00131  7.08611E-05 0.01461 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  6.43323E-03 0.01260  1.90840E-04 0.07296  1.02002E-03 0.03260  5.69304E-04 0.04443  1.27688E-03 0.02971  2.16191E-03 0.02038  5.77330E-04 0.04314  5.01609E-04 0.04675  1.35331E-04 0.08584 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  3.96418E-01 0.02084  1.24667E-02 0.0E+00  2.82917E-02 4.1E-09  4.25244E-02 7.9E-09  1.33042E-01 5.0E-09  2.92467E-01 4.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.1E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  4.15886E-05 0.00258  4.15827E-05 0.00261  4.19343E-05 0.04064 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  7.02654E-05 0.00252  7.02556E-05 0.00256  7.08255E-05 0.04055 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.59459E-03 0.03291  1.53233E-04 0.19327  1.01667E-03 0.08559  5.77778E-04 0.11190  1.50498E-03 0.07285  2.11318E-03 0.05654  5.25015E-04 0.11568  5.41424E-04 0.11674  1.62303E-04 0.20548 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.06299E-01 0.05715  1.24667E-02 4.6E-09  2.82917E-02 0.0E+00  4.25244E-02 1.9E-09  1.33042E-01 4.6E-09  2.92467E-01 4.7E-09  6.66488E-01 4.8E-09  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.64664E-03 0.03173  1.64711E-04 0.19397  1.03115E-03 0.08167  5.89272E-04 0.10876  1.50319E-03 0.07237  2.12857E-03 0.05402  5.07129E-04 0.11320  5.61101E-04 0.10866  1.61521E-04 0.19755 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.08251E-01 0.05506  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 1.9E-09  1.33042E-01 4.6E-09  2.92467E-01 4.3E-09  6.66488E-01 5.0E-09  1.63478E+00 0.0E+00  3.55460E+00 7.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.58882E+02 0.03330 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  4.15843E-05 0.00085 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  7.02577E-05 0.00055 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.49694E-03 0.00639 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.56255E+02 0.00645 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  8.52652E-07 0.00068 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  3.58367E-06 0.00075  3.58378E-06 0.00075  3.57314E-06 0.00990 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  7.20213E-05 0.00088  7.20247E-05 0.00089  7.15728E-05 0.01017 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  8.99903E-01 0.00027  8.97547E-01 0.00028  1.51440E+00 0.01431 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.31581E+01 0.01957 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.31919E+01 0.00052  4.37878E+01 0.00058 ];

