""" 
    This is a script to iterivatly call Serpent and GeN-Foam in order to arrive at 
    a converged neutronic/thermal-hydraulic solution. This script should be run
    in the same directory as the serpent input file. 
    
    Order of Operations:
        1. run serpent
           a. have serpent print the powerDensity output into the genfoam/0/fluidRegion/ folder
           b. rename serpent output files (like .m) in case it is necessary later
           c. serpent should read the temperature form genfoam/<steadyStateTime>/fluidRegion/T 
           d. powerDensity needs to be rewritten taking volumeFraction into account (see note below)
           e. this code should append the powerDensity file with boundary conditions
        2. calculate desired inlet velocity from serpent power
           a. calculate required mass flowrate to have outletT of ~3000K
           b. calculate inlet velocity based on this flowrate
           c. update genfoam input file with this inlet velocity
        3. run genfoam until steady state is reached
           a. recalculate coolant density field based on genfoam's temperature and pressure field

    Author: Zach Hughes - zhughes@tamu.edu
    Date: Jan.30.2024

NOTE!
    - I am now imposing an inlet temperature of 81.1 K. This value was initially calculated to 
      provide a correct mass flowrate based on an imposed velocity. In the future, I will 
      either have to come up with a legitimate reason to impose temperature or velocity. This
      will likely come down to a decision on if you want to model a hot bleed or expander cycle. 

    - GeN-Foam power density reads the power density in the structure of the power generating 
      cellZone. This is an extremely confusing part of GeN-Foam, but it ends up meaning that 
      the power density must be divided by the volumeFraction given for the fuel cellZone in 
      constant/fluidRegion/phaseProperties. Similarly, the functionObject that volume integrates
      the powerDensityNeutronics file must have a scaleFactor of volumeFraction in order to 
      compensate
"""

# --- user entered information
path2serpentExe = '/home/grads/z/zhughes/Serpent2/src/sss2'
genfoamDirectory = './genfoamCase/'
serpentMasterFile = './coupledMain'
numCoresSerpent = 8
nominalDensity = 3.7 # this is the nominal density given in the solid3 card. 
volumeFraction = 0.295 # this is the volume fraction of the fuel material in the power generating cellZone **
# --- 

# --- necessary variables
exeSerpent = [path2serpentExe,serpentMasterFile,'-omp',f'{numCoresSerpent}']
cellZoneOrder = ['o1','o2','fuel','outerPropellant','o3','hotFrit','innerPropellant','o4','o5']
boundaryFieldText = """boundaryField
{
    innerPropellantOutlet
    {
        type            zeroGradient;
    }
    hotFritOutlet
    {
        type            zeroGradient;
    }
    fuelOutlet
    {
        type            zeroGradient;
    }
    coldFritOutlet
    {
        type            zeroGradient;
    }
    outerPropellantOutlet
    {
        type            zeroGradient;
    }
    outerPropellantOR
    {
        type            zeroGradient;
    }
    innerPropellantInlet
    {
        type            zeroGradient;
    }
    hotFritInlet
    {
        type            zeroGradient;
    }
    fuelInlet
    {
        type            zeroGradient;
    }
    coldFritInlet
    {
        type            zeroGradient;
    }
    outerPropellantInlet
    {
        type            zeroGradient;
    }
}"""
# ---

# --- importing libraries
import os
import subprocess
import time
import numpy as np
import shutil
# ---

# --- defining funcitons
def rmFile(filepath):
    """ this deletes filepath"""
    try:
        os.remove(filepath)
        print(f"File '{filepath}' successfully deleted.")
    except OSError as e:
        print(f"Error: {e}")
def rmFolder(folderpath):
    """ this deletes folderpath """
    try:
        shutil.rmtree(folderpath)
        print(f"Folder '{folderpath}' deleted successfully.")
    except OSError as e:
        print(f"Error: {folderpath} - {e.strerror}")
        
def perfectGas(pressure,temperature):
    """ pressure in MPA, temperature in K
        returns density in g/cc
    """
    pressure = pressure * 9.869 # MPa to atm conversion
    return 1e-3*(pressure * 2.01568)/ (0.0821 * temperature) # g/mol to kg/mol, then using L to m^3

def buildDensityFile():
    """ 
        This is a function to build a density field from the GeN-Foam output. It
        uses the pressure and temperature files and finds the density of the coolant
        assuming it is a perfect gas. Both of the frits and the fuel keep a constant
        density. 
    """
    global cellZoneOrder
    try:
        celltoregion = np.genfromtxt('cellToRegion',skip_header=1,skip_footer=25205-25203)
        p = np.genfromtxt(genfoamDirectory+'1/fluidRegion/p',skip_header=23,skip_footer=25319-25226)
        T = np.genfromtxt(genfoamDirectory+'1/fluidRegion/T',skip_header=23,skip_footer=25277-25226)
    except OSError as e:
        print(f"!!! Cannot find {e} !!! The density field will not be updated !")
        
    coolantRho = perfectGas(p*1e-6,T) # pressure to MPa
    fuelDens = (0.006171 *0.41999993) + (6.73*0.29222371) + (5.94719911*0.28777637)
    densityList = [1.85,1.85,fuelDens,0.0004922,1.85,3.365,0.01023,1.85,1.85] # from .out file

    with open(genfoamDirectory+'1/fluidRegion/density','w') as file:
        for i,region in enumerate(celltoregion):
            if i == 0: file.write(f"{len(celltoregion)} "+'\n')

            if cellZoneOrder[int(region)] == 'outerPropellant' or cellZoneOrder[int(region)] == 'innerPropellant':
                file.write(f"{coolantRho[i]/nominalDensity} "+'\n')
            else:
                file.write(f"{densityList[int(region)]/nominalDensity} "+'\n')
    return max(p), max(T)

def rewritePowerDensity():
    """
        this function rewrites the power by incorporating the volumeFraction
        of the fuel in the power generating cellZone.

        GeN-Foam wants the powerDensity in the power generating structure, and not
        the total powerDensity in teh cellZone
    """
    global genfoamDirectory,volumeFraction
    with open(genfoamDirectory+"0/fluidRegion/powerDensityNeutronics",'r') as file: # find begining/end of numbers
        lines = file.readlines()
        for i,line in enumerate(lines):
            if line.startswith('('):
                startnum = i
            elif line.startswith(')'):
                endnum = i

    for i in range(startnum+1,endnum): # looping thru all numbers and dividing by volumeFraction
        numbers = [format(float(number) / volumeFraction, '.6e') for number in lines[i].split()]
        lines[i] = ' '.join(map(str, numbers)) + '\n'

    with open(genfoamDirectory+"0/fluidRegion/powerDensityNeutronics", 'w') as file: # rewriting
        file.writelines(lines)

def runSerpent():
    """ a single function to run serpent 
        - after this function is run the genfoam case will
          be ready to be run with the power results
    """
    global exeSerpent, genfoamDirectory, boundaryFieldText, volumeFraction
    start = time.time()
    print('Running Serpent...')
    # --- 1. running serpent
    logserpent = open('log.serpent','w')
    process = subprocess.Popen(exeSerpent,                           # run serpent 
                            stdout=logserpent,                       # log serpent output (1a.)
                            stderr=subprocess.PIPE, 
                            shell=False)
    #rmFile(genfoamDirectory+'0/fluidRegion/powerDensityNeutronics')  # delete powerDensity so serpent can write new one
    #rmFolder(genfoamDirectory+'1')
    process.wait()                                                   # wait for serpent to finish so it can write powerDens
    logserpent.close()
    # - 1d. rewrite power density taking volumeFraction into account
    rewritePowerDensity()
    # -
    # - 1e. append boundaryField
    # powerDensityFile = open(genfoamDirectory+'0/fluidRegion/powerDensityNeutronics','a')
    # powerDensityFile.write(boundaryFieldText)
    # -
    # - 1f. confirm power magnitude
    q = np.genfromtxt(genfoamDirectory+'0/fluidRegion/powerDensityNeutronics',skip_header=14,skip_footer=25272-25212)
    v = np.genfromtxt(genfoamDirectory+'constant/cellVolumes',skip_header=23,skip_footer=25287-25226)
    power = [volumeFraction*q[i]*v[i] for i in range(len(q))]
    # !!!!!!!!!!!!!!
    #       NOTE! : you can use this power to recalculate mfr and thus recalculate inlet velocity
    # !!!!!!!!!!!!!!
    print(f"Total Power = {np.sum(power):0.3e} W")
    print(f"Serpent ran in {(time.time()-start)/60:.3f} minutes")
    # - 
    # --- 
    return np.sum(power)

def runGenfoam():
    """ a single function to run genfoam
        - after this function, the coupled serpent simulation will be 
          ready to run with the temperature and density results
    """
    global genfoamDirectory
    start = time.time()
    
    print('Running GeN-Foam...')
    # --- 3. running genfoam
    #loggenfoam = open('log.genfoam','w')
    os.chdir(genfoamDirectory)
    os.rmdir("./0")
    shutil.copy("./1/","./0/")
    process = subprocess.run('GeN-Foam',                           # run genfoam
                            check=True)
    os.rmdir('1')
    os.rename('0.05','1') # ONLY BEING DONE TO SPEED UP DEVELOPMENT ---------------------------------------------------------------
    #process.wait()   
    #loggenfoam.close()
    os.chdir('./../')
    maxP,maxT = buildDensityFile()
    print(f"GeN-Foam ran in {(time.time()-start)/60:.3f} minutes")
    print(f"Results: Max. Pressure = {maxP*1e-6:0.3f} MPa, Max Temperature = {maxT:0.3f} K")
    # --- 
    
def findMFR(power:float=27.0707e6,Cp: float=16e3,deltaT: float=3100):
    """ find mass flow rate 
    power  : W    , power in assembly
    Cp     : J/kgK, specific heat
    deltaT : K    , change in coolant temperature inlet->outlet
    """
    return power/(Cp*deltaT)

def inletVelocity(power:float=27.0707e6,temperature: float=200.0):
    """finds the desired inlet velocity based on serpent power

    Args:
        power       (float): W, power in assembly
        temperature (float): K, inlet temperature 
    """
    iR, oR = 2.382e-2, 2.682e-2 # innerRadius, outerRadius - from Cristian's SNTP ppt
    inletArea = 3.14159 * (oR**2 - iR**2)
    rho = perfectGas(9.1,temperature)*1e3 # 8.1MPa is roughly the inlet pressure I am seeing, g/cc -> kg/m^3
    massFlowrate = findMFR(power)         # using 18e3 as known avg Cp with goal of 2800K outletT
    inletVelocity = -1*(massFlowrate / (rho*inletArea)) # negative bc I want in -x direction
    return inletVelocity

def updateVelocity(power:float=27.0707e6):
    """ goes into the 0/fluidRegion/U file and changes inlet and internal
        velocities in order to satisfy the desired mass flow rate that the
        Serpent-calculated power requires
    
    Args:
        power       (float): W, serpent calculated power 
    """
    global genfoamDirectory
    Unew = inletVelocity(power)
    exec1 = ['foamDictionary',genfoamDirectory+'0/fluidRegion/U','-entry','internalField','-set',f"uniform ({Unew:0.3f} 0 0)"]
    exec2 = ['foamDictionary',genfoamDirectory+'0/fluidRegion/U','-entry','boundaryField.outerPropellantInlet.value','-set',f"uniform ({Unew:0.3f} 0 0)"]
    try:
        result1 = subprocess.run(exec1, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        print(f"Internal velocity field successfully updated to {Unew:0.2f} m/s")
    except Exception as e:
        print("!!!!!! The Internal velocity field  did not get updated !!!!!!")
    try:
        result2 = subprocess.run(exec2, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        print(f"Inlet velocity successfully updated to {Unew:0.2f} m/s")
    except Exception as e:
        print("!!!!!! The inlet velocity did not get updated !!!!!!")                
# --- 
# x=buildDensityFile()
# updateVelocity()

if __name__ == "__main__":
#if False: 
    for iter in range(3):
        start = time.time()
        
        # 1. running serpent
        Q = runSerpent()
        #os.rename(genfoamDirectory+'1/',genfoamDirectory+f'1.iter{iter}') # change folder name in case I want to see how iterations progress
        # -
        
        # 2. updating inlet velocity
        updateVelocity(Q)
        # -
        
        # 3. running genfoam
        runGenfoam()
        # -
        print(f"Iteration {int(iter+1)} took {(time.time()-start)/60:.3f} minutes")
        
