
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Jan 25 2024 16:02:48' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  13]) = './coupledMain' ;
WORKING_DIRECTORY         (idx, [1:  96]) = '/home/grads/z/zhughes/space-nuclear-thermal-propulsion-ludewig/montecarlo/kinfComparison/coupled' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-mj0jdqvh.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  36]) = '12th Gen Intel(R) Core(TM) i7-12700T' ;
CPU_MHZ                   (idx, 1)        = 50.0 ;
START_DATE                (idx, [1:  24]) = 'Mon Apr  1 13:48:50 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Mon Apr  1 13:51:47 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711997330051 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  9.96879E-01  9.93343E-01  1.00534E+00  1.00493E+00  1.00236E+00  9.99703E-01  9.94865E-01  1.00259E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  54]) = '/home/grads/z/zhughes/Serpent2/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  3.10793E-01 0.00085  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  6.89207E-01 0.00038  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  5.93088E-01 0.00017  0.00000E+00 0.0E+00 ];
IFC_COL_EFF               (idx, [1:   4]) = [  8.67452E-01 0.00016  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  8.97366E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_SAMPLING_EFF          (idx, [1:   2]) = [  5.52179E-01 0.00050 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.14382E-01 0.00021  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  6.06254E+00 0.00163  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  8.97528E-01 4.1E-05  1.01846E-01 0.00036  6.26717E-04 0.00203  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_FAIL_STAT             (idx, [1:   6]) = [  8.51803E-08 1.00000  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.33633E+01 0.00065  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33633E+01 0.00065  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.64413E+01 0.00076  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.99397E+01 0.00123  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 999965 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99965E+03 0.00100 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99965E+03 0.00100 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.19426E+01 ;
RUNNING_TIME              (idx, 1)        =  2.96137E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  5.17583E-01  5.17583E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.51666E-03  1.51666E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.44223E+00  2.44223E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.96122E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.40961 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99874E+00 0.00011 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  8.22593E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31765.09 ;
ALLOC_MEMSIZE             (idx, 1)        = 2273.66 ;
MEMSIZE                   (idx, 1)        = 1864.07 ;
XS_MEMSIZE                (idx, 1)        = 1587.32 ;
MAT_MEMSIZE               (idx, 1)        = 157.64 ;
RES_MEMSIZE               (idx, 1)        = 0.29 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 118.82 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 409.59 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 10 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 859346 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 11 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 29 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 29 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 940 ;
TOT_TRANSMU_REA           (idx, 1)        = 169 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 1 ;
TMS_MODE                  (idx, 1)        = 2 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.22077E+14 0.00101  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.44302E-03 0.01595 ];
U235_FISS                 (idx, [1:   4]) = [  8.34881E+17 0.00099  9.99892E-01 1.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  9.05628E+13 0.11496  1.08145E-04 0.11459 ];
U235_CAPT                 (idx, [1:   4]) = [  1.68431E+17 0.00270  4.18286E-01 0.00182 ];
U238_CAPT                 (idx, [1:   4]) = [  4.44390E+15 0.01608  1.10355E-02 0.01592 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 999965 1.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 1.38156E+04 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 999965 1.01382E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 325424 3.29829E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 674541 6.83987E+05 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 999965 1.01382E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -9.08040E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  2.70707E+07 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.19225E+02 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  2.03594E+18 1.5E-06 ];
TOT_FISSRATE              (idx, [1:   2]) = [  8.35327E+17 7.1E-09 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  4.02819E+17 0.00057 ];
TOT_ABSRATE               (idx, [1:   2]) = [  1.23815E+18 0.00019 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.22077E+18 0.00101 ];
TOT_FLUX                  (idx, [1:   2]) = [  9.78754E+19 0.00082 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.23815E+18 0.00019 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.72315E+19 0.00078 ];
INI_FMASS                 (idx, 1)        =  2.27056E-01 ;
TOT_FMASS                 (idx, 1)        =  2.27056E-01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05459E+00 0.00048 ];
SIX_FF_F                  (idx, [1:   2]) = [  8.18515E-01 0.00044 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.07357E-01 0.00037 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09251E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
SIX_FF_LT                 (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.66706E+00 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.66706E+00 0.00064 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43729E+00 1.5E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02270E+02 7.0E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.66787E+00 0.00076  1.65627E+00 0.00065  1.07865E-02 0.01407 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.66705E+00 0.00019 ];
COL_KEFF                  (idx, [1:   2]) = [  1.66792E+00 0.00101 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.66705E+00 0.00019 ];
ABS_KINF                  (idx, [1:   2]) = [  1.66705E+00 0.00019 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.88140E+01 0.00015 ];
IMP_ALF                   (idx, [1:   2]) = [  1.88124E+01 7.6E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  1.35013E-07 0.00279 ];
IMP_EALF                  (idx, [1:   2]) = [  1.35197E-07 0.00143 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.22383E-02 0.01946 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.24071E-02 0.00276 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  4.08777E-03 0.01148  1.27723E-04 0.07685  6.21169E-04 0.03082  3.69898E-04 0.03921  7.97242E-04 0.02502  1.37370E-03 0.02097  3.80259E-04 0.03935  3.35687E-04 0.04580  8.20966E-05 0.09379 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.00309E-01 0.02072  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.7E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 2.6E-09  1.63478E+00 0.0E+00  3.55460E+00 6.1E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  6.61869E-03 0.01710  1.97204E-04 0.11475  9.84330E-04 0.04398  5.52251E-04 0.05645  1.34427E-03 0.03509  2.23780E-03 0.02660  6.17852E-04 0.05707  5.54171E-04 0.06392  1.30822E-04 0.11808 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.03713E-01 0.02721  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.7E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.1E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  4.49806E-05 0.00159  4.49807E-05 0.00160  4.51683E-05 0.01707 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  7.50188E-05 0.00151  7.50191E-05 0.00152  7.53346E-05 0.01707 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  6.48541E-03 0.01473  2.10219E-04 0.09654  9.94544E-04 0.03720  5.73126E-04 0.04767  1.29938E-03 0.03308  2.20417E-03 0.02471  5.67440E-04 0.05449  5.33912E-04 0.06170  1.02622E-04 0.13451 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  3.80271E-01 0.02318  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.7E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  4.02402E-05 0.03368  4.02528E-05 0.03367  3.79377E-05 0.05768 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  6.70732E-05 0.03368  6.70943E-05 0.03367  6.32261E-05 0.05767 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  5.89864E-03 0.05426  1.85736E-04 0.27342  8.93581E-04 0.11822  4.51716E-04 0.13900  1.32333E-03 0.10652  1.82023E-03 0.09091  6.40065E-04 0.14731  4.99716E-04 0.15841  8.42733E-05 0.30194 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.93661E-01 0.06191  1.24667E-02 5.4E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 4.8E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  5.80281E-03 0.05226  1.89893E-04 0.25481  8.38510E-04 0.11620  4.27213E-04 0.13606  1.28686E-03 0.09744  1.86580E-03 0.08899  6.35887E-04 0.14071  4.59684E-04 0.15334  9.89616E-05 0.28893 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.96619E-01 0.05817  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 4.8E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.46971E+02 0.04337 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  4.44695E-05 0.00570 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  7.41592E-05 0.00554 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.26320E-03 0.01191 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.40613E+02 0.00968 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  9.14595E-07 0.00073 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  3.46648E-06 0.00105  3.46641E-06 0.00105  3.48381E-06 0.01113 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  7.68623E-05 0.00093  7.68571E-05 0.00094  7.75643E-05 0.01165 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  9.10337E-01 0.00036  9.08080E-01 0.00037  1.47965E+00 0.01667 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.24136E+01 0.02786 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.62661E+01 0.00064  4.69806E+01 0.00074 ];

