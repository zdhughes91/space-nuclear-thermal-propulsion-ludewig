"""

"""
fuelFraction = 0.58
lenval = 100 # placekeeper

head = """
/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2312                                  |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    arch        "LSB;label=32;scalar=64";
    class       volScalarField;
    location    "1/neutroRegion";
    object      powerDensityNeutronics;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

dimensions      [1 -1 -3 0 0 0 0];

internalField   nonuniform List<scalar> 
1
(
"""

foot = """
)
;

boundaryField
{
    ".*"
    {
        type            zeroGradient;
    }
}


// ************************************************************************* //
"""




import numpy as np
pdensFromSerpent = np.genfromtxt('./polyMeshNeutroRegion/powerDensityNeutronics',skip_header=14,skip_footer=2)
celltoregion = np.genfromtxt('./polyMeshNeutroRegion/cellToRegion',skip_header=21,skip_footer=25223-25221)
cellZoneOrder = ['outerPropellant', 'o5', 'o3', 'o1', 'o2', 'hotFrit', 'o4', 'innerPropellant', 'fuel', 'moderVol']
assemblyCells = np.genfromtxt('./polyMeshNeutroRegion/sets/region0',skip_header=21,skip_footer=12625-12623,dtype=int)

# ---
assembly15Portion = pdensFromSerpent[assemblyCells]
flPortion = pdensFromSerpent[:-41588]
thPortion = pdensFromSerpent[-41588:]

print(len(flPortion),flPortion)
print(len(thPortion),thPortion)

def writeNew(region:str,data:list):
    global head, foot,fuelFraction
    with open(f'./polyMeshNeutroRegion/powerDensityNeutronics{region}','w') as file:

        for i, pdensi in enumerate(data):
            if i == 0: 
                file.write(head)
            
            if region == 'FluidRegion':
                if celltoregion[i] == 8: # this is the fuel region
                    file.write(f'{pdensi/fuelFraction:0.6e}\n') # genfoam takes power in a strange way, necessitating this
                else:
                    file.write(f'{pdensi:0.6e}\n')
            else:
                file.write(f'{pdensi:0.6e}\n')

            if i == (len(data)-1):
                print(f'Length = {len(data)}')
                file.write(foot)

#writeNew('ThermoMechanical',thPortion)
#writeNew('FluidRegion',flPortion)
writeNew('FluidRegion',assembly15Portion)