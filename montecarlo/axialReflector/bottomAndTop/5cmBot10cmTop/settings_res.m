
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  87]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/axialReflector/bottomAndTop/5cmBot10cmTop' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu May  2 16:09:34 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu May  2 17:30:03 2024' ;

% Run parameters:

POP                       (idx, 1)        = 75000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1714684174804 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 10 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:  10]) = [  1.39128E+00  1.10340E+00  6.88882E-01  9.93897E-01  9.88401E-01  1.12197E+00  9.89671E-01  8.22021E-01  1.06102E+00  8.39458E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 1.9E-09 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  2.91199E-01 0.00059  4.46992E-01 0.00020 ];
DT_FRAC                   (idx, [1:   4]) = [  7.08801E-01 0.00024  5.53008E-01 0.00016 ];
DT_EFF                    (idx, [1:   4]) = [  2.80794E-01 0.00032  2.44190E-01 0.00011 ];
IFC_COL_EFF               (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  7.89994E-01 0.00036  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_SAMPLING_EFF          (idx, [1:   2]) = [  2.05979E-01 0.00080 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.07225E-01 0.00078  3.22048E-01 0.00012 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  8.07846E+00 0.00094  5.07950E+00 0.00020  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.35548E-01 2.3E-05  6.03799E-02 0.00033  4.07213E-03 0.00039  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_FAIL_STAT             (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.60422E+01 0.00048  2.37318E+01 0.00041 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.57989E+01 0.00048  2.26653E+01 0.00041 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.76883E+01 0.00108  4.77135E+01 0.00044 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  2.83841E+01 0.00057  4.42710E+01 0.00061 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 7500043 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  7.50004E+04 0.00065 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  7.50004E+04 0.00065 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  7.98837E+02 ;
RUNNING_TIME              (idx, 1)        =  8.04819E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  5.81233E-01  5.81233E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.03167E-02  1.03167E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.98903E+01  7.98903E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  8.04653E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 9.92567 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  9.97611E+00 0.00027 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.89605E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 6368.28 ;
MEMSIZE                   (idx, 1)        = 6259.97 ;
XS_MEMSIZE                (idx, 1)        = 4658.84 ;
MAT_MEMSIZE               (idx, 1)        = 635.24 ;
RES_MEMSIZE               (idx, 1)        = 296.53 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 669.36 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 108.32 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 59 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 819727 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 8081 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 23 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 85 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 67 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 18 ;
TOT_REA_CHANNELS          (idx, 1)        = 1692 ;
TOT_TRANSMU_REA           (idx, 1)        = 103 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 2 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.27254E+14 0.00048  9.27254E+14 0.00048 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 7241709 7.41119E+06 3.42303E+05 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 715282 7.32545E+05 3.74330E+05 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 25059372 2.56495E+07 9.05873E+05 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 29118899 2.98428E+07 3.28047E+07 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 62135262 6.36361E+07 3.44272E+07 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 54136331 5.54214E+07 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 7998931 8.21465E+06 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 62135262 6.36361E+07 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 -4.32134E-06 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  7.61699E+19 0.00050 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  5.10494E+20 0.00036 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  3.39620E+18 0.00153 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  5.90060E+20 0.00035 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  1.61546E+21 0.00036 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  8.37462E+21 0.00037 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  3.78002E+07 0.00041 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  8.14085E-10 0.00017 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.28378E-03 0.00737 ];
U235_FISS                 (idx, [1:   4]) = [  3.24165E+19 0.00040  9.99890E-01 5.6E-06 ];
U238_FISS                 (idx, [1:   4]) = [  3.52412E+15 0.05220  1.08717E-04 0.05222 ];
U235_CAPT                 (idx, [1:   4]) = [  6.36511E+18 0.00110  2.93845E-01 0.00097 ];
U238_CAPT                 (idx, [1:   4]) = [  1.65529E+17 0.00749  7.64191E-03 0.00752 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 7500043 7.50000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 2.95676E+05 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 7500043 7.79568E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 2263175 2.33612E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 3411861 3.49639E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 1825007 1.96316E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 7500043 7.79568E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 6.15604E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.00000E+09 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.14452E+02 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  7.90619E+19 3.9E-05 ];
TOT_FISSRATE              (idx, [1:   2]) = [  3.24262E+19 3.9E-05 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.16606E+19 0.00045 ];
TOT_ABSRATE               (idx, [1:   2]) = [  5.40868E+19 0.00017 ];
TOT_SRCRATE               (idx, [1:   2]) = [  6.95441E+19 0.00048 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.58256E+21 0.00047 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  2.76824E+20 5.1E-05  2.76757E+20 0.00034 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.82039E+19 0.00116 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  7.22907E+19 0.00036 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.18154E+21 0.00050 ];
INI_FMASS                 (idx, 1)        =  8.73726E+00 ;
TOT_FMASS                 (idx, 1)        =  8.73726E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06665E+00 0.00020 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.49679E-01 0.00028 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.08342E-01 0.00020 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09406E+00 0.00018 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.96592E-01 0.00024 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.26753E-01 0.00013 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.53968E+00 0.00037 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.13666E+00 0.00046 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43821E+00 7.2E-07 ];
FISSE                     (idx, [1:   2]) = [  1.92484E+02 3.9E-05 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.13683E+00 0.00047  1.12817E+00 0.00047  8.48801E-03 0.00622 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.13679E+00 0.00037 ];
COL_KEFF                  (idx, [1:   2]) = [  1.13689E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.13679E+00 0.00037 ];
ABS_KINF                  (idx, [1:   2]) = [  1.53981E+00 0.00019 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93620E+01 7.2E-05 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93625E+01 3.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.80292E-08 0.00140 ];
IMP_EALF                  (idx, [1:   2]) = [  7.79823E-08 0.00071 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.44167E-02 0.00719 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.44501E-02 0.00102 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.07936E-03 0.00514  2.06048E-04 0.02367  1.11477E-03 0.01012  1.04660E-03 0.01232  2.34619E-03 0.00770  9.67064E-04 0.01149  3.98693E-04 0.02024 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.66476E-01 0.00769  1.33360E-02 0.0E+00  3.27388E-02 4.8E-06  1.20781E-01 3.8E-06  3.02790E-01 1.1E-05  8.49490E-01 0.0E+00  2.85312E+00 3.0E-05 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.42128E-03 0.00593  2.59061E-04 0.03510  1.33398E-03 0.01472  1.30002E-03 0.01598  2.84489E-03 0.00913  1.18942E-03 0.01637  4.93908E-04 0.02967 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  4.69595E-01 0.01082  1.33360E-02 0.0E+00  3.27386E-02 1.1E-05  1.20781E-01 7.3E-06  3.02790E-01 1.5E-05  8.49490E-01 0.0E+00  2.85308E+00 3.0E-05 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  7.81742E-05 0.00098  7.81914E-05 0.00099  7.59567E-05 0.00969 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  8.88686E-05 0.00086  8.88881E-05 0.00087  8.63506E-05 0.00971 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.46739E-03 0.00645  2.60881E-04 0.03149  1.35798E-03 0.01353  1.28563E-03 0.01448  2.88343E-03 0.01073  1.17554E-03 0.01584  5.03933E-04 0.02849 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.70332E-01 0.01047  1.33360E-02 0.0E+00  3.27390E-02 6.3E-09  1.20780E-01 3.6E-06  3.02794E-01 2.1E-05  8.49490E-01 0.0E+00  2.85313E+00 4.5E-05 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  7.39303E-05 0.02317  7.39420E-05 0.02317  7.14419E-05 0.03395 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  8.40413E-05 0.02316  8.40545E-05 0.02316  8.12314E-05 0.03401 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.12095E-03 0.03271  2.42030E-04 0.12572  1.31377E-03 0.05992  1.23546E-03 0.05378  2.76050E-03 0.04229  1.09095E-03 0.05754  4.78243E-04 0.08331 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  4.60650E-01 0.02808  1.33360E-02 0.0E+00  3.27390E-02 6.1E-09  1.20782E-01 1.0E-05  3.02807E-01 8.5E-05  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.05950E-03 0.03207  2.36415E-04 0.11528  1.28969E-03 0.05584  1.23437E-03 0.05395  2.72145E-03 0.04114  1.09647E-03 0.05741  4.81106E-04 0.08380 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  4.65923E-01 0.02829  1.33360E-02 0.0E+00  3.27390E-02 6.1E-09  1.20782E-01 1.0E-05  3.02804E-01 7.4E-05  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -9.63502E+01 0.02315 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  7.79640E-05 0.00062 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  8.86298E-05 0.00042 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.54879E-03 0.00329 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -9.68325E+01 0.00350 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.07815E-06 0.00043 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.59907E-06 0.00033  4.60049E-06 0.00033  4.40672E-06 0.00498 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.13594E-04 0.00055  1.13611E-04 0.00055  1.11413E-04 0.00583 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.29042E-01 0.00026  7.28096E-01 0.00026  8.85956E-01 0.00716 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.11399E+01 0.01086 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  8.81558E+00 0.00024  4.56006E+01 0.00036 ];

