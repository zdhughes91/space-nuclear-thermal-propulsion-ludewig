"""
    A script to generate a density field for the serpent input. This 
    calculates the density field only in the outerPropellant and
    innerPropellant regions, and assumes the hydrogen is a perfect gas

    Author: Zach Hughes - zhughes@tamu.edu
    Date: Jan.31.2024
"""
def perfectGas(pressure,temperature):
    """ pressure in MPA, temperature in K
        returns density in g/cc
    """
    pressure = pressure * 9.869 # MPa to atm conversion
    return 1e-3*(pressure * 2.01568)/ (0.0821 * temperature) # g/mol to kg/mol, then using L to m^3

nominalDensity = 3.7 # what is nominal density defined as in type9.ifc

#cellZoneOrder = ['moderVol', 'outerPropellant', 'o5', 'o3', 'o1', 'o2', 'hotFrit', 'o4', 'innerPropellant' ,'fuel' ,'o5a', 'o3a' ,'o1a' ,'o2a' ,'o4a', 'o5b', 'o3b', 'o1b' ,'o2b' ,'o4b', 'o5c' ,'o3c', 'o1c','o2c', 'o4c' ,'o5d' ,'o3d', 'o1d', 'o2d', 'o4d', 'o5e', 'o3e', 'o1e' ,'o2e', 'o4e', 'o5f' ,'o3f', 'o1f', 'o2f', 'o4f' ]
cellZoneOrder = ['outerPropellant', 'o5', 'o3', 'o1', 'o2', 'hotFrit', 'o4', 'innerPropellant', 'fuel', 'moderVol', 'o5a', 'o3a' ,'o1a', 'o2a', 'o4a', 'o5b', 'o3b', 'o1b', 'o2b', 'o4b', 'o5c', 'o3c', 'o1c', 'o2c', 'o4c' ,'o5d', 'o3d', 'o1d', 'o2d' ,'o4d', 'o5e', 'o3e', 'o1e', 'o2e' ,'o4e' ,'o5f', 'o3f', 'o1f', 'o2f', 'o4f' ]
import numpy as np

celltoregion = np.genfromtxt('./polyMeshNeutroRegion/cellToRegion',skip_header=21,skip_footer=25223-25221,dtype=int)

# p = np.genfromtxt('genfoamCase/1/fluidRegion/p',skip_header=23,skip_footer=25319-25226)
# T = np.genfromtxt('genfoamCase/1/fluidRegion/T',skip_header=23,skip_footer=25319-25268)
# coolantRho = perfectGas(p*1e-6,T) # pressure to MPa
# print(coolantRho)
# print(min(coolantRho))

fuelDens = (0.006171 *0.41999993) + (6.73*0.29222371) + (5.94719911*0.28777637)

#densityList = [1.85,1.85,fuelDens,0.0004922,1.85,3.365,0.01023,1.85,1.85] # from .out file
densityList = [0.0004922,1.85,1.85,1.85,1.85,3.365,1.85,0.0004922,fuelDens,0.74856442] #

with open('./polyMeshNeutroRegion/density','w') as file:
    for i,region in enumerate(celltoregion):
        if i == 0: file.write(f"{len(celltoregion)} "+'\n')

        if cellZoneOrder[int(region)] == 'outerPropellant' or cellZoneOrder[int(region)] == 'innerPropellant':
            #file.write(f"{coolantRho[i]/nominalDensity} "+'\n')
            file.write(f"{densityList[int(region)]/nominalDensity} "+'\n') # not using density fields curently
        else:
            file.write(f"{densityList[int(region)]/nominalDensity} "+'\n')




# with open('./polyMeshNeutroRegion/T','w') as file:
#     for i in range(len(celltoregion)):
#         if i == 0: file.write(f'{len(celltoregion)}\n')
#         file.write('300.0\n')