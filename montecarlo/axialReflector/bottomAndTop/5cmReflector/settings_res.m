
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  86]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/axialReflector/bottomAndTop/5cmReflector' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Tue Apr 30 19:01:37 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Tue Apr 30 19:25:31 2024' ;

% Run parameters:

POP                       (idx, 1)        = 30000 ;
CYCLES                    (idx, 1)        = 150 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1714521697393 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 10 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:  10]) = [  1.11351E+00  1.02138E+00  9.41695E-01  9.85356E-01  1.10021E+00  7.82050E-01  1.07128E+00  1.08627E+00  8.65262E-01  1.03299E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.6E-09  5.00000E-02 2.5E-09 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  2.86406E-01 0.00086  4.01250E-01 0.00027 ];
DT_FRAC                   (idx, [1:   4]) = [  7.13594E-01 0.00035  5.98750E-01 0.00018 ];
DT_EFF                    (idx, [1:   4]) = [  2.80348E-01 0.00044  2.55434E-01 0.00015 ];
IFC_COL_EFF               (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  7.70622E-01 0.00050  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_SAMPLING_EFF          (idx, [1:   2]) = [  2.56566E-01 0.00105 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.00447E-01 0.00108  3.22760E-01 0.00015 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  7.71432E+00 0.00115  4.83189E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.36038E-01 3.2E-05  5.95940E-02 0.00047  4.36794E-03 0.00050  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_FAIL_STAT             (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.61683E+01 0.00063  2.29533E+01 0.00054 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.59223E+01 0.00063  2.19382E+01 0.00055 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.75337E+01 0.00152  4.60324E+01 0.00051 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  2.68513E+01 0.00070  3.58122E+01 0.00071 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 150 ;
SIMULATED_HISTORIES       (idx, 1)        = 4499975 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  2.99998E+04 0.00093 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  2.99998E+04 0.00093 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.35232E+02 ;
RUNNING_TIME              (idx, 1)        =  2.39083E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  4.22283E-01  4.22283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.85000E-03  7.85000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.34781E+01  2.34781E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.38915E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 9.83893 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  9.96525E+00 0.00093 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78249E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 6067.88 ;
MEMSIZE                   (idx, 1)        = 5959.57 ;
XS_MEMSIZE                (idx, 1)        = 4658.84 ;
MAT_MEMSIZE               (idx, 1)        = 635.24 ;
RES_MEMSIZE               (idx, 1)        = 296.55 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 368.95 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 108.31 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 59 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 819727 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 8081 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 23 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 85 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 67 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 18 ;
TOT_REA_CHANNELS          (idx, 1)        = 1692 ;
TOT_TRANSMU_REA           (idx, 1)        = 103 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 2 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.32382E+15 0.00057  2.32382E+15 0.00057 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 4537418 4.66104E+06 2.16494E+05 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 450428 4.63174E+05 2.36681E+05 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 15242052 1.56591E+07 5.41818E+05 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 17406666 1.79034E+07 1.97117E+07 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 37636564 3.86867E+07 2.07066E+07 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 33068881 3.39792E+07 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 4567683 4.70751E+06 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 37636564 3.86867E+07 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 5.81145E-07 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  7.29272E+19 0.00057 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  5.22805E+20 0.00044 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  3.58768E+18 0.00204 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  5.99320E+20 0.00042 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  1.57297E+21 0.00045 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  7.70645E+21 0.00047 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  3.82648E+07 0.00045 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  7.51170E-10 0.00025 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.35948E-03 0.00968 ];
U235_FISS                 (idx, [1:   4]) = [  3.24118E+19 0.00047  9.99883E-01 6.8E-06 ];
U238_FISS                 (idx, [1:   4]) = [  3.78091E+15 0.05862  1.16570E-04 0.05851 ];
U235_CAPT                 (idx, [1:   4]) = [  6.35540E+18 0.00146  2.91627E-01 0.00132 ];
U238_CAPT                 (idx, [1:   4]) = [  1.68222E+17 0.00968  7.71762E-03 0.00953 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 4499975 4.50000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 1.85040E+05 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 4499975 4.68504E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 1359070 1.40675E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 2034020 2.09246E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 1106885 1.18583E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 4499975 4.68504E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.62981E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.00000E+09 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.14133E+02 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  7.90333E+19 4.7E-05 ];
TOT_FISSRATE              (idx, [1:   2]) = [  3.24148E+19 4.7E-05 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.18247E+19 0.00065 ];
TOT_ABSRATE               (idx, [1:   2]) = [  5.42395E+19 0.00025 ];
TOT_SRCRATE               (idx, [1:   2]) = [  6.97145E+19 0.00057 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.35299E+21 0.00059 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  2.77496E+20 7.0E-05  2.77393E+20 0.00040 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.83717E+19 0.00125 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  7.26112E+19 0.00042 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.12120E+21 0.00060 ];
INI_FMASS                 (idx, 1)        =  8.76172E+00 ;
TOT_FMASS                 (idx, 1)        =  8.76172E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06721E+00 0.00030 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.47478E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.09710E-01 0.00027 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09513E+00 0.00022 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.91056E-01 0.00027 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.31011E-01 0.00015 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.53939E+00 0.00050 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.13373E+00 0.00058 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43818E+00 8.6E-07 ];
FISSE                     (idx, [1:   2]) = [  1.92551E+02 4.7E-05 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.13388E+00 0.00060  1.12542E+00 0.00059  8.30708E-03 0.00793 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.13323E+00 0.00043 ];
COL_KEFF                  (idx, [1:   2]) = [  1.13373E+00 0.00060 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.13323E+00 0.00043 ];
ABS_KINF                  (idx, [1:   2]) = [  1.53847E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93537E+01 9.5E-05 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93541E+01 4.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.86956E-08 0.00185 ];
IMP_EALF                  (idx, [1:   2]) = [  7.86445E-08 0.00084 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.41945E-02 0.00969 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.42429E-02 0.00123 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.05604E-03 0.00628  2.08930E-04 0.03311  1.09447E-03 0.01422  1.03678E-03 0.01410  2.34568E-03 0.00920  9.53898E-04 0.01463  4.16272E-04 0.02365 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.73940E-01 0.00847  1.33360E-02 1.3E-09  3.27383E-02 1.2E-05  1.20781E-01 8.5E-06  3.02784E-01 8.2E-06  8.49523E-01 2.2E-05  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.29645E-03 0.00806  2.57211E-04 0.04502  1.36350E-03 0.02016  1.23140E-03 0.01991  2.81279E-03 0.01338  1.13761E-03 0.01989  4.93938E-04 0.03437 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  4.69020E-01 0.01309  1.33360E-02 1.3E-09  3.27385E-02 1.2E-05  1.20781E-01 9.2E-06  3.02788E-01 2.0E-05  8.49513E-01 1.9E-05  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  7.68031E-05 0.00116  7.68082E-05 0.00116  7.62040E-05 0.01200 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  8.70812E-05 0.00100  8.70869E-05 0.00100  8.64099E-05 0.01206 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.32640E-03 0.00829  2.58376E-04 0.04437  1.35624E-03 0.02137  1.23721E-03 0.02148  2.83209E-03 0.01346  1.14990E-03 0.01933  4.92596E-04 0.03411 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.68941E-01 0.01305  1.33360E-02 1.3E-09  3.27381E-02 2.0E-05  1.20782E-01 1.1E-05  3.02786E-01 1.4E-05  8.49526E-01 3.0E-05  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  7.36008E-05 0.01546  7.35919E-05 0.01546  7.42812E-05 0.03455 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  8.34523E-05 0.01546  8.34423E-05 0.01546  8.42070E-05 0.03449 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.94628E-03 0.03136  2.43613E-04 0.17002  1.40195E-03 0.06533  1.15347E-03 0.07330  2.66843E-03 0.04936  1.01480E-03 0.06571  4.64020E-04 0.10226 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  4.61480E-01 0.03977  1.33360E-02 0.0E+00  3.27390E-02 4.9E-09  1.20780E-01 0.0E+00  3.02780E-01 7.3E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.93032E-03 0.03118  2.50558E-04 0.16191  1.42498E-03 0.06456  1.13117E-03 0.07021  2.62423E-03 0.04710  1.02453E-03 0.06330  4.74854E-04 0.10457 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  4.63949E-01 0.03863  1.33360E-02 0.0E+00  3.27390E-02 4.9E-09  1.20780E-01 0.0E+00  3.02780E-01 7.3E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -9.45193E+01 0.02762 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  7.65914E-05 0.00085 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  8.68407E-05 0.00057 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.39209E-03 0.00471 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -9.65108E+01 0.00458 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.07226E-06 0.00050 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.41008E-06 0.00051  4.41075E-06 0.00050  4.32108E-06 0.00567 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.08737E-04 0.00065  1.08745E-04 0.00066  1.07713E-04 0.00859 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.24683E-01 0.00030  7.23812E-01 0.00031  8.72636E-01 0.00932 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.10222E+01 0.01313 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  8.58587E+00 0.00028  4.59478E+01 0.00046 ];

