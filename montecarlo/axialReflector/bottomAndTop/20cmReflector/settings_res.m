
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  87]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/axialReflector/bottomAndTop/20cmReflector' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Tue Apr 30 20:00:56 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Tue Apr 30 20:24:38 2024' ;

% Run parameters:

POP                       (idx, 1)        = 30000 ;
CYCLES                    (idx, 1)        = 150 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1714525256459 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 10 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:  10]) = [  1.19193E+00  1.04495E+00  9.31645E-01  1.28309E+00  9.05127E-01  9.06627E-01  8.24300E-01  9.11540E-01  9.11399E-01  1.08939E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  2.89851E-01 0.00073  3.99277E-01 0.00030 ];
DT_FRAC                   (idx, [1:   4]) = [  7.10149E-01 0.00030  6.00723E-01 0.00020 ];
DT_EFF                    (idx, [1:   4]) = [  2.82930E-01 0.00036  2.51900E-01 0.00014 ];
IFC_COL_EFF               (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  7.83022E-01 0.00041  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_SAMPLING_EFF          (idx, [1:   2]) = [  2.54194E-01 0.00095 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.11634E-01 0.00088  3.27741E-01 0.00014 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  7.83343E+00 0.00118  4.78308E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.36656E-01 2.7E-05  5.91015E-02 0.00040  4.24268E-03 0.00042  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_FAIL_STAT             (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  6.08468E+01 0.00055  2.52310E+01 0.00044 ];
AVG_REAL_COL              (idx, [1:   4]) = [  6.06285E+01 0.00056  2.42420E+01 0.00045 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.17226E+01 0.00127  4.97250E+01 0.00046 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  2.75726E+01 0.00075  3.70722E+01 0.00071 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 150 ;
SIMULATED_HISTORIES       (idx, 1)        = 4499955 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  2.99997E+04 0.00080 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  2.99997E+04 0.00080 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.33577E+02 ;
RUNNING_TIME              (idx, 1)        =  2.36981E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  3.52350E-01  3.52350E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  8.03333E-03  8.03333E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.33377E+01  2.33377E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.36809E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 9.85635 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  9.96614E+00 0.00090 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81006E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 6067.88 ;
MEMSIZE                   (idx, 1)        = 5959.57 ;
XS_MEMSIZE                (idx, 1)        = 4658.84 ;
MAT_MEMSIZE               (idx, 1)        = 635.24 ;
RES_MEMSIZE               (idx, 1)        = 296.55 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 368.95 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 108.31 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 59 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 819727 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 8081 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 23 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 85 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 67 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 18 ;
TOT_REA_CHANNELS          (idx, 1)        = 1692 ;
TOT_TRANSMU_REA           (idx, 1)        = 103 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 2 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.23936E+15 0.00054  2.23936E+15 0.00054 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 4674895 4.81189E+06 2.23369E+05 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 465250 4.79037E+05 2.44787E+05 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 15771058 1.62361E+07 5.59618E+05 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 17979251 1.85332E+07 2.04416E+07 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 38890454 4.00602E+07 2.14693E+07 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 34439887 3.54610E+07 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 4450566 4.59927E+06 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 1 9.91867E-01 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 38890454 4.00602E+07 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 2.60770E-07 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  6.86614E+19 0.00059 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  1.47822E+13 1.00000 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  5.25810E+20 0.00043 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  3.57565E+18 0.00212 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  5.98047E+20 0.00041 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  1.67876E+21 0.00045 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  8.15879E+21 0.00046 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  3.92373E+07 0.00047 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  7.83659E-10 0.00022 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.25027E-03 0.01022 ];
U235_FISS                 (idx, [1:   4]) = [  3.23438E+19 0.00045  9.99892E-01 7.0E-06 ];
U238_FISS                 (idx, [1:   4]) = [  3.49599E+15 0.06492  1.08019E-04 0.06486 ];
U235_CAPT                 (idx, [1:   4]) = [  6.32662E+18 0.00150  2.88664E-01 0.00130 ];
U238_CAPT                 (idx, [1:   4]) = [  1.63692E+17 0.01025  7.46649E-03 0.01003 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 4499955 4.50000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 2.00141E+05 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 4499955 4.70014E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 1415447 1.46810E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 2101825 2.16679E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 982683 1.06526E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 4499955 4.70014E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -6.51926E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.00000E+09 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.13936E+02 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  7.88017E+19 4.7E-05 ];
TOT_FISSRATE              (idx, [1:   2]) = [  3.23206E+19 4.8E-05 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.19080E+19 0.00065 ];
TOT_ABSRATE               (idx, [1:   2]) = [  5.42286E+19 0.00025 ];
TOT_SRCRATE               (idx, [1:   2]) = [  6.71809E+19 0.00054 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.59363E+21 0.00061 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  2.76514E+20 6.4E-05  2.76717E+20 0.00039 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.59039E+19 0.00130 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  7.01325E+19 0.00040 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.32639E+21 0.00064 ];
INI_FMASS                 (idx, 1)        =  8.77685E+00 ;
TOT_FMASS                 (idx, 1)        =  8.77685E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06794E+00 0.00027 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.44915E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.13687E-01 0.00025 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09279E+00 0.00021 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.24646E-01 0.00025 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.25580E-01 0.00016 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.53806E+00 0.00045 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.17396E+00 0.00051 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43812E+00 7.8E-07 ];
FISSE                     (idx, [1:   2]) = [  1.93112E+02 4.8E-05 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.17403E+00 0.00052  1.16542E+00 0.00050  8.54699E-03 0.00808 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.17371E+00 0.00041 ];
COL_KEFF                  (idx, [1:   2]) = [  1.17303E+00 0.00057 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.17371E+00 0.00041 ];
ABS_KINF                  (idx, [1:   2]) = [  1.53802E+00 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93729E+01 8.7E-05 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93705E+01 4.2E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.71937E-08 0.00168 ];
IMP_EALF                  (idx, [1:   2]) = [  7.73647E-08 0.00081 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.34477E-02 0.01074 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.36801E-02 0.00117 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.90590E-03 0.00552  2.09688E-04 0.02916  1.05722E-03 0.01415  1.02435E-03 0.01611  2.29790E-03 0.00980  9.31159E-04 0.01408  3.85589E-04 0.02069 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.65445E-01 0.00779  1.33363E-02 2.1E-05  3.27390E-02 3.7E-09  1.20780E-01 0.0E+00  3.02787E-01 1.1E-05  8.49522E-01 2.2E-05  2.85307E+00 2.5E-05 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.24390E-03 0.00862  2.59384E-04 0.04272  1.27484E-03 0.01943  1.29899E-03 0.02038  2.79526E-03 0.01357  1.13303E-03 0.01899  4.82397E-04 0.02998 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  4.66721E-01 0.01119  1.33365E-02 3.9E-05  3.27390E-02 3.7E-09  1.20780E-01 0.0E+00  3.02782E-01 6.7E-06  8.49535E-01 3.1E-05  2.85302E+00 7.4E-06 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  7.80704E-05 0.00122  7.80822E-05 0.00123  7.63333E-05 0.01249 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  9.16534E-05 0.00110  9.16672E-05 0.00112  8.96060E-05 0.01243 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.25463E-03 0.00808  2.67354E-04 0.04115  1.29739E-03 0.01860  1.29330E-03 0.01994  2.78981E-03 0.01391  1.12555E-03 0.01962  4.81219E-04 0.02950 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.65516E-01 0.01114  1.33364E-02 3.4E-05  3.27390E-02 3.7E-09  1.20780E-01 0.0E+00  3.02787E-01 1.5E-05  8.49567E-01 5.3E-05  2.85311E+00 3.8E-05 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  7.47425E-05 0.01548  7.47600E-05 0.01549  7.24894E-05 0.03590 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  8.77388E-05 0.01547  8.77592E-05 0.01548  8.51042E-05 0.03598 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.19579E-03 0.02931  2.84265E-04 0.13033  1.29052E-03 0.06415  1.36543E-03 0.05714  2.66888E-03 0.04238  1.12037E-03 0.07367  4.66326E-04 0.09352 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  4.59299E-01 0.03902  1.33360E-02 0.0E+00  3.27390E-02 4.9E-09  1.20780E-01 0.0E+00  3.02780E-01 7.3E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.21414E-03 0.02959  2.92835E-04 0.12952  1.30296E-03 0.06114  1.36206E-03 0.05555  2.68848E-03 0.04265  1.12280E-03 0.07232  4.45008E-04 0.09210 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  4.51900E-01 0.03825  1.33360E-02 0.0E+00  3.27390E-02 4.9E-09  1.20780E-01 0.0E+00  3.02780E-01 7.3E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -9.62556E+01 0.02483 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  7.76817E-05 0.00076 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  9.11967E-05 0.00053 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.47051E-03 0.00493 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -9.61753E+01 0.00496 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.12671E-06 0.00048 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.66295E-06 0.00052  4.66397E-06 0.00052  4.52091E-06 0.00538 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.18466E-04 0.00063  1.18496E-04 0.00063  1.14361E-04 0.00755 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.58897E-01 0.00031  7.57924E-01 0.00031  9.26657E-01 0.00814 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.09803E+01 0.01298 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  9.11049E+00 0.00030  4.78625E+01 0.00047 ];

