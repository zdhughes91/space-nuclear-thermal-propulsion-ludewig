
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  81]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/axialReflector/topOnly/5cmReflector' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Fri May 10 07:50:36 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Fri May 10 10:02:40 2024' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 200 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1715345436977 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.31169E+00  9.29563E-01  7.64986E-01  1.12050E+00  9.08486E-01  1.10495E+00  8.42044E-01  1.01778E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.5E-09  5.00000E-02 2.8E-09 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  2.82596E-01 0.00040  4.03793E-01 0.00016 ];
DT_FRAC                   (idx, [1:   4]) = [  7.17404E-01 0.00016  5.96207E-01 0.00011 ];
DT_EFF                    (idx, [1:   4]) = [  2.78876E-01 0.00021  2.57146E-01 6.1E-05 ];
IFC_COL_EFF               (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  7.66115E-01 0.00025  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_SAMPLING_EFF          (idx, [1:   2]) = [  2.54291E-01 0.00050 ];
TOT_COL_EFF               (idx, [1:   4]) = [  2.97195E-01 0.00053  3.23916E-01 7.0E-05 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  7.77911E+00 0.00052  4.84962E+00 0.00012  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.37851E-01 1.5E-05  5.76850E-02 0.00023  4.46438E-03 0.00025  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_FAIL_STAT             (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.43422E+01 0.00031  2.18076E+01 0.00023 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.40720E+01 0.00032  2.08056E+01 0.00023 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.53216E+01 0.00073  4.34259E+01 0.00024 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  2.54513E+01 0.00041  3.43277E+01 0.00035 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 200 ;
SIMULATED_HISTORIES       (idx, 1)        = 20000465 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00002E+05 0.00038 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00002E+05 0.00038 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  8.22346E+02 ;
RUNNING_TIME              (idx, 1)        =  1.32059E+02 ;
INIT_TIME                 (idx, [1:   2]) = [  3.76867E-01  3.76867E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.91667E-03  7.91667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.31674E+02  1.31674E+02  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.32044E+02  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 6.22710 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.91359E+00 0.00576 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.94110E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 6470.15 ;
MEMSIZE                   (idx, 1)        = 6374.28 ;
XS_MEMSIZE                (idx, 1)        = 4614.67 ;
MAT_MEMSIZE               (idx, 1)        = 635.18 ;
RES_MEMSIZE               (idx, 1)        = 294.67 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 829.76 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 95.87 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 58 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 819727 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 8081 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 23 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 85 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 67 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 18 ;
TOT_REA_CHANNELS          (idx, 1)        = 1692 ;
TOT_TRANSMU_REA           (idx, 1)        = 103 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 2 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  7.25385E+14 0.00028  7.25385E+14 0.00028 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 19533449 2.00344E+07 9.30315E+05 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 1943946 1.99455E+06 1.01921E+06 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 65259806 6.69477E+07 2.32151E+06 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 74771238 7.67877E+07 8.46578E+07 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 161508439 1.65764E+08 8.89289E+07 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 141468328 1.45148E+08 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 20040111 2.06166E+07 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 161508439 1.65764E+08 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 2.53320E-06 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  7.47741E+19 0.00031 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  5.22818E+20 0.00021 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  3.61701E+18 0.00102 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  6.01209E+20 0.00021 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  1.54995E+21 0.00021 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  7.58519E+21 0.00022 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  3.79484E+07 0.00023 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  7.38815E-10 0.00011 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.25681E-03 0.00447 ];
U235_FISS                 (idx, [1:   4]) = [  3.24734E+19 0.00025  9.99882E-01 3.7E-06 ];
U238_FISS                 (idx, [1:   4]) = [  3.80629E+15 0.03194  1.17184E-04 0.03191 ];
U235_CAPT                 (idx, [1:   4]) = [  6.37964E+18 0.00071  2.89518E-01 0.00062 ];
U238_CAPT                 (idx, [1:   4]) = [  1.64727E+17 0.00449  7.47524E-03 0.00444 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 20000465 2.00000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.56567E+05 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 20000465 2.07566E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 5877190 6.07556E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 8718857 8.95455E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 5404418 5.72645E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 20000465 2.07566E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -5.55068E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.00000E+09 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.14273E+02 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  7.91632E+19 2.0E-05 ];
TOT_FISSRATE              (idx, [1:   2]) = [  3.24672E+19 2.0E-05 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.20470E+19 0.00029 ];
TOT_ABSRATE               (idx, [1:   2]) = [  5.45142E+19 0.00012 ];
TOT_SRCRATE               (idx, [1:   2]) = [  7.25385E+19 0.00028 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.32376E+21 0.00029 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  2.78514E+20 3.5E-05  2.78541E+20 0.00020 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.07696E+19 0.00058 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  7.52838E+19 0.00020 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.11707E+21 0.00030 ];
INI_FMASS                 (idx, 1)        =  8.75100E+00 ;
TOT_FMASS                 (idx, 1)        =  8.75100E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06675E+00 0.00013 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.43898E-01 0.00017 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.08629E-01 0.00012 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09497E+00 0.00011 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.67881E-01 0.00014 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.29411E-01 7.2E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.52965E+00 0.00022 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.09167E+00 0.00026 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43825E+00 4.3E-07 ];
FISSE                     (idx, [1:   2]) = [  1.92241E+02 2.0E-05 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.09167E+00 0.00027  1.08366E+00 0.00027  8.00831E-03 0.00400 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.09134E+00 0.00021 ];
COL_KEFF                  (idx, [1:   2]) = [  1.09135E+00 0.00029 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.09134E+00 0.00021 ];
ABS_KINF                  (idx, [1:   2]) = [  1.52918E+00 0.00012 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93502E+01 5.0E-05 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93503E+01 2.1E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.89585E-08 0.00097 ];
IMP_EALF                  (idx, [1:   2]) = [  7.89405E-08 0.00042 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.48449E-02 0.00471 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.48622E-02 0.00059 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.27948E-03 0.00312  2.11261E-04 0.01661  1.14333E-03 0.00661  1.08300E-03 0.00713  2.43412E-03 0.00457  9.88678E-04 0.00742  4.19088E-04 0.01090 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.68759E-01 0.00425  1.33360E-02 6.1E-09  3.27388E-02 3.6E-06  1.20781E-01 2.2E-06  3.02788E-01 6.0E-06  8.49519E-01 1.0E-05  2.85302E+00 7.8E-06 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.34144E-03 0.00392  2.48064E-04 0.02074  1.34291E-03 0.00996  1.30245E-03 0.00976  2.81809E-03 0.00631  1.15244E-03 0.01048  4.77495E-04 0.01548 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  4.62904E-01 0.00607  1.33360E-02 6.1E-09  3.27389E-02 2.1E-06  1.20780E-01 1.1E-06  3.02789E-01 8.6E-06  8.49513E-01 1.1E-05  2.85302E+00 7.9E-06 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  7.93358E-05 0.00064  7.93412E-05 0.00064  7.86137E-05 0.00603 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  8.66076E-05 0.00058  8.66136E-05 0.00058  8.58177E-05 0.00602 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.32110E-03 0.00418  2.54354E-04 0.02092  1.34697E-03 0.01009  1.28086E-03 0.00941  2.81754E-03 0.00651  1.13844E-03 0.01076  4.82925E-04 0.01523 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.64178E-01 0.00604  1.33360E-02 6.1E-09  3.27389E-02 3.3E-06  1.20781E-01 3.2E-06  3.02790E-01 9.4E-06  8.49521E-01 1.7E-05  2.85303E+00 1.2E-05 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  7.69190E-05 0.01143  7.69113E-05 0.01143  7.73415E-05 0.01983 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  8.39706E-05 0.01143  8.39621E-05 0.01143  8.44293E-05 0.01983 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.15043E-03 0.01779  2.18600E-04 0.08023  1.34543E-03 0.03188  1.24882E-03 0.03340  2.72785E-03 0.02454  1.14522E-03 0.03652  4.64522E-04 0.05174 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  4.64734E-01 0.01899  1.33360E-02 3.9E-09  3.27389E-02 2.2E-06  1.20782E-01 1.4E-05  3.02800E-01 4.2E-05  8.49518E-01 3.3E-05  2.85300E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.14217E-03 0.01719  2.20869E-04 0.07826  1.33005E-03 0.03158  1.25165E-03 0.03293  2.73276E-03 0.02403  1.13640E-03 0.03501  4.70452E-04 0.05054 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  4.66638E-01 0.01872  1.33360E-02 3.7E-09  3.27389E-02 2.7E-06  1.20782E-01 1.4E-05  3.02800E-01 3.8E-05  8.49513E-01 2.8E-05  2.85300E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -9.30022E+01 0.01374 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  7.91566E-05 0.00037 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  8.64120E-05 0.00026 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.32129E-03 0.00219 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -9.24957E+01 0.00226 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.07522E-06 0.00027 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.25963E-06 0.00025  4.26060E-06 0.00025  4.12689E-06 0.00299 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.07538E-04 0.00035  1.07549E-04 0.00035  1.06085E-04 0.00367 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.02783E-01 0.00017  7.02078E-01 0.00017  8.15967E-01 0.00446 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.09803E+01 0.00610 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  8.47932E+00 0.00015  4.56139E+01 0.00023 ];

