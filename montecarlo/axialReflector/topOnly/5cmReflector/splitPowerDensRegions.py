"""

"""
fuelFraction = 0.58
lenval = 100 # placekeeper

head = """
/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2312                                  |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    arch        "LSB;label=32;scalar=64";
    class       volScalarField;
    location    "1/neutroRegion";
    object      powerDensityNeutronics;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

dimensions      [1 -1 -3 0 0 0 0];

internalField   nonuniform List<scalar> 
1
(
"""

foot = """
)
;

boundaryField
{
    o5Right
    {
        type            zeroGradient;
    }
    o4Right
    {
        type            zeroGradient;
    }
    innerPropellantRight
    {
        type            zeroGradient;
    }
    hotFritRight
    {
        type            zeroGradient;
    }
    o3Right
    {
        type            zeroGradient;
    }
    outerPropellantRight
    {
        type            zeroGradient;
    }
    fuelRight
    {
        type            zeroGradient;
    }
    o2Right
    {
        type            zeroGradient;
    }
    o1Right
    {
        type            zeroGradient;
    }
    o5Left
    {
        type            zeroGradient;
    }
    o4Left
    {
        type            zeroGradient;
    }
    innerPropellantLeft
    {
        type            zeroGradient;
    }
    hotFritLeft
    {
        type            zeroGradient;
    }
    o3Left
    {
        type            zeroGradient;
    }
    outerPropellantLeft
    {
        type            zeroGradient;
    }
    fuelLeft
    {
        type            zeroGradient;
    }
    o2Left
    {
        type            zeroGradient;
    }
    o1Left
    {
        type            zeroGradient;
    }
    innerPropellantOutlet15
    {
        type            zeroGradient;
    }
    hotFritOutlet
    {
        type            zeroGradient;
    }
    fuelOutlet
    {
        type            zeroGradient;
    }
    coldFritOutlet
    {
        type            zeroGradient;
    }
    outerPropellantOutlet
    {
        type            zeroGradient;
    }
    innerPropellantInlet
    {
        type            zeroGradient;
    }
    hotFritInlet
    {
        type            zeroGradient;
    }
    fuelInlet
    {
        type            zeroGradient;
    }
    coldFritInlet
    {
        type            zeroGradient;
    }
    outerPropellantInlet15
    {
        type            zeroGradient;
    }
    outerPropellantOR15
    {
        type            zeroGradient;
    }
    innerPropellantOutlet16
    {
        type            zeroGradient;
    }
    outerPropellantInlet16
    {
        type            zeroGradient;
    }
    outerPropellantOR16
    {
        type            zeroGradient;
    }
    innerPropellantOutlet17
    {
        type            zeroGradient;
    }
    outerPropellantInlet17
    {
        type            zeroGradient;
    }
    outerPropellantOR17
    {
        type            zeroGradient;
    }
    innerPropellantOutlet18
    {
        type            zeroGradient;
    }
    outerPropellantInlet18
    {
        type            zeroGradient;
    }
    outerPropellantOR18
    {
        type            zeroGradient;
    }
    innerPropellantOutlet22
    {
        type            zeroGradient;
    }
    outerPropellantOR22
    {
        type            zeroGradient;
    }
    outerPropellantInlet22
    {
        type            zeroGradient;
    }
    innerPropellantOutlet23
    {
        type            zeroGradient;
    }
    outerPropellantInlet23
    {
        type            zeroGradient;
    }
    outerPropellantOR23
    {
        type            zeroGradient;
    }
    outside
    {
        type            zeroGradient;
    }
    top
    {
        type            zeroGradient;
    }
    bottom
    {
        type            zeroGradient;
    }
    ifc18
    {
        type            zeroGradient;
    }
    ifc23
    {
        type            zeroGradient;
    }
    ifc22
    {
        type            zeroGradient;
    }
    ifc17
    {
        type            zeroGradient;
    }
    ifc16
    {
        type            zeroGradient;
    }
    ifc15
    {
        type            zeroGradient;
    }
    zeroGrad
    {
        type            zeroGradient;
    }
}


// ************************************************************************* //
"""




import numpy as np
pdensFromSerpent = np.genfromtxt('./polyMeshNeutroRegion/powerDensityNeutronics',skip_header=14,skip_footer=2)
celltoregion = np.genfromtxt('./polyMeshNeutroRegion/cellToRegion',skip_header=21,skip_footer=25223-25221)
cellZoneOrder = ['outerPropellant', 'o5', 'o3', 'o1', 'o2', 'hotFrit', 'o4', 'innerPropellant', 'fuel', 'moderVol']

# ---

flPortion = pdensFromSerpent[:-41588]
thPortion = pdensFromSerpent[-41588:]

print(len(flPortion),flPortion)
print(len(thPortion),thPortion)

def writeNew(region:str,data:list):
    global head, foot,fuelFraction
    with open(f'./polyMeshNeutroRegion/powerDensityNeutronics{region}','w') as file:

        for i, pdensi in enumerate(data):
            if i == 0: 
                file.write(head)
            
            if region == 'FluidRegion':
                if celltoregion[i] == 8: # this is the fuel region
                    file.write(f'{pdensi/fuelFraction:0.6e}\n') # genfoam takes power in a strange way, necessitating this
                else:
                    file.write(f'{pdensi:0.6e}\n')
            else:
                file.write(f'{pdensi:0.6e}\n')

            if i == (len(data)-1):
                print(f'Length = {len(data)}')
                file.write(foot)

#writeNew('ThermoMechanical',thPortion)
writeNew('FluidRegion',flPortion)