
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  82]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/axialReflector/topOnly/20cmReflector' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Fri May 10 14:05:48 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Sat May 11 12:12:56 2024' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 200 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1715367948664 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.31193E+00  9.92711E-01  9.43992E-01  9.69611E-01  9.48138E-01  9.42728E-01  8.89607E-01  1.00128E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.6E-09  5.00000E-02 1.3E-09 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  2.94791E-01 0.00039  4.04063E-01 0.00013 ];
DT_FRAC                   (idx, [1:   4]) = [  7.05209E-01 0.00016  5.95937E-01 8.9E-05 ];
DT_EFF                    (idx, [1:   4]) = [  2.80457E-01 0.00019  2.51537E-01 6.4E-05 ];
IFC_COL_EFF               (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  7.89640E-01 0.00021  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_SAMPLING_EFF          (idx, [1:   2]) = [  2.47079E-01 0.00049 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.21726E-01 0.00047  3.38101E-01 7.1E-05 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  7.86998E+00 0.00056  4.76425E+00 0.00010  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.39747E-01 1.4E-05  5.59822E-02 0.00022  4.27047E-03 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_FAIL_STAT             (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  6.35838E+01 0.00033  2.55636E+01 0.00026 ];
AVG_REAL_COL              (idx, [1:   4]) = [  6.33519E+01 0.00033  2.46281E+01 0.00026 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.21435E+01 0.00069  4.82143E+01 0.00025 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  2.59639E+01 0.00040  3.56142E+01 0.00034 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 200 ;
SIMULATED_HISTORIES       (idx, 1)        = 20001332 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00007E+05 0.00043 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00007E+05 0.00043 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.12183E+03 ;
RUNNING_TIME              (idx, 1)        =  1.32713E+03 ;
INIT_TIME                 (idx, [1:   2]) = [  5.12883E-01  5.12883E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.70000E-03  7.70000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.32661E+03  1.32661E+03  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.32711E+03  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.84531 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.81408E+00 0.00998 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.99209E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 6470.15 ;
MEMSIZE                   (idx, 1)        = 6374.28 ;
XS_MEMSIZE                (idx, 1)        = 4614.67 ;
MAT_MEMSIZE               (idx, 1)        = 635.18 ;
RES_MEMSIZE               (idx, 1)        = 294.67 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 829.76 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 95.87 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 58 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 819727 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 8081 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 23 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 85 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 67 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 18 ;
TOT_REA_CHANNELS          (idx, 1)        = 1692 ;
TOT_TRANSMU_REA           (idx, 1)        = 103 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 2 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  6.92615E+14 0.00029  6.92615E+14 0.00029 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 20299661 2.08765E+07 9.73187E+05 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 2038800 2.09804E+06 1.07209E+06 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 67772064 6.97017E+07 2.40616E+06 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 77841329 8.01652E+07 8.91054E+07 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 167951854 1.72841E+08 9.35568E+07 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 149239305 1.53525E+08 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 18712548 1.93160E+07 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 1 9.87547E-01 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 167951854 1.72841E+08 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 1.04904E-05 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  6.68922E+19 0.00028 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  3.40569E+12 1.00000 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  5.28031E+20 0.00021 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  3.63279E+18 0.00100 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  5.98556E+20 0.00020 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  1.75767E+21 0.00024 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  8.42265E+21 0.00023 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  3.96012E+07 0.00022 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  7.93779E-10 0.00012 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.10421E-03 0.00462 ];
U235_FISS                 (idx, [1:   4]) = [  3.23253E+19 0.00025  9.99889E-01 3.2E-06 ];
U238_FISS                 (idx, [1:   4]) = [  3.58806E+15 0.02919  1.10989E-04 0.02919 ];
U235_CAPT                 (idx, [1:   4]) = [  6.30809E+18 0.00077  2.78911E-01 0.00070 ];
U238_CAPT                 (idx, [1:   4]) = [  1.57988E+17 0.00464  6.98500E-03 0.00457 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 20001332 2.00000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 8.28055E+05 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 20001332 2.08281E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 6298294 6.53094E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 9064736 9.33543E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 4638302 4.96169E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 20001332 2.08281E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.35228E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.00000E+09 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.13873E+02 5.9E-09 ];
TOT_GENRATE               (idx, [1:   2]) = [  7.88363E+19 2.3E-05 ];
TOT_FISSRATE              (idx, [1:   2]) = [  3.23342E+19 2.3E-05 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.26151E+19 0.00032 ];
TOT_ABSRATE               (idx, [1:   2]) = [  5.49493E+19 0.00013 ];
TOT_SRCRATE               (idx, [1:   2]) = [  6.92615E+19 0.00029 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.88796E+21 0.00033 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  2.77709E+20 3.6E-05  2.77656E+20 0.00019 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.71829E+19 0.00063 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  7.21322E+19 0.00021 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.62346E+21 0.00036 ];
INI_FMASS                 (idx, 1)        =  8.78173E+00 ;
TOT_FMASS                 (idx, 1)        =  8.78173E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06710E+00 0.00014 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.33072E-01 0.00019 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.16137E-01 0.00012 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09026E+00 0.00010 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.17626E-01 0.00012 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.19633E-01 7.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.51355E+00 0.00025 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.13807E+00 0.00030 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43817E+00 4.3E-07 ];
FISSE                     (idx, [1:   2]) = [  1.93031E+02 2.3E-05 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.13807E+00 0.00030  1.12982E+00 0.00030  8.24951E-03 0.00361 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.13818E+00 0.00021 ];
COL_KEFF                  (idx, [1:   2]) = [  1.13826E+00 0.00030 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.13818E+00 0.00021 ];
ABS_KINF                  (idx, [1:   2]) = [  1.51367E+00 0.00014 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93867E+01 4.5E-05 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93850E+01 2.1E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.61216E-08 0.00087 ];
IMP_EALF                  (idx, [1:   2]) = [  7.62477E-08 0.00040 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.40160E-02 0.00477 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.40796E-02 0.00063 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.04517E-03 0.00280  2.13243E-04 0.01565  1.09263E-03 0.00701  1.03585E-03 0.00712  2.33616E-03 0.00411  9.68837E-04 0.00742  3.98441E-04 0.01203 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.68103E-01 0.00422  1.33360E-02 6.1E-09  3.27389E-02 2.5E-06  1.20781E-01 2.2E-06  3.02786E-01 5.1E-06  8.49530E-01 1.1E-05  2.85312E+00 1.7E-05 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.22182E-03 0.00401  2.61984E-04 0.01999  1.31302E-03 0.00922  1.24420E-03 0.00945  2.78030E-03 0.00644  1.14306E-03 0.01012  4.79263E-04 0.01678 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  4.67219E-01 0.00591  1.33360E-02 6.1E-09  3.27389E-02 3.4E-06  1.20781E-01 3.1E-06  3.02786E-01 7.0E-06  8.49527E-01 1.8E-05  2.85307E+00 1.4E-05 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  8.77439E-05 0.00066  8.77681E-05 0.00066  8.44819E-05 0.00718 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  9.98571E-05 0.00058  9.98846E-05 0.00059  9.61425E-05 0.00715 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.24329E-03 0.00381  2.57935E-04 0.02172  1.32290E-03 0.00878  1.24895E-03 0.01018  2.78453E-03 0.00629  1.15545E-03 0.01021  4.73533E-04 0.01657 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.65571E-01 0.00591  1.33360E-02 6.1E-09  3.27389E-02 3.0E-06  1.20781E-01 6.0E-06  3.02785E-01 6.8E-06  8.49536E-01 1.7E-05  2.85307E+00 1.8E-05 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  8.63597E-05 0.01149  8.63795E-05 0.01149  8.35571E-05 0.02418 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  9.82752E-05 0.01148  9.82976E-05 0.01148  9.51034E-05 0.02428 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.97616E-03 0.01775  2.31224E-04 0.07243  1.28575E-03 0.03181  1.20938E-03 0.03265  2.67770E-03 0.02381  1.10804E-03 0.03363  4.64057E-04 0.04897 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  4.67697E-01 0.01699  1.33360E-02 5.1E-09  3.27390E-02 0.0E+00  1.20780E-01 9.4E-07  3.02781E-01 2.2E-06  8.49531E-01 3.9E-05  2.85300E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.00291E-03 0.01751  2.37256E-04 0.06893  1.28844E-03 0.03090  1.23639E-03 0.03255  2.68295E-03 0.02342  1.10953E-03 0.03331  4.48348E-04 0.04781 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  4.60691E-01 0.01612  1.33360E-02 5.3E-09  3.27390E-02 0.0E+00  1.20780E-01 3.2E-06  3.02782E-01 6.1E-06  8.49531E-01 3.9E-05  2.85300E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -8.07914E+01 0.01369 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  8.81882E-05 0.00043 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  1.00363E-04 0.00031 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.25460E-03 0.00247 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -8.22629E+01 0.00244 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.27521E-06 0.00027 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.63263E-06 0.00024  4.63442E-06 0.00024  4.38148E-06 0.00265 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.38590E-04 0.00039  1.38644E-04 0.00039  1.30878E-04 0.00442 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.54681E-01 0.00015  7.53891E-01 0.00016  8.85743E-01 0.00383 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.10523E+01 0.00581 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  9.60113E+00 0.00019  4.97287E+01 0.00029 ];

