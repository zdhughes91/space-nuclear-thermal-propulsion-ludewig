
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  82]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/axialReflector/topOnly/10cmReflector' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Fri May 17 20:08:34 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Sat May 18 06:40:27 2024' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 1400 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1715994514013 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 15 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:  15]) = [  1.18185E+00  9.81222E-01  9.85270E-01  9.84190E-01  9.75475E-01  9.84303E-01  9.81517E-01  9.93926E-01  9.86869E-01  9.75642E-01  9.88861E-01  9.99511E-01  1.00394E+00  9.73739E-01  1.00369E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  2.84221E-01 0.00015  4.02437E-01 5.6E-05 ];
DT_FRAC                   (idx, [1:   4]) = [  7.15779E-01 6.1E-05  5.97563E-01 3.8E-05 ];
DT_EFF                    (idx, [1:   4]) = [  2.79877E-01 7.7E-05  2.54577E-01 2.4E-05 ];
IFC_COL_EFF               (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  7.73327E-01 9.0E-05  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_SAMPLING_EFF          (idx, [1:   2]) = [  2.50545E-01 0.00019 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.04135E-01 0.00019  3.27681E-01 2.6E-05 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  7.83358E+00 0.00021  4.81474E+00 4.1E-05  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.38556E-01 5.6E-06  5.70726E-02 8.6E-05  4.37155E-03 9.2E-05  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_FAIL_STAT             (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.76650E+01 0.00011  2.32764E+01 9.0E-05 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.74139E+01 0.00012  2.22897E+01 9.1E-05 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.85790E+01 0.00027  4.57329E+01 9.0E-05 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  2.57536E+01 0.00014  3.50938E+01 0.00013 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 1400 ;
SIMULATED_HISTORIES       (idx, 1)        = 140001806 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00001E+05 0.00014 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00001E+05 0.00014 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  9.34437E+03 ;
RUNNING_TIME              (idx, 1)        =  6.31888E+02 ;
INIT_TIME                 (idx, [1:   2]) = [  4.80817E-01  4.80817E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  8.90000E-03  8.90000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  6.31398E+02  6.31398E+02  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  6.31872E+02  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 14.78802 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  1.48083E+01 0.00035 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.91957E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 6697.74 ;
MEMSIZE                   (idx, 1)        = 6558.68 ;
XS_MEMSIZE                (idx, 1)        = 4769.26 ;
MAT_MEMSIZE               (idx, 1)        = 635.38 ;
RES_MEMSIZE               (idx, 1)        = 301.59 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 852.45 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 139.06 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 58 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 819727 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 8081 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 23 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 85 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 67 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 18 ;
TOT_REA_CHANNELS          (idx, 1)        = 1692 ;
TOT_TRANSMU_REA           (idx, 1)        = 103 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 2 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.95313E+14 9.9E-05  4.95313E+14 9.9E-05 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 139488817 1.43243E+08 6.65521E+06 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 13920344 1.43005E+07 7.30752E+06 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 466882392 4.79513E+08 1.65842E+07 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 534773102 5.49904E+08 6.07423E+08 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1155064655 1.18696E+09 6.37970E+08 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1016924589 1.04462E+09 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 138140062 1.42339E+08 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 4 3.99410E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1155064655 1.18696E+09 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 2.12193E-05 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  5.03586E+19 0.00011 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  1.41204E+12 0.49947 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  3.67048E+20 8.0E-05 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  2.52970E+18 0.00040 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  4.19937E+20 7.7E-05 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  1.13538E+21 8.2E-05 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  5.52408E+21 8.4E-05 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  2.70348E+07 8.6E-05 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  7.60203E-10 4.1E-05 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.18176E-03 0.00178 ];
U235_FISS                 (idx, [1:   4]) = [  2.26804E+19 8.8E-05  9.99887E-01 1.3E-06 ];
U238_FISS                 (idx, [1:   4]) = [  2.54974E+15 0.01177  1.12407E-04 0.01177 ];
U235_CAPT                 (idx, [1:   4]) = [  4.44460E+18 0.00028  2.85479E-01 0.00024 ];
U238_CAPT                 (idx, [1:   4]) = [  1.12954E+17 0.00178  7.25501E-03 0.00177 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 140001806 1.40000E+08 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 5.58041E+06 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 140001806 1.45580E+08 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 42511808 4.40058E+07 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 62346576 6.41137E+07 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 35143422 3.74609E+07 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 140001806 1.45580E+08 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -4.61936E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  7.00000E+08 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  7.98427E+01 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  5.53086E+19 8.2E-06 ];
TOT_FISSRATE              (idx, [1:   2]) = [  2.26841E+19 8.2E-06 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  1.55664E+19 0.00011 ];
TOT_ABSRATE               (idx, [1:   2]) = [  3.82505E+19 4.2E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  4.95313E+19 9.9E-05 ];
TOT_FLUX                  (idx, [1:   2]) = [  3.83182E+21 0.00010 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  1.94576E+20 1.2E-05  1.94580E+20 7.2E-05 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.32536E+19 0.00022 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  5.15041E+19 7.3E-05 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  2.28706E+21 0.00011 ];
INI_FMASS                 (idx, 1)        =  8.76724E+00 ;
TOT_FMASS                 (idx, 1)        =  8.76724E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06664E+00 5.4E-05 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.39944E-01 6.4E-05 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.12259E-01 4.6E-05 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09283E+00 4.0E-05 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.90338E-01 4.9E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.26720E-01 2.9E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.52452E+00 8.4E-05 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.11659E+00 9.8E-05 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43821E+00 1.6E-07 ];
FISSE                     (idx, [1:   2]) = [  1.92604E+02 8.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.11659E+00 9.9E-05  1.10842E+00 9.8E-05  8.17300E-03 0.00149 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.11668E+00 7.4E-05 ];
COL_KEFF                  (idx, [1:   2]) = [  1.11666E+00 0.00010 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.11668E+00 7.4E-05 ];
ABS_KINF                  (idx, [1:   2]) = [  1.52465E+00 4.6E-05 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93674E+01 1.8E-05 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93673E+01 8.3E-06 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.76107E-08 0.00034 ];
IMP_EALF                  (idx, [1:   2]) = [  7.76154E-08 0.00016 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.43968E-02 0.00184 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.44360E-02 0.00023 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.15731E-03 0.00111  2.14258E-04 0.00574  1.11449E-03 0.00252  1.06577E-03 0.00261  2.37452E-03 0.00179  9.77289E-04 0.00275  4.10985E-04 0.00432 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.69309E-01 0.00167  1.33360E-02 5.4E-07  3.27387E-02 1.4E-06  1.20781E-01 7.6E-07  3.02786E-01 1.9E-06  8.49515E-01 3.5E-06  2.85308E+00 5.5E-06 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.31694E-03 0.00151  2.57439E-04 0.00798  1.32340E-03 0.00347  1.27658E-03 0.00370  2.80504E-03 0.00251  1.16677E-03 0.00382  4.87709E-04 0.00600 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  4.69152E-01 0.00230  1.33360E-02 4.9E-09  3.27387E-02 2.1E-06  1.20781E-01 1.0E-06  3.02787E-01 3.0E-06  8.49519E-01 6.4E-06  2.85311E+00 1.0E-05 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  8.08854E-05 0.00022  8.08926E-05 0.00022  7.98979E-05 0.00235 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  9.03143E-05 0.00020  9.03223E-05 0.00020  8.92118E-05 0.00235 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.31625E-03 0.00152  2.56815E-04 0.00780  1.33007E-03 0.00351  1.27443E-03 0.00367  2.80401E-03 0.00248  1.16317E-03 0.00378  4.87761E-04 0.00591 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.68853E-01 0.00230  1.33360E-02 2.4E-09  3.27387E-02 2.3E-06  1.20780E-01 9.5E-07  3.02786E-01 2.8E-06  8.49518E-01 5.9E-06  2.85311E+00 1.0E-05 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  8.04091E-05 0.00169  8.04178E-05 0.00169  7.91836E-05 0.00611 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  8.97824E-05 0.00169  8.97921E-05 0.00169  8.84131E-05 0.00611 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.32866E-03 0.00521  2.61733E-04 0.02562  1.32442E-03 0.01150  1.26774E-03 0.01199  2.82058E-03 0.00807  1.15943E-03 0.01202  4.94756E-04 0.01866 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  4.70995E-01 0.00702  1.33360E-02 0.0E+00  3.27389E-02 1.4E-06  1.20781E-01 3.3E-06  3.02783E-01 4.2E-06  8.49506E-01 1.1E-05  2.85315E+00 4.0E-05 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.32549E-03 0.00501  2.62129E-04 0.02510  1.32461E-03 0.01122  1.26724E-03 0.01149  2.81594E-03 0.00781  1.15877E-03 0.01157  4.96812E-04 0.01805 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  4.71686E-01 0.00682  1.33360E-02 0.0E+00  3.27389E-02 3.1E-06  1.20781E-01 2.6E-06  3.02783E-01 4.8E-06  8.49507E-01 1.1E-05  2.85316E+00 4.0E-05 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -9.11604E+01 0.00497 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  8.08516E-05 0.00014 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  9.02765E-05 9.9E-05 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.36946E-03 0.00089 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -9.11502E+01 0.00090 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.12571E-06 9.9E-05 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.38547E-06 9.2E-05  4.38660E-06 9.2E-05  4.23007E-06 0.00109 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.15058E-04 0.00013  1.15077E-04 0.00013  1.12368E-04 0.00147 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.26189E-01 5.9E-05  7.25408E-01 6.1E-05  8.53864E-01 0.00162 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.10303E+01 0.00237 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  8.85129E+00 5.3E-05  4.70216E+01 8.3E-05 ];

