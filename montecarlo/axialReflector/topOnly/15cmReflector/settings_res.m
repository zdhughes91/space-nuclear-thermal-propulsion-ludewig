
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  82]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/axialReflector/topOnly/15cmReflector' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu May 16 22:08:24 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Fri May 17 08:20:56 2024' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 1400 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1715915304171 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 15 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:  15]) = [  1.24820E+00  9.58791E-01  9.81296E-01  9.91279E-01  9.83061E-01  9.78997E-01  9.98801E-01  9.96018E-01  9.90692E-01  9.63310E-01  9.75998E-01  1.00653E+00  9.87597E-01  9.76993E-01  9.62432E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 8.7E-10  5.00000E-02 1.7E-09 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  2.88713E-01 0.00015  4.02954E-01 5.3E-05 ];
DT_FRAC                   (idx, [1:   4]) = [  7.11287E-01 6.3E-05  5.97046E-01 3.6E-05 ];
DT_EFF                    (idx, [1:   4]) = [  2.80397E-01 7.8E-05  2.52742E-01 2.5E-05 ];
IFC_COL_EFF               (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  7.82019E-01 8.8E-05  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_SAMPLING_EFF          (idx, [1:   2]) = [  2.48357E-01 0.00019 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.12993E-01 0.00019  3.33130E-01 2.7E-05 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  7.85967E+00 0.00022  4.78602E+00 4.1E-05  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.39203E-01 5.6E-06  5.64848E-02 8.7E-05  4.31225E-03 9.1E-05  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_FAIL_STAT             (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  6.08421E+01 0.00012  2.45680E+01 8.9E-05 ];
AVG_REAL_COL              (idx, [1:   4]) = [  6.06027E+01 0.00012  2.36064E+01 9.0E-05 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.08206E+01 0.00027  4.72561E+01 8.9E-05 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  2.58960E+01 0.00014  3.54539E+01 0.00013 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 1400 ;
SIMULATED_HISTORIES       (idx, 1)        = 140001621 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00001E+05 0.00015 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00001E+05 0.00015 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  9.12439E+03 ;
RUNNING_TIME              (idx, 1)        =  6.12538E+02 ;
INIT_TIME                 (idx, [1:   2]) = [  3.63333E-01  3.63333E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  9.08333E-03  9.08333E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  6.12166E+02  6.12166E+02  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  6.12522E+02  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 14.89602 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  1.49037E+01 0.00013 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.92472E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 6697.74 ;
MEMSIZE                   (idx, 1)        = 6558.68 ;
XS_MEMSIZE                (idx, 1)        = 4769.26 ;
MAT_MEMSIZE               (idx, 1)        = 635.38 ;
RES_MEMSIZE               (idx, 1)        = 301.59 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 852.45 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 139.06 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 58 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 819727 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 8081 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 23 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 85 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 67 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 18 ;
TOT_REA_CHANNELS          (idx, 1)        = 1692 ;
TOT_TRANSMU_REA           (idx, 1)        = 103 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 2 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.88550E+14 9.7E-05  4.88550E+14 9.7E-05 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 141090759 1.45016E+08 6.74618E+06 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 14112398 1.45124E+07 7.41582E+06 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 471833075 4.85008E+08 1.67526E+07 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 541203644 5.57049E+08 6.17126E+08 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1168239876 1.20159E+09 6.48040E+08 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1033618338 1.06271E+09 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 134621531 1.38873E+08 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 7 7.00291E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1168239876 1.20159E+09 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 -1.57356E-05 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  4.84615E+19 0.00011 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  2.44122E+12 0.37716 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  3.68313E+20 7.5E-05 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  2.53213E+18 0.00039 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  4.19307E+20 7.2E-05 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  1.18740E+21 7.7E-05 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  5.73080E+21 7.8E-05 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  2.74208E+07 8.2E-05 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  7.79285E-10 4.2E-05 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.11575E-03 0.00188 ];
U235_FISS                 (idx, [1:   4]) = [  2.26519E+19 8.6E-05  9.99889E-01 1.3E-06 ];
U238_FISS                 (idx, [1:   4]) = [  2.50419E+15 0.01185  1.10525E-04 0.01184 ];
U235_CAPT                 (idx, [1:   4]) = [  4.42955E+18 0.00028  2.82193E-01 0.00024 ];
U238_CAPT                 (idx, [1:   4]) = [  1.10986E+17 0.00190  7.07048E-03 0.00189 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 140001621 1.40000E+08 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 5.72699E+06 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 140001621 1.45727E+08 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 43409831 4.49816E+07 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 63069519 6.49195E+07 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 33522271 3.58259E+07 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 140001621 1.45727E+08 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 5.51343E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  7.00000E+08 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  7.99353E+01 6.6E-09 ];
TOT_GENRATE               (idx, [1:   2]) = [  5.52349E+19 7.9E-06 ];
TOT_FISSRATE              (idx, [1:   2]) = [  2.26541E+19 8.0E-06 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  1.56975E+19 0.00011 ];
TOT_ABSRATE               (idx, [1:   2]) = [  3.83516E+19 4.2E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  4.88550E+19 9.7E-05 ];
TOT_FLUX                  (idx, [1:   2]) = [  3.97783E+21 0.00011 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  1.94415E+20 1.2E-05  1.94417E+20 6.7E-05 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.25021E+19 0.00022 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  5.08537E+19 7.2E-05 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  2.41496E+21 0.00011 ];
INI_FMASS                 (idx, 1)        =  8.75708E+00 ;
TOT_FMASS                 (idx, 1)        =  8.75708E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06682E+00 5.3E-05 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.36434E-01 6.5E-05 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.14616E-01 4.4E-05 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09146E+00 3.9E-05 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.06966E-01 4.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.22097E-01 3.0E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.51944E+00 8.7E-05 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.13061E+00 0.00010 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43819E+00 1.5E-07 ];
FISSE                     (idx, [1:   2]) = [  1.92859E+02 8.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.13062E+00 0.00010  1.12242E+00 0.00010  8.19819E-03 0.00144 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.13061E+00 7.3E-05 ];
COL_KEFF                  (idx, [1:   2]) = [  1.13060E+00 0.00010 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.13061E+00 7.3E-05 ];
ABS_KINF                  (idx, [1:   2]) = [  1.51943E+00 4.6E-05 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93783E+01 1.7E-05 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93785E+01 7.7E-06 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.67674E-08 0.00034 ];
IMP_EALF                  (idx, [1:   2]) = [  7.67489E-08 0.00015 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.42238E-02 0.00185 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.42066E-02 0.00022 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.08123E-03 0.00108  2.12191E-04 0.00570  1.10239E-03 0.00263  1.05102E-03 0.00255  2.35108E-03 0.00172  9.60655E-04 0.00263  4.03897E-04 0.00416 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.68070E-01 0.00160  1.33360E-02 1.5E-06  3.27387E-02 1.4E-06  1.20781E-01 9.0E-07  3.02785E-01 1.9E-06  8.49518E-01 3.8E-06  2.85307E+00 5.1E-06 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.25280E-03 0.00150  2.57190E-04 0.00806  1.32434E-03 0.00357  1.27243E-03 0.00355  2.77908E-03 0.00244  1.14276E-03 0.00381  4.77005E-04 0.00579 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  4.65165E-01 0.00220  1.33360E-02 9.8E-07  3.27388E-02 1.7E-06  1.20781E-01 1.5E-06  3.02786E-01 3.0E-06  8.49523E-01 6.1E-06  2.85305E+00 5.4E-06 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  8.41582E-05 0.00024  8.41700E-05 0.00024  8.25255E-05 0.00232 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  9.51498E-05 0.00022  9.51632E-05 0.00022  9.33044E-05 0.00232 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.25080E-03 0.00147  2.56065E-04 0.00805  1.31993E-03 0.00349  1.26146E-03 0.00356  2.78685E-03 0.00238  1.14904E-03 0.00375  4.77457E-04 0.00588 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.66304E-01 0.00222  1.33360E-02 1.9E-06  3.27388E-02 1.8E-06  1.20781E-01 1.6E-06  3.02786E-01 2.9E-06  8.49523E-01 6.2E-06  2.85306E+00 6.5E-06 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  8.40009E-05 0.00170  8.40140E-05 0.00170  8.21816E-05 0.00660 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  9.49724E-05 0.00169  9.49872E-05 0.00169  9.29156E-05 0.00660 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.29074E-03 0.00506  2.61610E-04 0.02465  1.33601E-03 0.01126  1.25475E-03 0.01210  2.79070E-03 0.00788  1.16129E-03 0.01221  4.86378E-04 0.01845 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  4.68429E-01 0.00693  1.33362E-02 1.8E-05  3.27390E-02 5.7E-09  1.20780E-01 1.7E-06  3.02785E-01 8.0E-06  8.49517E-01 1.2E-05  2.85300E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.28592E-03 0.00490  2.60301E-04 0.02374  1.33502E-03 0.01080  1.25342E-03 0.01170  2.79300E-03 0.00764  1.16017E-03 0.01197  4.84015E-04 0.01802 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  4.67485E-01 0.00675  1.33362E-02 1.5E-05  3.27390E-02 5.7E-09  1.20780E-01 1.7E-06  3.02785E-01 7.3E-06  8.49517E-01 1.3E-05  2.85300E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -8.68318E+01 0.00485 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  8.43018E-05 0.00015 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  9.53122E-05 0.00011 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.29087E-03 0.00094 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -8.64881E+01 0.00096 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.19650E-06 0.00011 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.53300E-06 8.9E-05  4.53447E-06 8.9E-05  4.32821E-06 0.00102 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.26028E-04 0.00014  1.26054E-04 0.00015  1.22407E-04 0.00156 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.43544E-01 5.5E-05  7.42756E-01 5.7E-05  8.73782E-01 0.00156 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.10934E+01 0.00243 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  9.25035E+00 5.8E-05  4.85112E+01 8.9E-05 ];

