"""
    A code built to automatically rotate the control drums
    in the USNC-modified Ludewig SNTP Serpent2 input
    
    Author: Zach Hughes - zhughes@tamu.edu
"""
import numpy as np

# - user inputs
editFile = True # do you want this to rewrite the file contents?
angle = 180
drumUniverseFilename = 'drumUniverses'
# -------------


# - consants
#coreRadius = 0.6392 / 2 # m
coreRadius = .3091
drumRadius = 0.046      # m, this may be wrong
coolantAnnulusGap = 0.2e-2
numDrums = 12
initialAngle = 30 + angle# angle that d1 needs to be transformed by to face the origin
deltaAngle = -30 # 12 drums in 360 degree circle
# ----------



drumCenterFromOrigin = coreRadius + drumRadius + coolantAnnulusGap

print(f"Origin to drum centerline = {drumCenterFromOrigin*100:0.3f}cm")

drumAngleList = []
for i in range(numDrums):
    #drumAngleDict[f"d{i+1}"] = {}
    drumAngle = initialAngle + i*deltaAngle
    if drumAngle > 360: 
        drumAngle = -(drumAngle-360)

    drumAngleList.append(drumAngle)


with open(drumUniverseFilename, 'r') as file:
    lines = file.readlines()

if editFile:
    c = 0
    with open(drumUniverseFilename,'w') as file:
        for line in lines:
            if line.startswith('t'): # if the line is a trans card this is true
                transLine = line.split() # split line so I can access last number
                transLine[-1] = str(drumAngleList[c])
                modified_line = ' '.join(transLine)
                file.write(modified_line + '\n')
                c += 1
            else:
                file.write(line)