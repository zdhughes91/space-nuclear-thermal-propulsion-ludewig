
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  71]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/keffComparison/notCoupled' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Fri May 17 18:21:22 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Fri May 17 20:07:31 2024' ;

% Run parameters:

POP                       (idx, 1)        = 200000 ;
CYCLES                    (idx, 1)        = 1000 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1715988082604 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 10 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:  10]) = [  1.33628E+00  9.73553E-01  9.64652E-01  9.51598E-01  9.37284E-01  9.87654E-01  9.73361E-01  9.36521E-01  9.41078E-01  9.98016E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 8.1E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.12112E-01 4.8E-05  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.87888E-01 3.4E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.84585E-01 2.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.58108E-01 2.6E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.32172E+00 4.7E-05  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.34165E-01 2.1E-06  6.37140E-02 3.0E-05  2.12095E-03 0.00011  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.31135E+01 5.8E-05  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.28467E+01 5.9E-05  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.39247E+01 6.3E-05  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.68157E+01 6.7E-05  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 1000 ;
SIMULATED_HISTORIES       (idx, 1)        = 200002745 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  2.00003E+05 0.00011 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  2.00003E+05 0.00011 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.01129E+03 ;
RUNNING_TIME              (idx, 1)        =  1.06155E+02 ;
INIT_TIME                 (idx, [1:   2]) = [  1.28433E-01  1.28433E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.58333E-03  4.58333E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.06022E+02  1.06022E+02  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.06153E+02  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 9.52658 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  9.53618E+00 0.00011 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.45422E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 4443.50 ;
MEMSIZE                   (idx, 1)        = 4285.44 ;
XS_MEMSIZE                (idx, 1)        = 2425.82 ;
MAT_MEMSIZE               (idx, 1)        = 513.22 ;
RES_MEMSIZE               (idx, 1)        = 10.70 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 1335.69 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 158.06 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 54 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 819781 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 16 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 52 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 52 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 1214 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.39085E+14 8.8E-05  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.20973E-03 0.00151 ];
U235_FISS                 (idx, [1:   4]) = [  2.15970E+19 7.6E-05  9.99884E-01 1.1E-06 ];
U238_FISS                 (idx, [1:   4]) = [  2.49628E+15 0.00985  1.15567E-04 0.00985 ];
U235_CAPT                 (idx, [1:   4]) = [  4.23602E+18 0.00024  2.93962E-01 0.00021 ];
U238_CAPT                 (idx, [1:   4]) = [  1.08295E+17 0.00151  7.51531E-03 0.00151 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 200002745 2.00000E+08 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.39444E+06 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 200002745 2.07394E+08 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 58410975 6.02720E+07 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 88225500 9.03427E+07 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 53366270 5.67798E+07 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 200002745 2.07394E+08 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.92821E-05 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  7.00000E+08 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  7.98045E+01 3.5E-09 ];
TOT_GENRATE               (idx, [1:   2]) = [  5.26668E+19 1.4E-07 ];
TOT_FISSRATE              (idx, [1:   2]) = [  2.16001E+19 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  1.44127E+19 9.5E-05 ];
TOT_ABSRATE               (idx, [1:   2]) = [  3.60128E+19 3.8E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  4.78170E+19 8.8E-05 ];
TOT_FLUX                  (idx, [1:   2]) = [  3.71611E+21 9.1E-05 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.35753E+19 0.00018 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.95880E+19 6.5E-05 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  2.12593E+21 9.6E-05 ];
INI_FMASS                 (idx, 1)        =  8.77144E+00 ;
TOT_FMASS                 (idx, 1)        =  8.77144E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06678E+00 4.4E-05 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.49785E-01 5.2E-05 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.07193E-01 4.0E-05 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09406E+00 3.4E-05 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.74579E-01 4.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.24503E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.53805E+00 7.1E-05 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.10140E+00 8.1E-05 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43827E+00 1.4E-07 ];
FISSE                     (idx, [1:   2]) = [  2.02271E+02 0.0E+00 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.10138E+00 8.2E-05  1.09322E+00 8.1E-05  8.17666E-03 0.00121 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.10138E+00 6.3E-05 ];
COL_KEFF                  (idx, [1:   2]) = [  1.10143E+00 8.8E-05 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.10138E+00 6.3E-05 ];
ABS_KINF                  (idx, [1:   2]) = [  1.53799E+00 3.9E-05 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93586E+01 1.5E-05 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93586E+01 6.3E-06 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.82931E-08 0.00028 ];
IMP_EALF                  (idx, [1:   2]) = [  7.82905E-08 0.00012 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.49813E-02 0.00148 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.49748E-02 0.00019 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.20304E-03 0.00097  2.18440E-04 0.00481  1.11608E-03 0.00215  1.06862E-03 0.00215  2.40641E-03 0.00149  9.82200E-04 0.00224  4.11287E-04 0.00353 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.68314E-01 0.00132  1.33360E-02 1.2E-06  3.27387E-02 1.2E-06  1.20781E-01 6.4E-07  3.02786E-01 1.6E-06  8.49515E-01 3.0E-06  2.85308E+00 4.7E-06 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.43484E-03 0.00124  2.65064E-04 0.00668  1.34425E-03 0.00291  1.29038E-03 0.00296  2.86036E-03 0.00197  1.18058E-03 0.00317  4.94203E-04 0.00481 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  4.68356E-01 0.00176  1.33361E-02 2.8E-06  3.27387E-02 1.7E-06  1.20780E-01 8.2E-07  3.02786E-01 2.2E-06  8.49518E-01 4.7E-06  2.85306E+00 5.8E-06 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  8.08301E-05 0.00019  8.08369E-05 0.00019  7.99313E-05 0.00187 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  8.90241E-05 0.00017  8.90315E-05 0.00017  8.80337E-05 0.00186 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.42003E-03 0.00124  2.66359E-04 0.00668  1.34430E-03 0.00284  1.28907E-03 0.00294  2.85069E-03 0.00191  1.17711E-03 0.00313  4.92495E-04 0.00487 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.67784E-01 0.00179  1.33360E-02 1.6E-06  3.27387E-02 1.9E-06  1.20781E-01 1.0E-06  3.02788E-01 2.8E-06  8.49517E-01 4.7E-06  2.85307E+00 6.0E-06 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  7.99700E-05 0.00321  7.99791E-05 0.00321  7.86962E-05 0.00572 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  8.80763E-05 0.00321  8.80863E-05 0.00321  8.66744E-05 0.00572 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.39723E-03 0.00515  2.61993E-04 0.02136  1.35291E-03 0.01000  1.28405E-03 0.01002  2.82425E-03 0.00747  1.17449E-03 0.01066  4.99535E-04 0.01632 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  4.70663E-01 0.00616  1.33360E-02 0.0E+00  3.27383E-02 1.1E-05  1.20780E-01 1.9E-06  3.02787E-01 9.5E-06  8.49545E-01 2.3E-05  2.85304E+00 1.1E-05 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.41191E-03 0.00504  2.60328E-04 0.02073  1.35732E-03 0.00959  1.29077E-03 0.00973  2.83201E-03 0.00721  1.17205E-03 0.01029  4.99440E-04 0.01553 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  4.69865E-01 0.00588  1.33360E-02 0.0E+00  3.27384E-02 8.0E-06  1.20780E-01 1.8E-06  3.02788E-01 1.0E-05  8.49544E-01 2.2E-05  2.85304E+00 1.3E-05 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -9.25025E+01 0.00406 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  8.08090E-05 0.00056 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  8.90010E-05 0.00056 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.44070E-03 0.00106 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -9.20692E+01 0.00086 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.07831E-06 8.3E-05 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.53118E-06 8.0E-05  4.53245E-06 8.0E-05  4.35950E-06 0.00086 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.13283E-04 0.00011  1.13300E-04 0.00011  1.11007E-04 0.00116 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.08230E-01 5.0E-05  7.07428E-01 5.1E-05  8.37512E-01 0.00140 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.10467E+01 0.00202 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.28467E+01 5.9E-05  4.56129E+01 6.9E-05 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   5]) = 'iPuni' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.72687E+05 0.00154  7.36349E+05 0.00089  1.50259E+06 0.00061  1.97150E+06 0.00054  1.74127E+06 0.00054  1.37349E+06 0.00073  1.06167E+06 0.00068  7.67366E+05 0.00095  5.76553E+05 0.00067  4.59516E+05 0.00096  3.87807E+05 0.00108  3.48449E+05 0.00121  3.17869E+05 0.00146  3.02239E+05 0.00129  2.92294E+05 0.00118  2.54668E+05 0.00105  2.49610E+05 0.00139  2.52214E+05 0.00139  2.49976E+05 0.00148  4.91650E+05 0.00102  4.72497E+05 0.00096  3.51700E+05 0.00128  2.27169E+05 0.00119  2.69747E+05 0.00108  2.62978E+05 0.00132  2.28932E+05 0.00144  4.23548E+05 0.00106  9.28895E+04 0.00260  1.15804E+05 0.00231  1.05391E+05 0.00190  5.98234E+04 0.00316  1.04803E+05 0.00177  7.01685E+04 0.00275  5.72610E+04 0.00301  1.04648E+04 0.00579  1.01722E+04 0.00640  1.05795E+04 0.00645  1.11578E+04 0.00573  1.11841E+04 0.00661  1.09219E+04 0.00823  1.13371E+04 0.00583  1.07062E+04 0.00736  2.00430E+04 0.00583  3.19497E+04 0.00422  4.03336E+04 0.00364  1.03797E+05 0.00251  1.03590E+05 0.00234  1.01426E+05 0.00195  5.84683E+04 0.00282  3.81291E+04 0.00312  2.69434E+04 0.00328  2.90769E+04 0.00399  4.90124E+04 0.00295  5.94497E+04 0.00271  1.10084E+05 0.00207  1.89728E+05 0.00172  3.95046E+05 0.00105  3.52266E+05 0.00124  3.12382E+05 0.00116  2.59026E+05 0.00152  2.57880E+05 0.00141  2.79061E+05 0.00117  2.54532E+05 0.00145  1.80913E+05 0.00157  1.73894E+05 0.00168  1.58001E+05 0.00139  1.33874E+05 0.00178  9.90091E+04 0.00245  5.53612E+04 0.00250  1.32514E+04 0.00629 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.98861E+20 0.00022  4.41160E+19 0.00038 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  3.11376E-03 0.00012  7.66195E-03 8.5E-05 ];
INF_CAPT                  (idx, [1:   4]) = [  1.14239E-06 0.00051  6.57133E-05 0.00020 ];
INF_ABS                   (idx, [1:   4]) = [  1.14239E-06 0.00051  6.57133E-05 0.00020 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  5.34255E-08 0.00050  3.05939E-06 0.00020 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  3.11280E-03 0.00063  7.59943E-03 0.00079 ];
INF_SCATT1                (idx, [1:   4]) = [  2.06867E-03 0.00069  3.35368E-03 0.00118 ];
INF_SCATT2                (idx, [1:   4]) = [  7.85408E-04 0.00129  1.29753E-03 0.00254 ];
INF_SCATT3                (idx, [1:   4]) = [  1.40885E-05 0.05699  4.89301E-04 0.00560 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.30660E-04 0.00494  1.97142E-04 0.01390 ];
INF_SCATT5                (idx, [1:   4]) = [ -1.33178E-05 0.04910  9.56504E-05 0.02119 ];
INF_SCATT6                (idx, [1:   4]) = [  4.33281E-05 0.01481  5.61863E-05 0.03723 ];
INF_SCATT7                (idx, [1:   4]) = [  6.93286E-06 0.08401  4.01455E-05 0.04741 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  3.11280E-03 0.00063  7.59943E-03 0.00079 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.06867E-03 0.00069  3.35368E-03 0.00118 ];
INF_SCATTP2               (idx, [1:   4]) = [  7.85408E-04 0.00129  1.29753E-03 0.00254 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.40885E-05 0.05699  4.89301E-04 0.00560 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.30660E-04 0.00494  1.97142E-04 0.01390 ];
INF_SCATTP5               (idx, [1:   4]) = [ -1.33178E-05 0.04910  9.56504E-05 0.02119 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.33281E-05 0.01481  5.61863E-05 0.03723 ];
INF_SCATTP7               (idx, [1:   4]) = [  6.93286E-06 0.08401  4.01455E-05 0.04741 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  5.19411E-04 0.00137  3.84710E-03 0.00090 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  6.41811E+02 0.00137  8.66488E+01 0.00090 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.14239E-06 0.00051  6.57133E-05 0.00020 ];
INF_REMXS                 (idx, [1:   4]) = [  1.69347E-04 0.01089  6.39251E-05 0.09131 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.94442E-03 0.00063  1.68385E-04 0.00280  1.40414E-06 0.05907  7.59802E-03 0.00079 ];
INF_S1                    (idx, [1:   8]) = [  2.00016E-03 0.00069  6.85075E-05 0.00331  1.28026E-06 0.05998  3.35240E-03 0.00118 ];
INF_S2                    (idx, [1:   8]) = [  7.99642E-04 0.00124 -1.42338E-05 0.01109  1.06266E-06 0.06361  1.29647E-03 0.00254 ];
INF_S3                    (idx, [1:   8]) = [  4.58453E-05 0.01703 -3.17568E-05 0.00396  8.00410E-07 0.07367  4.88501E-04 0.00560 ];
INF_S4                    (idx, [1:   8]) = [ -1.16871E-04 0.00529 -1.37890E-05 0.00862  5.44379E-07 0.09554  1.96598E-04 0.01395 ];
INF_S5                    (idx, [1:   8]) = [ -1.43318E-05 0.04537  1.01397E-06 0.11121  3.31798E-07 0.13745  9.53186E-05 0.02133 ];
INF_S6                    (idx, [1:   8]) = [  4.02139E-05 0.01625  3.11421E-06 0.03640  1.78703E-07 0.21876  5.60076E-05 0.03737 ];
INF_S7                    (idx, [1:   8]) = [  6.22617E-06 0.09453  7.06693E-07 0.15409  8.17143E-08 0.40875  4.00638E-05 0.04771 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.94442E-03 0.00063  1.68385E-04 0.00280  1.40414E-06 0.05907  7.59802E-03 0.00079 ];
INF_SP1                   (idx, [1:   8]) = [  2.00016E-03 0.00069  6.85075E-05 0.00331  1.28026E-06 0.05998  3.35240E-03 0.00118 ];
INF_SP2                   (idx, [1:   8]) = [  7.99642E-04 0.00124 -1.42338E-05 0.01109  1.06266E-06 0.06361  1.29647E-03 0.00254 ];
INF_SP3                   (idx, [1:   8]) = [  4.58453E-05 0.01703 -3.17568E-05 0.00396  8.00410E-07 0.07367  4.88501E-04 0.00560 ];
INF_SP4                   (idx, [1:   8]) = [ -1.16871E-04 0.00529 -1.37890E-05 0.00862  5.44379E-07 0.09554  1.96598E-04 0.01395 ];
INF_SP5                   (idx, [1:   8]) = [ -1.43318E-05 0.04537  1.01397E-06 0.11121  3.31798E-07 0.13745  9.53186E-05 0.02133 ];
INF_SP6                   (idx, [1:   8]) = [  4.02139E-05 0.01625  3.11421E-06 0.03640  1.78703E-07 0.21876  5.60076E-05 0.03737 ];
INF_SP7                   (idx, [1:   8]) = [  6.22617E-06 0.09453  7.06693E-07 0.15409  8.17143E-08 0.40875  4.00638E-05 0.04771 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  1.20030E+01 0.00174  1.47290E+01 0.00584 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  1.62954E+01 0.00352  2.92598E+01 0.02255 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  1.63255E+01 0.00342  2.85699E+01 0.01726 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  7.85897E+00 0.00250  7.50049E+00 0.00562 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  2.77750E-02 0.00175  2.26689E-02 0.00583 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  2.04681E-02 0.00352  1.16629E-02 0.02139 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  2.04296E-02 0.00342  1.18309E-02 0.01657 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  4.24273E-02 0.00250  4.45127E-02 0.00583 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  71]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/keffComparison/notCoupled' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Fri May 17 18:21:22 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Fri May 17 20:07:31 2024' ;

% Run parameters:

POP                       (idx, 1)        = 200000 ;
CYCLES                    (idx, 1)        = 1000 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1715988082604 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 10 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:  10]) = [  1.33628E+00  9.73553E-01  9.64652E-01  9.51598E-01  9.37284E-01  9.87654E-01  9.73361E-01  9.36521E-01  9.41078E-01  9.98016E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 8.1E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.12112E-01 4.8E-05  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.87888E-01 3.4E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.84585E-01 2.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.58108E-01 2.6E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.32172E+00 4.7E-05  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.34165E-01 2.1E-06  6.37140E-02 3.0E-05  2.12095E-03 0.00011  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.31135E+01 5.8E-05  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.28467E+01 5.9E-05  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.39247E+01 6.3E-05  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.68157E+01 6.7E-05  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 1000 ;
SIMULATED_HISTORIES       (idx, 1)        = 200002745 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  2.00003E+05 0.00011 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  2.00003E+05 0.00011 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.01129E+03 ;
RUNNING_TIME              (idx, 1)        =  1.06155E+02 ;
INIT_TIME                 (idx, [1:   2]) = [  1.28433E-01  1.28433E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.58333E-03  4.58333E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.06022E+02  1.06022E+02  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.06153E+02  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 9.52657 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  9.53618E+00 0.00011 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.45422E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 4443.50 ;
MEMSIZE                   (idx, 1)        = 4285.44 ;
XS_MEMSIZE                (idx, 1)        = 2425.82 ;
MAT_MEMSIZE               (idx, 1)        = 513.22 ;
RES_MEMSIZE               (idx, 1)        = 10.70 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 1335.69 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 158.06 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 54 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 819781 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 16 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 52 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 52 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 1214 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.39085E+14 8.8E-05  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.20973E-03 0.00151 ];
U235_FISS                 (idx, [1:   4]) = [  2.15970E+19 7.6E-05  9.99884E-01 1.1E-06 ];
U238_FISS                 (idx, [1:   4]) = [  2.49628E+15 0.00985  1.15567E-04 0.00985 ];
U235_CAPT                 (idx, [1:   4]) = [  4.23602E+18 0.00024  2.93962E-01 0.00021 ];
U238_CAPT                 (idx, [1:   4]) = [  1.08295E+17 0.00151  7.51531E-03 0.00151 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 200002745 2.00000E+08 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.39444E+06 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 200002745 2.07394E+08 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 58410975 6.02720E+07 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 88225500 9.03427E+07 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 53366270 5.67798E+07 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 200002745 2.07394E+08 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.92821E-05 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  7.00000E+08 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  7.98045E+01 3.5E-09 ];
TOT_GENRATE               (idx, [1:   2]) = [  5.26668E+19 1.4E-07 ];
TOT_FISSRATE              (idx, [1:   2]) = [  2.16001E+19 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  1.44127E+19 9.5E-05 ];
TOT_ABSRATE               (idx, [1:   2]) = [  3.60128E+19 3.8E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  4.78170E+19 8.8E-05 ];
TOT_FLUX                  (idx, [1:   2]) = [  3.71611E+21 9.1E-05 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.35753E+19 0.00018 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.95880E+19 6.5E-05 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  2.12593E+21 9.6E-05 ];
INI_FMASS                 (idx, 1)        =  8.77144E+00 ;
TOT_FMASS                 (idx, 1)        =  8.77144E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06678E+00 4.4E-05 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.49785E-01 5.2E-05 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.07193E-01 4.0E-05 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09406E+00 3.4E-05 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.74579E-01 4.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.24503E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.53805E+00 7.1E-05 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.10140E+00 8.1E-05 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43827E+00 1.4E-07 ];
FISSE                     (idx, [1:   2]) = [  2.02271E+02 0.0E+00 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.10138E+00 8.2E-05  1.09322E+00 8.1E-05  8.17666E-03 0.00121 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.10138E+00 6.3E-05 ];
COL_KEFF                  (idx, [1:   2]) = [  1.10143E+00 8.8E-05 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.10138E+00 6.3E-05 ];
ABS_KINF                  (idx, [1:   2]) = [  1.53799E+00 3.9E-05 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93586E+01 1.5E-05 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93586E+01 6.3E-06 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.82931E-08 0.00028 ];
IMP_EALF                  (idx, [1:   2]) = [  7.82905E-08 0.00012 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.49813E-02 0.00148 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.49748E-02 0.00019 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.20304E-03 0.00097  2.18440E-04 0.00481  1.11608E-03 0.00215  1.06862E-03 0.00215  2.40641E-03 0.00149  9.82200E-04 0.00224  4.11287E-04 0.00353 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.68314E-01 0.00132  1.33360E-02 1.2E-06  3.27387E-02 1.2E-06  1.20781E-01 6.4E-07  3.02786E-01 1.6E-06  8.49515E-01 3.0E-06  2.85308E+00 4.7E-06 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.43484E-03 0.00124  2.65064E-04 0.00668  1.34425E-03 0.00291  1.29038E-03 0.00296  2.86036E-03 0.00197  1.18058E-03 0.00317  4.94203E-04 0.00481 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  4.68356E-01 0.00176  1.33361E-02 2.8E-06  3.27387E-02 1.7E-06  1.20780E-01 8.2E-07  3.02786E-01 2.2E-06  8.49518E-01 4.7E-06  2.85306E+00 5.8E-06 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  8.08301E-05 0.00019  8.08369E-05 0.00019  7.99313E-05 0.00187 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  8.90241E-05 0.00017  8.90315E-05 0.00017  8.80337E-05 0.00186 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.42003E-03 0.00124  2.66359E-04 0.00668  1.34430E-03 0.00284  1.28907E-03 0.00294  2.85069E-03 0.00191  1.17711E-03 0.00313  4.92495E-04 0.00487 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.67784E-01 0.00179  1.33360E-02 1.6E-06  3.27387E-02 1.9E-06  1.20781E-01 1.0E-06  3.02788E-01 2.8E-06  8.49517E-01 4.7E-06  2.85307E+00 6.0E-06 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  7.99700E-05 0.00321  7.99791E-05 0.00321  7.86962E-05 0.00572 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  8.80763E-05 0.00321  8.80863E-05 0.00321  8.66744E-05 0.00572 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.39723E-03 0.00515  2.61993E-04 0.02136  1.35291E-03 0.01000  1.28405E-03 0.01002  2.82425E-03 0.00747  1.17449E-03 0.01066  4.99535E-04 0.01632 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  4.70663E-01 0.00616  1.33360E-02 0.0E+00  3.27383E-02 1.1E-05  1.20780E-01 1.9E-06  3.02787E-01 9.5E-06  8.49545E-01 2.3E-05  2.85304E+00 1.1E-05 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.41191E-03 0.00504  2.60328E-04 0.02073  1.35732E-03 0.00959  1.29077E-03 0.00973  2.83201E-03 0.00721  1.17205E-03 0.01029  4.99440E-04 0.01553 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  4.69865E-01 0.00588  1.33360E-02 0.0E+00  3.27384E-02 8.0E-06  1.20780E-01 1.8E-06  3.02788E-01 1.0E-05  8.49544E-01 2.2E-05  2.85304E+00 1.3E-05 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -9.25025E+01 0.00406 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  8.08090E-05 0.00056 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  8.90010E-05 0.00056 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.44070E-03 0.00106 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -9.20692E+01 0.00086 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.07831E-06 8.3E-05 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.53118E-06 8.0E-05  4.53245E-06 8.0E-05  4.35950E-06 0.00086 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.13283E-04 0.00011  1.13300E-04 0.00011  1.11007E-04 0.00116 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.08230E-01 5.0E-05  7.07428E-01 5.1E-05  8.37512E-01 0.00140 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.10467E+01 0.00202 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.28467E+01 5.9E-05  4.56129E+01 6.9E-05 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   5]) = 'hFuni' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  6.53051E+04 0.00281  2.80876E+05 0.00120  5.71960E+05 0.00077  7.47725E+05 0.00073  6.56582E+05 0.00067  5.14455E+05 0.00068  3.92327E+05 0.00089  2.80700E+05 0.00108  2.08578E+05 0.00127  1.65109E+05 0.00122  1.38449E+05 0.00112  1.24634E+05 0.00149  1.13351E+05 0.00165  1.07347E+05 0.00144  1.03929E+05 0.00157  9.03293E+04 0.00163  8.90610E+04 0.00143  8.92235E+04 0.00206  8.90790E+04 0.00169  1.73922E+05 0.00118  1.67304E+05 0.00112  1.24589E+05 0.00158  8.01644E+04 0.00153  9.49899E+04 0.00173  9.26172E+04 0.00181  8.06808E+04 0.00168  1.49442E+05 0.00114  3.29240E+04 0.00253  4.13493E+04 0.00230  3.76344E+04 0.00235  2.12769E+04 0.00370  3.73108E+04 0.00209  2.47483E+04 0.00322  2.02608E+04 0.00369  3.63908E+03 0.00904  3.59738E+03 0.00846  3.79645E+03 0.00785  3.85012E+03 0.00906  3.87662E+03 0.00718  3.83588E+03 0.00730  3.95591E+03 0.00810  3.75555E+03 0.00856  7.11876E+03 0.00585  1.12017E+04 0.00485  1.42990E+04 0.00384  3.67116E+04 0.00257  3.65817E+04 0.00277  3.56880E+04 0.00277  2.04515E+04 0.00341  1.31628E+04 0.00483  9.32219E+03 0.00497  9.94721E+03 0.00562  1.69098E+04 0.00367  2.05170E+04 0.00365  3.82852E+04 0.00269  6.66234E+04 0.00174  1.38855E+05 0.00144  1.23937E+05 0.00121  1.09124E+05 0.00149  9.00366E+04 0.00149  8.93771E+04 0.00165  9.61950E+04 0.00183  8.69289E+04 0.00167  6.14674E+04 0.00195  5.85683E+04 0.00193  5.28815E+04 0.00206  4.41515E+04 0.00244  3.21400E+04 0.00275  1.77075E+04 0.00303  4.14515E+03 0.00579 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  7.30151E+19 0.00022  1.52178E+19 0.00047 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  7.13831E-02 8.3E-05  9.66937E-02 3.5E-05 ];
INF_CAPT                  (idx, [1:   4]) = [  1.50801E-04 0.00147  9.92493E-04 0.00025 ];
INF_ABS                   (idx, [1:   4]) = [  1.50801E-04 0.00147  9.92493E-04 0.00025 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  5.15078E-08 0.00058  3.02827E-06 0.00025 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  7.12319E-02 9.0E-05  9.57052E-02 5.7E-05 ];
INF_SCATT1                (idx, [1:   4]) = [  1.56789E-02 0.00057  1.37969E-02 0.00163 ];
INF_SCATT2                (idx, [1:   4]) = [  7.09880E-03 0.00083  4.69017E-03 0.00358 ];
INF_SCATT3                (idx, [1:   4]) = [  1.01055E-03 0.00615  1.76230E-03 0.00915 ];
INF_SCATT4                (idx, [1:   4]) = [  3.07420E-04 0.01676  6.92613E-04 0.01846 ];
INF_SCATT5                (idx, [1:   4]) = [  2.70045E-04 0.01701  3.55935E-04 0.03426 ];
INF_SCATT6                (idx, [1:   4]) = [  3.03760E-04 0.01391  1.94703E-04 0.05465 ];
INF_SCATT7                (idx, [1:   4]) = [  5.26191E-05 0.07696  1.59768E-04 0.07325 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  7.12347E-02 9.1E-05  9.57052E-02 5.7E-05 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.56790E-02 0.00057  1.37969E-02 0.00163 ];
INF_SCATTP2               (idx, [1:   4]) = [  7.09866E-03 0.00083  4.69017E-03 0.00358 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.01054E-03 0.00615  1.76230E-03 0.00915 ];
INF_SCATTP4               (idx, [1:   4]) = [  3.07415E-04 0.01674  6.92613E-04 0.01846 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.70039E-04 0.01702  3.55935E-04 0.03426 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.03754E-04 0.01391  1.94703E-04 0.05465 ];
INF_SCATTP7               (idx, [1:   4]) = [  5.26275E-05 0.07694  1.59768E-04 0.07325 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  4.64178E-02 0.00029  8.23374E-02 0.00028 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  7.18119E+00 0.00029  4.04840E+00 0.00028 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.48034E-04 0.00176  9.92493E-04 0.00025 ];
INF_REMXS                 (idx, [1:   4]) = [  8.72080E-04 0.00379  1.05887E-03 0.00447 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  7.05110E-02 8.9E-05  7.20941E-04 0.00191  7.03165E-05 0.01559  9.56348E-02 5.9E-05 ];
INF_S1                    (idx, [1:   8]) = [  1.54841E-02 0.00057  1.94759E-04 0.00442  6.16595E-06 0.11564  1.37907E-02 0.00162 ];
INF_S2                    (idx, [1:   8]) = [  7.15007E-03 0.00083 -5.12785E-05 0.01318  2.74790E-06 0.22549  4.68742E-03 0.00355 ];
INF_S3                    (idx, [1:   8]) = [  1.11991E-03 0.00554 -1.09364E-04 0.00502  1.17532E-06 0.31581  1.76113E-03 0.00916 ];
INF_S4                    (idx, [1:   8]) = [  3.54321E-04 0.01443 -4.69014E-05 0.01246  2.20481E-06 0.19935  6.90408E-04 0.01847 ];
INF_S5                    (idx, [1:   8]) = [  2.66529E-04 0.01690  3.51590E-06 0.11880  1.20267E-06 0.28289  3.54732E-04 0.03428 ];
INF_S6                    (idx, [1:   8]) = [  2.93026E-04 0.01412  1.07341E-05 0.03593  8.94910E-07 0.38788  1.93808E-04 0.05470 ];
INF_S7                    (idx, [1:   8]) = [  5.04574E-05 0.07891  2.16168E-06 0.17581  5.09204E-07 0.58780  1.59259E-04 0.07363 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  7.05138E-02 8.9E-05  7.20941E-04 0.00191  7.03165E-05 0.01559  9.56348E-02 5.9E-05 ];
INF_SP1                   (idx, [1:   8]) = [  1.54843E-02 0.00057  1.94759E-04 0.00442  6.16595E-06 0.11564  1.37907E-02 0.00162 ];
INF_SP2                   (idx, [1:   8]) = [  7.14994E-03 0.00083 -5.12785E-05 0.01318  2.74790E-06 0.22549  4.68742E-03 0.00355 ];
INF_SP3                   (idx, [1:   8]) = [  1.11991E-03 0.00555 -1.09364E-04 0.00502  1.17532E-06 0.31581  1.76113E-03 0.00916 ];
INF_SP4                   (idx, [1:   8]) = [  3.54317E-04 0.01441 -4.69014E-05 0.01246  2.20481E-06 0.19935  6.90408E-04 0.01847 ];
INF_SP5                   (idx, [1:   8]) = [  2.66523E-04 0.01691  3.51590E-06 0.11880  1.20267E-06 0.28289  3.54732E-04 0.03428 ];
INF_SP6                   (idx, [1:   8]) = [  2.93020E-04 0.01412  1.07341E-05 0.03593  8.94910E-07 0.38788  1.93808E-04 0.05470 ];
INF_SP7                   (idx, [1:   8]) = [  5.04658E-05 0.07888  2.16168E-06 0.17581  5.09204E-07 0.58780  1.59259E-04 0.07363 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  1.04621E+00 0.00094  1.38948E+00 0.00339 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  1.28310E+00 0.00151  2.21976E+00 0.00812 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  1.28746E+00 0.00175  2.23023E+00 0.00746 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  7.62650E-01 0.00129  7.95089E-01 0.00401 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  3.18624E-01 0.00094  2.40034E-01 0.00341 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  2.59816E-01 0.00151  1.50660E-01 0.00825 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  2.58947E-01 0.00175  1.49868E-01 0.00743 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  4.37108E-01 0.00128  4.19574E-01 0.00405 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  71]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/keffComparison/notCoupled' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Fri May 17 18:21:22 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Fri May 17 20:07:31 2024' ;

% Run parameters:

POP                       (idx, 1)        = 200000 ;
CYCLES                    (idx, 1)        = 1000 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1715988082604 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 10 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:  10]) = [  1.33628E+00  9.73553E-01  9.64652E-01  9.51598E-01  9.37284E-01  9.87654E-01  9.73361E-01  9.36521E-01  9.41078E-01  9.98016E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 8.1E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.12112E-01 4.8E-05  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.87888E-01 3.4E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.84585E-01 2.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.58108E-01 2.6E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.32172E+00 4.7E-05  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.34165E-01 2.1E-06  6.37140E-02 3.0E-05  2.12095E-03 0.00011  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.31135E+01 5.8E-05  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.28467E+01 5.9E-05  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.39247E+01 6.3E-05  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.68157E+01 6.7E-05  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 1000 ;
SIMULATED_HISTORIES       (idx, 1)        = 200002745 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  2.00003E+05 0.00011 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  2.00003E+05 0.00011 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.01129E+03 ;
RUNNING_TIME              (idx, 1)        =  1.06155E+02 ;
INIT_TIME                 (idx, [1:   2]) = [  1.28433E-01  1.28433E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.58333E-03  4.58333E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.06022E+02  1.06022E+02  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.06153E+02  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 9.52657 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  9.53618E+00 0.00011 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.45422E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 4443.50 ;
MEMSIZE                   (idx, 1)        = 4285.44 ;
XS_MEMSIZE                (idx, 1)        = 2425.82 ;
MAT_MEMSIZE               (idx, 1)        = 513.22 ;
RES_MEMSIZE               (idx, 1)        = 10.70 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 1335.69 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 158.06 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 54 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 819781 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 16 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 52 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 52 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 1214 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.39085E+14 8.8E-05  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.20973E-03 0.00151 ];
U235_FISS                 (idx, [1:   4]) = [  2.15970E+19 7.6E-05  9.99884E-01 1.1E-06 ];
U238_FISS                 (idx, [1:   4]) = [  2.49628E+15 0.00985  1.15567E-04 0.00985 ];
U235_CAPT                 (idx, [1:   4]) = [  4.23602E+18 0.00024  2.93962E-01 0.00021 ];
U238_CAPT                 (idx, [1:   4]) = [  1.08295E+17 0.00151  7.51531E-03 0.00151 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 200002745 2.00000E+08 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.39444E+06 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 200002745 2.07394E+08 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 58410975 6.02720E+07 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 88225500 9.03427E+07 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 53366270 5.67798E+07 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 200002745 2.07394E+08 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.92821E-05 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  7.00000E+08 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  7.98045E+01 3.5E-09 ];
TOT_GENRATE               (idx, [1:   2]) = [  5.26668E+19 1.4E-07 ];
TOT_FISSRATE              (idx, [1:   2]) = [  2.16001E+19 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  1.44127E+19 9.5E-05 ];
TOT_ABSRATE               (idx, [1:   2]) = [  3.60128E+19 3.8E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  4.78170E+19 8.8E-05 ];
TOT_FLUX                  (idx, [1:   2]) = [  3.71611E+21 9.1E-05 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.35753E+19 0.00018 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.95880E+19 6.5E-05 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  2.12593E+21 9.6E-05 ];
INI_FMASS                 (idx, 1)        =  8.77144E+00 ;
TOT_FMASS                 (idx, 1)        =  8.77144E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06678E+00 4.4E-05 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.49785E-01 5.2E-05 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.07193E-01 4.0E-05 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09406E+00 3.4E-05 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.74579E-01 4.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.24503E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.53805E+00 7.1E-05 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.10140E+00 8.1E-05 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43827E+00 1.4E-07 ];
FISSE                     (idx, [1:   2]) = [  2.02271E+02 0.0E+00 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.10138E+00 8.2E-05  1.09322E+00 8.1E-05  8.17666E-03 0.00121 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.10138E+00 6.3E-05 ];
COL_KEFF                  (idx, [1:   2]) = [  1.10143E+00 8.8E-05 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.10138E+00 6.3E-05 ];
ABS_KINF                  (idx, [1:   2]) = [  1.53799E+00 3.9E-05 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93586E+01 1.5E-05 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93586E+01 6.3E-06 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.82931E-08 0.00028 ];
IMP_EALF                  (idx, [1:   2]) = [  7.82905E-08 0.00012 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.49813E-02 0.00148 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.49748E-02 0.00019 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.20304E-03 0.00097  2.18440E-04 0.00481  1.11608E-03 0.00215  1.06862E-03 0.00215  2.40641E-03 0.00149  9.82200E-04 0.00224  4.11287E-04 0.00353 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.68314E-01 0.00132  1.33360E-02 1.2E-06  3.27387E-02 1.2E-06  1.20781E-01 6.4E-07  3.02786E-01 1.6E-06  8.49515E-01 3.0E-06  2.85308E+00 4.7E-06 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.43484E-03 0.00124  2.65064E-04 0.00668  1.34425E-03 0.00291  1.29038E-03 0.00296  2.86036E-03 0.00197  1.18058E-03 0.00317  4.94203E-04 0.00481 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  4.68356E-01 0.00176  1.33361E-02 2.8E-06  3.27387E-02 1.7E-06  1.20780E-01 8.2E-07  3.02786E-01 2.2E-06  8.49518E-01 4.7E-06  2.85306E+00 5.8E-06 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  8.08301E-05 0.00019  8.08369E-05 0.00019  7.99313E-05 0.00187 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  8.90241E-05 0.00017  8.90315E-05 0.00017  8.80337E-05 0.00186 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.42003E-03 0.00124  2.66359E-04 0.00668  1.34430E-03 0.00284  1.28907E-03 0.00294  2.85069E-03 0.00191  1.17711E-03 0.00313  4.92495E-04 0.00487 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.67784E-01 0.00179  1.33360E-02 1.6E-06  3.27387E-02 1.9E-06  1.20781E-01 1.0E-06  3.02788E-01 2.8E-06  8.49517E-01 4.7E-06  2.85307E+00 6.0E-06 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  7.99700E-05 0.00321  7.99791E-05 0.00321  7.86962E-05 0.00572 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  8.80763E-05 0.00321  8.80863E-05 0.00321  8.66744E-05 0.00572 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.39723E-03 0.00515  2.61993E-04 0.02136  1.35291E-03 0.01000  1.28405E-03 0.01002  2.82425E-03 0.00747  1.17449E-03 0.01066  4.99535E-04 0.01632 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  4.70663E-01 0.00616  1.33360E-02 0.0E+00  3.27383E-02 1.1E-05  1.20780E-01 1.9E-06  3.02787E-01 9.5E-06  8.49545E-01 2.3E-05  2.85304E+00 1.1E-05 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.41191E-03 0.00504  2.60328E-04 0.02073  1.35732E-03 0.00959  1.29077E-03 0.00973  2.83201E-03 0.00721  1.17205E-03 0.01029  4.99440E-04 0.01553 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  4.69865E-01 0.00588  1.33360E-02 0.0E+00  3.27384E-02 8.0E-06  1.20780E-01 1.8E-06  3.02788E-01 1.0E-05  8.49544E-01 2.2E-05  2.85304E+00 1.3E-05 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -9.25025E+01 0.00406 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  8.08090E-05 0.00056 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  8.90010E-05 0.00056 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.44070E-03 0.00106 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -9.20692E+01 0.00086 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.07831E-06 8.3E-05 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.53118E-06 8.0E-05  4.53245E-06 8.0E-05  4.35950E-06 0.00086 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.13283E-04 0.00011  1.13300E-04 0.00011  1.11007E-04 0.00116 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.08230E-01 5.0E-05  7.07428E-01 5.1E-05  8.37512E-01 0.00140 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.10467E+01 0.00202 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.28467E+01 5.9E-05  4.56129E+01 6.9E-05 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   4]) = 'funi' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  2.80618E+05 0.00073  1.20075E+06 0.00036  2.43878E+06 0.00028  3.15539E+06 0.00027  2.74509E+06 0.00022  2.12421E+06 0.00028  1.60738E+06 0.00031  1.14123E+06 0.00032  8.42871E+05 0.00038  6.64056E+05 0.00041  5.56866E+05 0.00052  4.99884E+05 0.00042  4.56017E+05 0.00064  4.31574E+05 0.00054  4.18802E+05 0.00058  3.65103E+05 0.00064  3.58474E+05 0.00054  3.59568E+05 0.00065  3.55601E+05 0.00064  6.97189E+05 0.00043  6.70980E+05 0.00044  4.98102E+05 0.00054  3.20768E+05 0.00069  3.80286E+05 0.00053  3.70173E+05 0.00060  3.23508E+05 0.00058  5.99511E+05 0.00046  1.32138E+05 0.00102  1.65067E+05 0.00083  1.51019E+05 0.00098  8.51765E+04 0.00128  1.49537E+05 0.00080  9.98344E+04 0.00121  8.11204E+04 0.00104  1.45848E+04 0.00185  1.43677E+04 0.00210  1.49474E+04 0.00211  1.55778E+04 0.00224  1.54499E+04 0.00233  1.53244E+04 0.00208  1.59012E+04 0.00217  1.49919E+04 0.00230  2.83807E+04 0.00126  4.49833E+04 0.00129  5.69404E+04 0.00148  1.46385E+05 0.00093  1.45933E+05 0.00083  1.42310E+05 0.00070  8.16668E+04 0.00103  5.28752E+04 0.00094  3.73209E+04 0.00115  3.97636E+04 0.00134  6.73661E+04 0.00100  8.22882E+04 0.00088  1.53740E+05 0.00067  2.66647E+05 0.00052  5.58912E+05 0.00032  5.00098E+05 0.00031  4.41527E+05 0.00032  3.66032E+05 0.00036  3.65103E+05 0.00031  3.95906E+05 0.00031  3.60257E+05 0.00025  2.56617E+05 0.00033  2.46655E+05 0.00027  2.26186E+05 0.00032  1.92830E+05 0.00034  1.45496E+05 0.00030  8.49196E+04 0.00044  2.23374E+04 0.00065 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  2.00613E+00 6.7E-05 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.00345E+20 0.00010  6.25541E+19 3.8E-05 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.15867E-01 2.5E-05  6.13236E-01 2.4E-05 ];
INF_CAPT                  (idx, [1:   4]) = [  3.73645E-03 0.00019  5.65478E-02 4.0E-05 ];
INF_ABS                   (idx, [1:   4]) = [  9.87646E-03 0.00015  3.72372E-01 3.9E-05 ];
INF_FISS                  (idx, [1:   4]) = [  6.14001E-03 0.00017  3.15825E-01 3.9E-05 ];
INF_NSF                   (idx, [1:   4]) = [  1.50744E-02 0.00017  7.69570E-01 3.9E-05 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.45510E+00 1.9E-06  2.43670E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02276E+02 8.2E-09  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  5.01562E-08 0.00023  3.08335E-06 3.4E-05 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.05991E-01 2.9E-05  2.40898E-01 0.00010 ];
INF_SCATT1                (idx, [1:   4]) = [  3.03350E-02 0.00019  7.36851E-03 0.00247 ];
INF_SCATT2                (idx, [1:   4]) = [  1.56752E-02 0.00036  8.12652E-04 0.01717 ];
INF_SCATT3                (idx, [1:   4]) = [  3.77330E-03 0.00143  2.46236E-04 0.04630 ];
INF_SCATT4                (idx, [1:   4]) = [  2.87918E-03 0.00150  9.16538E-05 0.12716 ];
INF_SCATT5                (idx, [1:   4]) = [  1.26257E-03 0.00341  3.70653E-05 0.26840 ];
INF_SCATT6                (idx, [1:   4]) = [  6.74060E-04 0.00570  3.08844E-05 0.29124 ];
INF_SCATT7                (idx, [1:   4]) = [  1.56073E-04 0.02436  1.55703E-05 0.45157 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.06005E-01 2.9E-05  2.40898E-01 0.00010 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.03354E-02 0.00019  7.36851E-03 0.00247 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.56753E-02 0.00035  8.12652E-04 0.01717 ];
INF_SCATTP3               (idx, [1:   4]) = [  3.77333E-03 0.00143  2.46236E-04 0.04630 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.87918E-03 0.00150  9.16538E-05 0.12716 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.26259E-03 0.00341  3.70653E-05 0.26840 ];
INF_SCATTP6               (idx, [1:   4]) = [  6.74069E-04 0.00569  3.08844E-05 0.29124 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.56072E-04 0.02432  1.55703E-05 0.45157 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  1.52742E-01 6.8E-05  5.45982E-01 4.4E-05 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  2.18233E+00 6.8E-05  6.10520E-01 4.4E-05 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.86233E-03 0.00015  3.72372E-01 3.9E-05 ];
INF_REMXS                 (idx, [1:   4]) = [  1.04052E-02 0.00028  3.72548E-01 7.9E-05 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.05462E-01 2.9E-05  5.29251E-04 0.00139  2.09828E-04 0.00451  2.40688E-01 0.00011 ];
INF_S1                    (idx, [1:   8]) = [  3.04338E-02 0.00019 -9.87433E-05 0.00400  5.33140E-06 0.09003  7.36317E-03 0.00246 ];
INF_S2                    (idx, [1:   8]) = [  1.56905E-02 0.00036 -1.52332E-05 0.02061 -6.21882E-06 0.06510  8.18870E-04 0.01696 ];
INF_S3                    (idx, [1:   8]) = [  3.78667E-03 0.00141 -1.33670E-05 0.01884 -4.27338E-06 0.09053  2.50510E-04 0.04548 ];
INF_S4                    (idx, [1:   8]) = [  2.88534E-03 0.00148 -6.15926E-06 0.03212 -2.21461E-06 0.14706  9.38684E-05 0.12379 ];
INF_S5                    (idx, [1:   8]) = [  1.26234E-03 0.00341  2.31619E-07 0.78704 -1.56888E-06 0.17384  3.86341E-05 0.25804 ];
INF_S6                    (idx, [1:   8]) = [  6.72994E-04 0.00574  1.06545E-06 0.16497 -9.26985E-07 0.24248  3.18114E-05 0.28361 ];
INF_S7                    (idx, [1:   8]) = [  1.56247E-04 0.02415 -1.73318E-07 0.97357 -1.00675E-06 0.19197  1.65771E-05 0.42393 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.05476E-01 2.9E-05  5.29251E-04 0.00139  2.09828E-04 0.00451  2.40688E-01 0.00011 ];
INF_SP1                   (idx, [1:   8]) = [  3.04341E-02 0.00019 -9.87433E-05 0.00400  5.33140E-06 0.09003  7.36317E-03 0.00246 ];
INF_SP2                   (idx, [1:   8]) = [  1.56906E-02 0.00036 -1.52332E-05 0.02061 -6.21882E-06 0.06510  8.18870E-04 0.01696 ];
INF_SP3                   (idx, [1:   8]) = [  3.78669E-03 0.00141 -1.33670E-05 0.01884 -4.27338E-06 0.09053  2.50510E-04 0.04548 ];
INF_SP4                   (idx, [1:   8]) = [  2.88533E-03 0.00148 -6.15926E-06 0.03212 -2.21461E-06 0.14706  9.38684E-05 0.12379 ];
INF_SP5                   (idx, [1:   8]) = [  1.26235E-03 0.00341  2.31619E-07 0.78704 -1.56888E-06 0.17384  3.86341E-05 0.25804 ];
INF_SP6                   (idx, [1:   8]) = [  6.73004E-04 0.00573  1.06545E-06 0.16497 -9.26985E-07 0.24248  3.18114E-05 0.28361 ];
INF_SP7                   (idx, [1:   8]) = [  1.56246E-04 0.02411 -1.73318E-07 0.97357 -1.00675E-06 0.19197  1.65771E-05 0.42393 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  5.42796E-01 0.00038  3.54475E-01 0.00091 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  6.18964E-01 0.00047  4.83431E-01 0.00177 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  6.19003E-01 0.00050  4.83563E-01 0.00167 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  4.35580E-01 0.00063  2.31149E-01 0.00103 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  6.14108E-01 0.00038  9.40396E-01 0.00091 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  5.38540E-01 0.00047  6.89621E-01 0.00176 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  5.38507E-01 0.00050  6.89422E-01 0.00166 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  7.65279E-01 0.00063  1.44214E+00 0.00103 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.43484E-03 0.00124  2.65064E-04 0.00668  1.34425E-03 0.00291  1.29038E-03 0.00296  2.86036E-03 0.00197  1.18058E-03 0.00317  4.94203E-04 0.00481 ];
LAMBDA                    (idx, [1:  14]) = [  4.68356E-01 0.00176  1.33361E-02 2.8E-06  3.27387E-02 1.7E-06  1.20780E-01 8.2E-07  3.02786E-01 2.2E-06  8.49518E-01 4.7E-06  2.85306E+00 5.8E-06 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  71]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/keffComparison/notCoupled' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Fri May 17 18:21:22 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Fri May 17 20:07:31 2024' ;

% Run parameters:

POP                       (idx, 1)        = 200000 ;
CYCLES                    (idx, 1)        = 1000 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1715988082604 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 10 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:  10]) = [  1.33628E+00  9.73553E-01  9.64652E-01  9.51598E-01  9.37284E-01  9.87654E-01  9.73361E-01  9.36521E-01  9.41078E-01  9.98016E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 8.1E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.12112E-01 4.8E-05  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.87888E-01 3.4E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.84585E-01 2.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.58108E-01 2.6E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.32172E+00 4.7E-05  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.34165E-01 2.1E-06  6.37140E-02 3.0E-05  2.12095E-03 0.00011  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.31135E+01 5.8E-05  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.28467E+01 5.9E-05  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.39247E+01 6.3E-05  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.68157E+01 6.7E-05  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 1000 ;
SIMULATED_HISTORIES       (idx, 1)        = 200002745 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  2.00003E+05 0.00011 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  2.00003E+05 0.00011 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.01129E+03 ;
RUNNING_TIME              (idx, 1)        =  1.06155E+02 ;
INIT_TIME                 (idx, [1:   2]) = [  1.28433E-01  1.28433E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.58333E-03  4.58333E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.06022E+02  1.06022E+02  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.06153E+02  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 9.52657 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  9.53618E+00 0.00011 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.45422E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 4443.50 ;
MEMSIZE                   (idx, 1)        = 4285.44 ;
XS_MEMSIZE                (idx, 1)        = 2425.82 ;
MAT_MEMSIZE               (idx, 1)        = 513.22 ;
RES_MEMSIZE               (idx, 1)        = 10.70 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 1335.69 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 158.06 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 54 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 819781 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 16 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 52 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 52 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 1214 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.39085E+14 8.8E-05  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.20973E-03 0.00151 ];
U235_FISS                 (idx, [1:   4]) = [  2.15970E+19 7.6E-05  9.99884E-01 1.1E-06 ];
U238_FISS                 (idx, [1:   4]) = [  2.49628E+15 0.00985  1.15567E-04 0.00985 ];
U235_CAPT                 (idx, [1:   4]) = [  4.23602E+18 0.00024  2.93962E-01 0.00021 ];
U238_CAPT                 (idx, [1:   4]) = [  1.08295E+17 0.00151  7.51531E-03 0.00151 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 200002745 2.00000E+08 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.39444E+06 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 200002745 2.07394E+08 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 58410975 6.02720E+07 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 88225500 9.03427E+07 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 53366270 5.67798E+07 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 200002745 2.07394E+08 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.92821E-05 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  7.00000E+08 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  7.98045E+01 3.5E-09 ];
TOT_GENRATE               (idx, [1:   2]) = [  5.26668E+19 1.4E-07 ];
TOT_FISSRATE              (idx, [1:   2]) = [  2.16001E+19 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  1.44127E+19 9.5E-05 ];
TOT_ABSRATE               (idx, [1:   2]) = [  3.60128E+19 3.8E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  4.78170E+19 8.8E-05 ];
TOT_FLUX                  (idx, [1:   2]) = [  3.71611E+21 9.1E-05 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.35753E+19 0.00018 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.95880E+19 6.5E-05 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  2.12593E+21 9.6E-05 ];
INI_FMASS                 (idx, 1)        =  8.77144E+00 ;
TOT_FMASS                 (idx, 1)        =  8.77144E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06678E+00 4.4E-05 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.49785E-01 5.2E-05 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.07193E-01 4.0E-05 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09406E+00 3.4E-05 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.74579E-01 4.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.24503E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.53805E+00 7.1E-05 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.10140E+00 8.1E-05 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43827E+00 1.4E-07 ];
FISSE                     (idx, [1:   2]) = [  2.02271E+02 0.0E+00 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.10138E+00 8.2E-05  1.09322E+00 8.1E-05  8.17666E-03 0.00121 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.10138E+00 6.3E-05 ];
COL_KEFF                  (idx, [1:   2]) = [  1.10143E+00 8.8E-05 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.10138E+00 6.3E-05 ];
ABS_KINF                  (idx, [1:   2]) = [  1.53799E+00 3.9E-05 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93586E+01 1.5E-05 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93586E+01 6.3E-06 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.82931E-08 0.00028 ];
IMP_EALF                  (idx, [1:   2]) = [  7.82905E-08 0.00012 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.49813E-02 0.00148 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.49748E-02 0.00019 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.20304E-03 0.00097  2.18440E-04 0.00481  1.11608E-03 0.00215  1.06862E-03 0.00215  2.40641E-03 0.00149  9.82200E-04 0.00224  4.11287E-04 0.00353 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.68314E-01 0.00132  1.33360E-02 1.2E-06  3.27387E-02 1.2E-06  1.20781E-01 6.4E-07  3.02786E-01 1.6E-06  8.49515E-01 3.0E-06  2.85308E+00 4.7E-06 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.43484E-03 0.00124  2.65064E-04 0.00668  1.34425E-03 0.00291  1.29038E-03 0.00296  2.86036E-03 0.00197  1.18058E-03 0.00317  4.94203E-04 0.00481 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  4.68356E-01 0.00176  1.33361E-02 2.8E-06  3.27387E-02 1.7E-06  1.20780E-01 8.2E-07  3.02786E-01 2.2E-06  8.49518E-01 4.7E-06  2.85306E+00 5.8E-06 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  8.08301E-05 0.00019  8.08369E-05 0.00019  7.99313E-05 0.00187 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  8.90241E-05 0.00017  8.90315E-05 0.00017  8.80337E-05 0.00186 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.42003E-03 0.00124  2.66359E-04 0.00668  1.34430E-03 0.00284  1.28907E-03 0.00294  2.85069E-03 0.00191  1.17711E-03 0.00313  4.92495E-04 0.00487 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.67784E-01 0.00179  1.33360E-02 1.6E-06  3.27387E-02 1.9E-06  1.20781E-01 1.0E-06  3.02788E-01 2.8E-06  8.49517E-01 4.7E-06  2.85307E+00 6.0E-06 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  7.99700E-05 0.00321  7.99791E-05 0.00321  7.86962E-05 0.00572 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  8.80763E-05 0.00321  8.80863E-05 0.00321  8.66744E-05 0.00572 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.39723E-03 0.00515  2.61993E-04 0.02136  1.35291E-03 0.01000  1.28405E-03 0.01002  2.82425E-03 0.00747  1.17449E-03 0.01066  4.99535E-04 0.01632 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  4.70663E-01 0.00616  1.33360E-02 0.0E+00  3.27383E-02 1.1E-05  1.20780E-01 1.9E-06  3.02787E-01 9.5E-06  8.49545E-01 2.3E-05  2.85304E+00 1.1E-05 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.41191E-03 0.00504  2.60328E-04 0.02073  1.35732E-03 0.00959  1.29077E-03 0.00973  2.83201E-03 0.00721  1.17205E-03 0.01029  4.99440E-04 0.01553 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  4.69865E-01 0.00588  1.33360E-02 0.0E+00  3.27384E-02 8.0E-06  1.20780E-01 1.8E-06  3.02788E-01 1.0E-05  8.49544E-01 2.2E-05  2.85304E+00 1.3E-05 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -9.25025E+01 0.00406 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  8.08090E-05 0.00056 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  8.90010E-05 0.00056 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.44070E-03 0.00106 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -9.20692E+01 0.00086 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.07831E-06 8.3E-05 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.53118E-06 8.0E-05  4.53245E-06 8.0E-05  4.35950E-06 0.00086 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.13283E-04 0.00011  1.13300E-04 0.00011  1.11007E-04 0.00116 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.08230E-01 5.0E-05  7.07428E-01 5.1E-05  8.37512E-01 0.00140 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.10467E+01 0.00202 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.28467E+01 5.9E-05  4.56129E+01 6.9E-05 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   5]) = 'cFuni' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.52383E+04 0.00197  3.18617E+05 0.00102  6.46161E+05 0.00056  8.52450E+05 0.00056  7.46864E+05 0.00052  5.89044E+05 0.00051  4.58898E+05 0.00062  3.34228E+05 0.00064  2.53581E+05 0.00057  2.04206E+05 0.00069  1.74427E+05 0.00069  1.56623E+05 0.00091  1.44988E+05 0.00085  1.37448E+05 0.00086  1.34052E+05 0.00101  1.16901E+05 0.00088  1.15574E+05 0.00092  1.14830E+05 0.00102  1.13308E+05 0.00086  2.23211E+05 0.00062  2.17101E+05 0.00077  1.60566E+05 0.00079  1.04694E+05 0.00096  1.24691E+05 0.00093  1.22331E+05 0.00102  1.05943E+05 0.00104  1.96928E+05 0.00075  4.26308E+04 0.00148  5.29818E+04 0.00113  4.81009E+04 0.00142  2.74174E+04 0.00186  4.78164E+04 0.00145  3.20003E+04 0.00191  2.65660E+04 0.00179  4.92375E+03 0.00450  4.85552E+03 0.00372  5.02164E+03 0.00438  5.16158E+03 0.00412  5.16905E+03 0.00378  5.06651E+03 0.00374  5.21890E+03 0.00415  4.91158E+03 0.00434  9.30012E+03 0.00301  1.47266E+04 0.00279  1.85909E+04 0.00243  4.79228E+04 0.00130  4.81401E+04 0.00131  4.76994E+04 0.00142  2.78616E+04 0.00171  1.84395E+04 0.00306  1.32711E+04 0.00235  1.42190E+04 0.00227  2.41533E+04 0.00222  2.93265E+04 0.00162  5.42054E+04 0.00140  9.45943E+04 0.00124  2.02627E+05 0.00059  1.86239E+05 0.00074  1.68623E+05 0.00067  1.42694E+05 0.00082  1.45401E+05 0.00059  1.61945E+05 0.00083  1.52064E+05 0.00071  1.11695E+05 0.00089  1.11087E+05 0.00078  1.06562E+05 0.00094  9.63209E+04 0.00090  7.96137E+04 0.00092  5.43054E+04 0.00110  1.97402E+04 0.00182 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  8.78792E+19 0.00016  2.52334E+19 0.00026 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  1.66769E-01 6.3E-05  2.53812E-01 1.4E-05 ];
INF_CAPT                  (idx, [1:   4]) = [  5.84958E-04 0.00035  4.18975E-04 0.00012 ];
INF_ABS                   (idx, [1:   4]) = [  5.84958E-04 0.00035  4.18975E-04 0.00012 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  5.56705E-08 0.00037  3.33828E-06 0.00012 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  1.66186E-01 6.5E-05  2.53391E-01 1.5E-05 ];
INF_SCATT1                (idx, [1:   4]) = [  2.37578E-02 0.00054  2.34315E-02 0.00139 ];
INF_SCATT2                (idx, [1:   4]) = [  7.31517E-03 0.00114  3.87523E-03 0.00606 ];
INF_SCATT3                (idx, [1:   4]) = [  1.98955E-03 0.00368  1.24619E-03 0.01651 ];
INF_SCATT4                (idx, [1:   4]) = [  2.57264E-04 0.02719  5.02531E-04 0.02999 ];
INF_SCATT5                (idx, [1:   4]) = [  7.86804E-05 0.09171  2.68413E-04 0.06357 ];
INF_SCATT6                (idx, [1:   4]) = [  1.26916E-04 0.05378  1.65837E-04 0.08213 ];
INF_SCATT7                (idx, [1:   4]) = [  5.35888E-05 0.09027  1.16997E-04 0.10248 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  1.67873E-01 6.5E-05  2.53391E-01 1.5E-05 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42064E-02 0.00055  2.34315E-02 0.00139 ];
INF_SCATTP2               (idx, [1:   4]) = [  7.39864E-03 0.00115  3.87523E-03 0.00606 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.00241E-03 0.00364  1.24619E-03 0.01651 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.58305E-04 0.02736  5.02531E-04 0.02999 ];
INF_SCATTP5               (idx, [1:   4]) = [  7.79093E-05 0.09505  2.68413E-04 0.06357 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.26516E-04 0.05566  1.65837E-04 0.08213 ];
INF_SCATTP7               (idx, [1:   4]) = [  5.11158E-05 0.09873  1.16997E-04 0.10248 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  1.06309E-01 0.00021  2.29247E-01 0.00014 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  3.13551E+00 0.00021  1.45404E+00 0.00014 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [ -1.10224E-03 0.00171  4.18975E-04 0.00012 ];
INF_REMXS                 (idx, [1:   4]) = [  2.26909E-03 0.00103  5.35574E-04 0.00443 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  1.64500E-01 6.3E-05  1.68567E-03 0.00143  1.14633E-04 0.01030  2.53277E-01 1.6E-05 ];
INF_S1                    (idx, [1:   8]) = [  2.39664E-02 0.00054 -2.08617E-04 0.00593  4.40670E-05 0.01406  2.33875E-02 0.00140 ];
INF_S2                    (idx, [1:   8]) = [  7.38134E-03 0.00109 -6.61721E-05 0.01508  8.22252E-06 0.05448  3.86701E-03 0.00604 ];
INF_S3                    (idx, [1:   8]) = [  2.05906E-03 0.00351 -6.95138E-05 0.00832 -1.65500E-06 0.20913  1.24784E-03 0.01651 ];
INF_S4                    (idx, [1:   8]) = [  2.87458E-04 0.02467 -3.01937E-05 0.02203 -2.79380E-06 0.12103  5.05325E-04 0.02970 ];
INF_S5                    (idx, [1:   8]) = [  7.76681E-05 0.09309  1.01233E-06 0.56044 -2.24397E-06 0.14228  2.70657E-04 0.06304 ];
INF_S6                    (idx, [1:   8]) = [  1.20140E-04 0.05736  6.77663E-06 0.08609 -1.80527E-06 0.20131  1.67642E-04 0.08126 ];
INF_S7                    (idx, [1:   8]) = [  5.31832E-05 0.09092  4.05620E-07 1.00000 -1.10575E-06 0.26323  1.18103E-04 0.10132 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  1.66187E-01 6.3E-05  1.68567E-03 0.00143  1.14633E-04 0.01030  2.53277E-01 1.6E-05 ];
INF_SP1                   (idx, [1:   8]) = [  2.44150E-02 0.00054 -2.08617E-04 0.00593  4.40670E-05 0.01406  2.33875E-02 0.00140 ];
INF_SP2                   (idx, [1:   8]) = [  7.46482E-03 0.00111 -6.61721E-05 0.01508  8.22252E-06 0.05448  3.86701E-03 0.00604 ];
INF_SP3                   (idx, [1:   8]) = [  2.07193E-03 0.00347 -6.95138E-05 0.00832 -1.65500E-06 0.20913  1.24784E-03 0.01651 ];
INF_SP4                   (idx, [1:   8]) = [  2.88499E-04 0.02481 -3.01937E-05 0.02203 -2.79380E-06 0.12103  5.05325E-04 0.02970 ];
INF_SP5                   (idx, [1:   8]) = [  7.68970E-05 0.09655  1.01233E-06 0.56044 -2.24397E-06 0.14228  2.70657E-04 0.06304 ];
INF_SP6                   (idx, [1:   8]) = [  1.19740E-04 0.05936  6.77663E-06 0.08609 -1.80527E-06 0.20131  1.67642E-04 0.08126 ];
INF_SP7                   (idx, [1:   8]) = [  5.07102E-05 0.09956  4.05620E-07 1.00000 -1.10575E-06 0.26323  1.18103E-04 0.10132 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  1.15655E-01 0.00030  1.27851E-01 0.00090 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  1.31366E-01 0.00044  1.74423E-01 0.00167 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  1.31416E-01 0.00041  1.74652E-01 0.00159 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  9.33078E-02 0.00049  8.32994E-02 0.00099 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  2.88214E+00 0.00030  2.60731E+00 0.00090 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  2.53747E+00 0.00044  1.91132E+00 0.00166 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  2.53650E+00 0.00041  1.90879E+00 0.00159 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  3.57245E+00 0.00049  4.00182E+00 0.00099 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  71]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/keffComparison/notCoupled' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Fri May 17 18:21:22 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Fri May 17 20:07:31 2024' ;

% Run parameters:

POP                       (idx, 1)        = 200000 ;
CYCLES                    (idx, 1)        = 1000 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1715988082604 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 10 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:  10]) = [  1.33628E+00  9.73553E-01  9.64652E-01  9.51598E-01  9.37284E-01  9.87654E-01  9.73361E-01  9.36521E-01  9.41078E-01  9.98016E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 8.1E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.12112E-01 4.8E-05  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.87888E-01 3.4E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.84585E-01 2.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.58108E-01 2.6E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.32172E+00 4.7E-05  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.34165E-01 2.1E-06  6.37140E-02 3.0E-05  2.12095E-03 0.00011  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.31135E+01 5.8E-05  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.28467E+01 5.9E-05  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.39247E+01 6.3E-05  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.68157E+01 6.7E-05  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 1000 ;
SIMULATED_HISTORIES       (idx, 1)        = 200002745 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  2.00003E+05 0.00011 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  2.00003E+05 0.00011 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.01129E+03 ;
RUNNING_TIME              (idx, 1)        =  1.06155E+02 ;
INIT_TIME                 (idx, [1:   2]) = [  1.28433E-01  1.28433E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.58333E-03  4.58333E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.06022E+02  1.06022E+02  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.06153E+02  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 9.52658 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  9.53618E+00 0.00011 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.45422E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 4443.50 ;
MEMSIZE                   (idx, 1)        = 4285.44 ;
XS_MEMSIZE                (idx, 1)        = 2425.82 ;
MAT_MEMSIZE               (idx, 1)        = 513.22 ;
RES_MEMSIZE               (idx, 1)        = 10.70 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 1335.69 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 158.06 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 54 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 819781 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 16 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 52 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 52 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 1214 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.39085E+14 8.8E-05  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.20973E-03 0.00151 ];
U235_FISS                 (idx, [1:   4]) = [  2.15970E+19 7.6E-05  9.99884E-01 1.1E-06 ];
U238_FISS                 (idx, [1:   4]) = [  2.49628E+15 0.00985  1.15567E-04 0.00985 ];
U235_CAPT                 (idx, [1:   4]) = [  4.23602E+18 0.00024  2.93962E-01 0.00021 ];
U238_CAPT                 (idx, [1:   4]) = [  1.08295E+17 0.00151  7.51531E-03 0.00151 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 200002745 2.00000E+08 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.39444E+06 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 200002745 2.07394E+08 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 58410975 6.02720E+07 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 88225500 9.03427E+07 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 53366270 5.67798E+07 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 200002745 2.07394E+08 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.92821E-05 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  7.00000E+08 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  7.98045E+01 3.5E-09 ];
TOT_GENRATE               (idx, [1:   2]) = [  5.26668E+19 1.4E-07 ];
TOT_FISSRATE              (idx, [1:   2]) = [  2.16001E+19 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  1.44127E+19 9.5E-05 ];
TOT_ABSRATE               (idx, [1:   2]) = [  3.60128E+19 3.8E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  4.78170E+19 8.8E-05 ];
TOT_FLUX                  (idx, [1:   2]) = [  3.71611E+21 9.1E-05 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.35753E+19 0.00018 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.95880E+19 6.5E-05 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  2.12593E+21 9.6E-05 ];
INI_FMASS                 (idx, 1)        =  8.77144E+00 ;
TOT_FMASS                 (idx, 1)        =  8.77144E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06678E+00 4.4E-05 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.49785E-01 5.2E-05 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.07193E-01 4.0E-05 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09406E+00 3.4E-05 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.74579E-01 4.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.24503E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.53805E+00 7.1E-05 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.10140E+00 8.1E-05 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43827E+00 1.4E-07 ];
FISSE                     (idx, [1:   2]) = [  2.02271E+02 0.0E+00 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.10138E+00 8.2E-05  1.09322E+00 8.1E-05  8.17666E-03 0.00121 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.10138E+00 6.3E-05 ];
COL_KEFF                  (idx, [1:   2]) = [  1.10143E+00 8.8E-05 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.10138E+00 6.3E-05 ];
ABS_KINF                  (idx, [1:   2]) = [  1.53799E+00 3.9E-05 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93586E+01 1.5E-05 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93586E+01 6.3E-06 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.82931E-08 0.00028 ];
IMP_EALF                  (idx, [1:   2]) = [  7.82905E-08 0.00012 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.49813E-02 0.00148 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.49748E-02 0.00019 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.20304E-03 0.00097  2.18440E-04 0.00481  1.11608E-03 0.00215  1.06862E-03 0.00215  2.40641E-03 0.00149  9.82200E-04 0.00224  4.11287E-04 0.00353 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.68314E-01 0.00132  1.33360E-02 1.2E-06  3.27387E-02 1.2E-06  1.20781E-01 6.4E-07  3.02786E-01 1.6E-06  8.49515E-01 3.0E-06  2.85308E+00 4.7E-06 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.43484E-03 0.00124  2.65064E-04 0.00668  1.34425E-03 0.00291  1.29038E-03 0.00296  2.86036E-03 0.00197  1.18058E-03 0.00317  4.94203E-04 0.00481 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  4.68356E-01 0.00176  1.33361E-02 2.8E-06  3.27387E-02 1.7E-06  1.20780E-01 8.2E-07  3.02786E-01 2.2E-06  8.49518E-01 4.7E-06  2.85306E+00 5.8E-06 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  8.08301E-05 0.00019  8.08369E-05 0.00019  7.99313E-05 0.00187 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  8.90241E-05 0.00017  8.90315E-05 0.00017  8.80337E-05 0.00186 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.42003E-03 0.00124  2.66359E-04 0.00668  1.34430E-03 0.00284  1.28907E-03 0.00294  2.85069E-03 0.00191  1.17711E-03 0.00313  4.92495E-04 0.00487 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.67784E-01 0.00179  1.33360E-02 1.6E-06  3.27387E-02 1.9E-06  1.20781E-01 1.0E-06  3.02788E-01 2.8E-06  8.49517E-01 4.7E-06  2.85307E+00 6.0E-06 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  7.99700E-05 0.00321  7.99791E-05 0.00321  7.86962E-05 0.00572 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  8.80763E-05 0.00321  8.80863E-05 0.00321  8.66744E-05 0.00572 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.39723E-03 0.00515  2.61993E-04 0.02136  1.35291E-03 0.01000  1.28405E-03 0.01002  2.82425E-03 0.00747  1.17449E-03 0.01066  4.99535E-04 0.01632 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  4.70663E-01 0.00616  1.33360E-02 0.0E+00  3.27383E-02 1.1E-05  1.20780E-01 1.9E-06  3.02787E-01 9.5E-06  8.49545E-01 2.3E-05  2.85304E+00 1.1E-05 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.41191E-03 0.00504  2.60328E-04 0.02073  1.35732E-03 0.00959  1.29077E-03 0.00973  2.83201E-03 0.00721  1.17205E-03 0.01029  4.99440E-04 0.01553 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  4.69865E-01 0.00588  1.33360E-02 0.0E+00  3.27384E-02 8.0E-06  1.20780E-01 1.8E-06  3.02788E-01 1.0E-05  8.49544E-01 2.2E-05  2.85304E+00 1.3E-05 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -9.25025E+01 0.00406 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  8.08090E-05 0.00056 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  8.90010E-05 0.00056 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.44070E-03 0.00106 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -9.20692E+01 0.00086 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.07831E-06 8.3E-05 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.53118E-06 8.0E-05  4.53245E-06 8.0E-05  4.35950E-06 0.00086 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.13283E-04 0.00011  1.13300E-04 0.00011  1.11007E-04 0.00116 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.08230E-01 5.0E-05  7.07428E-01 5.1E-05  8.37512E-01 0.00140 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.10467E+01 0.00202 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.28467E+01 5.9E-05  4.56129E+01 6.9E-05 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   5]) = 'oPuni' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.12546E+05 0.00200  4.76223E+05 0.00086  9.62081E+05 0.00060  1.27927E+06 0.00050  1.12973E+06 0.00062  8.99188E+05 0.00066  7.10521E+05 0.00074  5.26991E+05 0.00093  4.05745E+05 0.00098  3.30104E+05 0.00122  2.84112E+05 0.00107  2.56264E+05 0.00108  2.38113E+05 0.00125  2.26061E+05 0.00103  2.19825E+05 0.00094  1.91821E+05 0.00116  1.90289E+05 0.00108  1.88633E+05 0.00133  1.86732E+05 0.00131  3.68002E+05 0.00088  3.58984E+05 0.00095  2.65679E+05 0.00101  1.73439E+05 0.00121  2.07514E+05 0.00113  2.04050E+05 0.00118  1.77018E+05 0.00112  3.28164E+05 0.00080  7.06597E+04 0.00183  8.81573E+04 0.00173  7.94067E+04 0.00174  4.54762E+04 0.00260  7.86591E+04 0.00180  5.28148E+04 0.00248  4.40234E+04 0.00226  8.35877E+03 0.00598  8.12030E+03 0.00665  8.41525E+03 0.00520  8.70807E+03 0.00602  8.60468E+03 0.00534  8.41806E+03 0.00567  8.72988E+03 0.00552  8.07164E+03 0.00752  1.54906E+04 0.00387  2.45042E+04 0.00367  3.08688E+04 0.00353  7.99288E+04 0.00194  8.02802E+04 0.00174  8.00897E+04 0.00220  4.73508E+04 0.00242  3.13405E+04 0.00305  2.27299E+04 0.00317  2.44172E+04 0.00297  4.14243E+04 0.00284  4.99899E+04 0.00233  9.24713E+04 0.00166  1.61539E+05 0.00130  3.47450E+05 0.00115  3.21508E+05 0.00094  2.92732E+05 0.00082  2.48642E+05 0.00124  2.54518E+05 0.00096  2.84159E+05 0.00092  2.68664E+05 0.00081  1.98151E+05 0.00105  1.98557E+05 0.00083  1.91267E+05 0.00105  1.75029E+05 0.00092  1.45511E+05 0.00105  1.00801E+05 0.00114  3.78241E+04 0.00163 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.38365E+20 0.00018  4.41883E+19 0.00027 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  4.10340E-02 0.00012  1.00468E-01 5.2E-05 ];
INF_CAPT                  (idx, [1:   4]) = [  1.57377E-05 0.00046  9.11230E-04 0.00011 ];
INF_ABS                   (idx, [1:   4]) = [  1.57377E-05 0.00046  9.11230E-04 0.00011 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  5.86794E-08 0.00045  3.38373E-06 0.00011 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.10102E-02 0.00016  9.95555E-02 5.8E-05 ];
INF_SCATT1                (idx, [1:   4]) = [  2.72506E-02 0.00020  4.16730E-02 0.00024 ];
INF_SCATT2                (idx, [1:   4]) = [  1.03487E-02 0.00048  1.59016E-02 0.00067 ];
INF_SCATT3                (idx, [1:   4]) = [  1.90501E-04 0.02163  6.11831E-03 0.00174 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.71832E-03 0.00193  2.59371E-03 0.00342 ];
INF_SCATT5                (idx, [1:   4]) = [ -1.73860E-04 0.01605  1.32650E-03 0.00808 ];
INF_SCATT6                (idx, [1:   4]) = [  5.74649E-04 0.00422  8.23943E-04 0.00906 ];
INF_SCATT7                (idx, [1:   4]) = [  9.28728E-05 0.02627  5.90884E-04 0.01223 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.10102E-02 0.00016  9.95555E-02 5.8E-05 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.72506E-02 0.00020  4.16730E-02 0.00024 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.03487E-02 0.00048  1.59016E-02 0.00067 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.90501E-04 0.02163  6.11831E-03 0.00174 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.71832E-03 0.00193  2.59371E-03 0.00342 ];
INF_SCATTP5               (idx, [1:   4]) = [ -1.73860E-04 0.01605  1.32650E-03 0.00808 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.74649E-04 0.00422  8.23943E-04 0.00906 ];
INF_SCATTP7               (idx, [1:   4]) = [  9.28728E-05 0.02627  5.90884E-04 0.01223 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  6.85768E-03 0.00039  5.09324E-02 0.00025 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  4.86077E+01 0.00039  6.54465E+00 0.00025 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.57377E-05 0.00046  9.11230E-04 0.00011 ];
INF_REMXS                 (idx, [1:   4]) = [  2.34230E-03 0.00181  9.26288E-04 0.00265 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  3.86917E-02 0.00017  2.31852E-03 0.00112  1.40399E-05 0.01724  9.95414E-02 5.8E-05 ];
INF_S1                    (idx, [1:   8]) = [  2.63043E-02 0.00021  9.46355E-04 0.00139  1.29116E-05 0.01701  4.16601E-02 0.00024 ];
INF_S2                    (idx, [1:   8]) = [  1.05429E-02 0.00045 -1.94204E-04 0.00395  1.09118E-05 0.01691  1.58906E-02 0.00067 ];
INF_S3                    (idx, [1:   8]) = [  6.28712E-04 0.00649 -4.38211E-04 0.00180  8.45738E-06 0.01773  6.10985E-03 0.00174 ];
INF_S4                    (idx, [1:   8]) = [ -1.52768E-03 0.00224 -1.90640E-04 0.00344  5.98259E-06 0.02096  2.58772E-03 0.00342 ];
INF_S5                    (idx, [1:   8]) = [ -1.87286E-04 0.01525  1.34265E-05 0.04119  3.81667E-06 0.02924  1.32268E-03 0.00809 ];
INF_S6                    (idx, [1:   8]) = [  5.32409E-04 0.00458  4.22397E-05 0.01085  2.12890E-06 0.04883  8.21815E-04 0.00906 ];
INF_S7                    (idx, [1:   8]) = [  8.32582E-05 0.03012  9.61466E-06 0.04659  9.42717E-07 0.10389  5.89942E-04 0.01218 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  3.86917E-02 0.00017  2.31852E-03 0.00112  1.40399E-05 0.01724  9.95414E-02 5.8E-05 ];
INF_SP1                   (idx, [1:   8]) = [  2.63043E-02 0.00021  9.46355E-04 0.00139  1.29116E-05 0.01701  4.16601E-02 0.00024 ];
INF_SP2                   (idx, [1:   8]) = [  1.05429E-02 0.00045 -1.94204E-04 0.00395  1.09118E-05 0.01691  1.58906E-02 0.00067 ];
INF_SP3                   (idx, [1:   8]) = [  6.28712E-04 0.00649 -4.38211E-04 0.00180  8.45738E-06 0.01773  6.10985E-03 0.00174 ];
INF_SP4                   (idx, [1:   8]) = [ -1.52768E-03 0.00224 -1.90640E-04 0.00344  5.98259E-06 0.02096  2.58772E-03 0.00342 ];
INF_SP5                   (idx, [1:   8]) = [ -1.87286E-04 0.01525  1.34265E-05 0.04119  3.81667E-06 0.02924  1.32268E-03 0.00809 ];
INF_SP6                   (idx, [1:   8]) = [  5.32409E-04 0.00458  4.22397E-05 0.01085  2.12890E-06 0.04883  8.21815E-04 0.00906 ];
INF_SP7                   (idx, [1:   8]) = [  8.32582E-05 0.03012  9.61466E-06 0.04659  9.42717E-07 0.10389  5.89942E-04 0.01218 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  1.59682E-01 0.00034  2.07836E-01 0.00077 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  1.81603E-01 0.00043  2.83116E-01 0.00156 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  1.81625E-01 0.00038  2.83296E-01 0.00143 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  1.28619E-01 0.00055  1.35652E-01 0.00092 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  2.08750E+00 0.00034  1.60388E+00 0.00077 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.83552E+00 0.00043  1.17751E+00 0.00155 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.83529E+00 0.00038  1.17674E+00 0.00143 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  2.59167E+00 0.00055  2.45737E+00 0.00092 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  71]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/keffComparison/notCoupled' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Fri May 17 18:21:22 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Fri May 17 20:07:31 2024' ;

% Run parameters:

POP                       (idx, 1)        = 200000 ;
CYCLES                    (idx, 1)        = 1000 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1715988082604 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 10 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:  10]) = [  1.33628E+00  9.73553E-01  9.64652E-01  9.51598E-01  9.37284E-01  9.87654E-01  9.73361E-01  9.36521E-01  9.41078E-01  9.98016E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 8.1E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.12112E-01 4.8E-05  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.87888E-01 3.4E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.84585E-01 2.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.58108E-01 2.6E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.32172E+00 4.7E-05  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.34165E-01 2.1E-06  6.37140E-02 3.0E-05  2.12095E-03 0.00011  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.31135E+01 5.8E-05  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.28467E+01 5.9E-05  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.39247E+01 6.3E-05  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.68157E+01 6.7E-05  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 1000 ;
SIMULATED_HISTORIES       (idx, 1)        = 200002745 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  2.00003E+05 0.00011 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  2.00003E+05 0.00011 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.01129E+03 ;
RUNNING_TIME              (idx, 1)        =  1.06155E+02 ;
INIT_TIME                 (idx, [1:   2]) = [  1.28433E-01  1.28433E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.58333E-03  4.58333E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.06022E+02  1.06022E+02  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.06153E+02  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 9.52658 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  9.53618E+00 0.00011 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.45422E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 4443.50 ;
MEMSIZE                   (idx, 1)        = 4285.44 ;
XS_MEMSIZE                (idx, 1)        = 2425.82 ;
MAT_MEMSIZE               (idx, 1)        = 513.22 ;
RES_MEMSIZE               (idx, 1)        = 10.70 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 1335.69 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 158.06 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 54 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 819781 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 16 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 52 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 52 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 1214 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.39085E+14 8.8E-05  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.20973E-03 0.00151 ];
U235_FISS                 (idx, [1:   4]) = [  2.15970E+19 7.6E-05  9.99884E-01 1.1E-06 ];
U238_FISS                 (idx, [1:   4]) = [  2.49628E+15 0.00985  1.15567E-04 0.00985 ];
U235_CAPT                 (idx, [1:   4]) = [  4.23602E+18 0.00024  2.93962E-01 0.00021 ];
U238_CAPT                 (idx, [1:   4]) = [  1.08295E+17 0.00151  7.51531E-03 0.00151 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 200002745 2.00000E+08 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.39444E+06 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 200002745 2.07394E+08 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 58410975 6.02720E+07 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 88225500 9.03427E+07 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 53366270 5.67798E+07 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 200002745 2.07394E+08 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.92821E-05 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  7.00000E+08 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  7.98045E+01 3.5E-09 ];
TOT_GENRATE               (idx, [1:   2]) = [  5.26668E+19 1.4E-07 ];
TOT_FISSRATE              (idx, [1:   2]) = [  2.16001E+19 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  1.44127E+19 9.5E-05 ];
TOT_ABSRATE               (idx, [1:   2]) = [  3.60128E+19 3.8E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  4.78170E+19 8.8E-05 ];
TOT_FLUX                  (idx, [1:   2]) = [  3.71611E+21 9.1E-05 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.35753E+19 0.00018 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.95880E+19 6.5E-05 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  2.12593E+21 9.6E-05 ];
INI_FMASS                 (idx, 1)        =  8.77144E+00 ;
TOT_FMASS                 (idx, 1)        =  8.77144E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06678E+00 4.4E-05 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.49785E-01 5.2E-05 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.07193E-01 4.0E-05 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09406E+00 3.4E-05 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.74579E-01 4.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.24503E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.53805E+00 7.1E-05 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.10140E+00 8.1E-05 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43827E+00 1.4E-07 ];
FISSE                     (idx, [1:   2]) = [  2.02271E+02 0.0E+00 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.10138E+00 8.2E-05  1.09322E+00 8.1E-05  8.17666E-03 0.00121 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.10138E+00 6.3E-05 ];
COL_KEFF                  (idx, [1:   2]) = [  1.10143E+00 8.8E-05 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.10138E+00 6.3E-05 ];
ABS_KINF                  (idx, [1:   2]) = [  1.53799E+00 3.9E-05 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93586E+01 1.5E-05 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93586E+01 6.3E-06 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.82931E-08 0.00028 ];
IMP_EALF                  (idx, [1:   2]) = [  7.82905E-08 0.00012 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.49813E-02 0.00148 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.49748E-02 0.00019 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.20304E-03 0.00097  2.18440E-04 0.00481  1.11608E-03 0.00215  1.06862E-03 0.00215  2.40641E-03 0.00149  9.82200E-04 0.00224  4.11287E-04 0.00353 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.68314E-01 0.00132  1.33360E-02 1.2E-06  3.27387E-02 1.2E-06  1.20781E-01 6.4E-07  3.02786E-01 1.6E-06  8.49515E-01 3.0E-06  2.85308E+00 4.7E-06 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.43484E-03 0.00124  2.65064E-04 0.00668  1.34425E-03 0.00291  1.29038E-03 0.00296  2.86036E-03 0.00197  1.18058E-03 0.00317  4.94203E-04 0.00481 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  4.68356E-01 0.00176  1.33361E-02 2.8E-06  3.27387E-02 1.7E-06  1.20780E-01 8.2E-07  3.02786E-01 2.2E-06  8.49518E-01 4.7E-06  2.85306E+00 5.8E-06 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  8.08301E-05 0.00019  8.08369E-05 0.00019  7.99313E-05 0.00187 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  8.90241E-05 0.00017  8.90315E-05 0.00017  8.80337E-05 0.00186 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.42003E-03 0.00124  2.66359E-04 0.00668  1.34430E-03 0.00284  1.28907E-03 0.00294  2.85069E-03 0.00191  1.17711E-03 0.00313  4.92495E-04 0.00487 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.67784E-01 0.00179  1.33360E-02 1.6E-06  3.27387E-02 1.9E-06  1.20781E-01 1.0E-06  3.02788E-01 2.8E-06  8.49517E-01 4.7E-06  2.85307E+00 6.0E-06 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  7.99700E-05 0.00321  7.99791E-05 0.00321  7.86962E-05 0.00572 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  8.80763E-05 0.00321  8.80863E-05 0.00321  8.66744E-05 0.00572 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.39723E-03 0.00515  2.61993E-04 0.02136  1.35291E-03 0.01000  1.28405E-03 0.01002  2.82425E-03 0.00747  1.17449E-03 0.01066  4.99535E-04 0.01632 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  4.70663E-01 0.00616  1.33360E-02 0.0E+00  3.27383E-02 1.1E-05  1.20780E-01 1.9E-06  3.02787E-01 9.5E-06  8.49545E-01 2.3E-05  2.85304E+00 1.1E-05 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.41191E-03 0.00504  2.60328E-04 0.02073  1.35732E-03 0.00959  1.29077E-03 0.00973  2.83201E-03 0.00721  1.17205E-03 0.01029  4.99440E-04 0.01553 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  4.69865E-01 0.00588  1.33360E-02 0.0E+00  3.27384E-02 8.0E-06  1.20780E-01 1.8E-06  3.02788E-01 1.0E-05  8.49544E-01 2.2E-05  2.85304E+00 1.3E-05 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -9.25025E+01 0.00406 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  8.08090E-05 0.00056 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  8.90010E-05 0.00056 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.44070E-03 0.00106 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -9.20692E+01 0.00086 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.07831E-06 8.3E-05 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.53118E-06 8.0E-05  4.53245E-06 8.0E-05  4.35950E-06 0.00086 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.13283E-04 0.00011  1.13300E-04 0.00011  1.11007E-04 0.00116 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.08230E-01 5.0E-05  7.07428E-01 5.1E-05  8.37512E-01 0.00140 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.10467E+01 0.00202 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.28467E+01 5.9E-05  4.56129E+01 6.9E-05 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   4]) = 'muni' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.09380E+05 0.00062  2.96095E+06 0.00029  5.94737E+06 0.00020  8.00737E+06 0.00014  7.23527E+06 0.00012  5.92171E+06 0.00013  4.81545E+06 0.00013  3.71259E+06 0.00012  2.93996E+06 0.00015  2.45159E+06 0.00014  2.13822E+06 0.00011  1.93163E+06 0.00014  1.80479E+06 0.00012  1.72084E+06 0.00012  1.68161E+06 0.00012  1.46510E+06 0.00012  1.45710E+06 0.00014  1.44379E+06 0.00016  1.43124E+06 0.00013  2.83056E+06 0.00011  2.78457E+06 9.2E-05  2.06288E+06 0.00012  1.35847E+06 0.00014  1.63561E+06 0.00013  1.61386E+06 0.00014  1.39385E+06 0.00013  2.58495E+06 0.00012  5.48960E+05 0.00018  6.80012E+05 0.00017  6.10534E+05 0.00020  3.53033E+05 0.00027  6.06447E+05 0.00019  4.08524E+05 0.00026  3.47962E+05 0.00027  6.67885E+04 0.00060  6.58629E+04 0.00054  6.75054E+04 0.00057  6.93372E+04 0.00059  6.84773E+04 0.00065  6.73373E+04 0.00068  6.90519E+04 0.00065  6.47813E+04 0.00058  1.21967E+05 0.00052  1.93387E+05 0.00036  2.44208E+05 0.00032  6.31017E+05 0.00018  6.37918E+05 0.00017  6.42005E+05 0.00022  3.86471E+05 0.00022  2.60937E+05 0.00027  1.89742E+05 0.00031  2.05542E+05 0.00040  3.48591E+05 0.00027  4.21871E+05 0.00024  7.83662E+05 0.00020  1.39420E+06 0.00016  3.09380E+06 0.00013  2.94774E+06 0.00014  2.73957E+06 0.00014  2.37249E+06 0.00017  2.46897E+06 0.00015  2.80798E+06 0.00015  2.70499E+06 0.00013  2.03559E+06 0.00013  2.07361E+06 0.00016  2.04532E+06 0.00015  1.92022E+06 0.00013  1.65856E+06 0.00015  1.20509E+06 0.00014  4.81154E+05 0.00016 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  9.72191E+20 7.5E-05  4.28273E+20 8.7E-05 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.74039E-01 2.5E-05  1.24687E+00 7.0E-06 ];
INF_CAPT                  (idx, [1:   4]) = [  2.39223E-04 7.3E-05  1.17912E-02 1.7E-05 ];
INF_ABS                   (idx, [1:   4]) = [  2.39223E-04 7.3E-05  1.17912E-02 1.7E-05 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  6.52863E-08 5.8E-05  3.56402E-06 1.7E-05 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.73799E-01 2.5E-05  1.23508E+00 7.8E-06 ];
INF_SCATT1                (idx, [1:   4]) = [  3.28364E-01 2.9E-05  4.63447E-01 2.4E-05 ];
INF_SCATT2                (idx, [1:   4]) = [  1.24048E-01 5.0E-05  1.73686E-01 7.7E-05 ];
INF_SCATT3                (idx, [1:   4]) = [  2.86224E-03 0.00168  6.77731E-02 0.00016 ];
INF_SCATT4                (idx, [1:   4]) = [ -2.00814E-02 0.00022  2.96754E-02 0.00031 ];
INF_SCATT5                (idx, [1:   4]) = [ -2.11546E-03 0.00197  1.56460E-02 0.00067 ];
INF_SCATT6                (idx, [1:   4]) = [  6.72741E-03 0.00063  9.95206E-03 0.00082 ];
INF_SCATT7                (idx, [1:   4]) = [  1.16499E-03 0.00307  7.17776E-03 0.00101 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.73800E-01 2.5E-05  1.23508E+00 7.8E-06 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.28364E-01 2.9E-05  4.63447E-01 2.4E-05 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.24048E-01 5.0E-05  1.73686E-01 7.7E-05 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.86224E-03 0.00168  6.77731E-02 0.00016 ];
INF_SCATTP4               (idx, [1:   4]) = [ -2.00814E-02 0.00022  2.96754E-02 0.00031 ];
INF_SCATTP5               (idx, [1:   4]) = [ -2.11545E-03 0.00197  1.56460E-02 0.00067 ];
INF_SCATTP6               (idx, [1:   4]) = [  6.72741E-03 0.00063  9.95206E-03 0.00082 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.16499E-03 0.00307  7.17776E-03 0.00101 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  1.64634E-01 6.3E-05  6.95413E-01 2.2E-05 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  2.02470E+00 6.3E-05  4.79332E-01 2.2E-05 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  2.39207E-04 7.6E-05  1.17912E-02 1.7E-05 ];
INF_REMXS                 (idx, [1:   4]) = [  2.93837E-02 5.2E-05  1.19630E-02 0.00021 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.44655E-01 2.5E-05  2.91446E-02 5.4E-05  1.73383E-04 0.00221  1.23490E+00 7.8E-06 ];
INF_S1                    (idx, [1:   8]) = [  3.16829E-01 2.9E-05  1.15352E-02 0.00010  1.25888E-04 0.00235  4.63321E-01 2.4E-05 ];
INF_S2                    (idx, [1:   8]) = [  1.26423E-01 4.8E-05 -2.37490E-03 0.00040  9.51490E-05 0.00248  1.73591E-01 7.8E-05 ];
INF_S3                    (idx, [1:   8]) = [  8.24936E-03 0.00056 -5.38712E-03 0.00014  7.11611E-05 0.00289  6.77020E-02 0.00016 ];
INF_S4                    (idx, [1:   8]) = [ -1.77192E-02 0.00025 -2.36214E-03 0.00033  4.94230E-05 0.00339  2.96260E-02 0.00032 ];
INF_S5                    (idx, [1:   8]) = [ -2.26148E-03 0.00179  1.46019E-04 0.00513  3.08118E-05 0.00463  1.56152E-02 0.00067 ];
INF_S6                    (idx, [1:   8]) = [  6.21618E-03 0.00066  5.11222E-04 0.00141  1.63572E-05 0.00845  9.93571E-03 0.00082 ];
INF_S7                    (idx, [1:   8]) = [  1.04448E-03 0.00339  1.20515E-04 0.00532  6.40080E-06 0.01797  7.17136E-03 0.00101 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.44655E-01 2.5E-05  2.91446E-02 5.4E-05  1.73383E-04 0.00221  1.23490E+00 7.8E-06 ];
INF_SP1                   (idx, [1:   8]) = [  3.16829E-01 2.9E-05  1.15352E-02 0.00010  1.25888E-04 0.00235  4.63321E-01 2.4E-05 ];
INF_SP2                   (idx, [1:   8]) = [  1.26423E-01 4.8E-05 -2.37490E-03 0.00040  9.51490E-05 0.00248  1.73591E-01 7.8E-05 ];
INF_SP3                   (idx, [1:   8]) = [  8.24936E-03 0.00056 -5.38712E-03 0.00014  7.11611E-05 0.00289  6.77020E-02 0.00016 ];
INF_SP4                   (idx, [1:   8]) = [ -1.77192E-02 0.00025 -2.36214E-03 0.00033  4.94230E-05 0.00339  2.96260E-02 0.00032 ];
INF_SP5                   (idx, [1:   8]) = [ -2.26147E-03 0.00179  1.46019E-04 0.00513  3.08118E-05 0.00463  1.56152E-02 0.00067 ];
INF_SP6                   (idx, [1:   8]) = [  6.21618E-03 0.00066  5.11222E-04 0.00141  1.63572E-05 0.00845  9.93571E-03 0.00082 ];
INF_SP7                   (idx, [1:   8]) = [  1.04448E-03 0.00339  1.20515E-04 0.00532  6.40080E-06 0.01797  7.17136E-03 0.00101 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  1.16012E-01 9.6E-05  3.85545E-01 0.00022 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  1.22609E-01 0.00014  4.30552E-01 0.00055 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  1.22620E-01 0.00014  4.31434E-01 0.00045 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  1.04734E-01 0.00015  3.18402E-01 0.00036 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  2.87326E+00 9.6E-05  8.64579E-01 0.00022 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  2.71868E+00 0.00014  7.74212E-01 0.00055 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  2.71843E+00 0.00014  7.72626E-01 0.00045 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  3.18268E+00 0.00015  1.04690E+00 0.00036 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

