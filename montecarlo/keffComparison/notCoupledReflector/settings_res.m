
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/keffComparison/notCoupledReflector' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Mon Apr 29 18:00:27 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Mon Apr 29 18:02:59 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1714431627003 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 1 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
SHARE_BUF_ARRAY           (idx, 1)        = 1 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.24595E-01 0.00069  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.75405E-01 0.00051  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.78360E-01 0.00033  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.69741E-01 0.00040  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.33456E+00 0.00058  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.36548E-01 3.0E-05  6.14973E-02 0.00045  1.95475E-03 0.00132  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.92027E+01 0.00099  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.89553E+01 0.00099  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.69696E+01 0.00088  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.72076E+01 0.00084  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 1000105 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00010E+04 0.00150 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00010E+04 0.00150 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.54594E+00 ;
RUNNING_TIME              (idx, 1)        =  2.54662E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  2.70233E-01  2.70233E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.00000E-03  7.00000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.26938E+00  2.26938E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.54632E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.99974 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  9.99972E-01 3.6E-05 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.39384E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 3746.92 ;
MEMSIZE                   (idx, 1)        = 3696.39 ;
XS_MEMSIZE                (idx, 1)        = 3030.45 ;
MAT_MEMSIZE               (idx, 1)        = 593.42 ;
RES_MEMSIZE               (idx, 1)        = 5.50 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 67.02 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 50.54 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 54 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 948120 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 20 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 53 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 53 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 1698 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  6.66817E+15 0.00107  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.20786E-03 0.01939 ];
U235_FISS                 (idx, [1:   4]) = [  3.08365E+19 0.00099  9.99875E-01 1.6E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.85457E+15 0.12526  1.25464E-04 0.12543 ];
U235_CAPT                 (idx, [1:   4]) = [  6.00677E+18 0.00328  2.88377E-01 0.00260 ];
U238_CAPT                 (idx, [1:   4]) = [  1.54452E+17 0.01948  7.41312E-03 0.01935 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 1000105 1.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.93066E+04 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 1000105 1.03931E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 302023 3.12368E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 450694 4.62532E+05 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 247388 2.64406E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 1000105 1.03931E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.59489E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.00000E+09 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.13889E+02 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  7.52146E+19 1.7E-06 ];
TOT_FISSRATE              (idx, [1:   2]) = [  3.08572E+19 7.0E-09 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.08446E+19 0.00117 ];
TOT_ABSRATE               (idx, [1:   2]) = [  5.17018E+19 0.00047 ];
TOT_SRCRATE               (idx, [1:   2]) = [  6.66817E+19 0.00107 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.74850E+21 0.00123 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.76322E+19 0.00241 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  6.93340E+19 0.00077 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.41180E+21 0.00139 ];
INI_FMASS                 (idx, 1)        =  8.78052E+00 ;
TOT_FMASS                 (idx, 1)        =  8.78052E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06731E+00 0.00063 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.43951E-01 0.00067 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.13008E-01 0.00054 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09154E+00 0.00047 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.03684E-01 0.00050 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.15276E-01 0.00040 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.53270E+00 0.00099 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.12743E+00 0.00107 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43750E+00 1.7E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02271E+02 7.8E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.12769E+00 0.00107  1.11889E+00 0.00108  8.54617E-03 0.01665 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.12771E+00 0.00074 ];
COL_KEFF                  (idx, [1:   2]) = [  1.12809E+00 0.00107 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.12771E+00 0.00074 ];
ABS_KINF                  (idx, [1:   2]) = [  1.53290E+00 0.00047 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93799E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93781E+01 9.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.66971E-08 0.00405 ];
IMP_EALF                  (idx, [1:   2]) = [  7.67925E-08 0.00187 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.49153E-02 0.02047 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.47001E-02 0.00260 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.25171E-03 0.01221  2.06791E-04 0.08419  9.68906E-04 0.03305  5.70877E-04 0.04142  1.26434E-03 0.02876  1.98477E-03 0.02022  5.62768E-04 0.04033  5.31306E-04 0.03989  1.61952E-04 0.07740 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.19768E-01 0.01894  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.2E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.5E-09  1.63478E+00 0.0E+00  3.55460E+00 5.8E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.68581E-03 0.01756  2.43120E-04 0.10572  1.18939E-03 0.05340  7.46828E-04 0.05661  1.55365E-03 0.03910  2.36348E-03 0.03105  7.43194E-04 0.05501  6.69891E-04 0.06306  1.76262E-04 0.10411 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.17090E-01 0.02722  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.0E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.8E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  8.82358E-05 0.00304  8.82857E-05 0.00303  8.21027E-05 0.02644 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  9.94910E-05 0.00285  9.95473E-05 0.00285  9.25621E-05 0.02633 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.55957E-03 0.01720  2.47402E-04 0.10642  1.20109E-03 0.04967  7.82544E-04 0.05562  1.48736E-03 0.04055  2.31711E-03 0.02962  7.16606E-04 0.05858  6.29467E-04 0.05886  1.77979E-04 0.10991 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08987E-01 0.02952  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.2E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.6E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  8.42664E-05 0.02418  8.43027E-05 0.02419  7.57347E-05 0.06833 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  9.50299E-05 0.02412  9.50718E-05 0.02414  8.52823E-05 0.06790 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  7.05636E-03 0.06679  1.36965E-04 0.39584  1.05224E-03 0.13398  8.41164E-04 0.15796  1.50563E-03 0.11874  1.95341E-03 0.12936  6.65014E-04 0.17788  6.44086E-04 0.18971  2.57844E-04 0.33638 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.70643E-01 0.09770  1.24667E-02 3.9E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  7.13771E-03 0.06593  1.55038E-04 0.35940  1.07081E-03 0.12644  7.79117E-04 0.15912  1.53778E-03 0.11224  1.94960E-03 0.12871  7.07102E-04 0.17448  6.79272E-04 0.18312  2.59005E-04 0.33198 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.77766E-01 0.09826  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.1E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -8.34870E+01 0.06030 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  8.90768E-05 0.00165 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  1.00439E-04 0.00119 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.60720E-03 0.00849 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -8.54599E+01 0.00914 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.24184E-06 0.00137 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.79587E-06 0.00103  4.79833E-06 0.00103  4.47210E-06 0.01088 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.39478E-04 0.00192  1.39576E-04 0.00193  1.26735E-04 0.02133 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.39797E-01 0.00062  7.38937E-01 0.00061  8.90264E-01 0.01951 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30932E+01 0.02543 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.89553E+01 0.00099  4.86512E+01 0.00114 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   5]) = 'iPuni' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.07306E+03 0.01757  3.58053E+04 0.01032  7.36125E+04 0.00776  9.59095E+04 0.00654  8.56185E+04 0.00623  6.79751E+04 0.00760  5.18878E+04 0.00490  3.85318E+04 0.00810  2.83915E+04 0.01503  2.27381E+04 0.01171  1.91372E+04 0.01299  1.74753E+04 0.00852  1.54788E+04 0.01560  1.46806E+04 0.01512  1.42201E+04 0.01093  1.23329E+04 0.01691  1.23962E+04 0.01320  1.25746E+04 0.02268  1.25252E+04 0.02287  2.41384E+04 0.01143  2.27047E+04 0.02397  1.71650E+04 0.01858  1.09255E+04 0.02502  1.32131E+04 0.01230  1.29226E+04 0.01966  1.11424E+04 0.01222  2.09153E+04 0.00701  4.43113E+03 0.04688  5.69411E+03 0.03217  5.19004E+03 0.01468  2.87821E+03 0.06380  5.26326E+03 0.03586  3.55448E+03 0.04966  2.80453E+03 0.04665  5.49329E+02 0.10921  5.35260E+02 0.12125  4.52741E+02 0.09510  5.11836E+02 0.08354  5.20047E+02 0.07369  5.44418E+02 0.10433  5.63353E+02 0.12762  5.40773E+02 0.05948  9.10743E+02 0.02800  1.61673E+03 0.03204  1.86680E+03 0.08259  5.08086E+03 0.04344  5.41765E+03 0.03400  4.91983E+03 0.04417  2.91448E+03 0.05515  1.85587E+03 0.04334  1.25187E+03 0.02783  1.52185E+03 0.06870  2.45241E+03 0.04680  2.88637E+03 0.04197  5.47266E+03 0.03311  9.67102E+03 0.01697  1.95585E+04 0.00730  1.75371E+04 0.01815  1.55427E+04 0.00885  1.27970E+04 0.01602  1.24049E+04 0.01624  1.43336E+04 0.03346  1.27674E+04 0.00839  9.06453E+03 0.01633  8.43321E+03 0.01668  7.96260E+03 0.01404  6.74308E+03 0.02398  4.99923E+03 0.03140  2.56289E+03 0.06401  7.00860E+02 0.06845 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  2.72060E+20 0.00248  6.12716E+19 0.00564 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  3.11543E-03 0.00146  7.66132E-03 0.00149 ];
INF_CAPT                  (idx, [1:   4]) = [  1.13997E-06 0.00556  6.56947E-05 0.00380 ];
INF_ABS                   (idx, [1:   4]) = [  1.13997E-06 0.00556  6.56947E-05 0.00380 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  5.33149E-08 0.00553  3.05852E-06 0.00380 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  3.10430E-03 0.00357  7.61181E-03 0.00510 ];
INF_SCATT1                (idx, [1:   4]) = [  2.06568E-03 0.00466  3.36690E-03 0.01986 ];
INF_SCATT2                (idx, [1:   4]) = [  7.84148E-04 0.01514  1.35086E-03 0.03796 ];
INF_SCATT3                (idx, [1:   4]) = [  1.06336E-05 1.00000  5.41695E-04 0.09195 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.34299E-04 0.05661  2.46017E-04 0.20198 ];
INF_SCATT5                (idx, [1:   4]) = [ -1.84918E-05 0.31681  1.20503E-04 0.40467 ];
INF_SCATT6                (idx, [1:   4]) = [  3.31798E-05 0.30809  7.64414E-05 0.37667 ];
INF_SCATT7                (idx, [1:   4]) = [  9.58690E-07 1.00000  7.01511E-05 0.60103 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  3.10430E-03 0.00357  7.61181E-03 0.00510 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.06568E-03 0.00466  3.36690E-03 0.01986 ];
INF_SCATTP2               (idx, [1:   4]) = [  7.84148E-04 0.01514  1.35086E-03 0.03796 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.06336E-05 1.00000  5.41695E-04 0.09195 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.34299E-04 0.05661  2.46017E-04 0.20198 ];
INF_SCATTP5               (idx, [1:   4]) = [ -1.84918E-05 0.31681  1.20503E-04 0.40467 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.31798E-05 0.30809  7.64414E-05 0.37667 ];
INF_SCATTP7               (idx, [1:   4]) = [  9.58690E-07 1.00000  7.01511E-05 0.60103 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  5.12627E-04 0.02888  3.74140E-03 0.01034 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  6.52589E+02 0.03110  8.91309E+01 0.01023 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.13997E-06 0.00556  6.56947E-05 0.00380 ];
INF_REMXS                 (idx, [1:   4]) = [  1.70446E-04 0.06956  4.95045E-05 0.82116 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.94498E-03 0.00547  1.59318E-04 0.03720  0.00000E+00 0.0E+00  7.61181E-03 0.00510 ];
INF_S1                    (idx, [1:   8]) = [  1.99715E-03 0.00653  6.85276E-05 0.06627  0.00000E+00 0.0E+00  3.36690E-03 0.01986 ];
INF_S2                    (idx, [1:   8]) = [  7.94652E-04 0.01591 -1.05035E-05 0.26263  0.00000E+00 0.0E+00  1.35086E-03 0.03796 ];
INF_S3                    (idx, [1:   8]) = [  4.23194E-05 0.26069 -3.16858E-05 0.02366  0.00000E+00 0.0E+00  5.41695E-04 0.09195 ];
INF_S4                    (idx, [1:   8]) = [ -1.20262E-04 0.05824 -1.40367E-05 0.09479  0.00000E+00 0.0E+00  2.46017E-04 0.20198 ];
INF_S5                    (idx, [1:   8]) = [ -2.20849E-05 0.26338  3.59306E-06 0.23716  0.00000E+00 0.0E+00  1.20503E-04 0.40467 ];
INF_S6                    (idx, [1:   8]) = [  2.87474E-05 0.31197  4.43238E-06 0.39631  0.00000E+00 0.0E+00  7.64414E-05 0.37667 ];
INF_S7                    (idx, [1:   8]) = [ -7.05710E-07 1.00000  1.66440E-06 0.93048  0.00000E+00 0.0E+00  7.01511E-05 0.60103 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.94498E-03 0.00547  1.59318E-04 0.03720  0.00000E+00 0.0E+00  7.61181E-03 0.00510 ];
INF_SP1                   (idx, [1:   8]) = [  1.99715E-03 0.00653  6.85276E-05 0.06627  0.00000E+00 0.0E+00  3.36690E-03 0.01986 ];
INF_SP2                   (idx, [1:   8]) = [  7.94652E-04 0.01591 -1.05035E-05 0.26263  0.00000E+00 0.0E+00  1.35086E-03 0.03796 ];
INF_SP3                   (idx, [1:   8]) = [  4.23194E-05 0.26069 -3.16858E-05 0.02366  0.00000E+00 0.0E+00  5.41695E-04 0.09195 ];
INF_SP4                   (idx, [1:   8]) = [ -1.20262E-04 0.05824 -1.40367E-05 0.09479  0.00000E+00 0.0E+00  2.46017E-04 0.20198 ];
INF_SP5                   (idx, [1:   8]) = [ -2.20849E-05 0.26338  3.59306E-06 0.23716  0.00000E+00 0.0E+00  1.20503E-04 0.40467 ];
INF_SP6                   (idx, [1:   8]) = [  2.87474E-05 0.31197  4.43238E-06 0.39631  0.00000E+00 0.0E+00  7.64414E-05 0.37667 ];
INF_SP7                   (idx, [1:   8]) = [ -7.05710E-07 1.00000  1.66440E-06 0.93048  0.00000E+00 0.0E+00  7.01511E-05 0.60103 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  1.19531E+01 0.03193  1.53874E+01 0.07899 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  1.78007E+01 0.07196  2.97598E+01 0.20194 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  1.76664E+01 0.04427 -2.65977E+01 1.00000 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  7.35172E+00 0.05653  8.63733E+00 0.06300 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  2.80013E-02 0.03205  2.21673E-02 0.07309 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.90825E-02 0.06538  1.34127E-02 0.20944 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.90177E-02 0.04460  1.38937E-02 0.29071 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  4.59036E-02 0.05437  3.91955E-02 0.06121 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/keffComparison/notCoupledReflector' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Mon Apr 29 18:00:27 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Mon Apr 29 18:02:59 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1714431627003 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 1 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
SHARE_BUF_ARRAY           (idx, 1)        = 1 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.24595E-01 0.00069  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.75405E-01 0.00051  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.78360E-01 0.00033  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.69741E-01 0.00040  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.33456E+00 0.00058  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.36548E-01 3.0E-05  6.14973E-02 0.00045  1.95475E-03 0.00132  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.92027E+01 0.00099  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.89553E+01 0.00099  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.69696E+01 0.00088  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.72076E+01 0.00084  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 1000105 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00010E+04 0.00150 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00010E+04 0.00150 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.54595E+00 ;
RUNNING_TIME              (idx, 1)        =  2.54662E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  2.70233E-01  2.70233E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.00000E-03  7.00000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.26938E+00  2.26938E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.54632E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.99974 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  9.99972E-01 3.6E-05 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.39384E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 3746.92 ;
MEMSIZE                   (idx, 1)        = 3696.39 ;
XS_MEMSIZE                (idx, 1)        = 3030.45 ;
MAT_MEMSIZE               (idx, 1)        = 593.42 ;
RES_MEMSIZE               (idx, 1)        = 5.50 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 67.02 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 50.54 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 54 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 948120 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 20 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 53 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 53 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 1698 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  6.66817E+15 0.00107  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.20786E-03 0.01939 ];
U235_FISS                 (idx, [1:   4]) = [  3.08365E+19 0.00099  9.99875E-01 1.6E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.85457E+15 0.12526  1.25464E-04 0.12543 ];
U235_CAPT                 (idx, [1:   4]) = [  6.00677E+18 0.00328  2.88377E-01 0.00260 ];
U238_CAPT                 (idx, [1:   4]) = [  1.54452E+17 0.01948  7.41312E-03 0.01935 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 1000105 1.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.93066E+04 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 1000105 1.03931E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 302023 3.12368E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 450694 4.62532E+05 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 247388 2.64406E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 1000105 1.03931E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.59489E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.00000E+09 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.13889E+02 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  7.52146E+19 1.7E-06 ];
TOT_FISSRATE              (idx, [1:   2]) = [  3.08572E+19 7.0E-09 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.08446E+19 0.00117 ];
TOT_ABSRATE               (idx, [1:   2]) = [  5.17018E+19 0.00047 ];
TOT_SRCRATE               (idx, [1:   2]) = [  6.66817E+19 0.00107 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.74850E+21 0.00123 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.76322E+19 0.00241 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  6.93340E+19 0.00077 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.41180E+21 0.00139 ];
INI_FMASS                 (idx, 1)        =  8.78052E+00 ;
TOT_FMASS                 (idx, 1)        =  8.78052E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06731E+00 0.00063 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.43951E-01 0.00067 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.13008E-01 0.00054 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09154E+00 0.00047 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.03684E-01 0.00050 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.15276E-01 0.00040 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.53270E+00 0.00099 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.12743E+00 0.00107 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43750E+00 1.7E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02271E+02 7.8E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.12769E+00 0.00107  1.11889E+00 0.00108  8.54617E-03 0.01665 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.12771E+00 0.00074 ];
COL_KEFF                  (idx, [1:   2]) = [  1.12809E+00 0.00107 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.12771E+00 0.00074 ];
ABS_KINF                  (idx, [1:   2]) = [  1.53290E+00 0.00047 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93799E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93781E+01 9.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.66971E-08 0.00405 ];
IMP_EALF                  (idx, [1:   2]) = [  7.67925E-08 0.00187 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.49153E-02 0.02047 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.47001E-02 0.00260 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.25171E-03 0.01221  2.06791E-04 0.08419  9.68906E-04 0.03305  5.70877E-04 0.04142  1.26434E-03 0.02876  1.98477E-03 0.02022  5.62768E-04 0.04033  5.31306E-04 0.03989  1.61952E-04 0.07740 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.19768E-01 0.01894  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.2E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.5E-09  1.63478E+00 0.0E+00  3.55460E+00 5.8E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.68581E-03 0.01756  2.43120E-04 0.10572  1.18939E-03 0.05340  7.46828E-04 0.05661  1.55365E-03 0.03910  2.36348E-03 0.03105  7.43194E-04 0.05501  6.69891E-04 0.06306  1.76262E-04 0.10411 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.17090E-01 0.02722  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.0E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.8E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  8.82358E-05 0.00304  8.82857E-05 0.00303  8.21027E-05 0.02644 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  9.94910E-05 0.00285  9.95473E-05 0.00285  9.25621E-05 0.02633 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.55957E-03 0.01720  2.47402E-04 0.10642  1.20109E-03 0.04967  7.82544E-04 0.05562  1.48736E-03 0.04055  2.31711E-03 0.02962  7.16606E-04 0.05858  6.29467E-04 0.05886  1.77979E-04 0.10991 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08987E-01 0.02952  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.2E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.6E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  8.42664E-05 0.02418  8.43027E-05 0.02419  7.57347E-05 0.06833 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  9.50299E-05 0.02412  9.50718E-05 0.02414  8.52823E-05 0.06790 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  7.05636E-03 0.06679  1.36965E-04 0.39584  1.05224E-03 0.13398  8.41164E-04 0.15796  1.50563E-03 0.11874  1.95341E-03 0.12936  6.65014E-04 0.17788  6.44086E-04 0.18971  2.57844E-04 0.33638 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.70643E-01 0.09770  1.24667E-02 3.9E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  7.13771E-03 0.06593  1.55038E-04 0.35940  1.07081E-03 0.12644  7.79117E-04 0.15912  1.53778E-03 0.11224  1.94960E-03 0.12871  7.07102E-04 0.17448  6.79272E-04 0.18312  2.59005E-04 0.33198 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.77766E-01 0.09826  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.1E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -8.34870E+01 0.06030 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  8.90768E-05 0.00165 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  1.00439E-04 0.00119 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.60720E-03 0.00849 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -8.54599E+01 0.00914 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.24184E-06 0.00137 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.79587E-06 0.00103  4.79833E-06 0.00103  4.47210E-06 0.01088 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.39478E-04 0.00192  1.39576E-04 0.00193  1.26735E-04 0.02133 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.39797E-01 0.00062  7.38937E-01 0.00061  8.90264E-01 0.01951 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30932E+01 0.02543 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.89553E+01 0.00099  4.86512E+01 0.00114 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   5]) = 'hFuni' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  3.36635E+03 0.01007  1.39787E+04 0.00844  2.83463E+04 0.01113  3.73023E+04 0.01202  3.26341E+04 0.01348  2.59798E+04 0.00894  2.00115E+04 0.01325  1.42251E+04 0.01395  1.04273E+04 0.01690  8.00039E+03 0.01662  6.82181E+03 0.00985  6.21015E+03 0.01392  5.53244E+03 0.03761  5.43512E+03 0.02118  5.15424E+03 0.01669  4.58153E+03 0.02920  4.59517E+03 0.01078  4.56457E+03 0.02804  4.30791E+03 0.01704  8.92274E+03 0.01221  8.41539E+03 0.00977  6.08178E+03 0.01330  4.02824E+03 0.02427  4.76233E+03 0.01507  4.60453E+03 0.02469  3.98333E+03 0.03023  7.38925E+03 0.02572  1.54090E+03 0.04204  1.93706E+03 0.04528  1.96963E+03 0.01040  1.04393E+03 0.07119  1.85208E+03 0.02463  1.22141E+03 0.03008  1.05331E+03 0.01170  1.63224E+02 0.13739  2.17812E+02 0.10479  1.91055E+02 0.08900  1.74653E+02 0.15549  1.93590E+02 0.11254  1.65816E+02 0.11331  2.06612E+02 0.19113  2.20674E+02 0.09201  4.28076E+02 0.11223  5.14580E+02 0.06939  7.21632E+02 0.04420  1.83131E+03 0.02552  1.86599E+03 0.02141  1.89290E+03 0.01605  9.74323E+02 0.06743  6.10206E+02 0.06015  4.82601E+02 0.08051  5.58020E+02 0.04948  8.82169E+02 0.03394  9.51307E+02 0.03998  1.85077E+03 0.04047  3.37631E+03 0.00899  6.94654E+03 0.01261  6.33568E+03 0.01972  5.39991E+03 0.01707  4.64752E+03 0.03032  4.45644E+03 0.02366  4.78306E+03 0.01812  4.34398E+03 0.01010  3.22221E+03 0.02155  3.12743E+03 0.02160  2.68916E+03 0.02467  2.38067E+03 0.04872  1.70543E+03 0.04940  8.30959E+02 0.05038  2.30770E+02 0.13510 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.01794E+20 0.00356  2.15193E+19 0.00425 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  6.55070E-02 0.00080  7.63246E-02 0.00039 ];
INF_CAPT                  (idx, [1:   4]) = [  1.48550E-04 0.02303  8.49080E-04 0.00484 ];
INF_ABS                   (idx, [1:   4]) = [  1.48550E-04 0.02303  8.49080E-04 0.00484 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  5.13420E-08 0.00298  3.04571E-06 0.00484 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  6.53746E-02 0.00136  7.55278E-02 0.00080 ];
INF_SCATT1                (idx, [1:   4]) = [  9.99683E-03 0.01263  5.03418E-03 0.06764 ];
INF_SCATT2                (idx, [1:   4]) = [  5.60807E-03 0.01858  1.76015E-03 0.09272 ];
INF_SCATT3                (idx, [1:   4]) = [  1.08203E-03 0.06426  9.08458E-04 0.17579 ];
INF_SCATT4                (idx, [1:   4]) = [  6.71564E-04 0.09061  2.87180E-05 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  3.18178E-04 0.14641  3.72010E-06 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  2.13034E-04 0.37348 -8.63006E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [ -4.30010E-05 1.00000  9.66241E-05 0.69678 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  6.53779E-02 0.00135  7.55278E-02 0.00080 ];
INF_SCATTP1               (idx, [1:   4]) = [  9.99795E-03 0.01259  5.03418E-03 0.06764 ];
INF_SCATTP2               (idx, [1:   4]) = [  5.60794E-03 0.01857  1.76015E-03 0.09272 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.08184E-03 0.06429  9.08458E-04 0.17579 ];
INF_SCATTP4               (idx, [1:   4]) = [  6.71236E-04 0.09026  2.87180E-05 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  3.18061E-04 0.14573  3.72010E-06 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  2.13165E-04 0.37389 -8.63006E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [ -4.33131E-05 1.00000  9.66241E-05 0.69678 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  4.60133E-02 0.00317  7.10126E-02 0.00472 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  7.24457E+00 0.00318  4.69442E+00 0.00470 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.45288E-04 0.02971  8.49080E-04 0.00484 ];
INF_REMXS                 (idx, [1:   4]) = [  4.50474E-04 0.10646  8.80581E-04 0.07780 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  6.50565E-02 0.00139  3.18147E-04 0.06244  8.37163E-05 0.27286  7.54441E-02 0.00090 ];
INF_S1                    (idx, [1:   8]) = [  9.98162E-03 0.01339  1.52096E-05 0.69684 -1.47641E-05 0.86641  5.04895E-03 0.06557 ];
INF_S2                    (idx, [1:   8]) = [  5.62481E-03 0.01804 -1.67447E-05 0.46680 -3.80758E-06 1.00000  1.76396E-03 0.09362 ];
INF_S3                    (idx, [1:   8]) = [  1.11764E-03 0.06272 -3.56044E-05 0.14232 -7.20070E-08 1.00000  9.08530E-04 0.17448 ];
INF_S4                    (idx, [1:   8]) = [  6.85210E-04 0.09250 -1.36455E-05 0.55895 -1.05936E-05 0.33115  3.93115E-05 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  3.20154E-04 0.16045 -1.97573E-06 1.00000  4.79991E-06 0.75797 -1.07981E-06 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  2.09149E-04 0.36678  3.88442E-06 0.90184  2.18569E-06 1.00000 -8.84863E-05 1.00000 ];
INF_S7                    (idx, [1:   8]) = [ -5.07606E-05 0.82150  7.75965E-06 0.33990  3.11532E-06 1.00000  9.35088E-05 0.69960 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  6.50597E-02 0.00137  3.18147E-04 0.06244  8.37163E-05 0.27286  7.54441E-02 0.00090 ];
INF_SP1                   (idx, [1:   8]) = [  9.98274E-03 0.01336  1.52096E-05 0.69684 -1.47641E-05 0.86641  5.04895E-03 0.06557 ];
INF_SP2                   (idx, [1:   8]) = [  5.62469E-03 0.01803 -1.67447E-05 0.46680 -3.80758E-06 1.00000  1.76396E-03 0.09362 ];
INF_SP3                   (idx, [1:   8]) = [  1.11744E-03 0.06275 -3.56044E-05 0.14232 -7.20070E-08 1.00000  9.08530E-04 0.17448 ];
INF_SP4                   (idx, [1:   8]) = [  6.84882E-04 0.09211 -1.36455E-05 0.55895 -1.05936E-05 0.33115  3.93115E-05 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  3.20037E-04 0.15978 -1.97573E-06 1.00000  4.79991E-06 0.75797 -1.07981E-06 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  2.09281E-04 0.36721  3.88442E-06 0.90184  2.18569E-06 1.00000 -8.84863E-05 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [ -5.10727E-05 0.81736  7.75965E-06 0.33990  3.11532E-06 1.00000  9.35088E-05 0.69960 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  1.31313E+00 0.01808  1.64005E+00 0.04582 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  1.67534E+00 0.03944  2.67196E+00 0.12228 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  1.70179E+00 0.03076  2.38822E+00 0.15821 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  9.14894E-01 0.03020  1.00739E+00 0.02956 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  2.54172E-01 0.01773  2.04919E-01 0.04459 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  2.00289E-01 0.04203  1.30720E-01 0.09417 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.96615E-01 0.03077  1.52048E-01 0.13361 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  3.65613E-01 0.02882  3.31987E-01 0.02804 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/keffComparison/notCoupledReflector' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Mon Apr 29 18:00:27 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Mon Apr 29 18:02:59 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1714431627003 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 1 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
SHARE_BUF_ARRAY           (idx, 1)        = 1 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.24595E-01 0.00069  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.75405E-01 0.00051  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.78360E-01 0.00033  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.69741E-01 0.00040  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.33456E+00 0.00058  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.36548E-01 3.0E-05  6.14973E-02 0.00045  1.95475E-03 0.00132  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.92027E+01 0.00099  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.89553E+01 0.00099  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.69696E+01 0.00088  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.72076E+01 0.00084  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 1000105 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00010E+04 0.00150 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00010E+04 0.00150 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.54595E+00 ;
RUNNING_TIME              (idx, 1)        =  2.54662E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  2.70233E-01  2.70233E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.00000E-03  7.00000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.26938E+00  2.26938E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.54632E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.99974 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  9.99972E-01 3.6E-05 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.39384E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 3746.92 ;
MEMSIZE                   (idx, 1)        = 3696.39 ;
XS_MEMSIZE                (idx, 1)        = 3030.45 ;
MAT_MEMSIZE               (idx, 1)        = 593.42 ;
RES_MEMSIZE               (idx, 1)        = 5.50 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 67.02 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 50.54 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 54 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 948120 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 20 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 53 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 53 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 1698 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  6.66817E+15 0.00107  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.20786E-03 0.01939 ];
U235_FISS                 (idx, [1:   4]) = [  3.08365E+19 0.00099  9.99875E-01 1.6E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.85457E+15 0.12526  1.25464E-04 0.12543 ];
U235_CAPT                 (idx, [1:   4]) = [  6.00677E+18 0.00328  2.88377E-01 0.00260 ];
U238_CAPT                 (idx, [1:   4]) = [  1.54452E+17 0.01948  7.41312E-03 0.01935 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 1000105 1.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.93066E+04 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 1000105 1.03931E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 302023 3.12368E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 450694 4.62532E+05 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 247388 2.64406E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 1000105 1.03931E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.59489E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.00000E+09 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.13889E+02 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  7.52146E+19 1.7E-06 ];
TOT_FISSRATE              (idx, [1:   2]) = [  3.08572E+19 7.0E-09 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.08446E+19 0.00117 ];
TOT_ABSRATE               (idx, [1:   2]) = [  5.17018E+19 0.00047 ];
TOT_SRCRATE               (idx, [1:   2]) = [  6.66817E+19 0.00107 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.74850E+21 0.00123 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.76322E+19 0.00241 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  6.93340E+19 0.00077 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.41180E+21 0.00139 ];
INI_FMASS                 (idx, 1)        =  8.78052E+00 ;
TOT_FMASS                 (idx, 1)        =  8.78052E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06731E+00 0.00063 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.43951E-01 0.00067 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.13008E-01 0.00054 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09154E+00 0.00047 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.03684E-01 0.00050 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.15276E-01 0.00040 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.53270E+00 0.00099 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.12743E+00 0.00107 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43750E+00 1.7E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02271E+02 7.8E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.12769E+00 0.00107  1.11889E+00 0.00108  8.54617E-03 0.01665 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.12771E+00 0.00074 ];
COL_KEFF                  (idx, [1:   2]) = [  1.12809E+00 0.00107 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.12771E+00 0.00074 ];
ABS_KINF                  (idx, [1:   2]) = [  1.53290E+00 0.00047 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93799E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93781E+01 9.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.66971E-08 0.00405 ];
IMP_EALF                  (idx, [1:   2]) = [  7.67925E-08 0.00187 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.49153E-02 0.02047 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.47001E-02 0.00260 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.25171E-03 0.01221  2.06791E-04 0.08419  9.68906E-04 0.03305  5.70877E-04 0.04142  1.26434E-03 0.02876  1.98477E-03 0.02022  5.62768E-04 0.04033  5.31306E-04 0.03989  1.61952E-04 0.07740 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.19768E-01 0.01894  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.2E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.5E-09  1.63478E+00 0.0E+00  3.55460E+00 5.8E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.68581E-03 0.01756  2.43120E-04 0.10572  1.18939E-03 0.05340  7.46828E-04 0.05661  1.55365E-03 0.03910  2.36348E-03 0.03105  7.43194E-04 0.05501  6.69891E-04 0.06306  1.76262E-04 0.10411 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.17090E-01 0.02722  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.0E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.8E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  8.82358E-05 0.00304  8.82857E-05 0.00303  8.21027E-05 0.02644 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  9.94910E-05 0.00285  9.95473E-05 0.00285  9.25621E-05 0.02633 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.55957E-03 0.01720  2.47402E-04 0.10642  1.20109E-03 0.04967  7.82544E-04 0.05562  1.48736E-03 0.04055  2.31711E-03 0.02962  7.16606E-04 0.05858  6.29467E-04 0.05886  1.77979E-04 0.10991 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08987E-01 0.02952  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.2E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.6E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  8.42664E-05 0.02418  8.43027E-05 0.02419  7.57347E-05 0.06833 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  9.50299E-05 0.02412  9.50718E-05 0.02414  8.52823E-05 0.06790 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  7.05636E-03 0.06679  1.36965E-04 0.39584  1.05224E-03 0.13398  8.41164E-04 0.15796  1.50563E-03 0.11874  1.95341E-03 0.12936  6.65014E-04 0.17788  6.44086E-04 0.18971  2.57844E-04 0.33638 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.70643E-01 0.09770  1.24667E-02 3.9E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  7.13771E-03 0.06593  1.55038E-04 0.35940  1.07081E-03 0.12644  7.79117E-04 0.15912  1.53778E-03 0.11224  1.94960E-03 0.12871  7.07102E-04 0.17448  6.79272E-04 0.18312  2.59005E-04 0.33198 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.77766E-01 0.09826  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.1E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -8.34870E+01 0.06030 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  8.90768E-05 0.00165 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  1.00439E-04 0.00119 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.60720E-03 0.00849 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -8.54599E+01 0.00914 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.24184E-06 0.00137 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.79587E-06 0.00103  4.79833E-06 0.00103  4.47210E-06 0.01088 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.39478E-04 0.00192  1.39576E-04 0.00193  1.26735E-04 0.02133 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.39797E-01 0.00062  7.38937E-01 0.00061  8.90264E-01 0.01951 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30932E+01 0.02543 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.89553E+01 0.00099  4.86512E+01 0.00114 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   4]) = 'funi' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.37734E+04 0.00670  5.93012E+04 0.00393  1.20943E+05 0.00672  1.58164E+05 0.00144  1.36949E+05 0.00221  1.06139E+05 0.00421  7.99416E+04 0.00464  5.77396E+04 0.00377  4.25376E+04 0.00336  3.33480E+04 0.00579  2.76502E+04 0.00613  2.49440E+04 0.00382  2.27555E+04 0.01094  2.16936E+04 0.00817  2.08099E+04 0.00736  1.82498E+04 0.00499  1.81185E+04 0.00272  1.80344E+04 0.00692  1.77893E+04 0.01187  3.50131E+04 0.00837  3.33766E+04 0.00893  2.47716E+04 0.00815  1.60211E+04 0.00729  1.90474E+04 0.00536  1.82974E+04 0.00952  1.62860E+04 0.00529  3.02380E+04 0.01139  6.66930E+03 0.00868  8.40984E+03 0.00872  7.50447E+03 0.00901  4.31832E+03 0.01097  7.40692E+03 0.01154  4.93775E+03 0.01118  4.07667E+03 0.01837  7.39062E+02 0.02429  7.53716E+02 0.03519  7.66854E+02 0.03077  7.78663E+02 0.02818  7.72000E+02 0.03966  7.57963E+02 0.03790  7.86726E+02 0.01310  7.73763E+02 0.03177  1.42441E+03 0.03310  2.21591E+03 0.02160  2.80595E+03 0.01597  7.36603E+03 0.00940  7.36499E+03 0.01086  7.17934E+03 0.02385  4.05592E+03 0.01508  2.62482E+03 0.01412  1.87933E+03 0.01097  1.98453E+03 0.00795  3.37582E+03 0.01264  4.03932E+03 0.00888  7.60798E+03 0.01233  1.34769E+04 0.00674  2.86110E+04 0.00450  2.57046E+04 0.00491  2.26706E+04 0.00308  1.88457E+04 0.00354  1.88119E+04 0.00365  2.03327E+04 0.00394  1.86624E+04 0.00377  1.31757E+04 0.00489  1.27258E+04 0.00430  1.16623E+04 0.00593  9.87048E+03 0.00882  7.54562E+03 0.00631  4.35208E+03 0.00576  1.14391E+03 0.02102 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  2.00889E+00 0.00107 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.18494E+20 0.00125  8.92545E+19 0.00045 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.22182E-01 0.00061  6.11024E-01 0.00036 ];
INF_CAPT                  (idx, [1:   4]) = [  3.72987E-03 0.00271  5.67761E-02 0.00067 ];
INF_ABS                   (idx, [1:   4]) = [  9.87873E-03 0.00235  3.73706E-01 0.00058 ];
INF_FISS                  (idx, [1:   4]) = [  6.14886E-03 0.00269  3.16930E-01 0.00057 ];
INF_NSF                   (idx, [1:   4]) = [  1.50800E-02 0.00266  7.72085E-01 0.00057 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.45249E+00 2.9E-05  2.43614E+00 5.9E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02277E+02 1.7E-07  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  5.03049E-08 0.00167  3.09349E-06 0.00052 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.12298E-01 0.00059  2.37751E-01 0.00176 ];
INF_SCATT1                (idx, [1:   4]) = [  2.87873E-02 0.00388  7.37935E-03 0.04385 ];
INF_SCATT2                (idx, [1:   4]) = [  1.74192E-02 0.00250  8.34035E-04 0.23666 ];
INF_SCATT3                (idx, [1:   4]) = [  3.85101E-03 0.01635  1.38351E-04 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [  3.14683E-03 0.01218  2.77725E-04 0.44923 ];
INF_SCATT5                (idx, [1:   4]) = [  1.30790E-03 0.06618  2.35770E-05 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  7.32409E-04 0.07596  1.81069E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  1.55093E-04 0.28741 -6.34893E-05 0.94722 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.12316E-01 0.00059  2.37751E-01 0.00176 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.87869E-02 0.00390  7.37935E-03 0.04385 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.74192E-02 0.00249  8.34035E-04 0.23666 ];
INF_SCATTP3               (idx, [1:   4]) = [  3.85103E-03 0.01637  1.38351E-04 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [  3.14703E-03 0.01211  2.77725E-04 0.44923 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.30785E-03 0.06657  2.35770E-05 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  7.31863E-04 0.07563  1.81069E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.54816E-04 0.28725 -6.34893E-05 0.94722 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  1.56951E-01 0.00123  5.43501E-01 0.00096 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  2.12382E+00 0.00123  6.13310E-01 0.00096 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.86076E-03 0.00231  3.73706E-01 0.00058 ];
INF_REMXS                 (idx, [1:   4]) = [  1.04050E-02 0.00270  3.73473E-01 0.00120 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.11777E-01 0.00057  5.20982E-04 0.01162  2.00309E-04 0.03194  2.37551E-01 0.00178 ];
INF_S1                    (idx, [1:   8]) = [  2.88834E-02 0.00386 -9.61192E-05 0.04813  3.39171E-06 1.00000  7.37596E-03 0.04302 ];
INF_S2                    (idx, [1:   8]) = [  1.74376E-02 0.00232 -1.83569E-05 0.37051 -1.70819E-05 0.29479  8.51116E-04 0.23452 ];
INF_S3                    (idx, [1:   8]) = [  3.86703E-03 0.01602 -1.60117E-05 0.15893 -7.32461E-07 1.00000  1.39083E-04 1.00000 ];
INF_S4                    (idx, [1:   8]) = [  3.15545E-03 0.01265 -8.62220E-06 0.27125 -8.38079E-07 1.00000  2.78563E-04 0.43508 ];
INF_S5                    (idx, [1:   8]) = [  1.30565E-03 0.06607  2.24907E-06 1.00000 -5.78519E-06 0.69273  2.93622E-05 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  7.28501E-04 0.07798  3.90810E-06 0.48966 -1.15278E-06 1.00000  1.92597E-05 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  1.54058E-04 0.28559  1.03462E-06 1.00000 -6.66844E-07 1.00000 -6.28224E-05 0.96880 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.11795E-01 0.00057  5.20982E-04 0.01162  2.00309E-04 0.03194  2.37551E-01 0.00178 ];
INF_SP1                   (idx, [1:   8]) = [  2.88830E-02 0.00388 -9.61192E-05 0.04813  3.39171E-06 1.00000  7.37596E-03 0.04302 ];
INF_SP2                   (idx, [1:   8]) = [  1.74375E-02 0.00230 -1.83569E-05 0.37051 -1.70819E-05 0.29479  8.51116E-04 0.23452 ];
INF_SP3                   (idx, [1:   8]) = [  3.86704E-03 0.01604 -1.60117E-05 0.15893 -7.32461E-07 1.00000  1.39083E-04 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [  3.15565E-03 0.01257 -8.62220E-06 0.27125 -8.38079E-07 1.00000  2.78563E-04 0.43508 ];
INF_SP5                   (idx, [1:   8]) = [  1.30560E-03 0.06644  2.24907E-06 1.00000 -5.78519E-06 0.69273  2.93622E-05 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  7.27955E-04 0.07765  3.90810E-06 0.48966 -1.15278E-06 1.00000  1.92597E-05 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  1.53782E-04 0.28540  1.03462E-06 1.00000 -6.66844E-07 1.00000 -6.28224E-05 0.96880 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  5.44883E-01 0.00604  3.34680E-01 0.01403 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  6.23631E-01 0.01024  4.16787E-01 0.02107 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  6.24433E-01 0.00873  4.33448E-01 0.01838 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  4.34754E-01 0.00570  2.35244E-01 0.01873 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  6.11842E-01 0.00606  9.96755E-01 0.01394 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  5.34729E-01 0.01028  8.01155E-01 0.02053 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  5.33980E-01 0.00870  7.70120E-01 0.01928 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  7.66816E-01 0.00568  1.41899E+00 0.01903 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.68581E-03 0.01756  2.43120E-04 0.10572  1.18939E-03 0.05340  7.46828E-04 0.05661  1.55365E-03 0.03910  2.36348E-03 0.03105  7.43194E-04 0.05501  6.69891E-04 0.06306  1.76262E-04 0.10411 ];
LAMBDA                    (idx, [1:  18]) = [  4.17090E-01 0.02722  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.0E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.8E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/keffComparison/notCoupledReflector' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Mon Apr 29 18:00:27 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Mon Apr 29 18:02:59 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1714431627003 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 1 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
SHARE_BUF_ARRAY           (idx, 1)        = 1 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.24595E-01 0.00069  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.75405E-01 0.00051  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.78360E-01 0.00033  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.69741E-01 0.00040  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.33456E+00 0.00058  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.36548E-01 3.0E-05  6.14973E-02 0.00045  1.95475E-03 0.00132  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.92027E+01 0.00099  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.89553E+01 0.00099  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.69696E+01 0.00088  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.72076E+01 0.00084  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 1000105 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00010E+04 0.00150 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00010E+04 0.00150 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.54595E+00 ;
RUNNING_TIME              (idx, 1)        =  2.54663E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  2.70233E-01  2.70233E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.00000E-03  7.00000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.26938E+00  2.26938E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.54632E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.99973 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  9.99972E-01 3.6E-05 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.39377E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 3746.92 ;
MEMSIZE                   (idx, 1)        = 3696.39 ;
XS_MEMSIZE                (idx, 1)        = 3030.45 ;
MAT_MEMSIZE               (idx, 1)        = 593.42 ;
RES_MEMSIZE               (idx, 1)        = 5.50 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 67.02 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 50.54 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 54 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 948120 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 20 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 53 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 53 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 1698 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  6.66817E+15 0.00107  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.20786E-03 0.01939 ];
U235_FISS                 (idx, [1:   4]) = [  3.08365E+19 0.00099  9.99875E-01 1.6E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.85457E+15 0.12526  1.25464E-04 0.12543 ];
U235_CAPT                 (idx, [1:   4]) = [  6.00677E+18 0.00328  2.88377E-01 0.00260 ];
U238_CAPT                 (idx, [1:   4]) = [  1.54452E+17 0.01948  7.41312E-03 0.01935 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 1000105 1.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.93066E+04 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 1000105 1.03931E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 302023 3.12368E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 450694 4.62532E+05 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 247388 2.64406E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 1000105 1.03931E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.59489E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.00000E+09 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.13889E+02 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  7.52146E+19 1.7E-06 ];
TOT_FISSRATE              (idx, [1:   2]) = [  3.08572E+19 7.0E-09 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.08446E+19 0.00117 ];
TOT_ABSRATE               (idx, [1:   2]) = [  5.17018E+19 0.00047 ];
TOT_SRCRATE               (idx, [1:   2]) = [  6.66817E+19 0.00107 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.74850E+21 0.00123 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.76322E+19 0.00241 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  6.93340E+19 0.00077 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.41180E+21 0.00139 ];
INI_FMASS                 (idx, 1)        =  8.78052E+00 ;
TOT_FMASS                 (idx, 1)        =  8.78052E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06731E+00 0.00063 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.43951E-01 0.00067 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.13008E-01 0.00054 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09154E+00 0.00047 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.03684E-01 0.00050 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.15276E-01 0.00040 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.53270E+00 0.00099 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.12743E+00 0.00107 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43750E+00 1.7E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02271E+02 7.8E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.12769E+00 0.00107  1.11889E+00 0.00108  8.54617E-03 0.01665 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.12771E+00 0.00074 ];
COL_KEFF                  (idx, [1:   2]) = [  1.12809E+00 0.00107 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.12771E+00 0.00074 ];
ABS_KINF                  (idx, [1:   2]) = [  1.53290E+00 0.00047 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93799E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93781E+01 9.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.66971E-08 0.00405 ];
IMP_EALF                  (idx, [1:   2]) = [  7.67925E-08 0.00187 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.49153E-02 0.02047 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.47001E-02 0.00260 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.25171E-03 0.01221  2.06791E-04 0.08419  9.68906E-04 0.03305  5.70877E-04 0.04142  1.26434E-03 0.02876  1.98477E-03 0.02022  5.62768E-04 0.04033  5.31306E-04 0.03989  1.61952E-04 0.07740 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.19768E-01 0.01894  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.2E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.5E-09  1.63478E+00 0.0E+00  3.55460E+00 5.8E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.68581E-03 0.01756  2.43120E-04 0.10572  1.18939E-03 0.05340  7.46828E-04 0.05661  1.55365E-03 0.03910  2.36348E-03 0.03105  7.43194E-04 0.05501  6.69891E-04 0.06306  1.76262E-04 0.10411 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.17090E-01 0.02722  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.0E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.8E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  8.82358E-05 0.00304  8.82857E-05 0.00303  8.21027E-05 0.02644 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  9.94910E-05 0.00285  9.95473E-05 0.00285  9.25621E-05 0.02633 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.55957E-03 0.01720  2.47402E-04 0.10642  1.20109E-03 0.04967  7.82544E-04 0.05562  1.48736E-03 0.04055  2.31711E-03 0.02962  7.16606E-04 0.05858  6.29467E-04 0.05886  1.77979E-04 0.10991 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08987E-01 0.02952  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.2E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.6E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  8.42664E-05 0.02418  8.43027E-05 0.02419  7.57347E-05 0.06833 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  9.50299E-05 0.02412  9.50718E-05 0.02414  8.52823E-05 0.06790 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  7.05636E-03 0.06679  1.36965E-04 0.39584  1.05224E-03 0.13398  8.41164E-04 0.15796  1.50563E-03 0.11874  1.95341E-03 0.12936  6.65014E-04 0.17788  6.44086E-04 0.18971  2.57844E-04 0.33638 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.70643E-01 0.09770  1.24667E-02 3.9E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  7.13771E-03 0.06593  1.55038E-04 0.35940  1.07081E-03 0.12644  7.79117E-04 0.15912  1.53778E-03 0.11224  1.94960E-03 0.12871  7.07102E-04 0.17448  6.79272E-04 0.18312  2.59005E-04 0.33198 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.77766E-01 0.09826  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.1E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -8.34870E+01 0.06030 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  8.90768E-05 0.00165 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  1.00439E-04 0.00119 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.60720E-03 0.00849 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -8.54599E+01 0.00914 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.24184E-06 0.00137 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.79587E-06 0.00103  4.79833E-06 0.00103  4.47210E-06 0.01088 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.39478E-04 0.00192  1.39576E-04 0.00193  1.26735E-04 0.02133 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.39797E-01 0.00062  7.38937E-01 0.00061  8.90264E-01 0.01951 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30932E+01 0.02543 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.89553E+01 0.00099  4.86512E+01 0.00114 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   5]) = 'cFuni' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  3.47556E+03 0.02431  1.60691E+04 0.01633  3.20703E+04 0.00897  4.25078E+04 0.00612  3.70456E+04 0.00471  2.92008E+04 0.00669  2.27568E+04 0.00818  1.65222E+04 0.01214  1.26378E+04 0.00680  1.02486E+04 0.00689  8.52397E+03 0.01612  7.92136E+03 0.01402  7.20165E+03 0.00322  6.88112E+03 0.01028  6.55920E+03 0.00930  5.82460E+03 0.00354  5.74706E+03 0.00903  5.77628E+03 0.01281  5.68183E+03 0.00669  1.10487E+04 0.00807  1.08116E+04 0.01077  8.02013E+03 0.00595  5.09912E+03 0.02065  6.29474E+03 0.01347  6.08151E+03 0.01229  5.21956E+03 0.01548  9.85449E+03 0.00545  2.10213E+03 0.02081  2.65380E+03 0.01810  2.42254E+03 0.03916  1.37125E+03 0.01044  2.31408E+03 0.02563  1.57378E+03 0.01735  1.35174E+03 0.02717  2.50431E+02 0.04406  2.36194E+02 0.02296  2.40799E+02 0.06531  2.46824E+02 0.05287  2.61551E+02 0.08199  2.51561E+02 0.04738  2.44201E+02 0.06963  2.38534E+02 0.05509  4.71891E+02 0.04333  7.22732E+02 0.02944  9.72666E+02 0.02822  2.41203E+03 0.01850  2.42063E+03 0.01813  2.39819E+03 0.00971  1.45898E+03 0.03202  8.71743E+02 0.02870  7.18534E+02 0.02756  6.81717E+02 0.04344  1.19763E+03 0.01929  1.49204E+03 0.01985  2.66387E+03 0.03322  4.88441E+03 0.02102  1.01830E+04 0.00907  9.74108E+03 0.00965  8.61419E+03 0.00378  7.34396E+03 0.01293  7.38983E+03 0.01923  8.25747E+03 0.01202  7.96970E+03 0.01470  5.75883E+03 0.01110  5.67495E+03 0.01218  5.43712E+03 0.00749  4.97294E+03 0.00435  4.14031E+03 0.01991  2.79463E+03 0.01840  1.05004E+03 0.02753 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.21835E+20 0.00328  3.60467E+19 0.00323 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  1.67819E-01 0.00093  2.57922E-01 0.00022 ];
INF_CAPT                  (idx, [1:   4]) = [  5.51375E-04 0.00493  4.21804E-04 0.00174 ];
INF_ABS                   (idx, [1:   4]) = [  5.51375E-04 0.00493  4.21804E-04 0.00174 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  5.58054E-08 0.00469  3.35103E-06 0.00174 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  1.67283E-01 0.00094  2.57491E-01 0.00031 ];
INF_SCATT1                (idx, [1:   4]) = [  2.51909E-02 0.00663  2.44081E-02 0.01338 ];
INF_SCATT2                (idx, [1:   4]) = [  7.76243E-03 0.00773  4.37298E-03 0.06283 ];
INF_SCATT3                (idx, [1:   4]) = [  2.09630E-03 0.06319  1.73634E-03 0.14846 ];
INF_SCATT4                (idx, [1:   4]) = [  8.51329E-05 1.00000  6.96325E-04 0.20584 ];
INF_SCATT5                (idx, [1:   4]) = [ -1.77159E-05 1.00000  5.35322E-04 0.50312 ];
INF_SCATT6                (idx, [1:   4]) = [ -5.52455E-05 1.00000  3.05993E-04 0.63986 ];
INF_SCATT7                (idx, [1:   4]) = [  3.13195E-05 1.00000  2.93662E-04 0.88143 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  1.69024E-01 0.00078  2.57491E-01 0.00031 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.55975E-02 0.00630  2.44081E-02 0.01338 ];
INF_SCATTP2               (idx, [1:   4]) = [  7.83000E-03 0.00895  4.37298E-03 0.06283 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.12120E-03 0.06290  1.73634E-03 0.14846 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.08723E-04 1.00000  6.96325E-04 0.20584 ];
INF_SCATTP5               (idx, [1:   4]) = [ -6.76417E-06 1.00000  5.35322E-04 0.50312 ];
INF_SCATTP6               (idx, [1:   4]) = [ -6.23215E-05 1.00000  3.05993E-04 0.63986 ];
INF_SCATTP7               (idx, [1:   4]) = [  3.79737E-05 1.00000  2.93662E-04 0.88143 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  1.06235E-01 0.00263  2.32132E-01 0.00156 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  3.13780E+00 0.00264  1.43598E+00 0.00156 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [ -1.18980E-03 0.02290  4.21804E-04 0.00174 ];
INF_REMXS                 (idx, [1:   4]) = [  2.31860E-03 0.01241  5.16393E-04 0.07364 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  1.65500E-01 0.00106  1.78261E-03 0.02018  8.51676E-05 0.15398  2.57406E-01 0.00032 ];
INF_S1                    (idx, [1:   8]) = [  2.53571E-02 0.00609 -1.66143E-04 0.11602  2.41845E-05 0.32219  2.43839E-02 0.01356 ];
INF_S2                    (idx, [1:   8]) = [  7.84712E-03 0.00765 -8.46906E-05 0.15088 -3.05842E-06 1.00000  4.37604E-03 0.06310 ];
INF_S3                    (idx, [1:   8]) = [  2.16777E-03 0.06244 -7.14713E-05 0.20968  8.78505E-07 1.00000  1.73546E-03 0.15097 ];
INF_S4                    (idx, [1:   8]) = [  1.18413E-04 1.00000 -3.32800E-05 0.25629  4.22622E-06 0.86393  6.92099E-04 0.20310 ];
INF_S5                    (idx, [1:   8]) = [ -1.55744E-05 1.00000 -2.14155E-06 1.00000  5.55270E-06 0.30983  5.29770E-04 0.51002 ];
INF_S6                    (idx, [1:   8]) = [ -5.12271E-05 1.00000 -4.01840E-06 1.00000  3.60175E-06 0.56902  3.02391E-04 0.64904 ];
INF_S7                    (idx, [1:   8]) = [  2.05134E-05 1.00000  1.08061E-05 0.69976 -1.40693E-06 1.00000  2.95068E-04 0.87104 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  1.67242E-01 0.00089  1.78261E-03 0.02018  8.51676E-05 0.15398  2.57406E-01 0.00032 ];
INF_SP1                   (idx, [1:   8]) = [  2.57637E-02 0.00575 -1.66143E-04 0.11602  2.41845E-05 0.32219  2.43839E-02 0.01356 ];
INF_SP2                   (idx, [1:   8]) = [  7.91469E-03 0.00885 -8.46906E-05 0.15088 -3.05842E-06 1.00000  4.37604E-03 0.06310 ];
INF_SP3                   (idx, [1:   8]) = [  2.19267E-03 0.06272 -7.14713E-05 0.20968  8.78505E-07 1.00000  1.73546E-03 0.15097 ];
INF_SP4                   (idx, [1:   8]) = [  1.42003E-04 0.98182 -3.32800E-05 0.25629  4.22622E-06 0.86393  6.92099E-04 0.20310 ];
INF_SP5                   (idx, [1:   8]) = [ -4.62262E-06 1.00000 -2.14155E-06 1.00000  5.55270E-06 0.30983  5.29770E-04 0.51002 ];
INF_SP6                   (idx, [1:   8]) = [ -5.83031E-05 1.00000 -4.01840E-06 1.00000  3.60175E-06 0.56902  3.02391E-04 0.64904 ];
INF_SP7                   (idx, [1:   8]) = [  2.71676E-05 1.00000  1.08061E-05 0.69976 -1.40693E-06 1.00000  2.95068E-04 0.87104 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  1.15217E-01 0.00419  1.20844E-01 0.01285 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  1.30998E-01 0.00887  1.51923E-01 0.01830 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  1.31168E-01 0.00754  1.55666E-01 0.02174 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  9.27883E-02 0.00446  8.47187E-02 0.01451 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  2.89330E+00 0.00419  2.76022E+00 0.01299 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  2.54536E+00 0.00889  2.19705E+00 0.01839 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  2.54185E+00 0.00753  2.14560E+00 0.02286 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  3.59269E+00 0.00446  3.93800E+00 0.01493 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/keffComparison/notCoupledReflector' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Mon Apr 29 18:00:27 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Mon Apr 29 18:02:59 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1714431627003 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 1 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
SHARE_BUF_ARRAY           (idx, 1)        = 1 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.24595E-01 0.00069  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.75405E-01 0.00051  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.78360E-01 0.00033  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.69741E-01 0.00040  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.33456E+00 0.00058  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.36548E-01 3.0E-05  6.14973E-02 0.00045  1.95475E-03 0.00132  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.92027E+01 0.00099  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.89553E+01 0.00099  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.69696E+01 0.00088  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.72076E+01 0.00084  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 1000105 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00010E+04 0.00150 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00010E+04 0.00150 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.54596E+00 ;
RUNNING_TIME              (idx, 1)        =  2.54663E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  2.70233E-01  2.70233E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.00000E-03  7.00000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.26938E+00  2.26938E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.54632E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.99974 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  9.99972E-01 3.6E-05 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.39377E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 3746.92 ;
MEMSIZE                   (idx, 1)        = 3696.39 ;
XS_MEMSIZE                (idx, 1)        = 3030.45 ;
MAT_MEMSIZE               (idx, 1)        = 593.42 ;
RES_MEMSIZE               (idx, 1)        = 5.50 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 67.02 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 50.54 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 54 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 948120 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 20 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 53 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 53 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 1698 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  6.66817E+15 0.00107  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.20786E-03 0.01939 ];
U235_FISS                 (idx, [1:   4]) = [  3.08365E+19 0.00099  9.99875E-01 1.6E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.85457E+15 0.12526  1.25464E-04 0.12543 ];
U235_CAPT                 (idx, [1:   4]) = [  6.00677E+18 0.00328  2.88377E-01 0.00260 ];
U238_CAPT                 (idx, [1:   4]) = [  1.54452E+17 0.01948  7.41312E-03 0.01935 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 1000105 1.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.93066E+04 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 1000105 1.03931E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 302023 3.12368E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 450694 4.62532E+05 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 247388 2.64406E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 1000105 1.03931E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.59489E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.00000E+09 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.13889E+02 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  7.52146E+19 1.7E-06 ];
TOT_FISSRATE              (idx, [1:   2]) = [  3.08572E+19 7.0E-09 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.08446E+19 0.00117 ];
TOT_ABSRATE               (idx, [1:   2]) = [  5.17018E+19 0.00047 ];
TOT_SRCRATE               (idx, [1:   2]) = [  6.66817E+19 0.00107 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.74850E+21 0.00123 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.76322E+19 0.00241 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  6.93340E+19 0.00077 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.41180E+21 0.00139 ];
INI_FMASS                 (idx, 1)        =  8.78052E+00 ;
TOT_FMASS                 (idx, 1)        =  8.78052E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06731E+00 0.00063 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.43951E-01 0.00067 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.13008E-01 0.00054 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09154E+00 0.00047 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.03684E-01 0.00050 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.15276E-01 0.00040 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.53270E+00 0.00099 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.12743E+00 0.00107 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43750E+00 1.7E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02271E+02 7.8E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.12769E+00 0.00107  1.11889E+00 0.00108  8.54617E-03 0.01665 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.12771E+00 0.00074 ];
COL_KEFF                  (idx, [1:   2]) = [  1.12809E+00 0.00107 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.12771E+00 0.00074 ];
ABS_KINF                  (idx, [1:   2]) = [  1.53290E+00 0.00047 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93799E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93781E+01 9.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.66971E-08 0.00405 ];
IMP_EALF                  (idx, [1:   2]) = [  7.67925E-08 0.00187 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.49153E-02 0.02047 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.47001E-02 0.00260 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.25171E-03 0.01221  2.06791E-04 0.08419  9.68906E-04 0.03305  5.70877E-04 0.04142  1.26434E-03 0.02876  1.98477E-03 0.02022  5.62768E-04 0.04033  5.31306E-04 0.03989  1.61952E-04 0.07740 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.19768E-01 0.01894  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.2E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.5E-09  1.63478E+00 0.0E+00  3.55460E+00 5.8E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.68581E-03 0.01756  2.43120E-04 0.10572  1.18939E-03 0.05340  7.46828E-04 0.05661  1.55365E-03 0.03910  2.36348E-03 0.03105  7.43194E-04 0.05501  6.69891E-04 0.06306  1.76262E-04 0.10411 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.17090E-01 0.02722  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.0E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.8E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  8.82358E-05 0.00304  8.82857E-05 0.00303  8.21027E-05 0.02644 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  9.94910E-05 0.00285  9.95473E-05 0.00285  9.25621E-05 0.02633 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.55957E-03 0.01720  2.47402E-04 0.10642  1.20109E-03 0.04967  7.82544E-04 0.05562  1.48736E-03 0.04055  2.31711E-03 0.02962  7.16606E-04 0.05858  6.29467E-04 0.05886  1.77979E-04 0.10991 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08987E-01 0.02952  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.2E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.6E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  8.42664E-05 0.02418  8.43027E-05 0.02419  7.57347E-05 0.06833 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  9.50299E-05 0.02412  9.50718E-05 0.02414  8.52823E-05 0.06790 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  7.05636E-03 0.06679  1.36965E-04 0.39584  1.05224E-03 0.13398  8.41164E-04 0.15796  1.50563E-03 0.11874  1.95341E-03 0.12936  6.65014E-04 0.17788  6.44086E-04 0.18971  2.57844E-04 0.33638 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.70643E-01 0.09770  1.24667E-02 3.9E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  7.13771E-03 0.06593  1.55038E-04 0.35940  1.07081E-03 0.12644  7.79117E-04 0.15912  1.53778E-03 0.11224  1.94960E-03 0.12871  7.07102E-04 0.17448  6.79272E-04 0.18312  2.59005E-04 0.33198 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.77766E-01 0.09826  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.1E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -8.34870E+01 0.06030 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  8.90768E-05 0.00165 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  1.00439E-04 0.00119 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.60720E-03 0.00849 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -8.54599E+01 0.00914 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.24184E-06 0.00137 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.79587E-06 0.00103  4.79833E-06 0.00103  4.47210E-06 0.01088 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.39478E-04 0.00192  1.39576E-04 0.00193  1.26735E-04 0.02133 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.39797E-01 0.00062  7.38937E-01 0.00061  8.90264E-01 0.01951 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30932E+01 0.02543 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.89553E+01 0.00099  4.86512E+01 0.00114 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   5]) = 'oPuni' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  5.69827E+03 0.03232  2.34186E+04 0.01568  4.79260E+04 0.00958  6.39839E+04 0.00874  5.63846E+04 0.01323  4.44273E+04 0.01025  3.48901E+04 0.00540  2.62028E+04 0.01360  1.98542E+04 0.01711  1.64453E+04 0.00675  1.41150E+04 0.01342  1.22748E+04 0.01844  1.16783E+04 0.01877  1.14392E+04 0.01530  1.09075E+04 0.02150  9.48093E+03 0.01340  9.19170E+03 0.00542  9.47602E+03 0.01982  9.16138E+03 0.01212  1.81168E+04 0.01479  1.80406E+04 0.00781  1.31514E+04 0.01042  8.76199E+03 0.01353  1.03634E+04 0.02002  1.04361E+04 0.01269  8.78928E+03 0.01697  1.62327E+04 0.01013  3.61214E+03 0.03083  4.44542E+03 0.01634  4.01059E+03 0.01900  2.33448E+03 0.03291  3.84204E+03 0.04449  2.76044E+03 0.03087  2.24892E+03 0.03527  3.90077E+02 0.04816  3.92636E+02 0.04908  3.81544E+02 0.17564  4.18592E+02 0.06780  4.34223E+02 0.02536  3.81761E+02 0.08177  3.92339E+02 0.07414  3.60246E+02 0.06391  7.98999E+02 0.07630  1.16719E+03 0.04480  1.52070E+03 0.04407  3.94898E+03 0.01368  4.05717E+03 0.03308  3.96624E+03 0.01356  2.36547E+03 0.04365  1.54966E+03 0.05823  1.14434E+03 0.05548  1.21671E+03 0.04711  2.08504E+03 0.04471  2.54707E+03 0.02526  4.58946E+03 0.01177  8.17577E+03 0.01277  1.73758E+04 0.00741  1.64877E+04 0.01225  1.48883E+04 0.00805  1.27498E+04 0.01915  1.31669E+04 0.01391  1.47875E+04 0.00761  1.35085E+04 0.01646  9.99790E+03 0.01794  1.02049E+04 0.01007  9.75407E+03 0.01427  8.76391E+03 0.01590  7.29000E+03 0.01360  5.17901E+03 0.01951  1.89988E+03 0.00249 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.91606E+20 0.00251  6.25980E+19 0.00461 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  4.10252E-02 0.00207  1.00494E-01 0.00045 ];
INF_CAPT                  (idx, [1:   4]) = [  1.57868E-05 0.00334  9.12176E-04 0.00097 ];
INF_ABS                   (idx, [1:   4]) = [  1.57868E-05 0.00334  9.12176E-04 0.00097 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  5.88601E-08 0.00332  3.38725E-06 0.00097 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.10156E-02 0.00219  9.95321E-02 0.00041 ];
INF_SCATT1                (idx, [1:   4]) = [  2.72350E-02 0.00294  4.15911E-02 0.00363 ];
INF_SCATT2                (idx, [1:   4]) = [  1.03215E-02 0.00734  1.57870E-02 0.01294 ];
INF_SCATT3                (idx, [1:   4]) = [  1.53283E-04 0.51871  6.02653E-03 0.02653 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.76216E-03 0.04055  2.46851E-03 0.06903 ];
INF_SCATT5                (idx, [1:   4]) = [ -2.07924E-04 0.19899  1.15710E-03 0.05519 ];
INF_SCATT6                (idx, [1:   4]) = [  5.56546E-04 0.01968  7.89754E-04 0.09389 ];
INF_SCATT7                (idx, [1:   4]) = [  1.06496E-04 0.22523  7.10577E-04 0.09704 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.10156E-02 0.00219  9.95321E-02 0.00041 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.72350E-02 0.00294  4.15911E-02 0.00363 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.03215E-02 0.00734  1.57870E-02 0.01294 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.53283E-04 0.51871  6.02653E-03 0.02653 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.76216E-03 0.04055  2.46851E-03 0.06903 ];
INF_SCATTP5               (idx, [1:   4]) = [ -2.07924E-04 0.19899  1.15710E-03 0.05519 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.56546E-04 0.01968  7.89754E-04 0.09389 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.06496E-04 0.22523  7.10577E-04 0.09704 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  6.79620E-03 0.00282  5.10593E-02 0.00334 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  4.90485E+01 0.00280  6.52865E+00 0.00335 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.57868E-05 0.00334  9.12176E-04 0.00097 ];
INF_REMXS                 (idx, [1:   4]) = [  2.36587E-03 0.02193  9.79785E-04 0.02069 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  3.86594E-02 0.00263  2.35619E-03 0.01079  1.81761E-05 0.19597  9.95139E-02 0.00042 ];
INF_S1                    (idx, [1:   8]) = [  2.62770E-02 0.00314  9.57987E-04 0.01008  1.61626E-05 0.18982  4.15750E-02 0.00357 ];
INF_S2                    (idx, [1:   8]) = [  1.05211E-02 0.00726 -1.99559E-04 0.04044  1.29213E-05 0.18602  1.57741E-02 0.01286 ];
INF_S3                    (idx, [1:   8]) = [  6.05342E-04 0.12944 -4.52059E-04 0.00770  9.55744E-06 0.20712  6.01697E-03 0.02671 ];
INF_S4                    (idx, [1:   8]) = [ -1.56193E-03 0.04047 -2.00234E-04 0.05539  6.89828E-06 0.28295  2.46162E-03 0.06988 ];
INF_S5                    (idx, [1:   8]) = [ -2.27437E-04 0.16180  1.95128E-05 0.45620  5.16804E-06 0.39646  1.15193E-03 0.05599 ];
INF_S6                    (idx, [1:   8]) = [  5.08419E-04 0.03204  4.81271E-05 0.12983  4.09609E-06 0.48105  7.85658E-04 0.09228 ];
INF_S7                    (idx, [1:   8]) = [  8.96771E-05 0.20618  1.68193E-05 0.52611  3.28538E-06 0.57036  7.07291E-04 0.09615 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  3.86594E-02 0.00263  2.35619E-03 0.01079  1.81761E-05 0.19597  9.95139E-02 0.00042 ];
INF_SP1                   (idx, [1:   8]) = [  2.62770E-02 0.00314  9.57987E-04 0.01008  1.61626E-05 0.18982  4.15750E-02 0.00357 ];
INF_SP2                   (idx, [1:   8]) = [  1.05211E-02 0.00726 -1.99559E-04 0.04044  1.29213E-05 0.18602  1.57741E-02 0.01286 ];
INF_SP3                   (idx, [1:   8]) = [  6.05342E-04 0.12944 -4.52059E-04 0.00770  9.55744E-06 0.20712  6.01697E-03 0.02671 ];
INF_SP4                   (idx, [1:   8]) = [ -1.56193E-03 0.04047 -2.00234E-04 0.05539  6.89828E-06 0.28295  2.46162E-03 0.06988 ];
INF_SP5                   (idx, [1:   8]) = [ -2.27437E-04 0.16180  1.95128E-05 0.45620  5.16804E-06 0.39646  1.15193E-03 0.05599 ];
INF_SP6                   (idx, [1:   8]) = [  5.08419E-04 0.03204  4.81271E-05 0.12983  4.09609E-06 0.48105  7.85658E-04 0.09228 ];
INF_SP7                   (idx, [1:   8]) = [  8.96771E-05 0.20618  1.68193E-05 0.52611  3.28538E-06 0.57036  7.07291E-04 0.09615 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  1.58794E-01 0.00508  1.94057E-01 0.00889 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  1.80750E-01 0.00813  2.42981E-01 0.01513 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  1.80992E-01 0.00515  2.48546E-01 0.01593 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  1.27664E-01 0.00733  1.36737E-01 0.01171 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  2.09938E+00 0.00509  1.71826E+00 0.00903 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.84466E+00 0.00808  1.37313E+00 0.01540 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.84189E+00 0.00513  1.34251E+00 0.01605 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  2.61158E+00 0.00732  2.43914E+00 0.01200 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/keffComparison/notCoupledReflector' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Mon Apr 29 18:00:27 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Mon Apr 29 18:02:59 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1714431627003 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 1 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
SHARE_BUF_ARRAY           (idx, 1)        = 1 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.24595E-01 0.00069  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.75405E-01 0.00051  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.78360E-01 0.00033  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.69741E-01 0.00040  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.33456E+00 0.00058  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.36548E-01 3.0E-05  6.14973E-02 0.00045  1.95475E-03 0.00132  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.92027E+01 0.00099  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.89553E+01 0.00099  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.69696E+01 0.00088  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.72076E+01 0.00084  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 1000105 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00010E+04 0.00150 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00010E+04 0.00150 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.54596E+00 ;
RUNNING_TIME              (idx, 1)        =  2.54663E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  2.70233E-01  2.70233E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.00000E-03  7.00000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.26938E+00  2.26938E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.54632E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.99974 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  9.99972E-01 3.6E-05 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.39377E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 3746.92 ;
MEMSIZE                   (idx, 1)        = 3696.39 ;
XS_MEMSIZE                (idx, 1)        = 3030.45 ;
MAT_MEMSIZE               (idx, 1)        = 593.42 ;
RES_MEMSIZE               (idx, 1)        = 5.50 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 67.02 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 50.54 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 54 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 948120 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 20 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 53 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 53 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 1698 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  6.66817E+15 0.00107  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.20786E-03 0.01939 ];
U235_FISS                 (idx, [1:   4]) = [  3.08365E+19 0.00099  9.99875E-01 1.6E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.85457E+15 0.12526  1.25464E-04 0.12543 ];
U235_CAPT                 (idx, [1:   4]) = [  6.00677E+18 0.00328  2.88377E-01 0.00260 ];
U238_CAPT                 (idx, [1:   4]) = [  1.54452E+17 0.01948  7.41312E-03 0.01935 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 1000105 1.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.93066E+04 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 1000105 1.03931E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 302023 3.12368E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 450694 4.62532E+05 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 247388 2.64406E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 1000105 1.03931E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.59489E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.00000E+09 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.13889E+02 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  7.52146E+19 1.7E-06 ];
TOT_FISSRATE              (idx, [1:   2]) = [  3.08572E+19 7.0E-09 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.08446E+19 0.00117 ];
TOT_ABSRATE               (idx, [1:   2]) = [  5.17018E+19 0.00047 ];
TOT_SRCRATE               (idx, [1:   2]) = [  6.66817E+19 0.00107 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.74850E+21 0.00123 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.76322E+19 0.00241 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  6.93340E+19 0.00077 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.41180E+21 0.00139 ];
INI_FMASS                 (idx, 1)        =  8.78052E+00 ;
TOT_FMASS                 (idx, 1)        =  8.78052E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06731E+00 0.00063 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.43951E-01 0.00067 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.13008E-01 0.00054 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09154E+00 0.00047 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.03684E-01 0.00050 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.15276E-01 0.00040 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.53270E+00 0.00099 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.12743E+00 0.00107 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43750E+00 1.7E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02271E+02 7.8E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.12769E+00 0.00107  1.11889E+00 0.00108  8.54617E-03 0.01665 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.12771E+00 0.00074 ];
COL_KEFF                  (idx, [1:   2]) = [  1.12809E+00 0.00107 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.12771E+00 0.00074 ];
ABS_KINF                  (idx, [1:   2]) = [  1.53290E+00 0.00047 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93799E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93781E+01 9.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.66971E-08 0.00405 ];
IMP_EALF                  (idx, [1:   2]) = [  7.67925E-08 0.00187 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.49153E-02 0.02047 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.47001E-02 0.00260 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.25171E-03 0.01221  2.06791E-04 0.08419  9.68906E-04 0.03305  5.70877E-04 0.04142  1.26434E-03 0.02876  1.98477E-03 0.02022  5.62768E-04 0.04033  5.31306E-04 0.03989  1.61952E-04 0.07740 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.19768E-01 0.01894  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.2E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.5E-09  1.63478E+00 0.0E+00  3.55460E+00 5.8E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.68581E-03 0.01756  2.43120E-04 0.10572  1.18939E-03 0.05340  7.46828E-04 0.05661  1.55365E-03 0.03910  2.36348E-03 0.03105  7.43194E-04 0.05501  6.69891E-04 0.06306  1.76262E-04 0.10411 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.17090E-01 0.02722  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.0E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.8E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  8.82358E-05 0.00304  8.82857E-05 0.00303  8.21027E-05 0.02644 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  9.94910E-05 0.00285  9.95473E-05 0.00285  9.25621E-05 0.02633 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.55957E-03 0.01720  2.47402E-04 0.10642  1.20109E-03 0.04967  7.82544E-04 0.05562  1.48736E-03 0.04055  2.31711E-03 0.02962  7.16606E-04 0.05858  6.29467E-04 0.05886  1.77979E-04 0.10991 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08987E-01 0.02952  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.2E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.6E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  8.42664E-05 0.02418  8.43027E-05 0.02419  7.57347E-05 0.06833 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  9.50299E-05 0.02412  9.50718E-05 0.02414  8.52823E-05 0.06790 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  7.05636E-03 0.06679  1.36965E-04 0.39584  1.05224E-03 0.13398  8.41164E-04 0.15796  1.50563E-03 0.11874  1.95341E-03 0.12936  6.65014E-04 0.17788  6.44086E-04 0.18971  2.57844E-04 0.33638 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.70643E-01 0.09770  1.24667E-02 3.9E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  7.13771E-03 0.06593  1.55038E-04 0.35940  1.07081E-03 0.12644  7.79117E-04 0.15912  1.53778E-03 0.11224  1.94960E-03 0.12871  7.07102E-04 0.17448  6.79272E-04 0.18312  2.59005E-04 0.33198 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.77766E-01 0.09826  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.1E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -8.34870E+01 0.06030 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  8.90768E-05 0.00165 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  1.00439E-04 0.00119 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.60720E-03 0.00849 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -8.54599E+01 0.00914 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.24184E-06 0.00137 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.79587E-06 0.00103  4.79833E-06 0.00103  4.47210E-06 0.01088 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.39478E-04 0.00192  1.39576E-04 0.00193  1.26735E-04 0.02133 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.39797E-01 0.00062  7.38937E-01 0.00061  8.90264E-01 0.01951 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30932E+01 0.02543 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.89553E+01 0.00099  4.86512E+01 0.00114 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   4]) = 'muni' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  3.48906E+04 0.01345  1.46061E+05 0.00328  2.94607E+05 0.00133  4.00774E+05 0.00202  3.62561E+05 0.00241  2.95997E+05 0.00133  2.40422E+05 0.00234  1.86077E+05 0.00194  1.47799E+05 0.00194  1.22990E+05 0.00078  1.07030E+05 0.00166  9.67818E+04 0.00186  9.03334E+04 0.00192  8.64260E+04 0.00223  8.43218E+04 0.00048  7.34209E+04 0.00107  7.29459E+04 0.00257  7.22594E+04 0.00131  7.15506E+04 0.00174  1.42210E+05 0.00149  1.39778E+05 0.00154  1.03388E+05 0.00223  6.80896E+04 0.00217  8.23519E+04 0.00121  8.11059E+04 0.00098  6.99813E+04 0.00046  1.30301E+05 0.00107  2.76898E+04 0.00039  3.43671E+04 0.00221  3.07383E+04 0.00190  1.77874E+04 0.00407  3.06692E+04 0.00362  2.05431E+04 0.00336  1.76062E+04 0.00415  3.36130E+03 0.00618  3.29846E+03 0.00694  3.39870E+03 0.00642  3.49600E+03 0.01042  3.44990E+03 0.00424  3.38293E+03 0.00712  3.47138E+03 0.00573  3.20322E+03 0.01290  6.10205E+03 0.00824  9.73928E+03 0.00702  1.22915E+04 0.00435  3.17899E+04 0.00151  3.22356E+04 0.00289  3.23725E+04 0.00263  1.95026E+04 0.00376  1.31677E+04 0.00249  9.56018E+03 0.00439  1.03569E+04 0.00463  1.75884E+04 0.00703  2.12785E+04 0.00391  3.97448E+04 0.00305  7.12952E+04 0.00233  1.59571E+05 0.00176  1.52068E+05 0.00099  1.41812E+05 0.00236  1.23094E+05 0.00175  1.27893E+05 0.00195  1.45559E+05 0.00218  1.40152E+05 0.00178  1.05256E+05 0.00175  1.07472E+05 0.00216  1.06079E+05 0.00252  9.97434E+04 0.00139  8.59752E+04 0.00279  6.25497E+04 0.00137  2.48869E+04 0.00281 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.35725E+21 0.00090  6.16542E+20 0.00145 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.74856E-01 0.00017  1.24724E+00 0.00011 ];
INF_CAPT                  (idx, [1:   4]) = [  2.40895E-04 0.00094  1.18138E-02 0.00026 ];
INF_ABS                   (idx, [1:   4]) = [  2.40895E-04 0.00094  1.18138E-02 0.00026 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  6.56580E-08 0.00039  3.57262E-06 0.00026 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.74616E-01 0.00017  1.23543E+00 0.00010 ];
INF_SCATT1                (idx, [1:   4]) = [  3.29393E-01 0.00012  4.63247E-01 0.00044 ];
INF_SCATT2                (idx, [1:   4]) = [  1.24239E-01 0.00049  1.73580E-01 0.00087 ];
INF_SCATT3                (idx, [1:   4]) = [  2.79397E-03 0.02398  6.78420E-02 0.00286 ];
INF_SCATT4                (idx, [1:   4]) = [ -2.00962E-02 0.00257  2.97759E-02 0.00453 ];
INF_SCATT5                (idx, [1:   4]) = [ -2.09359E-03 0.02826  1.58070E-02 0.00577 ];
INF_SCATT6                (idx, [1:   4]) = [  6.75566E-03 0.00834  9.99946E-03 0.00762 ];
INF_SCATT7                (idx, [1:   4]) = [  1.11795E-03 0.04502  7.29516E-03 0.00348 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.74616E-01 0.00017  1.23543E+00 0.00010 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.29393E-01 0.00012  4.63247E-01 0.00044 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.24239E-01 0.00049  1.73580E-01 0.00087 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.79397E-03 0.02398  6.78420E-02 0.00286 ];
INF_SCATTP4               (idx, [1:   4]) = [ -2.00962E-02 0.00257  2.97759E-02 0.00453 ];
INF_SCATTP5               (idx, [1:   4]) = [ -2.09359E-03 0.02826  1.58070E-02 0.00577 ];
INF_SCATTP6               (idx, [1:   4]) = [  6.75566E-03 0.00834  9.99946E-03 0.00762 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.11795E-03 0.04502  7.29516E-03 0.00348 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  1.64587E-01 0.00036  6.96072E-01 0.00034 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  2.02528E+00 0.00036  4.78878E-01 0.00034 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  2.40895E-04 0.00094  1.18138E-02 0.00026 ];
INF_REMXS                 (idx, [1:   4]) = [  2.95757E-02 0.00033  1.19852E-02 0.00249 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.45280E-01 0.00017  2.93359E-02 0.00042  1.72766E-04 0.03716  1.23525E+00 0.00010 ];
INF_S1                    (idx, [1:   8]) = [  3.17798E-01 0.00015  1.15949E-02 0.00085  1.25490E-04 0.03794  4.63121E-01 0.00043 ];
INF_S2                    (idx, [1:   8]) = [  1.26630E-01 0.00047 -2.39061E-03 0.00465  9.53073E-05 0.03574  1.73485E-01 0.00086 ];
INF_S3                    (idx, [1:   8]) = [  8.21732E-03 0.00748 -5.42335E-03 0.00237  7.29281E-05 0.03473  6.77690E-02 0.00287 ];
INF_S4                    (idx, [1:   8]) = [ -1.77222E-02 0.00273 -2.37394E-03 0.00181  5.14125E-05 0.05003  2.97245E-02 0.00460 ];
INF_S5                    (idx, [1:   8]) = [ -2.24055E-03 0.02812  1.46968E-04 0.07677  3.18678E-05 0.05678  1.57751E-02 0.00586 ];
INF_S6                    (idx, [1:   8]) = [  6.23099E-03 0.00928  5.24664E-04 0.01478  1.77848E-05 0.07152  9.98167E-03 0.00770 ];
INF_S7                    (idx, [1:   8]) = [  9.94254E-04 0.04951  1.23694E-04 0.05813  7.61004E-06 0.26261  7.28755E-03 0.00334 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.45280E-01 0.00017  2.93359E-02 0.00042  1.72766E-04 0.03716  1.23525E+00 0.00010 ];
INF_SP1                   (idx, [1:   8]) = [  3.17798E-01 0.00015  1.15949E-02 0.00085  1.25490E-04 0.03794  4.63121E-01 0.00043 ];
INF_SP2                   (idx, [1:   8]) = [  1.26630E-01 0.00047 -2.39061E-03 0.00465  9.53073E-05 0.03574  1.73485E-01 0.00086 ];
INF_SP3                   (idx, [1:   8]) = [  8.21732E-03 0.00748 -5.42335E-03 0.00237  7.29281E-05 0.03473  6.77690E-02 0.00287 ];
INF_SP4                   (idx, [1:   8]) = [ -1.77222E-02 0.00273 -2.37394E-03 0.00181  5.14125E-05 0.05003  2.97245E-02 0.00460 ];
INF_SP5                   (idx, [1:   8]) = [ -2.24055E-03 0.02812  1.46968E-04 0.07677  3.18678E-05 0.05678  1.57751E-02 0.00586 ];
INF_SP6                   (idx, [1:   8]) = [  6.23099E-03 0.00928  5.24664E-04 0.01478  1.77848E-05 0.07152  9.98167E-03 0.00770 ];
INF_SP7                   (idx, [1:   8]) = [  9.94254E-04 0.04951  1.23694E-04 0.05813  7.61004E-06 0.26261  7.28755E-03 0.00334 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  1.16481E-01 0.00158  3.60862E-01 0.00661 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  1.22890E-01 0.00263  3.82679E-01 0.00683 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  1.23172E-01 0.00322  3.85318E-01 0.01037 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  1.05278E-01 0.00210  3.22172E-01 0.00852 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  2.86172E+00 0.00158  9.23875E-01 0.00660 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  2.71253E+00 0.00264  8.71215E-01 0.00682 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  2.70636E+00 0.00323  8.65465E-01 0.01053 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  3.16629E+00 0.00211  1.03495E+00 0.00855 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

