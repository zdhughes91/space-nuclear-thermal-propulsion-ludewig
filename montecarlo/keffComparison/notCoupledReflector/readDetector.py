"""

"""

# --- importing libraries
import numpy as np
import os
from oct2py import octave as oct
from scipy.io import loadmat
import matplotlib.pyplot as plt
# ---

# --- needed constants
cwd = os.getcwd()
# ---

# --- reading octave output scripts
oct.eval(f"cd {cwd}")
oct.eval("settings_det0")
oct.eval("save -v7 myworkspace.mat")
m = loadmat(f"{cwd}/myworkspace.mat")
detList = []
for i in m:
    if i[:3] == "DET": detList.append(i) # save the detector names
d = {} # d is a dictionary containing detector results
for det in detList:
    if det[-1] == 'E':
        d[det] = [innerList[0] for innerList in m[det]]
        continue
    d[det] = [innerList[10] for innerList in m[det]]
# --- 

# --- plotting core neutron energy spectrum
# plt.figure(1,dpi=200)
# plt.plot(d['DETcore_spectrumE'],d['DETcore_spectrum'])
# plt.xscale('log')
# plt.xlim(5e-9,5e1)
# plt.xlabel("Neutron Energy (MeV)")
# plt.ylabel(r"Neutron Flux $\frac{n}{cm^{2} s}$")
# plt.title("Core Neutron Energy Spectrum")
# plt.grid(which='major', color='gray', linestyle='-')
# plt.minorticks_on()
# plt.grid(which='minor', color='lightgray', linestyle='--')
# plt.show()
# ---
