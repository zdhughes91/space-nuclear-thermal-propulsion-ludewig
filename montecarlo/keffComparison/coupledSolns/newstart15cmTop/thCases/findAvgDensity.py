import numpy as np


rhoDict = {'a15':[],
           'a16':[],
           'a17':[],
           'a18':[],
           'a22':[],
           'a23':[]
           }
multDict = {'a15':6,
           'a16':6,
           'a17':6,
           'a18':1,
           'a22':12,
           'a23':6
           }
sum = 0
for a in rhoDict:
    rhoDict[a] = np.genfromtxt(f'./{a}/0.5/fluidRegion/densityField',skip_header=23,skip_footer=74233-74172)
    sum += np.sum(rhoDict[a]/74148)*multDict[a]

print(f'av dens = {sum*1e-3/6.0:0.7f} g/cc')