

path = "0/fluidRegion/powerDensityNeutronics"
volumeFraction = 0.58

with open(path,'r') as file:
    lines = file.readlines()
    for i,line in enumerate(lines):
        if line.startswith('('):
            startnum = i
        elif line.startswith(')'):
            endnum = i

for i in range(startnum+1,endnum): # looping thru all numbers in 
    numbers = [format(float(number) / volumeFraction, '.6e') for number in lines[i].split()]
    lines[i] = ' '.join(map(str, numbers)) + '\n'

with open(path, 'w') as file:
    file.writelines(lines)


