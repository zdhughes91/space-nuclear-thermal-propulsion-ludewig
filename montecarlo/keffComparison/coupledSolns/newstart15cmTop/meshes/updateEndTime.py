""" 

    a code to update the time folder each of the meshes read from. 
    
    Author: Zach Hughes - zhughes@tamu.edu
    Date: Apr.14.2024
13-18
"""
# - user inputs
oldTime = 0
newTime = 0.5
# -

def changeTime(path):
    global newTime,oldTime
    with open(path,'r') as file:
        lines = file.readlines()
        
    for i, line in enumerate(lines):
        if i == 9:
            front = line[:14]
            back = line[14+len(str(oldTime)):]
            newline = front + str(newTime) + back
            lines[i] = newline
            
        if i == 10:
            front = line[:14]
            back = line[14+len(str(oldTime)):]
            newline = front + str(newTime) + back
            lines[i] = newline
        
    with open(path, 'w') as file:
        file.writelines(lines)
        
        
for i in range(37):
    changeTime(f'./m{i}/{i}.ifc')

# for i in range(37):
#     print(f'dm fuelmat{i}')