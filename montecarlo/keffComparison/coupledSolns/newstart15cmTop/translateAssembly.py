""" 
    =============================================================================
    This is an executable to translate the openfoam assembly mesh to the 
    desired position. In order for the serpent/openfoam coupling to work, the polyMesh must be
    translated so that the values within it occupy the points that the assembly
    occupies in serpent. transforming the universe in serpent does not work.
    
    This code also adds a transform card to the end of the 'openfoamUniCoolantChannels' file. This
    is necessary to put the geometry back at the origin so it can fit in the 'th' universe. If you 
    are changing from one lattice position to another, you will need to remove this transform card
    so there are not two
    
    The SNTP core lattice is shown below
    _ _ _ _ _ _ _ _ _ _ _               
     _ _ _ _ _ x x x x _ _                
      _ _ _ _ x x x x x _ _              
       _ _ _ x x x x x x _ _               
        _ _ x x x 0 1 2 3 _ _       ------------> y
         _ _ x x x 4 5 6 _ _ _             
          _ _ x x x 7 8 _ _ _ _            
           _ _ x x x 9 _ _ _ _ _         
            _ _ _ _ _ _ _ _ _ _ _         
                  |
                  |
                  |
                  |
                 \/
                  x
                  
    Since there is symmetry over a 1/6 slice, it is only necessary to put the
    assembly in any of the numbered locations. 
    
    !!!!
        Note: You need to create a backup polyMesh in case you make a mistake. This
              file will overwrite your previous polyMesh
    !!!!
    =============================================================================

    Author: Zach Hughes - zhughes@tamu.edu
    Date: Jan.29.2024
"""
# --- user inputs
assemblyLocation = 9        # location number shown in diagram above
updateMeshLocation = False  # do you want the code to change the polyMesh file?
openfoamCaseDirectory = './genfoamCase/'
# --- 

import numpy as np
import math
import matplotlib.pyplot as plt
import subprocess
import os


# geometric constants
f2f = 9.131 # cm    flat to flat distance
assembliesPerRing = [1,6,12,18]
initPoints = [i*f2f for i in range(4)]
deltaAngle = [360.0/assembly for assembly in assembliesPerRing ]

# executable
transCommand = ['transformPoints','-translate']


# first I will just solve the first 1/6 locations. Then I will expand to include the rest
assemblyLocList = []
# xl,yl=[],[]
column2, column3, column4 = 0,0,0
for i in range(0,12):
    if i < 4:
        y = f2f*i
        x = 0
    elif i < 7:
        y = f2f/2 + f2f*column2
        x = np.sqrt(3)*f2f/2
        column2 += 1
        if i == 5: lasty = y
    elif i < 10:
        if i == 7:
            column3 += 1
            continue
        y = f2f*column3
        x = np.sqrt(3)*f2f
        column3 += 1
    elif i == 11:
        y = lasty
        x = 3*np.sqrt(3)*f2f/2
    # xl.append(x)
    # yl.append(y)
    assemblyLocList.append([x*1e-2,y*1e-2])

# print(xl,yl)
# plt.scatter(xl,yl)
# plt.show()
transCommand.append(f'({assemblyLocList[assemblyLocation][0]:0.6e} {assemblyLocList[assemblyLocation][1]:0.6e} 0)')
# print(transCommand)
if updateMeshLocation:
    os.chdir(openfoamCaseDirectory)
    process = subprocess.Popen(transCommand, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
    return_code = process.wait()
    stdout, stderr = process.communicate()
    print(f'Return Code: {return_code}')
    print(f'Standard Output:\n{stdout.decode()}')
    print(f'Standard Error:\n{stderr.decode()}')
    os.chdir('./../')
    
    with open('./openfoamUniCoolantChannels','a') as file:
        xval = assemblyLocList[assemblyLocation][0]*100
        yval = assemblyLocList[assemblyLocation][1]*100 # m to cm for serpent
        if xval>0:
            xval = xval*-1 # need to move in opposite direction so its back at origin
        if yval>0:
            yval = yval*-1
        radians = math.atan(yval/xval)
        degrees = math.degrees(radians)
        print(degrees)
        file.write(f"trans 99 {xval:0.6e} {yval:0.6e} 0")
