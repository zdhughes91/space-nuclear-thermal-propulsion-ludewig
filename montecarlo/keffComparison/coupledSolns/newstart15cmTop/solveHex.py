"""
    
    Author: Zach Hughes - zhughes@tamu.edu
    Date: Feb.16.2024
"""
# --- user entered information
plot = False
# ---

# --- importing libraries
from hexalattice.hexalattice import *
import os
import subprocess
import shutil
import math
# ---

# --- constants
cwd = os.getcwd()
activeCoreRadius = 30.91 # cm
f2f = 9.131
# --- 

# --- functions
def getHexCoordinates():
    """ returns the x and y coordinates of each assembly"""
    hex_grid1, h_ax = create_hex_grid(nx=50,
                                    ny=50,
                                    rotate_deg=90,
                                    min_diam=f2f,
                                    crop_circ=activeCoreRadius,
                                    do_plot=True)
    tile_centers_x = hex_grid1[:, 0]
    tile_centers_y = hex_grid1[:, 1]
    return tile_centers_x, tile_centers_y

def translateMesh(i,x,y):
    """ translates the mesh using the x and y coordinates"""
    # executable
    transCommand = ['transformPoints','-translate',f"({x} {y} 0)"]
    process = subprocess.Popen(transCommand, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
    return_code = process.wait()
    stdout, stderr = process.communicate()
    print(f'Return Code: {return_code}')
    print(f'Standard Output:\n{stdout.decode()}')
    print(f'Standard Error:\n{stderr.decode()}')

def rotateMesh(i,x,y):
    """ rotates the mesh using x and y coordinates to find the angle """
    deg = math.degrees(math.atan2(-x,-y))
    if deg < 0:
        deg += 360
    deg -= 180
    deg = deg*-1
    print(i,deg)
    rotateCommand = ['transformPoints','-rotate-z',f"{deg}"]
    process = subprocess.Popen(rotateCommand, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
    return_code = process.wait()
    stdout, stderr = process.communicate()
    print(f'Return Code: {return_code}')
    print(f'Standard Output:\n{stdout.decode()}')
    print(f'Standard Error:\n{stderr.decode()}')

def updateMesh(i,x,y):
    """ 
        1. copies original mesh and pastes in case/constant/polymesh
        2. translates
        3. rotates
        4. copies into meshii/m{i}
        5. deletes case/constant/polyMesh
    """
    # 1.
    shutil.copytree("mOrig","case/constant/polyMesh")
    os.chdir("./case/")
    # 2. 
    rotateMesh(i,x,y)
    translateMesh(i,x,y)
    # 3. 
    #rotateMesh(i,x,y)
    os.chdir("./../")
    # 4. 
    shutil.copytree("case/constant/polyMesh",f"./m{int(i)}")
    # 5. 
    shutil.rmtree("case/constant/polyMesh")

def allMesh():
    """ loops through all assemblies to create all meshes"""
    xList, yList = getHexCoordinates()
    for i in range(37):
        updateMesh(i,xList[i]*1e-2,yList[i]*1e-2) # convert cm to meters
# i = 33
# x,y = getHexCoordinates()
# print(x[i],y[i])
# print('60deg')
# for i in [0,3,15,21,33,36]:
#    rotateMesh(i,x[i]*1e-2,y[i]*1e-2)
# print('30deg')
# for i in [13,6,10,23,30,26]:
#     rotateMesh(i,x[i]*1e-2,y[i]*1e-2)
# updateMesh(i,x[i]*1e-2,y[i]*1e-2)
# shutil.copytree(f"./m{int(i)}","case/constant/fluidRegion/polyMesh")
# shutil.copytree(f"./mOrig","case/constant/polyMesh")

# ---

# --- plotting
if False: 
    x,y = getHexCoordinates()
    for i, (xi, yi) in enumerate(zip(x, y)):
        plt.text(xi, yi, str(i), ha='center', va='bottom')
    plt.show()
# ---

# --- to create 37 polyMesh
if False:
    allMesh()
# --- 
    

# --- now writing an interface card to import and initialize all interfaces
"""
    The idea here is to have one file called by the main serpent input file that 
    imports all of the assemblies. I then want to add transform cards at the bottom to bring
    these assemblies to the origin so they can be placed in a new universe that resembles 
    their hexagon. I will also need cells for each one to create their universe. 
"""
# - first writing the solid 3 cards to call each ifc
# with open("all.ifc",'w') as file:
#     for m in range(37):
#         file.write(f"solid 3 \"meshes/m{m}/{m}.ifc\" \n")
# -
        
# - now putting creating individual universe for each assembly.
# cell thAssembly th fill u15 -opor -zBounds1                    % fill w thermal hydraulic model
# cell outerTH    th fill b15  opor -mohx 12 13 14 15 16 17      % moderator outside of th model
# with open("all.ifc",'a') as file:
#     for m in range(37):
#         file.write(f"cell bg{m} b{m} m11 \n")
#         file.write(f"cell a{m} {m} fill u{m} -opor -zBounds1 \n") # put assembly within cone
#         file.write(f"cell b{m} {m} fill b{m}  opor -mohx 12 13 14 15 16 17 \n") # put moderator outside cone + outside cc
# -
        
# - now putting all coolant channels in assembly universes and transforming them to correct ocations
# c0l = [4.747854,0]
# c1l = [2.373927,4.1117622]
# c2l = [-2.373927,4.1117622]
# c3l = [-4.747854,0]
# c4l = [-2.373927,-4.1117622]
# c5l = [2.373927,-4.1117622]
# with open("all.ifc",'a') as file:
#     for m in range(37):
#         # x, y = xList[m], yList[m] # i thought i had to transform these as well but i dont
#         # c0x, c0y = c0l[0], c0l[1]
#         # c1x, c1y = c1l[1], c0l[1]
#         # c2x, c2y = c2l[2], c0l[1]
#         # c3x, c3y = c3l[3], c0l[1]
#         # c4x, c4y = c4l[0], c0l[1]
#         # c5x, c5y = c5l[0], c0l[1]
#         file.write(f"% --- coolant channels for assembly {m} \n")
#         ccString = f"""cell cc0{m} {m} fill cc -12 -zBounds2 
# trans F cc0{m} 4.747854 0 0.0
# cell cc1{m} {m} fill cc -13 -zBounds2 
# trans F cc1{m} 2.373927 4.1117622 0.0
# cell cc2{m} {m} fill cc -14 -zBounds2 
# trans F cc2{m} -2.373927 4.1117622 0.0
# cell cc3{m} {m} fill cc -15 -zBounds2 
# trans F cc3{m} -4.747854 5.8144442e-16 0.0
# cell cc4{m} {m} fill cc -16 -zBounds2 
# trans F cc4{m} -2.373927 -4.1117622 0.0
# cell cc5{m} {m} fill cc -17 -zBounds2 
# trans F cc5{m} 2.373927 -4.1117622 0.0 \n"""
#         file.write(ccString)
#         file.write("\n")

# ---

# - now writing transform cards
# xList, yList = getHexCoordinates()
# with open("all.ifc",'a') as file:
#     for m in range(37):
#         x, y = xList[m]*-1, yList[m]*-1
#         file.write(f"trans u{m} {x:0.6e} {y:0.6e} 0 \n")
# -

# import shutil
# for i in range(37):
#     if i == 18: continue
#     shutil.copy("./meshes/m18/T",f"./meshes/m{i}/T")