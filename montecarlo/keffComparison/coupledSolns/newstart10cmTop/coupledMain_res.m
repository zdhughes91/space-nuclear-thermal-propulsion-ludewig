
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Jan 25 2024 16:02:48' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  13]) = './coupledMain' ;
WORKING_DIRECTORY         (idx, [1:  97]) = '/home/grads/z/zhughes/space-nuclear-thermal-propulsion-ludewig/montecarlo/keffComparison/newstart' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-mj0jdqvh.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  36]) = '12th Gen Intel(R) Core(TM) i7-12700T' ;
CPU_MHZ                   (idx, 1)        = 50.0 ;
START_DATE                (idx, [1:  24]) = 'Tue May 14 07:03:20 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Tue May 14 07:29:55 2024' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1715688200081 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.00150E+00  9.98022E-01  9.98048E-01  1.00141E+00  1.00183E+00  1.00297E+00  9.99405E-01  9.96808E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  54]) = '/home/grads/z/zhughes/Serpent2/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  7.32529E-01 0.00015  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  2.67471E-01 0.00042  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.83687E-01 0.00014  0.00000E+00 0.0E+00 ];
IFC_COL_EFF               (idx, [1:   4]) = [  7.65514E-01 0.00013  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  9.60962E-01 3.4E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_SAMPLING_EFF          (idx, [1:   2]) = [  7.09663E-01 0.00022 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.29937E-01 0.00018  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.22788E+01 0.00060  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.88732E-01 4.7E-06  1.02810E-02 0.00041  9.86638E-04 0.00064  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_FAIL_STAT             (idx, [1:   6]) = [  6.20148E-08 0.70354  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.94031E+01 0.00038  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.91028E+01 0.00039  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.62815E+01 0.00034  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.35267E+02 0.00057  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 5000089 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00009E+04 0.00078 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00009E+04 0.00078 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.10154E+02 ;
RUNNING_TIME              (idx, 1)        =  2.65897E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.11483E-01  2.11483E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.01833E-02  4.01833E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.63380E+01  2.63380E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.65106E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.90358 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.97890E+00 0.00212 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.82397E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31765.09 ;
ALLOC_MEMSIZE             (idx, 1)        = 4981.21 ;
MEMSIZE                   (idx, 1)        = 4797.57 ;
XS_MEMSIZE                (idx, 1)        = 792.67 ;
MAT_MEMSIZE               (idx, 1)        = 6.96 ;
RES_MEMSIZE               (idx, 1)        = 1755.33 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 2242.61 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 183.64 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 377 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1650769 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 23 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 79 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 79 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 2422 ;
TOT_TRANSMU_REA           (idx, 1)        = 169 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 1 ;
TMS_MODE                  (idx, 1)        = 2 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.83264E+14 0.00053  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.59865E-03 0.00869 ];
U235_FISS                 (idx, [1:   4]) = [  2.15976E+19 6.5E-06  9.99887E-01 6.4E-06 ];
U238_FISS                 (idx, [1:   4]) = [  2.43063E+15 0.05683  1.12529E-04 0.05683 ];
U235_CAPT                 (idx, [1:   4]) = [  4.35707E+18 0.00168  3.15409E-01 0.00129 ];
U238_CAPT                 (idx, [1:   4]) = [  1.18911E+17 0.00869  8.60852E-03 0.00870 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 5000089 5.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 1.95306E+05 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 5000089 5.19531E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 1359862 1.40489E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 2138916 2.19683E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 1501311 1.59358E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 5000089 5.19531E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 1.76020E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  7.00000E+08 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  8.33229E+01 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  5.26500E+19 6.7E-06 ];
TOT_FISSRATE              (idx, [1:   2]) = [  2.16001E+19 1.5E-07 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  1.38140E+19 0.00106 ];
TOT_ABSRATE               (idx, [1:   2]) = [  3.54141E+19 0.00041 ];
TOT_SRCRATE               (idx, [1:   2]) = [  4.91632E+19 0.00053 ];
TOT_FLUX                  (idx, [1:   2]) = [  3.82225E+21 0.00056 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.56695E+19 0.00122 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  5.10836E+19 0.00055 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  2.31201E+21 0.00061 ];
INI_FMASS                 (idx, 1)        =  8.40106E+00 ;
TOT_FMASS                 (idx, 1)        =  8.40106E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05658E+00 0.00027 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.65153E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.10812E-01 0.00021 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09679E+00 0.00020 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.73376E-01 0.00029 ];
SIX_FF_LT                 (idx, [1:   2]) = [  8.80921E-01 0.00019 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.57196E+00 0.00040 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07095E+00 0.00053 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43749E+00 6.7E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02271E+02 1.5E-07 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07071E+00 0.00054  1.06291E+00 0.00053  8.03937E-03 0.00766 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07095E+00 0.00053 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.88379E+01 8.8E-05 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  1.31795E-07 0.00167 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.46103E-02 0.01072 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.56048E-03 0.00628  2.15098E-04 0.03051  9.98287E-04 0.01340  5.99522E-04 0.02015  1.30600E-03 0.01290  2.15307E-03 0.01047  5.94561E-04 0.01775  5.50142E-04 0.02187  1.43805E-04 0.03893 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.06422E-01 0.00900  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.0E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.0E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  7.34144E-05 0.00110  7.34119E-05 0.00108  7.37511E-05 0.01248 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  7.86034E-05 0.00094  7.86007E-05 0.00093  7.89587E-05 0.01241 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.51113E-03 0.00767  2.65584E-04 0.04587  1.13664E-03 0.01918  7.03531E-04 0.02683  1.50256E-03 0.01834  2.44260E-03 0.01260  6.77068E-04 0.02438  6.24235E-04 0.02869  1.58915E-04 0.05818 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.01661E-01 0.01238  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.0E-09  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 3.5E-09  1.63478E+00 0.0E+00  3.55460E+00 5.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  6.59390E-05 0.03359  6.59513E-05 0.03359  6.43205E-05 0.04903 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  7.06050E-05 0.03359  7.06183E-05 0.03359  6.88649E-05 0.04900 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.46098E-03 0.04472  2.81580E-04 0.12918  9.65927E-04 0.08100  6.10656E-04 0.10400  1.20535E-03 0.07493  2.15602E-03 0.05791  5.51573E-04 0.10222  5.53972E-04 0.08771  1.35907E-04 0.17657 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.08683E-01 0.03833  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 5.0E-09  1.63478E+00 0.0E+00  3.55460E+00 5.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  7.25154E-05 0.00538 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  7.76433E-05 0.00540 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.45749E-03 0.00995 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.02597E+02 0.00670 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  8.92518E-07 0.00046 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.26921E-06 0.00052  4.27014E-06 0.00052  4.14533E-06 0.00533 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  9.24400E-05 0.00065  9.24395E-05 0.00066  9.25717E-05 0.00752 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.13456E-01 0.00030  7.12830E-01 0.00030  8.11969E-01 0.00979 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30212E+01 0.01050 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.53198E+01 0.00040  4.99779E+01 0.00044 ];

