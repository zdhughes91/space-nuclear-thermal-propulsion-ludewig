"""

    Author: Zach Hughes - zhughes@tamu.edu
    Date: Apr.1.2024
"""

# ---
import numpy as np
import matplotlib.pyplot as plt
import subprocess
#from analyzeSerpent import *
from porositySolver import *
#from azimuthallyDiscretize import *

# ---



def getData(casename: str) -> list:
    """ a function to read powerdensity output file from neutronics AND the cell volume
        file. vol.csv is the volume file for the 6300 cell 2D wedge
        
        THIS CURRENTLY ONLY SAVES DATA FROM THE FUEL REGION
    """
    if casename == "fixedValue":
        footskip = 135296-123514
    else:
        footskip = 137583-123514

    fuelIndices = np.genfromtxt(f'./{casename}/constant/fluidRegion/polyMesh/cellZones',skip_header=18101,skip_footer=25307-25302)

    x = np.genfromtxt(f'./{casename}/0/fluidRegion/Cx',skip_header=23,skip_footer=27035-25226)
    y = np.genfromtxt(f'./{casename}/0/fluidRegion/Cy',skip_header=23,skip_footer=27035-25226)
    z = np.genfromtxt(f'./{casename}/0/fluidRegion/Cz',skip_header=23,skip_footer=26763-25226)
    v = np.genfromtxt(f'./{casename}/0/fluidRegion/V',skip_header=23,skip_footer=25287-25226)  # 
    pdensData = np.genfromtxt(f'./{casename}/0/fluidRegion/powerDensityNeutronics',skip_header=14,skip_footer=25275-25215)#*0.58 not all power is in fuel region
    #tflux = np.genfromtxt(f'./{casename}/1/neutroRegion/flux1',skip_header=23,skip_footer=footskip)*1e-4 # converting m^2 to cm^2
    #fflux = np.genfromtxt(f'./{casename}/1/neutroRegion/flux0',skip_header=23,skip_footer=footskip)*1e-4 # converting m^2 to cm^2
    #print(len(fuelIndices),len(x),len(y),len(z),len(v),len(pdensData))

    for fueli in fuelIndices: # correcting fuel region for power. 
        pdensData[int(fueli)] = pdensData[int(fueli)] * 0.58

    data = {}
    pdens,rx,ry,rz,rv,rpow = {},{},{},{},{},{}

    data['pdens'] = pdensData
    data['x'] = x
    data['y'] = y
    data['z'] = z
    data['v'] = v
    #data['therm'] = tflux
    #data['fast'] = fflux

    data['pow'] = data['pdens'] * data['v']
    data['totPower'] = np.sum(data["pow"])
    #newData = {'a':data} # keeping old format so i dont have to edit any other
    return data

def flattenData(data: list,
                xyz: list,
                index: int,
                var:str='pow'
                ) -> list:
    """ a function to flatten the data to one variable. i.e. given data 
        with an x,y, and z location, it flattens it to only z

        returns data with 100 bins in the dimension chosen

    data : a list of the data 
    xyz  : a list of the xyz data in a list of lists; xyz = [[x0,x1,...],[y0,y1,...],[z0,...]]
    index: the index of xyz you want to exist at the end. (0,1,or 2)
    """
    l = len(data) 
    xbins = np.linspace(0,0.6392,100)
    y = np.asarray(data)
    #
    bin_indices = np.digitize(xyz[index], xbins)
    sums = np.zeros(100)

    for i in range(1, 100 + 1):
        if var == 'pow':       # this line is important !!!! i find average flux but i do not find average power !!!!!!! please note
            sums[i - 1] = np.sum(y[(bin_indices == i)])
        else:
            sums[i - 1] = np.sum(y[(bin_indices == i)]) / len(y[(bin_indices == i)])
    
    return sums,xbins

def returnAxialData(d: dict,
                    assembly:str,
                    var:str='power') -> tuple:
    """ 
    """
    if var == "power":
        ivar = 'pow'
    elif var == "therm" or var == "fast":
        ivar = var
    
    xyz = [d[assembly]['x'],d[assembly]['y'],d[assembly]['z']]
    y,x = flattenData(d[assembly][ivar],xyz,2,ivar)
    y = removeOutlier(y)
    return y,x


def findInAssemblyPeakingFactor(d:dict,regionname:str) -> float:
    """
    
    pinfo: this should be the list of powers in the fuel region (durrently d[4])
    """
    xyz = [d[regionname]['x']*1e2,d[regionname]['y']*1e2,d[regionname]['z']*1e2]
    y,x = flattenData(d[regionname]['pow'],xyz,0)
    y = removeOutlier(y)
    avg,max = np.mean(y), np.max(y)
    #print(f"       avg={avg:0.3e},max={max:0.3e}")
    return max/avg

def findAllPeakingFactors(d: dict) -> dict:
    """ one command to find all in-assembly and in-core peaking factors
    
    """
    sumPower = 0
    aPowerList = []
    ppfDict = {}
    for a in d:
        aPowerList.append(d[a]['totPower'])
        ppfDict[a] = findInAssemblyPeakingFactor(d,a)
        print(f'Assembly {a} Results: Diffusion')
        print(f'   Power = {np.sum(d[a]["totPower"])*1e-6:0.3f} MW')
        print(f'   Power peaking factor = {ppfDict[a]:0.4f}')
        sumPower += np.sum(d[a]["pow"])*1e-6
        
    print('Full slice results: Diffusion')
    print(f'   Total six assembly power = {sumPower:0.3f} MW')
    print(f'   Max-to-average assembly power peaking factor = {np.max(aPowerList)/27.0707e6:0.4f}') #27.1e6 is 1e9/37
    return ppfDict

def percentPPFchange(deterministicPPF,sssPPF) -> None:
    """ the percent change in power peaking factor between determinisic and serpent2"""
    for a in sssPPF:
        pctChange = ((deterministicPPF[a]-sssPPF[a]) / (sssPPF[a])) * 100
        print(f"Assembly {a} pct change = {pctChange:0.1f}")

def removeOutlier(y):
    """ the data consistentlly has one outlier, this removes it
    """
    maxloc = np.where(y==np.max(y))[0]
    y[maxloc] = (y[maxloc-1]+y[maxloc+1])/2
    return y
    
def plotAxialPower(d:dict,regionname:str,plotAll: bool=False) -> None:
    """ builds a plot of the axial power distribution
    
    d: what the getData() function returns
    """
    
    if not plotAll:
        xyz = [d[regionname]['x'],d[regionname]['y'],d[regionname]['z']]
        sums, xbins = flattenData(d[regionname]['pow'],xyz,0)
        sums = removeOutlier(sums)
        plt.figure(6)
        plt.scatter(xbins,sums)
        #plt.ylim(2e5,3e5)
        plt.title(f'Assembly {regionname} Axial Power Distribution: GeN-Foam Diffusion')
        plt.ylabel('Power (W)')
        plt.xlabel('Height (m)')
        plt.grid(which='major', color='gray', linestyle='-')
        plt.minorticks_on()
        plt.grid(which='minor', color='lightgray', linestyle='--')
        #plt.legend()
        plt.savefig(f'{regionname}.png')
        plt.show()
    else:
        for a in d:
            #print(a)
            xyz = [d[a]['x'],d[a]['y'],d[a]['z']]
            sums, xbins = flattenData(d[a]['pow'],xyz,2)
            #print(len(sums),len(xbins))
            #print(sums)
            sums = removeOutlier(sums)
            plt.plot(xbins,sums,label=a)
        plt.title('Axial Power Distribution for All Assemblies')
        plt.ylabel('Power (W)')
        plt.xlabel('Height (m)')
        plt.grid(which='major', color='gray', linestyle='-')
        plt.minorticks_on()
        plt.grid(which='minor', color='lightgray', linestyle='--')
        plt.legend()
        plt.savefig('allaxial.png')
        plt.show()

     
def findMFR(power:float=27.0707e6,Cp: float=16e3,deltaT: float=3100):
    """ find mass flow rate 
    power  : W    , power in assembly
    Cp     : J/kgK, specific heat
    deltaT : K    , change in coolant temperature inlet->outlet
    """
    return power/(Cp*deltaT)

def idealGas(pressure,density):
    """ pressure in MPA, temperature in K"""
    pressure = pressure * 9.869 # MPa to atm conversion
    return (pressure * 2.01568)/ (0.0821 * density) # g/mol to kg/mol, then using L to m^3

def inletVelocity(power:float=(27.0707e6),temperature: float=200,Ipressure: float=9.1):
    """finds the desired inlet velocity based on serpent power

    Args:
        power       (float): W, power in assembly
        temperature (float): K, inlet temperature 
        Ipressure   (float): MPa, inlet pressure
    """
    iR, oR = 2.382e-2, 2.682e-2 # innerRadius, outerRadius - from Cristian's SNTP ppt
    inletArea = 3.14159 * (oR**2 - iR**2)
    rho = idealGas(Ipressure,temperature) # 8.1MPa is roughly the inlet pressure I am seeing, g/cc -> kg/m^3
    massFlowrate = findMFR(power)         # using 18e3 as known avg Cp with goal of 2800K outletT
    #print(massFlowRate,'aaa')
    inletVelocity = massFlowrate / (rho*inletArea) # 10.82519kg/m3 says genfoam
    return inletVelocity

def updateVelocity(assemblyNumber: int,power: float=27.0707e6) -> None:
    """ updates the inlet velocity of the assembly based on power
    
    """
    mfr = findMFR(power) 
    # exe = ['foamDictionary','./0/fluidRegion/U','-entry',
    #         f'boundaryField.outerPropellantInlet{assemblyNumber}.value','-set',
    #         f"uniform (-{Unew:0.4f} 0 0)"]
    exec2 = ['foamDictionary','./fixedValue/0/fluidRegion/U','-entry',
             f'boundaryField.outerPropellantInlet{assemblyNumber}.massFlowRate','-set',f"constant {mfr:0.6e}"]
    print(exec2)
    try:
        result2 = subprocess.run(exec2, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        print(f"Inlet MFR successfully updated to {mfr:0.2f} m/s")
    except Exception as e:
        print("!!!!!! The inlet MFR did not get updated !!!!!!")   
    
def updateAllVelocity(d: dict):
    """ 
    
    d (dict): the data dictionary returned by getData()
    """
    
    for a in d:
        updateVelocity(int(a[1:]),power=d[a]['totPower'])

def plotComparison(sssData,fvData,abData, # serpent data, fixedValue data, albedo data
                   assembly: str,         # the assembly of interest
                   param: str,            # power/fast flux/ thermal flux
                   show: bool=False
                   ) -> None:
    """
    """
    # - get diffusion w/ fixed value BC data
    y_fvD,x_fvD = returnAxialData(fvData,assembly,param)
    # - 
    y_abD,x_abD = returnAxialData(abData,assembly,param)
    # - get sss data
    x_sss,y_sss = sssReturnAxialData(sssData,assembly,param)
    
    # - plot
    if param == 'power':
        title = f'Assembly {assembly[1:]} Axial Power:'
        ytitle = 'Power (W)'
    else:
        title = f'Assembly {assembly[1:]} {param} flux:'
        ytitle = r'Flux ($\frac{n}{cm^{2} \cdot s}$)'

    plt.figure()
    plt.plot(x_fvD,y_fvD,label='Diff. w/ Fixed Value BC',c='b')
    plt.plot(x_abD,y_abD,label='Diff. w/ Albedo BC',c='m')
    plt.scatter(x_sss,y_sss,label='Serpent',marker='x',c='r')
    plt.title(title)
    plt.ylabel(ytitle)
    plt.grid(which='major', color='gray', linestyle='-')
    plt.minorticks_on()
    plt.grid(which='minor', color='lightgray', linestyle='--')
    plt.xlabel('Height (cm)')
    plt.legend()
    plt.savefig(f'./images/assembly{assembly[1:]}_{param}.png')
    if show: plt.show()

def axiallyDiscretize(d:dict,a:str):
    """ this is designed to find power under each axial zone (5 rn) 
        to be used in proportionalPowerMethod() from porositySolver.py
    
    d(dict): dictionary returned by getData()
    a(str): assembly name
    """
    y,x = returnAxialData(d,a,'power')
    l = len(y)
    
    pDict = {}
    #orfNum = 5
    for i in range(5):
        #orfNum -= 1
        #pDict[f'o{orfNum+1}'] = np.sum(y[int(i*l/5):int((i+1)*l/5)])
        
        pDict[f'o{i+1}'] = np.sum(y[int(i*l/5):int((i+1)*l/5)])

    return pDict




if __name__ == "__main__":
    # --- reading data
    # - diffusion, fixedValue BC
    #fv = getData('./thCases/a15')
    assemblyList = ['a15','a16','a17','a18','a22','a23']
    
    d = {}
    for a in assemblyList:
        d[a] = getData(f'./thCases/{a}')
        #print(axiallyDiscretize(d,a))
    #     print(a)
    #     y,x = returnAxialData(d,a,'power')
    #     plt.plot(x,y,label=a)
    # plt.show()
    #plotAxialPower(d,'a15',True)














    # ------------- now azimuthal work
    # sums, aziBins, xBins = azimuthallyDiscretize(fv['a'],'a18')

    # powerDict = {}
    # powerDict = assignOrificePower(sums,
    #                                Nazimuthal=6 # number of azimuthal orifices
    #                               )
    # orificeDict = {}

    # orificeDict['propPower'] = proportionalPowerMethod(powerDict)
    # # for o in orificeDict['propPower']:
    # #     orificeDict['propPower'][o] = [0.6,2.5e-4]
    # #
    # buildAndUpdateCases(orificeDict)


    





