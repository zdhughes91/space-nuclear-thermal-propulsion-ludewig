
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Jan 25 2024 11:36:28' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'coupledMain' ;
WORKING_DIRECTORY         (idx, [1:  70]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/coupledTH/inCoreAssembly' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Feb 15 15:18:31 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Feb 15 15:22:17 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 200 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1708031911498 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.10384E+00  9.90126E-01  9.83049E-01  1.12750E+00  9.03267E-01  9.95252E-01  9.31563E-01  9.65403E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.55975E-01 0.00050  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.44025E-01 0.00042  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.68667E-01 0.00024  0.00000E+00 0.0E+00 ];
IFC_COL_EFF               (idx, [1:   4]) = [  7.63596E-01 0.00128  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  9.98491E-01 1.4E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_SAMPLING_EFF          (idx, [1:   2]) = [  6.06473E-01 0.00303 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.51608E-01 0.00029  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.97352E+00 0.00086  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.37690E-01 2.9E-05  5.93685E-02 0.00045  2.94130E-03 0.00093  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_FAIL_STAT             (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.08894E+01 0.00064  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.05529E+01 0.00065  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.27642E+01 0.00055  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  2.19398E+01 0.00103  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 200 ;
SIMULATED_HISTORIES       (idx, 1)        = 2000225 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00011E+04 0.00130 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00011E+04 0.00130 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.73909E+01 ;
RUNNING_TIME              (idx, 1)        =  3.75897E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  3.70750E-01  3.70750E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.88500E-02  1.88500E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  3.36937E+00  3.36937E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  3.65117E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.62650 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.40480E+00 0.00640 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  4.23787E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.07 ;
ALLOC_MEMSIZE             (idx, 1)        = 4126.73 ;
MEMSIZE                   (idx, 1)        = 3932.31 ;
XS_MEMSIZE                (idx, 1)        = 2216.97 ;
MAT_MEMSIZE               (idx, 1)        = 1.79 ;
RES_MEMSIZE               (idx, 1)        = 1593.93 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 119.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 194.42 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 59 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 4035089 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 106 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 203 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 203 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 6551 ;
TOT_TRANSMU_REA           (idx, 1)        = 1457 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 1 ;
TMS_MODE                  (idx, 1)        = 2 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  1.99811E+07 ;
TOT_DECAY_HEAT            (idx, 1)        =  1.48839E-05 ;
TOT_SF_RATE               (idx, 1)        =  1.01399E-01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.99811E+07 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.48839E-05 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.72288E+02 ;
INGESTION_TOXICITY        (idx, 1)        =  9.44388E-01 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.72288E+02 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.44388E-01 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  2.22139E+07  4.47357E-07 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.97748E+07 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  4.54349E+07 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  7.28759E+15 0.00091  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  3.53574E-03 0.01820 ];
U235_FISS                 (idx, [1:   4]) = [  3.08534E+19 1.3E-05  9.99877E-01 1.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.78158E+15 0.10199  1.22552E-04 0.10199 ];
U235_CAPT                 (idx, [1:   4]) = [  6.16140E+18 0.00289  3.29293E-01 0.00219 ];
U238_CAPT                 (idx, [1:   4]) = [  1.30114E+17 0.01813  6.95269E-03 0.01797 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 2000225 2.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 6.44889E+04 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 2000225 2.06449E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 498752 5.13441E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 828534 8.46983E+05 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 672939 7.04065E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 2000225 2.06449E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.60770E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.00000E+09 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  4.40421E+03 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  7.52195E+19 1.1E-05 ];
TOT_FISSRATE              (idx, [1:   2]) = [  3.08572E+19 2.9E-07 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  1.87104E+19 0.00179 ];
TOT_ABSRATE               (idx, [1:   2]) = [  4.95677E+19 0.00068 ];
TOT_SRCRATE               (idx, [1:   2]) = [  7.28759E+19 0.00091 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.35997E+21 0.00094 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.56579E+19 0.00182 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  7.52256E+19 0.00091 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.04381E+21 0.00103 ];
INI_FMASS                 (idx, 1)        =  2.27056E-01 ;
TOT_FMASS                 (idx, 1)        =  2.27056E-01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05767E+00 0.00049 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.79259E-01 0.00048 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.03268E-01 0.00039 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.10002E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.18189E-01 0.00051 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.02222E-01 0.00029 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.59318E+00 0.00070 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03233E+00 0.00091 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43766E+00 1.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02271E+02 2.9E-07 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03252E+00 0.00093  1.02447E+00 0.00091  7.86178E-03 0.01305 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03233E+00 0.00091 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.88070E+01 0.00015 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  1.36015E-07 0.00285 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.63644E-02 0.01479 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.61727E-03 0.00888  2.16765E-04 0.04904  9.75701E-04 0.02309  6.37880E-04 0.02731  1.31758E-03 0.01948  2.17219E-03 0.01543  5.91725E-04 0.03119  5.48984E-04 0.02963  1.56449E-04 0.06085 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.09477E-01 0.01423  1.24667E-02 0.0E+00  2.82917E-02 6.1E-09  4.25244E-02 8.3E-09  1.33042E-01 5.4E-09  2.92467E-01 6.0E-09  6.66488E-01 0.0E+00  1.63478E+00 5.5E-09  3.55460E+00 3.4E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  7.30575E-05 0.00205  7.30641E-05 0.00204  7.25950E-05 0.01779 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  7.54200E-05 0.00181  7.54267E-05 0.00179  7.49503E-05 0.01775 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.59212E-03 0.01346  2.51521E-04 0.07276  1.13493E-03 0.03396  7.42295E-04 0.03985  1.49651E-03 0.02818  2.48296E-03 0.02145  6.78398E-04 0.04205  6.16009E-04 0.04343  1.89494E-04 0.08732 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.09277E-01 0.02075  1.24667E-02 0.0E+00  2.82917E-02 6.1E-09  4.25244E-02 8.4E-09  1.33042E-01 5.4E-09  2.92467E-01 6.0E-09  6.66488E-01 0.0E+00  1.63478E+00 5.5E-09  3.55460E+00 5.1E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  6.86859E-05 0.01691  6.87269E-05 0.01691  6.45029E-05 0.05649 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  7.09001E-05 0.01689  7.09425E-05 0.01689  6.65979E-05 0.05667 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  7.43998E-03 0.04452  2.40788E-04 0.23087  1.01959E-03 0.11283  8.45733E-04 0.12887  1.66609E-03 0.08790  2.57469E-03 0.07386  5.06738E-04 0.14362  4.29115E-04 0.15291  1.57235E-04 0.28562 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.53559E-01 0.07466  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 4.0E-09  2.92467E-01 4.9E-09  6.66488E-01 5.0E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  7.24336E-05 0.00289 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  7.47787E-05 0.00277 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.62604E-03 0.00928 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.05214E+02 0.00875 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  8.50166E-07 0.00086 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.38395E-06 0.00079  4.38483E-06 0.00080  4.26910E-06 0.00926 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  9.03748E-05 0.00116  9.03792E-05 0.00117  8.98367E-05 0.01229 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.56181E-01 0.00058  6.55580E-01 0.00060  7.58373E-01 0.01366 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32142E+01 0.01710 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.04440E+01 0.00065  4.76529E+01 0.00079 ];

