"""
    A script for converting a volume mix in Serpent into a single material. This
    is necessary because the materials in mixes are not burnable

    Author: Zach Hughes - zhughes@tamu.edu
    Date: Feb.14.2024
"""

volFrac = [0.42000013, 0.28777627, 0.2922236]
rho = [0.0004922,5.94719911,6.73]
massFrac = [[0.00029402,4.41097e-08],
            [5.63397e-09,2.28204e-08,0.0253407,0.000284439,1.69769e-07,0.00292697,0.000200742,0.0143138,0.00312148,0.00477125,0.00483524,0.00077898],
            [0.0388233,0.000435776,0.0201988,0.00440487,0.00673293,0.00682323,0.00109925]
            ]

uList = ["B-10.00c","B-11.00c","C-12.00c","C-13.00c","U-234.00c","U-235.00c","U-238.00c","Zr-90.00c","Zr-91.00c","Zr-92.00c","Zr-94.00c","Zr-96.00c"]
zList = ["C-12.00c","C-13.00c","Zr-90.00c","Zr-91.00c","Zr-92.00c","Zr-94.00c","Zr-96.00c"]
hList = ["H-1.00c","H-2.00c"]

# H-1.00c 0.00029402
# H-2.00c 4.41097e-08

# C-12.00c 0.0388233
# C-13.00c 0.000435776
# Zr-90.00c 0.0201988
# Zr-91.00c 0.00440487
# Zr-92.00c 0.00673293
# Zr-94.00c 0.00682323
# Zr-96.00c 0.00109925
# u = []
#     B-10.00c
#     B-11.00c
#     C-12.00c
#     C-13.00c
#     U-234.00c
#     U-235.00c
#     U-238.00c
#     Zr-90.00c
#     Zr-91.00c
#     Zr-92.00c
#     Zr-94.00c 
#     Zr-96.00c 


# fuelDens = sum([volFrac[i]*rho[i] for i in range(3)]) # volume 

# matFracDict = {}
# for iso in hList: matFracDict[iso] = 0
# for iso in uList: matFracDict[iso] = 0
# for iso in zList: matFracDict[iso] = 0

# for i in range(2):
#     matFracDict[hList[i]] += massFrac[0][i]*volFrac[0]
# for i in range(len(uList)):
#     matFracDict[uList[i]] += massFrac[1][i]*volFrac[1]
# for i in range(len(zList)):
#     matFracDict[zList[i]] += massFrac[2][i]*volFrac[2]

# print(f"mat fuelmatname -{fuelDens:0.6f} burn")
# for iso in matFracDict:
#     print(iso+f" {matFracDict[iso]:0.6e}")

with open('tryfile','w') as file:
    for i in range(1,25201):
        file.write(f"{i} {i} \n")
