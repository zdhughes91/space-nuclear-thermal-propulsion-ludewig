""" 
    This is a script to iterivatly call Serpent and GeN-Foam in order to arrive at 
    a converged neutronic/thermal-hydraulic solution. This script should be run
    in the same directory as the serpent input file. 
    
    Order of Operations:
        1. run serpent
           a. have serpent print the powerDensity output into the genfoam/0/fluidRegion/ folder
           b. rename serpent output files (like .m) in case it is necessary later
           c. serpent should read the temperature form genfoam/<steadyStateTime>/fluidRegion/T 
           d. powerDensity needs to be rewritten taking volumeFraction into account (see note below)
           e. this code should append the powerDensity file with boundary conditions
        2. calculate desired inlet velocity from serpent power
           a. calculate required mass flowrate to have outletT of ~3000K
           b. calculate inlet velocity based on this flowrate
           c. update genfoam input file with this inlet velocity
        3. run genfoam until steady state is reached
           a. recalculate coolant density field based on genfoam's temperature and pressure field

    Author: Zach Hughes - zhughes@tamu.edu
    Date: Feb.19.2024

NOTE!
    - I am now imposing an inlet temperature of 81.1 K. This value was initially calculated to 
      provide a correct mass flowrate based on an imposed velocity. In the future, I will 
      either have to come up with a legitimate reason to impose temperature or velocity. This
      will likely come down to a decision on if you want to model a hot bleed or expander cycle. 

    - GeN-Foam power density reads the power density in the structure of the power generating 
      cellZone. it ends up meaning that 
      the power density must be divided by the volumeFraction given for the fuel cellZone in 
      constant/fluidRegion/phaseProperties. Similarly, the functionObject that volume integrates
      the powerDensityNeutronics file must have a scaleFactor of volumeFraction in order to 
      compensate

assembly relationships:
solved assembly - identical assemblies
15-33,36,21,3,0    
16-29,31,20,7,5
17-24,25,19,12,11
18-
22-28,34,35,32,27,14,8,2,1,4,9
23-30,26,13,6,10
"""

# --- user entered information
path2serpentExe = '/home/grads/z/zhughes/Serpent2/src/sss2'
thDirectory = './thCases/'
assemblyList = ['a15','a16','a17','a18','a22','a23']
genfoamDirectory = './genfoamCase/'
serpentMasterFile = './coupledMain'
numCoresSerpent = 8
nominalDensity = 3.7 # this is the nominal density given in the solid3 card. 
volumeFraction = 0.58 # this is the volume fraction of the fuel material in the power generating cellZone **
# --- 

# --- necessary variables
exeSerpent = [path2serpentExe,serpentMasterFile,'-omp',f'{numCoresSerpent}']
cellZoneOrder = ['o1','o2','fuel','outerPropellant','o3','hotFrit','innerPropellant','o4','o5']
boundaryFieldText = """boundaryField
{
    innerPropellantOutlet
    {
        type            zeroGradient;
    }
    hotFritOutlet
    {
        type            zeroGradient;
    }
    fuelOutlet
    {
        type            zeroGradient;
    }
    coldFritOutlet
    {
        type            zeroGradient;
    }
    outerPropellantOutlet
    {
        type            zeroGradient;
    }
    outerPropellantOR
    {
        type            zeroGradient;
    }
    innerPropellantInlet
    {
        type            zeroGradient;
    }
    hotFritInlet
    {
        type            zeroGradient;
    }
    fuelInlet
    {
        type            zeroGradient;
    }
    coldFritInlet
    {
        type            zeroGradient;
    }
    outerPropellantInlet
    {
        type            zeroGradient;
    }
}"""
# ---

# --- importing libraries
import os
import subprocess
import time
import numpy as np
import shutil
import multiprocessing
import psutil
# ---

# --- defining funcitons
def rmFile(filepath):
    """ this deletes filepath"""
    try:
        os.remove(filepath)
        print(f"File '{filepath}' successfully deleted.")
    except OSError as e:
        print(f"Error: {e}")

def rmFolder(folderpath):
    """ this deletes folderpath """
    try:
        shutil.rmtree(folderpath)
        print(f"Folder '{folderpath}' deleted successfully.")
    except OSError as e:
        print(f"Error: {folderpath} - {e.strerror}")
        
def perfectGas(pressure,temperature):
    """ pressure in MPA, temperature in K
        returns density in g/cc
    """
    pressure = pressure * 9.869 # MPa to atm conversion
    return 1e-3*(pressure * 2.01568)/ (0.0821 * temperature) # g/mol to kg/mol, then using L to m^3

def rewritePowerDensity(caseName):
    """
        this function rewrites the power by incorporating the volumeFraction
        of the fuel in the power generating cellZone.
    
    caseName(str) - name of the assembly case 
    """
    global thDirectory,volumeFraction
    with open(thDirectory+caseName+"/0/fluidRegion/powerDensityNeutronics",'r') as file: # find begining/end of numbers
        lines = file.readlines()
        for i,line in enumerate(lines):
            if line.startswith('('):
                startnum = i
            elif line.startswith(')'):
                endnum = i

    for i in range(startnum+1,endnum): # looping thru all numbers and dividing by volumeFraction
        numbers = [format(float(number) / volumeFraction, '.6e') for number in lines[i].split()]
        lines[i] = ' '.join(map(str, numbers)) + '\n'

    with open(thDirectory+caseName+"/0/fluidRegion/powerDensityNeutronics", 'w') as file: # rewriting
        file.writelines(lines)

def buildDensityFile(caseName):
    """ 
        This is a function to build a density field from the GeN-Foam output. It
        uses the pressure and temperature files and finds the density of the coolant
        assuming it is a perfect gas. Both of the frits and the fuel keep a constant
        density. 
    
    caseName(str) - name of the assembly case 
    """
    global cellZoneOrder, thDirectory
    try:
        celltoregion = np.genfromtxt('cellToRegion',skip_header=21,skip_footer=25223-25221)
        p = np.genfromtxt(thDirectory+caseName+'/1/fluidRegion/p',skip_header=23,skip_footer=25319-25226)
        T = np.genfromtxt(thDirectory+caseName+'/1/fluidRegion/T',skip_header=23,skip_footer=25319-25268)
    except OSError as e:
        print(f"!!! Cannot find {e} !!! The density field will not be updated !")
        
    coolantRho = perfectGas(p*1e-6,T) # pressure to MPa
    fuelDens = (0.006171 *0.41999993) + (6.73*0.29222371) + (5.94719911*0.28777637)
    densityList = [1.85,1.85,fuelDens,0.0004922,1.85,3.365,0.01023,1.85,1.85] # from .out file

    with open(thDirectory+caseName+'/1/fluidRegion/density','w') as file:
        for i,region in enumerate(celltoregion):
            if i == 0: file.write(f"{len(celltoregion)} "+'\n')

            if cellZoneOrder[int(region)] == 'outerPropellant' or cellZoneOrder[int(region)] == 'innerPropellant':
                file.write(f"{coolantRho[i]/nominalDensity} "+'\n')
            else:
                file.write(f"{densityList[int(region)]/nominalDensity} "+'\n')
    return max(p), max(T)

def runSerpent():
    """ a single function to run serpent 
        - after this function is run the genfoam case will
          be ready to be run with the power results
    """
    global exeSerpent, thDirectory, boundaryFieldText, volumeFraction, assemblyList
    start = time.time()
    print('-'*50)
    print('Running Serpent...')
    # --- 1. running serpent
    logserpent = open('log.serpent','w')
    process = subprocess.Popen(exeSerpent,                           # run serpent 
                            stdout=logserpent,                       # log serpent output (1a.)
                            stderr=subprocess.PIPE, 
                            shell=False)
    process.wait()                                                   # wait for serpent to finish so it can write powerDens
    logserpent.close()

    # - 1d. rewrite power density taking volumeFraction into account
    for assembly in assemblyList:
        shutil.copy(f'./meshes/m{assembly[1:]}/powerDensityNeutronics',thDirectory+f'{assembly}/0/fluidRegion/powerDensityNeutronics')
        rewritePowerDensity(assembly) 
    # -
    # - 1e. append boundaryField
    # powerDensityFile = open(genfoamDirectory+'0/fluidRegion/powerDensityNeutronics','a')
    # powerDensityFile.write(boundaryFieldText)
    # -
    # - 1f. confirm power magnitude
    powerList = []
    for assembly in assemblyList:
        q = np.genfromtxt(thDirectory+assembly+'/'+'0/fluidRegion/powerDensityNeutronics',skip_header=14,skip_footer=25272-25212)
        v = np.genfromtxt(thDirectory+assembly+'/'+'constant/cellVolumes',skip_header=23,skip_footer=25287-25226)
        power = [q[i]*v[i] for i in range(len(q))]
        powerList.append(np.sum(power)) # store total power bc i need to update inlet velocity
        
        print(f"Assembly {assembly}:")
        print(f"    Total Power = {np.sum(power):0.3e} W")
    # !!!!!!!!!!!!!!
    #       NOTE! : you can use this power to recalculate mfr and thus recalculate inlet velocity
    # !!!!!!!!!!!!!!
    # print(f"Total Power = {np.sum(power):0.3e} W")
    print(f"Serpent ran in {(time.time()-start)/60:.3f} minutes")
    print('-'*50)
    # - 
    
    # --- 
    return powerList

def findMFR(power:float=27.0707e6,Cp: float=18e3,deltaT: float=2700):
    """ find mass flow rate 
    power  : W    , power in assembly
    Cp     : J/kgK, specific heat
    deltaT : K    , change in coolant temperature inlet->outlet
    """
    return power/(Cp*deltaT)

def inletVelocity(power:float=27.0707e6,temperature: float=81.1):
    """finds the desired inlet velocity based on serpent power

    Args:
        power       (float): W, power in assembly
        temperature (float): K, inlet temperature 
    """
    iR, oR = 2.382e-2, 2.682e-2 # innerRadius, outerRadius - from Cristian's SNTP ppt
    inletArea = 3.14159 * (oR**2 - iR**2)
    rho = perfectGas(8.1,temperature)*1e3 # 8.1MPa is roughly the inlet pressure I am seeing, g/cc -> kg/m^3
    massFlowrate = findMFR(power)         # using 18e3 as known avg Cp with goal of 2800K outletT
    inletVelocity = -1*(massFlowrate / (rho*inletArea)) # negative bc I want in -x direction
    return inletVelocity

def updateVelocity(caseName,power:float=27.0707e6) -> None:
    """ goes into the 0/fluidRegion/U file and changes inlet and internal
        velocities in order to satisfy the desired mass flow rate that the
        Serpent-calculated power requires
    
    Args:
        power       (float): W, serpent calculated power 
    """
    global thDirectory
    Unew = inletVelocity(power)
    exec1 = ['foamDictionary',thDirectory+caseName+'/0/fluidRegion/U','-entry','internalField','-set',f"uniform ({Unew:0.3f} 0 0)"]
    exec2 = ['foamDictionary',thDirectory+caseName+'/0/fluidRegion/U','-entry','boundaryField.outerPropellantInlet.value','-set',f"uniform ({Unew:0.3f} 0 0)"]
    try:
        result1 = subprocess.run(exec1, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        print(f"Internal velocity field successfully updated to {Unew:0.2f} m/s")
    except Exception as e:
        print("!!!!!! The Internal velocity field  did not get updated !!!!!!")
    try:
        result2 = subprocess.run(exec2, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        print(f"Inlet velocity successfully updated to {Unew:0.2f} m/s")
    except Exception as e:
        print("!!!!!! The inlet velocity did not get updated !!!!!!")                

def updateAllVelocity(powerList: list) -> None:
    """ Loops through all assemblies and updates their input velocities to match
        the pressure
    powerList (list): the output list from runSerpent()
    """
    global thDirectory, assemblyList
    for i,assembly in enumerate(assemblyList):
        #print('blah+'+assembly)
        updateVelocity(assembly,powerList[i])

def runGenfoam(caseName):
    """ runs genfoam case using a parallel processing module so I can solve
        all genfoam cases simultaneously
    """
    global thDirectory
    
    print(f'Running GeN-Foam Case {caseName}...')
    # --- 3. running genfoam
    loggenfoam = open(f'log.genfoam.{caseName}','w')
    os.chdir(thDirectory+caseName)
    process = subprocess.run('GeN-Foam',                           # run genfoam
                            stdout=loggenfoam,                       # log genfoam output
                            stderr=loggenfoam, 
                            shell=True)
    #process.wait()   
    loggenfoam.close()
    os.chdir('./../')
    # --- 


def parallelGenfoam():
    """ runs all genfoam cases simulaneously in order to
        solve TH more efficiently
    """
    global assemblyList
    start = time.time()
    
    processDict = {}
    for i,assembly in enumerate(assemblyList):
        processDict[assembly] = multiprocessing.Process(target=runGenfoam,args=(assembly,))
        processDict[assembly].start() # start all cases
        
    for a in assemblyList:
        processDict[a].join() # wait for all to finish
    print('-'*50)
    print(f"    T/H Solved in {(time.time()-start)/60:.3f} minutes")
    
    for a in assemblyList:
        maxP,maxT = buildDensityFile(a)
        print(f"Assembly {a}:")
        print(f"    Results: Max. Pressure = {maxP*1e-6:0.3f} MPa, Max Temperature = {maxT:0.3f} K")
    print('-'*50)
        
    
#parallelGenfoam()
# ---

#if __name__ == "__main__":
if True: 
    for iter in range(3):
        print('='*20+f"  Start of Iteration {iter}  "+'='*20)
        start = time.time()
        
        # 1. running serpent
        Q = runSerpent()
        # -
        
        # 2. updating inlet velocity
        updateAllVelocity(Q)
        # -
        
        # 3. running genfoam
        parallelGenfoam()
        # -
        print(f"Iteration {int(iter+1)} took {(time.time()-start)/60:.3f} minutes")