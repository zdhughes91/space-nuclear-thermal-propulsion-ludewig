/*--------------------------------*- C++ -*----------------------------------*\
|       ______          _   __           ______                               |
|      / ____/  ___    / | / /          / ____/  ____   ____ _   ____ ___     |
|     / / __   / _ \  /  |/ /  ______  / /_     / __ \ / __ `/  / __ `__ \    |
|    / /_/ /  /  __/ / /|  /  /_____/ / __/    / /_/ // /_/ /  / / / / / /    |
|    \____/   \___/ /_/ |_/          /_/       \____/ \__,_/  /_/ /_/ /_/     |
|    Copyright (C) 2015 - 2022 EPFL                                           |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      functions;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// This dict is the set of functions to evaluate interesting values with the 
// -postProcess utility. Problems with running these functions while the 
// simulation runs in parallel have been encountered. Using these functions on 
// a reconstructed case works as excpected.

functions
{
    type            sets;
    libs            ( "libsampling.so" );
    interpolationScheme cell;
    setFormat       raw;
    sets            ( fuelRegionAxial { type uniform ; axis z ; start ( 0.001011 0.02756 0 ) ; end ( 0.6416 0.01659 0.0002386 ) ; nPoints 100 ; } );
    fields          ( T Tsurface.lumpedNuclearStructure Tmatrix.lumpedNuclearStructure Tmax.lumpedNuclearStructure p powerDensityNeutronics magU );
    mFlowInlet
    {
        type            massFlow;
        libs            ( "libfieldFunctionObjects.so" );
        log             true;
        writeFields     true;
        writeControl    adjustableRunTime;
        writeInterval   1;
        region          fluidRegion;
        regionType      patch;
        regionName      outerPropellantInlet;
        alphaRhoPhiName alphaRhoPhi;
    }
    mFlowOutlet
    {
        type            massFlow;
        libs            ( "libfieldFunctionObjects.so" );
        log             true;
        writeFields     true;
        writeControl    adjustableRunTime;
        writeInterval   1;
        region          fluidRegion;
        regionType      patch;
        regionName      innerPropellantOutlet;
        alphaRhoPhiName alphaRhoPhi;
    }
    TInlet
    {
        type            TBulk;
        libs            ( "libfieldFunctionObjects.so" );
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   1;
        region          fluidRegion;
        regionType      patch;
        regionName      outerPropellantInlet;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    TOutlet
    {
        type            TBulk;
        libs            ( "libfieldFunctionObjects.so" );
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   1;
        region          fluidRegion;
        regionType      patch;
        regionName      innerPropellantOutlet;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    vInlet
    {
        type            surfaceFieldValue;
        libs            ( fieldFunctionObjects );
        fields          ( magU p T alphaRhoPhi alphaPhi );
        operation       areaAverage;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   1;
        region          fluidRegion;
        regionType      patch;
        name            outerPropellantInlet;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    vOutlet
    {
        type            surfaceFieldValue;
        libs            ( fieldFunctionObjects );
        fields          ( magU p T alphaRhoPhi alphaPhi );
        operation       areaAverage;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   1;
        region          fluidRegion;
        regionType      patch;
        name            innerPropellantOutlet;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    minMaxTemperature
    {
        type            fieldMinMax;
        libs            ( fieldFunctionObjects );
        mode            magnitude;
        region          fluidRegion;
        fields          ( T Tsurface.lumpedNuclearStructure T.lumpedNuclearStructure Tmax.lumpedNuclearStructure );
        enabled         true;
        log             true;
        writeControl    runTime;
        writeInterval   1;
    }
    totalPowerFluid
    {
        type            volFieldValue;
        libs            ( fieldFunctionObjects );
        fields          ( powerDensityNeutronics );
        operation       volIntegrate;
        region          fluidRegion;
        regionType      all;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   1;
        scaleFactor     0.58;
    }
    totalPowerNeutro
    {
        type            volFieldValue;
        libs            ( fieldFunctionObjects );
        fields          ( powerDensity );
        operation       volIntegrate;
        region          neutroRegion;
        regionType      all;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   1;
        scaleFactor     0.58;
    }
    densityFieldFunction
    {
        type            exprField;
        libs            ( fieldFunctionObjects );
        field           densityField;
        region          fluidRegion;
        readFields      ( alphaRhoPhi alphaPhi );
        action          new;
        expression      "alphaRhoPhi / alphaPhi";
        autowrite       true;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   1;
    }
}

