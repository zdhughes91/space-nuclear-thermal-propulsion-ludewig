"""
    
    Author: Zach Hughes - zhughes@tamu.edu
    Date: Feb.16.2024
"""
# --- user entered information
plot = False
# ---

# --- importing libraries
from hexalattice.hexalattice import *
import os
import subprocess
import shutil
import math
# ---

# --- constants
cwd = os.getcwd()
activeCoreRadius = 30.91 # cm
f2f = 9.131
# --- 

# --- functions
def getHexCoordinates():
    """ returns the x and y coordinates of each assembly"""
    hex_grid1, h_ax = create_hex_grid(nx=50,
                                    ny=50,
                                    rotate_deg=90,
                                    min_diam=f2f,
                                    crop_circ=activeCoreRadius,
                                    do_plot=True)
    tile_centers_x = hex_grid1[:, 0]
    tile_centers_y = hex_grid1[:, 1]
    return tile_centers_x, tile_centers_y

def translateMesh(i,x,y):
    """ translates the mesh using the x and y coordinates"""
    # executable
    
    transCommand = ['transformPoints','-translate',f"({x} {y} 0)"]
    process = subprocess.Popen(transCommand, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
    return_code = process.wait()
    stdout, stderr = process.communicate()
    print(f'Return Code: {return_code}')
    print(f'Standard Output:\n{stdout.decode()}')
    print(f'Standard Error:\n{stderr.decode()}')

def rotateMesh(i,x,y):
    """ rotates the mesh using x and y coordinates to find the angle """
    deg = math.degrees(math.atan2(-x,-y))
    if deg < 0:
        deg += 360
    deg -= 180
    deg = deg*-1
    print(i,deg)
    rotateCommand = ['transformPoints','-rotate-z',f"{deg}"]
    process = subprocess.Popen(rotateCommand, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
    return_code = process.wait()
    stdout, stderr = process.communicate()
    print(f'Return Code: {return_code}')
    print(f'Standard Output:\n{stdout.decode()}')
    print(f'Standard Error:\n{stderr.decode()}')

def updateMesh(i,x,y):
    """ 
        1. copies original mesh and pastes in case/constant/polymesh
        2. translates
        3. rotates
        4. copies into meshii/m{i}
        5. deletes case/constant/polyMesh
    """
    # 1.
    shutil.copytree("mOrig","case/constant/polyMesh")
    os.chdir("./case/")
    # 2. 
    rotateMesh(i,x,y)
    translateMesh(i,x,y)
    # 3. 
    #rotateMesh(i,x,y)
    os.chdir("./../")
    # 4. 
    shutil.copytree("case/constant/polyMesh",f"./m{int(i)}")
    # 5. 
    shutil.rmtree("case/constant/polyMesh")

def allMesh():
    """ loops through all assemblies to create all meshes"""
    xList, yList = getHexCoordinates()
    for i in range(37):
        updateMesh(i,xList[i]*1e-2,yList[i]*1e-2) # convert cm to meters
# i = 33
x,y = getHexCoordinates()
for i in range(len(x)):
    if i==18 or i==17 or i==16 or i==15 or i==22 or i==23:
        print(i,x[i],y[i])
# print('now j')
# for i in y:
#     print(i)
z = {'a':1,'b':2}
# for i in z:
#     print(i)
# print(x[i],y[i])
# print('60deg')
# for i in [0,3,15,21,33,36]:
#    rotateMesh(i,x[i]*1e-2,y[i]*1e-2)
# print('30deg')
# for i in [13,6,10,23,30,26]:
#     rotateMesh(i,x[i]*1e-2,y[i]*1e-2)
# updateMesh(i,x[i]*1e-2,y[i]*1e-2)
# shutil.copytree(f"./m{int(i)}","case/constant/fluidRegion/polyMesh")
# shutil.copytree(f"./mOrig","case/constant/polyMesh")

# ---

# def merge(i):
#     """"""
#     shutil.rmtree("case1/constant/polyMesh")
#     shutil.copytree(f"m{i}","case1/constant/polyMesh")
#     process = subprocess.Popen(['mergeMeshes','-overwrite', 'case', 'case1'],
#                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
#     return_code = process.wait()
#     stdout, stderr = process.communicate()
#     print(f'Return Code: {return_code}')
#     print(f'Standard Output:\n{stdout.decode()}')
#     print(f'Standard Error:\n{stderr.decode()}')
# for j in range(1,37):
#     merge(j)
# def mergeAll():
#     """ a function to use mergeMesh in openfoam to combine all assemblies"""

# --- plotting
if False: 
    x,y = getHexCoordinates()
    for i, (xi, yi) in enumerate(zip(x, y)):
        plt.text(xi, yi, str(i), ha='center', va='bottom')
    plt.show()
# ---

# --- to create 37 polyMesh
if False:
    allMesh()
# --- 
    





# def replace_word_in_file(file_path, old_word, new_word):
#     # Read the content of the file
#     with open(file_path, 'r') as file:
#         content = file.read()

#     # Replace all instances of the old word with the new word
#     modified_content = content.replace(old_word, new_word)

#     # Write the modified content back to the file
#     with open(file_path, 'w') as file:
#         file.write(modified_content)

# cellZoneNames = [ 'outerPropellant', 'o5', 'o3', 'o1', 'o2',
#                   'hotFrit', 'o4', 'innerPropellant', 'fuel' ]
# boundaryNames = ['innerPropellantOutlet','hotFritOutlet','fuelOutlet','coldFritOutlet',
#                  'outerPropellantOutlet','outerPropellantOR','innerPropellantInlet',
#                  'hotFritInlet','fuelInlet','coldFritInlet','outerPropellantInlet']

# for i in range(37): # loop thru all meshes
    #if i ==1: break
    #replace_word_in_file(f'./m{i}/cellZones','innerPropellant','innerPropellant'+f'{i}')
    # for cz in cellZoneNames:
    #     replace_word_in_file(f'./m{i}/cellZones',cz+f'{i}',cz)
    ## replace_word_in_file(f'./m{i}/boundary','outerPropellantOR','outerPropellantOR'+f'{i}')
    # for b in boundaryNames:
    #     replace_word_in_file(f'./m{i}/boundary',b+f'{i}',b)
    
    

# --- loop