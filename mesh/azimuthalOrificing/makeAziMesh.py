"""
    A script to automate the process of building azimuthal cellZones in the
    cold frit region. 

    Steps:
        1. Starting with A18 polyMesh, rotate 30deg about x axis
            - done 6 times for half mesh(a17,16,15,22) and 12 times for whole mesh(a23)
        2. rename orifice zones in rotated mesh to something different and identifiable
        3. for each rotation done
            a. merge rotated mesh with original mesh
            b. stitchMesh all of the internal boundaries
            c. repeat

        
    Author: Zach Hughes - zhughes@tamu.edu
    Date: Apr.17.2024

    
NOTE !!!!
    - if this runs with an error, it is likely because of 
        1. /0/meshPhi
        2. /constant/polyMesh/faceZones
        3. /constant/polyMesh/pointZones
        4. /constant/polyMesh/meshModifiers
      delete these files everytime you see them. they always cause problems
"""
# --- user inputs
N = 6
# ---

# --- libraries
import subprocess
import shutil
import os
# ---
# --- constants
boundaryNames = ['o1','o2','o3','o4','o5','innerPropellant','hotFrit','outerPropellant','fuel']
mergeExe = ['mergeMeshes','-overwrite','case','case1']
stitchExe = ['stitchMesh','-overwrite',[],[]]
if N==6:
    azimuthalNames = ['a','b','c',
                      'd','e','f']
elif N==12:
    azimuthalNames = ['a','b','c','d','e','f',
                      'g','h','i','j','k','l'
                     ]
# ---

def silentremove(directory):
    if os.path.exists(directory):
        try:
            os.remove(directory)
            print(f"File '{directory}' deleted successfully.")
        except OSError as e:
            print(f"Error: {e.strerror}")
    else:
        print(f"File '{directory}' does not exist.")

def rotateMeshes(N:int=6) -> None:
    """rotate all meshes into desired locations

    N: the number of total rotations
    """
    degChange = int(180/N)
    shutil.copytree("./polyMesh0.orig/","./polyMesh0/",dirs_exist_ok=True)

    os.chdir('./case/')
    for i in range(1,N):
        shutil.copytree("./../polyMesh0.orig/","./constant/polyMesh",dirs_exist_ok=True) # put original wedge in polyMesh
        rotateCommand = ['transformPoints','-rotate-x',f'{int(degChange*i)}'] 
        process = subprocess.run(rotateCommand, # rotate 
                                 check=True)
        shutil.copytree("./constant/polyMesh",f"./../polyMesh{i}",dirs_exist_ok=True)

    os.chdir('./../')

def controlHall(N:int=6) -> None:
    """ performs ctrl+H in filepath to find a replace all things

    To replace:
        1. orifice cellZone names so volumeFraction can be changed
        2. right-side boundary names so the internal faces can be stitches

    ** only works if N is 6 or 12 **
    """
    global boundaryNames,azimuthalNames

    def controlH(filepath:str,origStr:str,newStr:str) -> None: # find and replaces
        with open(filepath, 'r') as file:
            file_content = file.read()

        updated_content = file_content.replace(origStr, newStr)

        with open(filepath, 'w') as file:
            file.write(updated_content)

    for i in range(0,N): # for reach rotation
        silentremove(f'./polyMesh{int(i)}/faceZones') # these files are always problematic
        silentremove(f'./polyMesh{int(i)}/pointZones')

        czPath = f'./polyMesh{int(i)}/cellZones'
        bdPath = f'./polyMesh{int(i)}/boundary'

        for j in range(1,6): # for each orifice
            origOrificeName = f'o{int(j)}'
            newOrificeName = f'o{int(j)}{azimuthalNames[i]}'
            controlH(czPath,
                     origOrificeName,
                     newOrificeName
                     )

        for k in boundaryNames: # for each right-side boundary
            for rl in ['Right','Left']:
                origBoundaryName = k + rl
                newBoundaryName = k + f'{rl}{azimuthalNames[i]}'
                controlH(bdPath,
                        origBoundaryName,
                        newBoundaryName
                        )

def mergeAndStitch(N:int=6) -> None:
    """ merges each mesh and stitches internal faces
    
    """
    global mergeExe, stitchExe, azimuthalNames, boundaryNames

    shutil.copytree("./polyMesh0","./case/constant/polyMesh",dirs_exist_ok=True)

    for i in range(1,N): # for each rotation
        silentremove(f'./case/0/meshPhi')
        silentremove(f'./case/constant/polyMesh/faceZones')
        silentremove(f'./case/constant/polyMesh/pointZones')
        silentremove(f'./case/constant/polyMesh/meshModifiers')

        shutil.copytree(f"./polyMesh{i}/","./case1/constant/polyMesh",dirs_exist_ok=True)

        subprocess.run(mergeExe,                           # run mergeMeshes
                       check=True)
        
        os.chdir("./case/")
        for k in boundaryNames: # for each right-side boundary
            stitchExe[2] = k + f'Left{azimuthalNames[i-1]}'
            stitchExe[3] = k + f'Right{azimuthalNames[i]}'
            #silentremove(f'./case/0/meshPhi') # this file is problematic
            subprocess.run(stitchExe,                      # run stitchMesh
                           check=True)
        os.chdir("./../")
        


if __name__ == "__main__": 
    N = 4
    rotateMeshes(N)
    controlHall(N)
    mergeAndStitch(N)