/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2306                                  |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
FoamFile
{
    version         2;
    format          ascii;
    class           dictionary;
    object          controlDict;
}

application     GeN-Foam;

startFrom       startTime;

stopAt          endTime;

startTime       0;

endTime         0.1;

deltaT          2e-05;

writeControl    adjustableRunTime;

writeInterval   0.1;

purgeWrite      0;

writeFormat     ascii;

writePrecision  8;

writeCompression off;

timeFormat      general;

timePrecision   8;

runTimeModifiable true;

solveFluidMechanics true;

solveEnergy     true;

solveNeutronics false;

solveThermalMechanics false;

liquidFuel      false;

removeBaffles   false;

adjustTimeStep  true;

maxDeltaT       1;

maxCo           1;

functions
{
    type            sets;
    libs            ( "libsampling.so" );
    interpolationScheme cell;
    setFormat       raw;
    sets            ( fuelRegionAxial { type uniform ; axis z ; start ( 0.001011 0.02756 0 ) ; end ( 0.6416 0.01659 0.0002386 ) ; nPoints 100 ; } );
    fields          ( T Tsurface.lumpedNuclearStructure Tmatrix.lumpedNuclearStructure Tmax.lumpedNuclearStructure p powerDensityNeutronics magU );
    mFlowInlet
    {
        type            massFlow;
        libs            ( "libfieldFunctionObjects.so" );
        log             true;
        writeFields     true;
        writeControl    adjustableRunTime;
        writeInterval   0.1;
        region          fluidRegion;
        regionType      patch;
        regionName      outerPropellantInlet;
        alphaRhoPhiName alphaRhoPhi;
        scaleFactor     120;
    }
    mFlowOutlet
    {
        type            massFlow;
        libs            ( "libfieldFunctionObjects.so" );
        log             true;
        writeFields     true;
        writeControl    adjustableRunTime;
        writeInterval   0.1;
        region          fluidRegion;
        regionType      patch;
        regionName      innerPropellantOutlet;
        alphaRhoPhiName alphaRhoPhi;
        scaleFactor     120;
    }
    TInlet
    {
        type            TBulk;
        libs            ( "libfieldFunctionObjects.so" );
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   0.1;
        region          fluidRegion;
        regionType      patch;
        regionName      outerPropellantInlet;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    TOutlet
    {
        type            TBulk;
        libs            ( "libfieldFunctionObjects.so" );
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   0.1;
        region          fluidRegion;
        regionType      patch;
        regionName      innerPropellantOutlet;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    vInlet
    {
        type            surfaceFieldValue;
        libs            ( fieldFunctionObjects );
        fields          ( magU p T alphaRhoPhi alphaPhi );
        operation       areaAverage;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   0.1;
        region          fluidRegion;
        regionType      patch;
        name            outerPropellantInlet;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    vOutlet
    {
        type            surfaceFieldValue;
        libs            ( fieldFunctionObjects );
        fields          ( magU p T alphaRhoPhi alphaPhi );
        operation       areaAverage;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   0.1;
        region          fluidRegion;
        regionType      patch;
        name            innerPropellantOutlet;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    minMaxTemperature
    {
        type            fieldMinMax;
        libs            ( fieldFunctionObjects );
        mode            magnitude;
        region          fluidRegion;
        fields          ( T Tsurface.lumpedNuclearStructure T.lumpedNuclearStructure Tmax.lumpedNuclearStructure );
        enabled         true;
        log             true;
        writeControl    runTime;
        writeInterval   0.1;
    }
    totalPowerFluid
    {
        type            volFieldValue;
        libs            ( fieldFunctionObjects );
        fields          ( powerDensityNeutronics );
        operation       volIntegrate;
        region          fluidRegion;
        regionType      all;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   0.1;
    }
    totalPowerNeutro
    {
        type            volFieldValue;
        libs            ( fieldFunctionObjects );
        fields          ( powerDensity );
        operation       volIntegrate;
        region          neutroRegion;
        regionType      all;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   0.1;
    }
    densityFieldFunction
    {
        type            exprField;
        libs            ( fieldFunctionObjects );
        field           densityField;
        region          fluidRegion;
        readFields      ( alphaRhoPhi alphaPhi );
        action          new;
        expression      "alphaRhoPhi / alphaPhi";
        autowrite       true;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   0.1;
    }
}


// ************************************************************************* //
