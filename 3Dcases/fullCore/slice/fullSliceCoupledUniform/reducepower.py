

import numpy as np

pdensTMH = np.genfromtxt(f'./0/thermoMechanicalRegion/powerDensityNeutronics',skip_header=23,skip_footer=41837-41615)  

pdensData = np.genfromtxt(f'./0/fluidRegion/powerDensityNeutronics',skip_header=23,skip_footer=82080-81926)

with open('pdnFR','w') as file:
    for i in pdensData:
        file.write(f'{i*0.6/0.7:0.6e}\n')

with open('pdnTMR','w') as file:
    for i in pdensTMH:
        file.write(f'{i*0.6/0.7:0.6e}\n')