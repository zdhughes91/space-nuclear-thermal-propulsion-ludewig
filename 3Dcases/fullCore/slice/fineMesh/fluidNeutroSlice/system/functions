/*--------------------------------*- C++ -*----------------------------------*\
|       ______          _   __           ______                               |
|      / ____/  ___    / | / /          / ____/  ____   ____ _   ____ ___     |
|     / / __   / _ \  /  |/ /  ______  / /_     / __ \ / __ `/  / __ `__ \    |
|    / /_/ /  /  __/ / /|  /  /_____/ / __/    / /_/ // /_/ /  / / / / / /    |
|    \____/   \___/ /_/ |_/          /_/       \____/ \__,_/  /_/ /_/ /_/     |
|    Copyright (C) 2015 - 2022 EPFL                                           |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      functions;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// This dict is the set of functions to evaluate interesting values with the 
// -postProcess utility. Problems with running these functions while the 
// simulation runs in parallel have been encountered. Using these functions on 
// a reconstructed case works as excpected.

functions
{
    // --- Mass flow
    // assembly 15
    mFlowInlet15
    {
        type            massFlow;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     true;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      outerPropellantTop15;
        alphaRhoPhiName alphaRhoPhi;
    }
    mFlowOutlet15
    {
        type            massFlow;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     true;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      innerPropellantBot15;
        alphaRhoPhiName alphaRhoPhi;
    }
    // assembly 16
    mFlowInlet16
    {
        type            massFlow;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     true;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      outerPropellantTop16;
        alphaRhoPhiName alphaRhoPhi;
    }
    mFlowOutlet16
    {
        type            massFlow;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     true;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      innerPropellantBot16;
        alphaRhoPhiName alphaRhoPhi;
    }
    // assembly 17
    mFlowInlet17
    {
        type            massFlow;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     true;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      outerPropellantTop17;
        alphaRhoPhiName alphaRhoPhi;
    }
    mFlowOutlet17
    {
        type            massFlow;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     true;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      innerPropellantBot17;
        alphaRhoPhiName alphaRhoPhi;
    }
    // assembly 18
    mFlowInlet18
    {
        type            massFlow;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     true;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      outerPropellantTop18;
        alphaRhoPhiName alphaRhoPhi;
    }
    mFlowOutlet18
    {
        type            massFlow;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     true;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      innerPropellantBot18;
        alphaRhoPhiName alphaRhoPhi;
    }
    // assembly 22
    mFlowInlet22
    {
        type            massFlow;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     true;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      outerPropellantTop22;
        alphaRhoPhiName alphaRhoPhi;
    }
    mFlowOutlet22
    {
        type            massFlow;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     true;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      innerPropellantBot22;
        alphaRhoPhiName alphaRhoPhi;
    }
    // assembly 23
    mFlowInlet23
    {
        type            massFlow;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     true;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      outerPropellantTop23;
        alphaRhoPhiName alphaRhoPhi;
    }
    mFlowOutlet23
    {
        type            massFlow;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     true;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      innerPropellantBot23;
        alphaRhoPhiName alphaRhoPhi;
    }


    // --- Bulk temperature
    // assembly 15
    TOutlet15
    {
        type            TBulk;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      innerPropellantBot15;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    // assembly 16
    TOutlet16
    {
        type            TBulk;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      innerPropellantBot16;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    // assembly 17
    TOutlet17
    {
        type            TBulk;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      innerPropellantBot17;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    // assembly 18
    TOutlet18
    {
        type            TBulk;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      innerPropellantBot18;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    // assembly 22
    TOutlet22
    {
        type            TBulk;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      innerPropellantBot22;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    // assembly 23
    TOutlet23
    {
        type            TBulk;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      innerPropellantBot23;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }

    // --- Bulk outlet velocity
    // assembly 15
    vOutlet15
    {
        type            surfaceFieldValue; // fieldAverage;
        libs            (fieldFunctionObjects);
        fields          (magU p T alphaRhoPhi alphaPhi);
        operation       areaAverage;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        name            innerPropellantBot15;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    // assembly 16
    vOutlet16
    {
        type            surfaceFieldValue; // fieldAverage;
        libs            (fieldFunctionObjects);
        fields          (magU p T alphaRhoPhi alphaPhi);
        operation       areaAverage;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        name            innerPropellantBot16;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    // assembly 17
    vOutlet17
    {
        type            surfaceFieldValue; // fieldAverage;
        libs            (fieldFunctionObjects);
        fields          (magU p T alphaRhoPhi alphaPhi);
        operation       areaAverage;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        name            innerPropellantBot17;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    // assembly 18
    vOutlet18
    {
        type            surfaceFieldValue; // fieldAverage;
        libs            (fieldFunctionObjects);
        fields          (magU p T alphaRhoPhi alphaPhi);
        operation       areaAverage;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        name            innerPropellantBot18;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    // assembly 22
    vOutlet22
    {
        type            surfaceFieldValue; // fieldAverage;
        libs            (fieldFunctionObjects);
        fields          (magU p T alphaRhoPhi alphaPhi);
        operation       areaAverage;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        name            innerPropellantBot22;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    // assembly 23
    vOutlet23
    {
        type            surfaceFieldValue; // fieldAverage;
        libs            (fieldFunctionObjects);
        fields          (magU p T alphaRhoPhi alphaPhi);
        operation       areaAverage;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        name            innerPropellantBot23;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }



















    // --- Power integral over fluidRegion
    totalPowerFluid
    {
        type            volFieldValue;
        libs            (fieldFunctionObjects);
        fields          (powerDensityNeutronics);
        operation       volIntegrate;
        region          fluidRegion;
        regionType      all;  
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   $writeInterval;
        scaleFactor     0.58; // Fuel fraction
    }



    // --- Power integral over fluidRegion
    totalPowerNeutro
    {
        type            volFieldValue;
        libs            (fieldFunctionObjects);
        fields          (powerDensity);
        operation       volIntegrate;
        region          neutroRegion;
        regionType      all;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   $writeInterval;
        scaleFactor     0.58;
    }

    totalPower15
    {
        type            volFieldValue;
        libs            (fieldFunctionObjects);
        fields          (powerDensity);
        operation       volIntegrate;
        region          neutroRegion;
        regionType      cellSet;
        name            region1;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   $writeInterval;
        scaleFactor     ${{0.58*2}}; // 0.58 is fuel structure packing fraction. 2 is bc half assemlby
    }
    totalPower16
    {
        type            volFieldValue;
        libs            (fieldFunctionObjects);
        fields          (powerDensity);
        operation       volIntegrate;
        region          neutroRegion;
        regionType      cellSet;
        name            region2;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   $writeInterval;
        scaleFactor     ${{0.58*2}}; // 0.58 is fuel structure packing fraction. 2 is bc half assemlby
    }
    totalPower17
    {
        type            volFieldValue;
        libs            (fieldFunctionObjects);
        fields          (powerDensity);
        operation       volIntegrate;
        region          neutroRegion;
        regionType      cellSet;
        name            region3;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   $writeInterval;
        scaleFactor     ${{0.58*2}}; // 0.58 is fuel structure packing fraction. 2 is bc half assemlby
    }
    totalPower18 // 18 is the 1/12 assembly 
    {
        type            volFieldValue;
        libs            (fieldFunctionObjects);
        fields          (powerDensity);
        operation       volIntegrate;
        region          neutroRegion;
        regionType      cellSet;
        name            region4;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   $writeInterval;
        scaleFactor     ${{0.58*12}}; // 0.58 is fuel structure packing fraction. 12 is bc 1/12 assemlby
    }
    totalPower22 // 22 is the only full cylinder
    {
        type            volFieldValue;
        libs            (fieldFunctionObjects);
        fields          (powerDensity);
        operation       volIntegrate;
        region          neutroRegion;
        regionType      cellSet;
        name            region5;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   $writeInterval;
        scaleFactor     0.58;
    }
    totalPower23
    {
        type            volFieldValue;
        libs            (fieldFunctionObjects);
        fields          (powerDensity);
        operation       volIntegrate;
        region          neutroRegion;
        regionType      cellSet;
        name            region6;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   $writeInterval;
        scaleFactor     ${{0.58*2}}; // 0.58 is fuel structure packing fraction. 2 is bc half assemlby
    }
}
// ************************************************************************* //
