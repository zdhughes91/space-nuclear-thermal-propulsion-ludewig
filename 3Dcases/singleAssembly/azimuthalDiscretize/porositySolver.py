""" 
*** this was written horribly but it works. might be worth a rewrite ***

    This code solves for the orifice porosities in each method. It then builds the
    cases and prepares the cases by automatically changing the packingFraction and 
    Dh in /constant/fluidRegion/phaseProperties
    
    This is a code to solve for the porosity distribution in the
    cold frit of the USNC-designed SNTP reactor. I build on analysis
    done in packColumnAnalysis.py and pressureAnalysis.py in order to
    build this method. This also automates the creation and editing of 
    each case folder necessary to generate .

    *** IT IS POSSIBLE THE AXIAL POWER DISTRIBUTION CHANGES ONCE ORIFICES ARE ADDED ***

    Methods:
        1. cold frit constant pressure drop
        2. constant presure drop through all porous regions (cold frit,fuel bed, hot frit)
        3. porosity inversely proportional to power

    Citations:
    1. Heat and Flow Characteristics of Packed Beds
        - E. Achenbach 
    2. Transport Phenomena (Second Edition)
        - R. Byron Bird, Warren E. Stewart, Edwin N. Lightfoot
    3. April Novak's Thesis
    
    Author: Zach Hughes - zhughes@tamu.edu
    Date  : Feb.13.2024
"""
# --- libraries
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import root
from scipy import integrate
import os
import subprocess
import shutil
import string
# ---

# --- user entered inputs
numOrifices = 5 

# ---

# --- constants
coreHeight = 0.6392 # m
r4,r9 = 3.244e-2, 2.382e-2 # m
orAvg = (r4+r9)/2
areaAvg = 2*np.pi*orAvg*0.6392 #m^2
height = 0.2e-2 # m
mfr = 0.568 # kg/s, this is based on thrust, which is better than based on power becuase of less assumtpions
assemblyPower = 27.070707e6 # W
mu = 4e-05 #
"""
    In April Novak's thesis in the paragraph under eqn. 2.81 she says that
        pebbleDiameter = bedDiameter / 30 for annular beds. In my case 
        my annular bedDiameter is 0.2cm, so 0.00006666666m is my pebbleDiameter
"""
Dp = 0.00006666666 # m
# --- 

# --- defining functions needed to solve
def epsilonEquation(epsil):
    """ 
    This is equation 6.4-9 on page 190 in [2] (the laminar equation)

    epsil(float) - void fraction
    """
    global mu,vo,Dp,height
    return height*(150*mu*vo*(1-epsil)**2)/(Dp**2)/(epsil**3) - 76356.910 #-15056.3


def equivalantDiameterEqn(epsilon):
    """ equation 2.92 in April Novak's thesis"""
    global Dp
    return Dp*epsilon/(1-epsilon)

def voEquation(mfr,iArea):
    """
    On page 190 in [2], in the paragraph between equations 6.4-8 and 6.4-9.
        this velocity input changes as the mass flow rate changes and as 
        the coolant density changes. 
    """
    return mfr/(iArea)

def getMassFlowrate(power):
    """ given power this should find mfr of hydrogen to cool it
            Cp      = ~16,000 J/kgK
            Toutlet = ~3000 K
    """
    return power/(16e3*3e3)

def carloEqn(gamma):
    """ This is equation 6.4-9 on page 190 in [2] (the laminar equation)
        the only difference is i am now including the pressure drop from 
        the fuel bed and the hot frit. 

        the main difference is there is now a gamma term which is a scalar
        multiplier for the cold frit porosity.
    """
    global mu,vo,Dp,height
    cfPart = height*(150*mu*vo*(1-(0.4*gamma))**2)/(Dp**2)/((0.4*gamma)**3) # 0.4 is the void fraction
    fbPart = (0.766e-2)*(150*mu*vo*(1-(0.42))**2)/(Dp**2)/((0.42)**3)
    hfPart = (0.3e-2)*(150*mu*vo*(1-(0.7))**2)/(Dp**2)/((0.7)**3)
    # print(f"cfPart={cfPart}")
    # print(f"fbPart={fbPart}")
    # print(f"hfPart={hfPart}")
    pDrop = cfPart + fbPart + hfPart # @ uniform conditions, pDrop is 317765.2702 Pa
    return cfPart + fbPart + hfPart - 317765.2702
# vo = voEquation(0.568,areaAvg)
# print(epsilonEquation(0.4))
# print(carloEqn(1.0))

def flattenData(data: list,
                xyz: list,
                index: int,
                var:str='pow'
                ) -> list:
    """ a function to flatten the data to one variable. i.e. given data 
        with an x,y, and z location, it flattens it to only z

        returns data with 100 bins in the dimension chosen

    data : a list of the data 
    xyz  : a list of the xyz data in a list of lists; xyz = [[x0,x1,...],[y0,y1,...],[z0,...]]
    index: the index of xyz you want to exist at the end. (0,1,or 2)
    """
    l = len(data) 
    xbins = np.linspace(0,0.6392,100)
    y = np.asarray(data)
    #
    bin_indices = np.digitize(xyz[index], xbins)
    sums = np.zeros(100)

    for i in range(1, 100 + 1):
        sums[i - 1] = np.sum(y[(bin_indices == i)])

    return sums,xbins

def getPowerData() -> list:
    """ a function to read powerdensity output file from neutronics AND the cell volume
        file. vol.csv is the volume file for the 6300 cell 2D wedge
    """
    powerDensData = np.genfromtxt(f'./rootCase/0/fluidRegion/powerDensityNeutronics',skip_header=23,skip_footer=82109-81927)*0.58 # powerDens
    volumeData = np.genfromtxt('./rootCase/0/fluidRegion/V',skip_header=23,skip_footer=82152-81926)                          # V
    xData = np.genfromtxt('./rootCase/0/fluidRegion/Cx',skip_header=23,skip_footer=107994-81926) # x
    yData = np.genfromtxt('./rootCase/0/fluidRegion/Cy',skip_header=23,skip_footer=108853-81926) # y
    zData = np.genfromtxt('./rootCase/0/fluidRegion/Cz',skip_header=23,skip_footer=108853-81926) # z
    print(len(powerDensData),len(volumeData),len(xData),len(yData),len(zData))
    powerData = []
    # print(volumeData[84,:])

    powerData.append(xData)                                  # x loc,0
    powerData.append(yData)                                  # y loc,1
    powerData.append(zData)                                  # z loc (not needed),2
    powerData.append(powerDensData*120)                      # powerDensity,3
    powerData.append(powerDensData*120*volumeData) # power,4
    powerData.append(volumeData)

    print(f" Total power = {sum(powerData[4])*10**-6:0.3f} MW")

    return powerData

def updatePhaseProperties(orfNum:int,caseName:str,pF:float=0.6,Deq:float=2.5e-4) -> None:
    """ 
    a function to automate the process of updating the volumeFraction and equivalent diameter in
    constant/fluidRegion/phaseProperties for each of the orifices. 

    orfNum  : the number of the orifice
    caseName: the name of the case you want to enter
    pF      : the desired packing fraction (volumeFraction in phaseProperties),(initial value so it can be reset to default)
    Deq     : the equivalent diameter (Dh in phaseProperties), (initial value so it can be reset to default),
    """
    os.chdir(f'./{caseName}/')
    exec1 = ['foamDictionary','./constant/fluidRegion/phaseProperties',  # setting volume fraction executable
             '-entry',f'structureProperties.{orfNum}.volumeFraction','-set',f"{pF}"]
    exec2 = ['foamDictionary','./constant/fluidRegion/phaseProperties',  # setting Dh executable
             '-entry',f'structureProperties.{orfNum}.Dh','-set',f"{Deq}"]
    
    try: # try updating the volume fraction
        result1 = subprocess.run(exec1, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        print(f"Volume Fraction successfully updated to {pF} for {orfNum}")
    except Exception as e:
        print(f"!!!!!! The volume Fraction  did not get updated for {orfNum}!!!!!!")
        print(e)

    try: # try updating the equivalent diameter
        result2 = subprocess.run(exec2, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        print(f"Equivalent diameter successfully updated to {Deq} for {orfNum}")
    except Exception as e:
        print(f"!!!!!! The equivalent diameter did not get updated for {orfNum}!!!!!!")
        print(e)
    
    os.chdir("./../")

def buildAndUpdateCases(updateInfo:dict) -> None: # has not been tested
    """
    a function to build all new cases and update the volumeFractions and Dh

    updateInfos: this should be a dictionary, like the one below:
                x = {
                    'casename1':{
                                'o1':[packingFraction as a float,equiv diam as a float],
                                o2':[packingFraction as a float,equiv diam as a float],
                                o3':[packingFraction as a float,equiv diam as a float],
                                ...
                                }
                    ... and so on
                }
    """

    for i,casename in enumerate(updateInfo): # loop through each case
        shutil.copytree('./rootCase/',f'./{casename}/',dirs_exist_ok=True)

        for orfNum in updateInfo[casename]: # loop through each orifice
            updatePhaseProperties(orfNum,
                                  casename,
                                  updateInfo[casename][orfNum][0],
                                  updateInfo[casename][orfNum][1])


def proportionalPowerMethod(powerDict: dict) -> dict:
    """ given a dictionary of orifice names as keys with power as values,
        this will return a dictionary with orifice names as keys with [volumeFraction,Dh]
        as the key
    """
    powerFrac = [powerDict['o1a'] /powerDict[ori] for ori in powerDict]  # get power in terms of fraction
    # print(powerFrac)
    totPowerFrac = sum(powerFrac)
    meanEpsilon = 0.4
    x = meanEpsilon * len(powerFrac) / totPowerFrac
    # for ori in oDict:
        #totPowerFrac += oDict[ori]['power'] / oDict['o1']['power']
        # print(oDict[ori]['epsil'] / oDict['o1']['epsil'])
        # print(oDict[ori]['mfr'] / oDict['o1']['mfr'])
        # print(oDict[ori]['power'] / oDict['o1']['power'])
    epsilons = [powerFrac[i]*x for i in range(len(powerFrac))] # discounting exponentially to combat axial pressure drop
    # x1 = 0.6  / np.mean(epsilons)
    # epsilons2 = np.array(epsilons)*x1
    Dhs = [np.abs(equivalantDiameterEqn(eps)) for eps in epsilons]

    updateCaseDict = {}
    for i,orf in enumerate(powerDict):
        updateCaseDict[orf] = [epsilons[i],
                                Dhs[i]
                                ]
    
    return updateCaseDict












if __name__ == "__main__":
        
    updateCaseDict = {                # initializing. to be used at end of code
                    'cfConstant':{},  # method 1 listed at top
                    'allConstant':{}, # method 2 listed at top
                    'propPower':{}    # method 3 listed at top
                    } 

        
    #updatePhaseProperties(1,'rootCase',0.61,2.51e-4)

    # ---
    # ---

    # --- getting axial power distribution
    powerData = getPowerData()
    sums, xbins = flattenData(powerData[4],
                            xyz=[powerData[0],powerData[1],powerData[2]],
                            index=0
                            )

    # plt.plot(xbins,sums)
    # plt.show()
    # --- finding orifice power and mfr
    oDict = {}
    #   
    for i in range(numOrifices):
        oDict[f"o{i+1}"] = {}
        oDict[f"o{i+1}"]['power'] = np.sum(sums[int(i*20):int((i+1)*20)])
        oDict[f"o{i+1}"]['mfr'] = getMassFlowrate(oDict[f"o{i+1}"]['power'])
        # t += oDict[f"o{i}"]['mfr']

    # ---
        
    # --- solving for porosities
    carloDict = {} # dictionary containing solutions from Carlo's method
    initial_guess = 0.5
    for i in range(1,numOrifices+1):
        carloDict[f"o{i}"] = {}
        vo = voEquation(
                        oDict[f"o{i}"]['mfr'], # required mfr in the assembly
                        areaAvg,               # input area 
                        )
        carloDict[f"o{i}"]['gamma'] = root(carloEqn,1.0).x[0]

        carloDict[f"o{i}"]['epsilCF'] = 0.4*carloDict[f"o{i}"]['gamma']
        carloDict[f"o{i}"]['DhCF'] = equivalantDiameterEqn(carloDict[f"o{i}"]['epsilCF'])

        carloDict[f"o{i}"]['epsilHF'] = 0.7*carloDict[f"o{i}"]['gamma']
        carloDict[f"o{i}"]['DhHF'] = equivalantDiameterEqn(carloDict[f"o{i}"]['epsilHF'])

        oDict[f"o{i}"]['epsil'] = root(epsilonEquation, initial_guess).x[0]
        oDict[f"o{i}"]['Dh'] = equivalantDiameterEqn(oDict[f"o{i}"]['epsil'])
        #print(f"Orifice {i} needs a porosity of {oDict[f'o{i}']['epsil']:0.6f}")

        # print(f"Orifice {i} needs a packing Fraction of {1-oDict[f'o{i}']['epsil']:0.6f}")
        # print(f"Orifice {i} needs a equivalent dimeter of {oDict[f'o{i}']['Dh']:0.6e} \n")
        # print(f"Orifice {i} needs a packing Fraction of {1-carloDict[f'o{i}']['epsilCF']:0.6f}")
        # print(f"Orifice {i} needs a equivalent dimeter of {carloDict[f'o{i}']['DhCF']:0.6e} \n")

        
        updateCaseDict['cfConstant'][f"o{i}"] = [ 1-oDict[f'o{i}']['epsil'] , 
                                                oDict[f'o{i}']['Dh'] 
                                                ] #[packing frac,equiv diam]
        updateCaseDict['allConstant'][f"o{i}"] = [1-carloDict[f'o{i}']['epsilCF'], 
                                                carloDict[f'o{i}']['DhCF']
                                                ] #[packing frac,equiv diam]


    # ---
    meanEpsilon = 0.6
    oVals = [oDict[orf]['epsil'] for orf in oDict]
    m = np.mean(oVals)

    avgedOvals = [(oDict[orf]['epsil']*meanEpsilon)/m for orf in oDict]
    avgedDhs = [equivalantDiameterEqn(eps) for eps in avgedOvals]
    pfavged = [1-oval for oval in avgedOvals]
    # print(pfavged)
    # print(avgedDhs)
    # ============================================================== POWER RATIOS WORK BELOW ! 
    # --- solving for power and porosity ratios
    # - i want to see if these are similar, and if I can normalize the values to make the mean 0.6
    # powerFrac = [oDict[ori]['power'] / oDict['o1']['power'] for ori in oDict]
    powerFrac = [oDict['o1']['power'] /oDict[ori]['power'] for ori in oDict]
    # print(powerFrac)
    totPowerFrac = sum(powerFrac)
    meanEpsilon = 0.4
    x = meanEpsilon * numOrifices / totPowerFrac
    # for ori in oDict:
        #totPowerFrac += oDict[ori]['power'] / oDict['o1']['power']
        # print(oDict[ori]['epsil'] / oDict['o1']['epsil'])
        # print(oDict[ori]['mfr'] / oDict['o1']['mfr'])
        # print(oDict[ori]['power'] / oDict['o1']['power'])
    epsilons = [powerFrac[i]*x for i in range(len(powerFrac))] # discounting exponentially to combat axial pressure drop
    Dhs = [equivalantDiameterEqn(eps) for eps in epsilons]

    for i,ep in enumerate(epsilons):
        updateCaseDict['propPower'][f'o{int(i+1)}'] = [ep,
                                                    Dhs[i]
                                                    ]
    # print(epsilons)
    # print(Dhs)
    # print(np.mean(epsilons))
    # ---
    # ============================================================== POWER RATIOS WORK ABOVE ! 

    print(updateCaseDict)

    if False:
        buildAndUpdateCases(updateCaseDict)
