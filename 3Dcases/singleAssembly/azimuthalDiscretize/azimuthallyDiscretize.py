"""
    This is a script made to azimuthally discretize the fuel assembly results. The goal
    is to find axial and azimuthal porosities so orificing can be done more delicately. This
    script was made to be imported into analyzeSlice.py

    Author: Zach Hughes - zhughes@tamu.edu
    Date: Apr.17.2024
"""
import numpy as np
import matplotlib.pyplot as plt
import string

def cart2pol(x, y) -> tuple:
    """ changing cartesian to polar coordinates
        - goes from 180 to -180 starting from positive x axis
    """
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)* 180 / np.pi 
    return(rho, phi)

def azimuthallyDiscretize(d:dict,
                          assembly:str
                          ) -> list:
    """ creates N discrete bins, with each bin covering 360/N degrees

    ** for now this only works for semi circle assemblies **

    data          : a list of the assembly specific data 
    assembly      : assembly name so i can access (y,z) origin loc

    15 0.0 27.393
    16 0.0 18.262
    17 0.0 9.131
    18 0.0 0.0
    22 7.907677961955699 22.827499999999986
    23 7.907677961955699 13.696500000000015
    """
    aLocDict = {'a15':[0.0,27.393],'a16':[0.0,18.262],'a17':[0.0,9.131],
                'a18':[0.0,0.0],'a22':[7.907678,22.8275],'a23':[7.907678,13.6965]}
    
    xyz = [d['x']*1e2,d['y']*1e2,d['z']*1e2]

    # rho,phi = cart2pol(np.asarray(xyz[1])-aLocDict[assembly][1], 
    #                    np.asarray(xyz[2])-aLocDict[assembly][0]
    #                    ) # convert to polar coordinate system. 
    rho,phi = cart2pol(np.asarray(xyz[1]), # this is currently at origin (although it is assembly 15)
                       np.asarray(xyz[2])
                       ) # convert to polar coordinate system. 
    
    xBins = np.linspace(0,63.92,5+1)
    aziBins = np.linspace(0.0,180.0,6+1)
    
    y = np.asarray(d['pow'])

    binIndicesX = np.digitize(xyz[0], xBins)
    binIndicesAzi = np.digitize(phi,aziBins)
    
    sums = [np.zeros(len(xBins)-1) for _ in range(len(aziBins)-1)] # first index is radial segment, second contains all axial data
    
    tot = 0
    for i in range(1,len(aziBins)):
        for j in range(1,len(xBins)):
            sums[i-1][j - 1] = np.sum(y[(binIndicesX == j) & (binIndicesAzi == i)])
            tot += sums[i-1][j - 1]
    
    # plt.imshow(sums,cmap='viridis',interpolation='nearest')
    # plt.colorbar()  # Add a color bar to show the scale
    # plt.show()
    #print(sums)
    return sums, aziBins, xBins

def assignOrificePower(powerMatrix:list,Nazimuthal:int=6,Naxial:int=5) -> dict:
    """ assigns orifice power values in a way that the funcitons in porositySolver.py can be leveraged
    
    powerMatrix    : this is the first item returned by the azimuthallyDiscretize function.
    Nazimuthal     : the number of azimuthal orifice zones
    Naxial         : the number of axial orifice zones
    """
    azimutalLabels = string.ascii_lowercase[:Nazimuthal]
    
    orificeDict = {}
    for i in range(Naxial):
        mult = (1.1)**(i)
        for j,oLabel in enumerate(azimutalLabels):
            #print(i,j)
            orificeDict[f'o{i+1}{oLabel}'] = powerMatrix[j][i] * mult
    
    return orificeDict 
