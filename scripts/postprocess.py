"""
    Author: Zach Hughes - zhughes@tamu.edu
"""

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from PIL import Image
import numpy as np

# Load the image
image_path = 'images/fig4p2p3.png'
image = Image.open(image_path)

# Create a figure and axis
# fig, ax = plt.subplots()

# # Display the image
# ax.imshow(image)

# # Plot a rectangle over the image
# rect = patches.Rectangle((0, 0), 900, 800, linewidth=2, edgecolor='r', facecolor='none')
# ax.add_patch(rect)

# # Show the plot
# plt.show()

powerDens = np.genfromtxt('rootCase/0/fluidRegion/powerDensityNeutronics',skip_header=173,
                          skip_footer=702-323)
print(powerDens)
# fig, ax = plt.subplots()


