

import matplotlib.pyplot as plt
import numpy as np

def massRatio(deltaV,Ichemical,Inuclear):
    return np.exp(-deltaV*(Ichemical-Inuclear)/(.00981*Inuclear*Ichemical))

deltaV = np.linspace(1,50,100)
mr = massRatio(deltaV,450,900)

# plt.figure(1)
# plt.plot(deltaV,mr)
# plt.grid(which='major', color='gray', linestyle='-')
# plt.minorticks_on()
# plt.grid(which='minor', color='lightgray', linestyle='--')
# plt.xlabel('deltaV (km/s)')
# plt.ylabel('Mass Ratio (nuclear/chemical)')
# plt.show()


leoVelocity = 28000/3600 #km/s
marsToEarthDistance = 471.6e6 # km, average distance from earth
travelTime = 6*28*24*3600 # s, 3 month travel time, assume 28 days per month

travelVelocity = marsToEarthDistance / travelTime
print(f'Travel Velocity to reach Mars in 4 months: {travelVelocity:0.3f} km/s')

massRatioForTravelTime = massRatio(travelVelocity-leoVelocity,450,900)
print(f'Mass ratio for desired travel time = {massRatioForTravelTime} ')