"""
    A script to speed up the process of building a fluid slice mesh
        - you have to set up the polyMeshA18 folder yourself

    Steps:
        1. if starting from .unv files. use ideasUnvToFoam to create polyMesh for
            the 1/6 assembly, 1/2 assembly, and whole assembly. place these in same
            directory as makeMesh.py and name them polyMeshA18,polyMeshHalf, and 
            polyMeshWhole respectively. They should all be located at the origin
        2. run 'moveMeshes()'
        3. in all of the boundary folders, the outerPropellantInlet_rotated,
            innerPropellantOutlet_rotated, and outerPropellantOR boundaries need
            to be renamed by putting the assembly number before the '_rotated' string.
            example for polyMeshA17 outerPropellantInlet_rotated: outerPropellantInlet17_rotated
        4. run 'mergeMeshes()'
        5. all 'outerPropellantInlet' and 'innerPropellantOutlet' must be type patch. the rest
            are type wall
        6. I usually change '_top' to 'Left' and add 'Right' to the bare-named walls. I then 
            remove '_rotated' from all boundaries & cellZones
        
    Author: Zach Hughes - zhughes@tamu.edu
    Date: Mar.28.2024
"""


import subprocess
import shutil
import os

assemblyList = ["15","16","17","18","22","23"]
mergeExe = ['mergeMeshes','-overwrite','case','case1']

def delete_directory_if_exists(directory):
    if os.path.exists(directory):
        try:
            shutil.rmtree(directory)
            print(f"Directory '{directory}' deleted successfully.")
        except OSError as e:
            print(f"Error: {e.strerror}")
    else:
        print(f"Directory '{directory}' does not exist.")

def mergeMeshes() -> None:
    """ after creating all the polyMeshes, now merge them into one mesh"""
    delete_directory_if_exists("./case/constant/polyMesh")
    delete_directory_if_exists("./case1/constant/polyMesh")
    for a in assemblyList:
        if a == "15": 
            shutil.copytree(f"polyMeshA{a}","./case/constant/polyMesh")
            continue

        shutil.copytree(f"polyMeshA{a}","./case1/constant/polyMesh")
        process = subprocess.run(mergeExe,                           # run mergeMeshes
                                check=True)
        shutil.rmtree("./case1/constant/polyMesh")
    shutil.copytree("./case/constant/polyMesh","./polyMeshFluidRegion")

def moveMeshes() -> None:
    """ moves all of the meshes into their correct positions """
    # executable
    global assemblyList
    y,z = 0,0.09131
    transCommand = ['transformPoints','-translate',f"(0 {y} {z})"]
    rotatCommand = ['transformPoints','-rotate-x','210'] # only rotating a23

    delete_directory_if_exists("./case/constant/polyMesh")

    shutil.copytree("polyMeshHalf","./case/constant/polyMesh") # polyMeshHalf should be @ (0,0,0)
    os.chdir("./case/")
    for i in range(3): # building 17,16,15
        process = subprocess.run(transCommand,
                                 check=True)
        shutil.copytree("./constant/polyMesh",f"./../polyMeshA{int(17-i)}")
    
    y,z = 13.6965e-2,7.907677e-2 # for a23
    transCommand = ['transformPoints','-translate',f"(0 {y} {z})"]
    shutil.rmtree("./constant/polyMesh")
    shutil.copytree("./../polyMeshHalf","./constant/polyMesh")
    process = subprocess.run(rotatCommand, # rotate 210 deg
                             check=True)
    process = subprocess.run(transCommand, # translate into position
                             check=True)
    shutil.copytree("./constant/polyMesh",f"./../polyMeshA23")

    shutil.rmtree("./constant/polyMesh") #
    shutil.copytree("./../polyMeshWhole","./constant/polyMesh")
    y,z = 13.6965e-2,7.907677e-2 # for a22
    transCommand = ['transformPoints','-translate',f"(0 {y} {z})"]
    process = subprocess.run(transCommand, # translate into position
                             check=True)
    shutil.copytree("./constant/polyMesh",f"./../polyMeshA22")
    shutil.rmtree("./constant/polyMesh") #

    os.chdir("./../")


if __name__ == "__main__":
    #moveMeshes()
    mergeMeshes()

