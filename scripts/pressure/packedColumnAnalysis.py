"""
    This work is based on:

    Transport Phenomena (Second Edition)
    By: R. Byron Bird, Warren E. Stewart, Edwin N. Lightfoot

    ---
    Author: Zach Hughes - zhughes@tamu.edu
    Feb.13.2024
"""
from scipy.optimize import root
import numpy as np

mu = 4e-05 #
mfr = 0.56 # kg/m^3
v = 67     # m/s
Re = 700   #  
epsilon = 0.4
rho = 5    # kg/m^3
height = 0.2e-2 # m
# i need to estimate outer diameter area of the cold frit
r4,r9 = 3.244e-2, 2.382e-2 # m
orAvg = (r4+r9)/2
areaAvg = 2*np.pi*orAvg*0.6392
print(f"Cold Frit inlet area: {areaAvg:0.5f} m^2")
Go = mfr/areaAvg # kg/m^2/s

"""
    In April Novak's thesis in the paragraph under eqn. 2.81 she says that
        pebbleDiameter = bedDiameter / 30 for annular beds. In my case 
        my annular bedDiameter is 0.2cm, so 0.00006666666 is my pebbleDiameter
"""
Dp = 0.00006666666 # m

def getVal(Dp,Go,mu,epsilon):
    """ at the bottom of [1] page 192 they measure this value"""
    return Dp*Go/(mu*(1-epsilon))

print(f"Measured Value = {getVal(Dp,Go,mu,epsilon):0.3f}")

"""
    The measured value found puts us a bit above the laminar region (<10). We could also
    be found in the transition region (0.1 < v < 10^5). I will try both to see impact on 
    final answer
"""

def eqn6p4dash9(mu,v,Dp,epsilon,L):
    """ [1] page 190 eqn 6.4-9 (laminar)"""
    return L*(150*mu*v*(1-epsilon)**2)/(Dp**2)/(epsilon**3)
vo = Go/rho

pdrop = eqn6p4dash9(mu,vo,Dp,epsilon,height)
print(f"Laminar-predicted pressure drop       = {pdrop:0.1f} Pa")

def eqn6p4dash12(mu,v,Dp,epsilon,rho,L):
    """ [1] page 191 eqn 6.4-12 (transition)"""
    term1 = (150*mu*v*(1-epsilon)**2)/(Dp**2)/(epsilon**3)
    term2 = (7*rho*(1-epsilon)*v**2) / (4*Dp*epsilon**3)
    return L*(term1 + term2)

pdrop2 = eqn6p4dash12(mu,vo,Dp,epsilon,rho,height)
print(f"Transition-predicted pressure drop    = {pdrop2:0.2f} Pa")

print(f"Pressure from paraFoam @halfway point = {7.6505e6-7.6375e6:0.1f} Pa")  # these are outlet and inlet pressures respectively


"""
    Based on the pressure drop results it seems the laminar flow
    equation is the best suited. I will work with this in the future
"""

""" ========== STRATEGY ==========
    I want to find the mean pressure loss and keep it the same as the
    results I have here for the packingFraction = 0.6 case. 

    1. find power + power ratios
        a. based on a given number of orifices, find the total
           power in each region. This will have to be normalized
           to a certain value
    2. determine 
"""
def solveEpsilon(epsil):
    """ [1] page 190 eqn 6.4-9 (laminar)"""
    global mu,vo,Dp,height
    return height*(150*mu*vo*(1-epsil)**2)/(Dp**2)/(epsil**3)-15056.3

initial_guess = 0.9

# Use the root function to find the root
result = root(solveEpsilon, initial_guess)

# Extract the root from the result
root_solution = result.x[0]

print("Root:", root_solution)
