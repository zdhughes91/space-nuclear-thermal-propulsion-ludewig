"""
    Author: Zach Hughes - zhughes@tamu.edu
    Jan 15 2024

    This is a script to analyze expected pressure drop within the Ludewig SNTP fuel assembly. 

    1. Heat and Flow Characteristics of Packed Beds
        - E. Achenbach 
"""
# packages
import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
from scipy import integrate
import math


# -- user entered information
epsilon = 0.4 # porosity of cold frit
numOrifices = 5

# --





# powerData = np.genfromtxt('pdensData.csv',delimiter=',',skip_header=1) # x, y, z, powerDens
# print(powerData)

# -- constants of SNTP design 1
coreHeight = 0.6392
reactorThrust = 1.96e5 # Newtons, 40k lbs of thrust = 178k Newtons
#exitBulkVelocity = 376 # m/s, exit velocity
numAssemblies = 37
specificImpulse = 950  # s
g = 9.81               # m/s^2
massFlowRate = reactorThrust / (specificImpulse * g * numAssemblies)
print(f" Mass flowrate = {massFlowRate:0.3f} kg/s")
# -- necesary functions
def getPressureDropCoefficient(Re: float,epsilon: float=0.4):
    """
    [1] - eqn. 21
    """
    term1 = 320 * (1-epsilon) / Re
    term2 = 6 / (Re * (1-epsilon))**0.1

    return term1 + term2

def assignPorousDh(epsilon: float=0.4,pebbleDiameter: float=1.0e-3):
    """ assigns Dh using eqn 2.92 in April Novak's thesis
        epsilon = 0.4 usuallly"""
    return epsilon*pebbleDiameter/(1-epsilon)

def getMassFlowrate(power):
    """ given power this should find mfr of hydrogen to cool it
            Cp      = ~15,000 J/kgK
            Toutlet = ~3150 K
    """
    return power/(15e3*3.15e3)

print(f'blahhhhhh {getMassFlowrate(1e9/37)}')

def coldFritORfunction(x: float) -> float:
    """ a function to find y position of cold frit or at any point axially """
    global coreHeight
    m = (0.03244-0.02382)/coreHeight
    return m*x+.02382

def coldFritIRfunction(x: float) -> float:
    """ a function to find y position of cold frit ir at any point axially """
    global coreHeight
    m = (.03044-.02182)/coreHeight
    return m*x+.02182

def Dh(epsilon,pebbleDiameter) -> float:
    """ assigns Dh using eqn 2.92 in April Novak's thesis
        epsilon = 0.4 usuallly [1]"""
    return epsilon*pebbleDiameter/(1-epsilon)

def darcyEqn(f,L,Dh,U,rho):
    """ Darcy weisbach eqn """
    return f*(L/Dh)*(U**2)*rho/2

def epsilonFromDh(Dh,pebbleDiameter) -> float:
    """ rearranged Dh equation """
    return (Dh/(pebbleDiameter*(1-Dh/pebbleDiameter)))

def getPorosity(mfr: float,area: float,f: float,rho: float,L: float,delP: float, pebbleDiameter) -> float:
    """   """
    return epsilonFromDh((f*rho*L*(mfr/area)**2)/(2*delP),pebbleDiameter)
# 
powerDensData = np.genfromtxt('pdens.csv',delimiter=',',skip_header=1) # x, y, z, powerDens
volumeData = np.genfromtxt('vol.csv',delimiter=',',skip_header=1) # x, y, z, powerDens
print(np.shape(volumeData))
powerData = [[],[],[],[],[]]
# print(volumeData[84,:])
totPower = 0
totVol = 0
for i in range(len(powerDensData)):
    powerDensity = powerDensData[i,3]
    totVol += volumeData[i,3]
    if powerDensity < 1: continue
    if powerDensData[i,1] < 0: continue
    if powerDensData[i,2] < 0: continue
    if powerDensData[i,0] == 0: continue
    #if powerData[1].count(powerDensData[i,1]) > 0: continue # i dont want repeat y values
    # print(i)
    # print(powerData[0])
    powerData[0].append(powerDensData[i,0])                     # x loc
    powerData[1].append(powerDensData[i,1])                     # y loc
    powerData[2].append(powerDensData[i,2])                     # z loc (not needed)
    powerData[3].append(powerDensData[i,3]*120)                 # powerDensity
    powerData[4].append(powerDensData[i,3]*120*volumeData[i,3]) # power

print(totVol)
print(f" Total power = {sum(powerData[4])*10**-6:0.3f} MW")


"""
    Step 2: Bin power axially. 
        now I want to bin the power axially so I can get a line of best fit to the
        numerical data. This will allow me to integrate this function based on the
        number of orifices I want, so I can know the power directly underneath the
        orifices
"""
numBins = 301
xbins = np.linspace(0,coreHeight,numBins+1)
y = np.asarray(powerData[4])
# Use numpy's digitize function to assign each x value to a bin
bin_indices = np.digitize(powerData[0], xbins)

# Initialize an array to store the sum of y values for each bin
sums = np.zeros(numBins)

# Sum y values for each bin
for i in range(1, numBins + 1):
    sums[i - 1] = np.sum(y[(bin_indices == i)])

degree = 8
c = np.polyfit(xbins[:-1], sums, degree)
print(c)
# Generate the line using the fitted c
fit_line = np.poly1d(c)

plt.figure(6)
plt.bar(xbins[:-1],sums,width=np.diff(xbins), align='edge',label='Data')
plt.plot(xbins[:-1], fit_line(xbins[:-1]), color='red', label=f'Fit (degree={degree})')
plt.title('Axial Power Distribution')
plt.ylabel('Power (W)')
plt.xlabel('Height (m)')
plt.legend()
plt.show()

print('ffff',sum(sums))

"""
    when calculating the OR and IR of the cold frit, I actually do not
    take the angle into account, so the line I make is not truely perpenticular.
    With that said, the angle is so small and is over such a short distance that
    the change is negligable, so I will ignore it for now. 
"""
# theta = math.atan((.03044-.02182)/.6392) # in radians !


"""
    Step 3: Integrate power and determine mass flowrate
        If
"""
orificeBins = np.linspace(0,coreHeight,numOrifices+1)

def powerPoly(x):
    """ a polynomail function for the axial power distribution """
    global c
    y = c[0] + c[1]*x + c[2]*x**2 + c[3]*x**3 + c[4]*x**4 + c[5]*x**5 + c[6]*x**6 + c[7]*x**7 + c[8]*x**8
    return y

orifice = {}
# orificePower = np.zeros(numOrifices)
# orificeMFR   = np.zeros(numOrifices)
t=0
for i in range(1,numOrifices+1): # integratingt
    orifice[f"{i}"] = {}
    if i != numOrifices:
        toploc = np.where(xbins > orificeBins[i])[0][0]
    else:
        toploc = -1
    botloc = np.where(xbins > orificeBins[i-1])[0][0]
    orifice[f"{i}"]['power'] = sum(sums[botloc:toploc])
    orifice[f"{i}"]['mfr']   = getMassFlowrate(orifice[f"{i}"]['power'])
    #print(orifice[f"{i}"]['power'],orifice[f"{i}"]['mfr'])
    t+=orifice[f"{i}"]['mfr'] # total mass flowrate, to check
    #orifice[f"{i}"]['']
print(f'mfr2 = {t}')
"""
    In April Novak's thesis in the paragraph under eqn. 2.81 she says that
        pebbleDiameter = bedDiameter / 30 for annular beds. In my case 
        my annular bedDiameter is 0.2cm, so 0.00006666666 is my pebbleDiameter
"""
delP = darcyEqn(f  = 0.55, # the correlation im using converges to this vlaue
                L  = 2e-3, # thickness of cold frit
                Dh = Dh(epsilon=0.4,pebbleDiameter=0.0000666666), # in comment above
                U  = 45, # from paraview at time 1.5s around midpoint. not sure if this U is inletU or withinPipe
                rho= .8  # im not too sure about the density of hydrogencoldFritAre a
                )
print(f" Cold Frit Uniform  Pressure Drop = {delP:2f}")

# (mfr,area,f,rho,L,delP)
coldFritArea = 0.11 #integrate.quad(coldFritORfunction,0,coreHeight)[0]
print('0'*10,coldFritArea)
epsilonOrifice = getPorosity(mfr  = orifice['1']['mfr'],
                             area = coldFritArea,
                             f    = 0.55,
                             rho  = 0.8,
                             L    = 2e-3,
                             delP = delP,
                             pebbleDiameter = 0.000066666
                             )

print(epsilonOrifice)


""" 
i did some math and i am now tryiing it out. feb.1.2024
"""
def term(f,L,V,mfr,deltaP,A,dp):
    """ this value should be between 0.4 and 0.8 """
    t = f*L*V*mfr/(2.0*deltaP*A*dp)
    return t
outlet, inlet = 0.03233, 0.02382
area = 2*3.14159*(np.sqrt(0.6392**2 + (outlet-inlet)**2))
dP = (7.095e6-7.081e6)
print(f'pdrop={dP} Pa') # i forget where I took this from but it agrees with packedColumnAnalysis.py
t = term(f=0.55,
         L=0.002, # thickness of cold frit
         V=40, # found by looking at paraview
         mfr=0.57, # roughly mfr in assembly
         deltaP=dP,
         A=area,
         dp=0.0000666666     #dp is the pebble diameter
         )
print(t)