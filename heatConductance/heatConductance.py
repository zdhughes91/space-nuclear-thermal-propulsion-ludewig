"""
    Author: Zach Hughes - zhughes@tamu.edu

    This script solves for temperature distributions in the
    particle fuel using both numerical and analytical methods.
    The analytical solution was found by integrating heat eqn in spherical
    coordinates as well as following the gFHR derivations pdf that is found
    in the GeN-Foam tutorials.

    kernel analytical temperature eqn:
        T(r) = T_{fuelSurf} + (q'''/(6*k_{fuel}))*(R_{fuel}^{2} - r^{2})

    shell analytical temperature eqn:
        T(r) = T_{BC} + (q'''/(4*\pi*k_{shell}))*((1/r) - (1/R_{shell}))

     
"""
import numpy as np
import os

if True: #ill fix this later
    path = ''
    #print('kk')
else:
    path = os.getcwd()+'/heatConductance/'
    #print(path)
# path = ''
kernel = {'T':np.genfromtxt(path+'numerical/1/kernelRegion/T',skip_header=23,skip_footer=111-74),
          'x':np.linspace(0,237.5e-6,51)
         }
shell = {'T':np.genfromtxt(path+'numerical/1/shellRegion/T',skip_header=23,skip_footer=116-74),
          'x':np.linspace(237.5e-6,297.5e-6,51)
         }

    
def v(r):
    return (4/3)*3.14159*(r**3)

kernelVolume, shellVolume = 0,0
# for rk,rs in zip(kernel['x'],shell['x']):
#     kernelVolume += v(rk)
#     shellVolume += v(rs)

kvolList, svolList = [],[]
kspace = kernel['x'][1] 
sspace = shell['x'][1] -shell['x'][0] 
for rk,rs in zip(kernel['x'],shell['x']):
    if rk == 0:
        svoli = v(rs) - v(rs-sspace) 
        svolList.append(svoli)
        continue
    kvoli = v(rk) - v(rk-kspace)
    kvolList.append(kvoli)

    svoli = v(rs) - v(rs-sspace) 
    svolList.append(svoli)



kernelVolume = v(237.5e-6)
#print(kernelVolume)
shellVolume = v(297.5e-6) - kernelVolume
# print('-- Volume Fractions -- ')
# print(f"kernelVolFrac = {kernelVolume/v(297.5e-6):0.3f}")
# print(f"shell VolFrac = {shellVolume/v(297.5e-6):0.3f}")

Tav_k_numerical, Tav_s_numerical = 0,0 
for i in range(1,50):
    rk = kernel['x'][i]
    rs = shell['x'][i]

    Tk = kernel['T'][i]
    Ts = shell['T'][i]

    Tav_k_numerical += (kvolList[i] * Tk) / kernelVolume
    Tav_s_numerical += (svolList[i] * Ts) / shellVolume

Tmax_k_numerical = np.max(kernel['T'])
Tmax_s_numerical = np.max(shell['T'])


# analytical math
k1=30         # W/mK
k2=45         # W/mK
R1=237.5e-6   # m
R2=297.5e-6   # m
qTrip=7.67e11 # W
T3=300        # K
def shellSoln(Tbc,qTrip,Rout,r,k) -> float:
    return Tbc+((qTrip/(4*3.14159*k))*((1/r) - (1/Rout)))

def generalSoln(Tout,qTrip,Rout,Rin,k) -> float:
    """ solivng the equaiton:
    (1) :  Tout-Tin = (qTrip/k)*(Rout^2 - Rin^2)/6
    """
    return Tout+((qTrip/k)*(Rout**2 - Rin**2)/6)

Txx = generalSoln(325+273,5.695e8,4.75e-3,4.177e-3,17.31) # to compare to soln found online
# print(f"Txx = {Txx-273:0.3f}") # prints 1K off from online result. 

T2 = shellSoln(T3,qTrip*kernelVolume,R2,R1,k2)

T1 = generalSoln(T2,qTrip,R1,0.0,k1)

# finding average temperature by int(T(r)*V(r)) / int(V(r))
def shellTEqn(r):
    return 300+((qTrip*kernelVolume/(4*3.14159*45))*((1/r) - (1/297.5e-6)))
def kernelTEqn(r):
    return 364.632819+((7.67e11/30)*(237.5e-6**2 - r**2)/6)
def shellIntegrand(r):
    return shellTEqn(r) * v(r)
def kernelIntegrand(r):
    return kernelTEqn(r) * v(r)

from scipy import integrate

kernelTVint,e = integrate.quad(kernelIntegrand,0,R1)
kernelVint, e = integrate.quad(v,0,R1)
shellTVint,e = integrate.quad(shellIntegrand,R1,R2)
shellVint, e = integrate.quad(v,R1,R2)
Tav_k_analytical = kernelTVint/kernelVint
Tav_s_analytical = shellTVint/shellVint
Tmax_k_analytical = T1

# now solving heat conductances:
def getHeatConductance(specificPower,t1,t2):
    return specificPower/(t2-t1)

H = {
    'numerical':{'surfToShellAv':getHeatConductance(qTrip,300.0,Tav_s_numerical),
                 'shellAvToFuelAv':getHeatConductance(qTrip,Tav_s_numerical,Tav_k_numerical),
                 'fuelAvToFuelMas':getHeatConductance(qTrip,Tav_k_numerical,Tmax_k_numerical)
                 },
    'analytical':{'surfToShellAv':getHeatConductance(qTrip,300.0,Tav_s_analytical),
                  'shellAvToFuelAv':getHeatConductance(qTrip,Tav_s_analytical,Tav_k_analytical),
                  'fuelAvToFuelMas':getHeatConductance(qTrip,Tav_k_analytical,Tmax_k_analytical)
                 }
    }



# printing results of interest
print('='*20+' Particle Temperatures '+'='*20)
print('       ------------------------------------------- ')
print('       Region | Temp. Val | Numerical | Analytical ')
print('       ------------------------------------------- ')
print(f"       kernel | Tavg(K)   | {Tav_k_numerical:0.3f}   |  {Tav_k_analytical:0.3f}")
print(f"       kernel | Tmax (K)  | {np.max(kernel['T']):0.3f}   |  {T1:0.3f}")
print(f"       shell  | Tavg(K)   | {Tav_s_numerical:0.3f}   |  {Tav_s_analytical:0.3f}")
print(f"       shell  | Tmax (K)  | {np.max(shell['T']):0.3f}   |  {T2:0.3f}")
print('       ------------------------------------------- ')
print('       Solution   |     nodes        |  H (W/(m^3K) ')
print('       ------------------------------------------- ')
for soln in H:
    #print('       Solution     |  nodes  |  H (W/(m^3K) --------------------------')
    for cond in H[soln]:
        l = len(cond)
        if soln == 'numerical':
            print(f"       {soln}  |  {cond}"+' '*(16-l)+f"|  {H[soln][cond]:0.3e}")
        else:
            print(f"       {soln} |  {cond}"+' '*(16-l)+f"|  {H[soln][cond]:0.3e}")
print('='*63)

def thermalResistance(r1,r2,k):
    return (r2-r1)/(4*3.14159*k*r1*r2)

# deltaT = qTrip * shellVolume * thermalResistance(R1,R2,45)
# print(deltaT,'kkkssssd')
# finally plotting
if True: # to plot 
    shellGnrlSoln = generalSoln(300,qTrip,R2,shell['x'],45)

    Tkernel = generalSoln(T2,qTrip,R1,kernel['x'],30)
    Tshell = shellSoln(T3,qTrip*kernelVolume,R2,shell['x'],45)
    import matplotlib.pyplot as plt
    plt.figure(1,dpi=200)
    plt.plot(kernel['x']*1e6,Tkernel,label='Analytical',c='b')
    plt.plot(shell['x']*1e6,Tshell,c='b')
    plt.plot(kernel['x'][:-1]*1e6,kernel['T'],linestyle='dashed',c='r')
    plt.plot(shell['x'][:-1]*1e6,shell['T'],linestyle='dashed',label='Numerical',c='r')
    #plt.plot(shell['x']*1e6,Tshell,linestyle='dashed',c='m',label='new') # general soln plotted
    plt.title('Analytical vs Numerical Particle T Distribution')
    plt.ylabel('Temperature (K)')
    plt.xlabel('Radius ($\mu m$)')
    plt.legend(loc='best')
    plt.savefig('../images/particleTemperatures.png')
    plt.show()



