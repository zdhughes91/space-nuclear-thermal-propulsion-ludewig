"""

    Author: Zach Hughes - zhughes@tamu.edu
    Date: Apr.1.2024
"""

# ---
import numpy as np
import matplotlib.pyplot as plt

# ---



def getData(casename: str) -> list:
    """ a function to read powerdensity output file from neutronics AND the cell volume
        file. vol.csv is the volume file for the 6300 cell 2D wedge
        
        THIS CURRENTLY ONLY SAVES DATA FROM THE FUEL REGION
    """
    x = np.genfromtxt(f'./{casename}/0/fluidRegion/Cx',skip_header=23,skip_footer=6763-6326)
    y = np.genfromtxt(f'./{casename}/0/fluidRegion/Cy',skip_header=23,skip_footer=6763-6326)
    z = np.genfromtxt(f'./{casename}/0/fluidRegion/Cz',skip_header=23,skip_footer=6763-6326)
    v = np.genfromtxt(f'./{casename}/0/fluidRegion/V',skip_header=23,skip_footer=6763-6326)*120  # because I have a 3 degree slice
    pdensData = np.genfromtxt(f'./{casename}/0.1/fluidRegion/powerDensityNeutronics',skip_header=23,skip_footer=6448-6326)*0.58 # 0.58 is fuel volumeFraction
    #fluxData = np.genfromtxt('./1/neutroRegion/oneGroupFlux',skip_header=23,skip_footer=6498-6326)
    # thermFluxData = np.genfromtxt(f'./{casename}/1/neutroRegion/flux1',skip_header=23,skip_footer=6498-6326)
    # fastFluxData = np.genfromtxt(f'./{casename}/1/neutroRegion/flux0',skip_header=23,skip_footer=6498-6326)
    #print(len(x),len(y),len(z),len(pdensData))
    data = [[],[],[],[],
            [],[],[]]
    
    for i in range(len(pdensData)):
        if pdensData[i] < 1: continue # only grabbing data in fuel region
        
        data[0].append(x[i])
        data[1].append(y[i])
        data[2].append(z[i])
        data[3].append(pdensData[i])
        data[4].append(pdensData[i]*v[i])                      # power
        # data[5].append(thermFluxData[i])                       # thermal flux
        # data[6].append(fastFluxData[i])                        # fast flux

    return data

def flattenData(data: list,xyz: list,index: int) -> list:
    """ a function to flatten the data to one variable. i.e. given data 
        with an x,y, and z location, it flattens it to only z

        returns data with 100 bins in the dimension chosen

    data : a list of the data 
    xyz  : a list of the xyz data in a list of lists; xyz = [[x0,x1,...],[y0,y1,...],[z0,...]]
    index: the index of xyz you want to exist at the end. (0,1,or 2)
    """
    l = len(data) 
    xbins = np.linspace(0,0.6392,100)
    y = np.asarray(data)
    #
    bin_indices = np.digitize(xyz[index], xbins)
    sums = np.zeros(100)

    for i in range(1, 100 + 1):
        sums[i - 1] = np.sum(y[(bin_indices == i)]) #/ len(y[(bin_indices == i)])
    
    return sums,xbins

def findInAssemblyPeakingFactor(pinfo:list) -> float:
    """
    
    pinfo: this should be the list of powers in the fuel region (durrently d[4])
    """
    avg,max = np.mean(pinfo), np.max(pinfo)
    return max/avg
    
def plotAxialPower(d:list,casename:str) -> None:
    """ builds a plot of the axial power distribution
    
    d: what the getData() function returns
    """
    xyz = [d[0],d[1],d[2]]
    sums, xbins = flattenData(d[4],xyz,0)
    serp = [ 39398.5 , 55434.3,  63052.,   76696.2,  86326.8,  94512.,  105873.,  110752.,
            124003.  ,139062.,  151852.,  153308.,  171692.,  180860.,  191174.,  191768.,
            203012.  ,211595.,  216684.,  223819.,  244334.,  257192.,  245288.,  277855.,
            270717.  ,285165.,  294383.,  300303. , 314031.,  317971.,  317871.,  321379.,
            335689.  ,349319.,  352972.,  360042.,  354932.,  345153.,  364621.,  381877.,
            391024.  ,388415.,  383867.,  386839.,  393990.,  392328.,  402136.,  403411.,
            407262.  ,408191.,  405799. , 403760.,  413038.,  406530.,  402046.,  406369.,
            402314.  ,401434.,  417046.,  403000.,  404975.,  388706.,  386708.,  385060.,
            396917.  ,399373.,  401809.,  388088.,  373416.,  369495.,  371910.,  360508.,
            355291.  ,343934.,  345286.,  327321.,  323249.,  318319.,  319863.,  296113.,
            285166.  ,282428.,  271546.,  259197.,  248522.,  243683.,  233871.,  219871.,
            201674.  ,179233. , 176855.,  171396.,  151629.,  140331.,  118377.,  108689.,
            93254.8  ,76827.6,  61997.5,  49229.6,]
    plt.figure(6)
    plt.scatter(xbins,sums,label='GeN-Foam: Diffusion')
    #plt.scatter(xbins,serp,label='Serpent2')
    #plt.ylim(2e5,3e5)
    plt.title(f'Axial Power Distribution: GeN-Foam Diffusion w/ {casename} BC')
    plt.ylabel('Power (W)')
    plt.xlabel('Height (m)')
    plt.grid(which='major', color='gray', linestyle='-')
    plt.minorticks_on()
    plt.grid(which='minor', color='lightgray', linestyle='--')
    plt.legend()
    plt.savefig(f'{casename}.png')
    plt.show()

def normalizePower():
    """ a function to normalize the total power 
    """

if __name__ == "__main__":
    a = getData('albedo')
    fv = getData('fixedValue')
    
    #plotAxialPower(a,'albedo')
    plotAxialPower(fv,'fixedValue')
    
    
    
    
    print('Albedo Results:')
    print(f'   Total assembly power = {np.sum(a[4])*1e-6:0.4f} MW')
    print(f'   Average in-assembly power peaking factor = {findInAssemblyPeakingFactor(a[4]):0.4f}')
    
    print('Fixed Value Results:')
    print(f'   Total assembly power = {np.sum(fv[4])*1e-6:0.4f} MW')
    print(f'   Average in-assembly power peaking factor = {findInAssemblyPeakingFactor(fv[4]):0.4f}')






