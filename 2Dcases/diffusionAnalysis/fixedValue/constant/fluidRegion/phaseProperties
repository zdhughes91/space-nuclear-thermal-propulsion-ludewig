/*--------------------------------*- C++ -*----------------------------------*\
|       ______          _   __           ______                               |
|      / ____/  ___    / | / /          / ____/  ____   ____ _   ____ ___     |
|     / / __   / _ \  /  |/ /  ______  / /_     / __ \ / __ `/  / __ `__ \    |
|    / /_/ /  /  __/ / /|  /  /_____/ / __/    / /_/ // /_/ /  / / / / / /    |
|    \____/   \___/ /_/ |_/          /_/       \____/ \__,_/  /_/ /_/ /_/     |
|    Copyright (C) 2015 - 2022 EPFL                                           |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      phaseProperties;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// ------------------------------------------------------------------------- //
// --- THERMAL-HYDRAULIC TYPE ---------------------------------------------- //
// ------------------------------------------------------------------------- //

thermalHydraulicsType    "onePhase";

// ------------------------------------------------------------------------- //
// --- STRUCTURES PROPERTIES ----------------------------------------------- //
// ------------------------------------------------------------------------- //

structureProperties
{
    innerPropellant
    {
        volumeFraction      0.0;
        Dh                  3.2400;
        T                   200;
    }
    outerPropellant
    {
        volumeFraction      0.0;
        Dh                  3.0375;
        T                   200;
    }
    hotFrit // porous ZrC
    {
        volumeFraction      0.3;
        Dh                  2.3333e-03;
        passiveProperties
        {
            volumetricArea  94.707459;
            rho             2019.0;
            Cp              365.0;
            T               200; // K
        }
    }
    o1 // porous beryllium
    {
        volumeFraction      0.6;
        Dh                  2.5000e-04;
        passiveProperties
        {
            volumetricArea  94.707459;
            rho             1750;
            Cp              3661.784;
            T               200; // K
        }
    }
    o4
    {
        ${o1};
        volumeFraction      0.6;
    }
    o5
    {
        ${o1};
        volumeFraction      0.6;
    }
    o2
    {
        ${o1};
        volumeFraction      0.6;
    }
    o3
    {
        ${o1};
        volumeFraction      0.6;
    }
    fuel
    {
        volumeFraction      0.58;
        Dh                  4.3086e-04;

        powerModel
        {
            type                lumpedNuclearStructure;
            volumetricArea      5848.739;  // m^-1
            // below powerDensity calculated using power in whole volume (i think..)
            // powerDensity        4.053e+10; // W/m^3, for use without neutroRegion
            powerDensity        0;
            nodesNumber         2; // Number of nodes
            nodeFuel            0; // 
            nodeClad            1; // 
            // Heat conductances from Tmax -> Tav,fuel -> Tav,shell -> Tsurf
            heatConductances    (4.787e+09 6.477e+09 2.912e+10); 
            rhoCp               (1.986e+06 4.374e+06); // for each node
            volumeFractions     (0.50878 0.49122); // Fraction of the volume of the structure occupied by each node 
            powerFractions      (1 0); // Fraction of total power in the strucure
            T0                  200;                         // that goes 
        }
    }
}

// ------------------------------------------------------------------------- //
// --- REGIME MAP MODEL ---------------------------------------------------- //
// ------------------------------------------------------------------------- //

regimeMapModel
{
    type    none;
}

// ------------------------------------------------------------------------- //
// --- REGIME PHYSICS FOR EACH REGIME -------------------------------------- //
// ------------------------------------------------------------------------- //

physicsModels
{
    dragModels
    {
        "fuel:hotFrit:o1:o2:o3:o4:o5"  // section 1.5.2 in [10] from preprocess.py
        {
            type        ReynoldsPower;
            const       0.55;
            coeff       1;
            exp         -1.0;
        }
        "innerPropellant:outerPropellant"
        {
            type    ReynoldsPower;
            coeff       0.184;
            exp         -0.2;
        }
    }

    heatTransferModels // 
    { 
        // "fuel:hotFrit:"o1:o2:o3:o4:o5":innerPropellant:outerPropellant"
        // {
        //     type    NusseltReynoldsPrandtlPower;
        //     const   0;
        //     coeff   0.0;
        //     expRe   0.0;
        //     expPr   0.0;
        // }
        fuel // equation 2.16 in [10] from preprocess.py
        {
            type    NusseltReynoldsPrandtlPower;
            const   0;
            coeff   0.6071;   // eqn. 2.16 in [10] (Nield)
            //coeff   0.9223;   // eqn.34 in [12] (Achenbach)
            expRe   ${{2.0/3.0}}; // eqn. 2.16 in [10] (Nield)
            //expRe   0.61;// eqn.34 in [12] (Achenbach)
            expPr   ${{1.0/3.0}};
        }
        hotFrit// equation 2.16 in [10] from preprocess.py
        {
            ${fuel};
            coeff   0.3643;// eqn. 2.16 in [10] (Nield)
            //coeff   0.6667;// eqn.34 in [12] (Achenbach)
        }
        "o1:o2:o3:o4:o5"// equation 2.16 in [10] from preprocess.py
        {
            ${fuel};
            coeff   1.2750;// eqn. 2.16 in [10] (Nield)
            //coeff   0.50;// eqn.34 in [12] (Achenbach)
        }
        innerPropellant// page15 in [11] from preprocess.py, need to recheck Tw/Tb
        {
            type    NusseltReynoldsPrandtlPower;
            const   0;
            coeff   0.023;
            expRe   0.8;
            expPr   0.4;
            expTc   -0.3;
        }
        outerPropellant// page15 in [11] from preprocess.py, need to recheck Tw/Tb
        {
            ${innerPropellant};
        }
    }
}

// ------------------------------------------------------------------------- //
// --- MISCELLANEA --------------------------------------------------------- //
// ------------------------------------------------------------------------- //

pMin        1e4;

pRefCell    0;

pRefValue   1e5;

// ************************************************************************* //
