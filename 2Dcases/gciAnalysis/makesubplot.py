"""
    This script reads into the axial and radial folders in order
    to build a subplot of both GCI results

    Author: Zach Hughes - zhughes@tamu.edu
    Date: Apr.13.2024
"""
import os
import matplotlib.pyplot as plt

# from radial import gciSolver
# import axial.gciSolver

def execute_file(file_path):
    with open(file_path, 'r') as file:
        file_content = file.read()
        exec(file_content, globals())

# Execute file1.py
os.chdir('./axial/')
execute_file('./gciSolver.py')
os.chdir('./../')

# Execute file2.py
os.chdir('./radial/')
execute_file('./gciSolver.py')
os.chdir('./../')

labelnames = {
    'outletTbulk':'Outlet Tbulk',
    'inletPress':'Inlet Pressure',
    'outletVelo':'Outlet Velocity',
    #'outletMFR':'m',
    'Tmax':'Max. Temperature'
}


# Create a figure and a grid of subplots with 1 row and 2 columns
fig, axes = plt.subplots(1, 2,figsize=(9, 4))

# Now you have two axes objects in the 'axes' array
# You can plot on each axis independently
# For example:
#plt.plot([50,100,150,200,225],gciDictAxial[result],label=result,c=cullahs[result],marker='o')
for result in cullahs:
    axes[0].plot([50,100,150,200,225],gciDictAxial[result],label=labelnames[result],c=cullahs[result],marker='o')
    axes[1].plot([1,2,4],gciDictRadial[result],label=labelnames[result],c=cullahs[result],marker='o')
axes[0].yaxis.set_major_formatter(FuncFormatter(percent_formatter))
axes[0].set_title('Axial GCI')
axes[0].set_ylabel('Normalized GCI')
axes[0].set_xlabel('Axial Cell Count')
axes[0].legend()
axes[0].grid(which='major', color='gray', linestyle='-')
axes[0].minorticks_on()
axes[0].grid(which='minor', color='lightgray', linestyle='--')

axes[1].yaxis.set_major_formatter(FuncFormatter(percent_formatter))
axes[1].set_title('Radial GCI')
axes[1].set_ylabel('Normalized GCI')
axes[1].set_xlabel('Radial Cell Count')
axes[1].legend()
axes[1].grid(which='major', color='gray', linestyle='-')
axes[1].minorticks_on()
axes[1].grid(which='minor', color='lightgray', linestyle='--')
#fig.suptitle('Axial & Radial Grid Convergence Index (GCI)')
# Optionally, adjust layout to prevent overlap
plt.tight_layout()

# Show the plots
plt.savefig('bothGCIconvergence.png')
#plt.show()