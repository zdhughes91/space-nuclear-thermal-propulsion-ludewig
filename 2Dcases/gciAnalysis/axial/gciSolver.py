""" 
    solves GCI using an imported library. for some reason the algorithm they
    use is unstable when the last case has 300 axial cells.
    
    Author: Zach Hughes - zhughes@tamu.edu
    Date: Apr.13.2024

"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
from convergence import Convergence
grids = [(1.000000, 0.970500),
         (2.000000, 0.968540),
          (4.000000, 0.961780)]
# convergence = Convergence()
# convergence.add_grids(grids)
# print(convergence)



ycells = {'outerProp':3,
          'coldFrit': 3,
          'fuel':     6,
          'hotFrit':  3,
          'innerProp':6
          }

xcells = {'6300':250, # i built this after finding the xlen below
          '4725':225,
          '4200':200,
          '3150':150,
          '2100':100,
          '1050':50}

def findXlen(totCells):
    """ refinding the x len becuase I forgot how I made them"""
    global ycells
    print(f"totCells={totCells}, xlen={totCells/21}")

# findXlen(6300)
# findXlen(4200)
# findXlen(3150)
# findXlen(2100)
# findXlen(1050)


#
def orderOfAccuracy(f3,f2,f1,r):
    """
    """
    #f3,f2,f1 = abs(f3),abs(f2),abs(f1)

    numerator = np.log(abs(f3-f2)/abs(f2-f1))
    denominator = np.log(r)
    return numerator/denominator

def gridConvergenceIndex(p,r,f2,f1):
    """"""
    return (np.abs(f2-f1)/(r**p - 1))


#
def getVal(line):
    keyval = line.strip().split()
    #key = keyval[0][-14:]
    val = keyval[1][:-1]
    #print(f"val={val}")
    return val

resultInfo = {}
imf,omf,otb,ipr,ovl,tmx = [],[],[],[],[],[]
for mesh in xcells: # loop thru meshes
    #if mesh == '3150': continue
    resultInfo[mesh] = {}
    meshpath = f"orificed{mesh}/0.5/uniform/functionObjects/functionObjectProperties"
    with open(meshpath,'r') as file: # open results file
        for line_number, line in enumerate(file,start=1):
            if line_number == 25:
                val = round(float(getVal(line)),5)
                imf.append((xcells[mesh],val))
            if line_number == 32:
                val = round(float(getVal(line)),5)
                omf.append((xcells[mesh],val))
            if line_number == 46:
                val = round(float(getVal(line)),5)
                otb.append((xcells[mesh],val))
            if line_number == 54:
                val = round(float(getVal(line)),5)
                ipr.append((xcells[mesh],val))
            if line_number == 64:
                val = round(float(getVal(line)),5)
                ovl.append((xcells[mesh],val))
            if line_number == 80:
                val = round(float(getVal(line)),5)
                tmx.append((xcells[mesh],val))


resultsDict = {#'inletMFR':imf,
               #'outletMFR':omf,
               'outletTbulk':otb,
               'inletPress':ipr,
               'outletVelo':ovl,
               'Tmax':tmx
               }

#print(resultsDict['inletMFR'])
convergenceDict = {}

for result in resultsDict:
    convergence = Convergence()
    
    convergence.add_grids(resultsDict[result])
    convergenceDict[result] = convergence
    # print(convergence.get_resolution(5))
    # print(result+'='*100)
    print(convergence)

gciDictAxial = {}

for result in resultsDict:
    gciDictAxial[result] = []
    for i in range(4):
        gciDictAxial[result].append(convergenceDict[result][i].fine.gci_coarse*100)
    gciDictAxial[result].append(convergenceDict[result][3].fine.gci_fine*100)


def percent_formatter(x,_):
    return f"{x :.1f}%"

cullahs = {
    'outletTbulk':   'r',
    'inletPress':'b',
    'outletVelo':'g',
    #'outletMFR':'m',
    'Tmax':     'c'
}

plt.figure(1,dpi=400)
for result in cullahs:
    plt.plot([50,100,150,200,225],gciDictAxial[result],label=result,c=cullahs[result],marker='o')
#     #plt.plot([150],gcipctb[result][1],label=result,c=cullahs[result],marker='o')
#     # plt.scatter(50,gcipct[result][0],label=result,c=cullahs[result])
#     # plt.scatter(100,gcipct[result][1],label=result,c=cullahs[result])
# #plt.ylim(0,5)
plt.gca().yaxis.set_major_formatter(FuncFormatter(percent_formatter))
#plt.title('Normalized GCI in 2D Wedge')
plt.ylabel('Normalized GCI')
plt.xlabel('Axial Cell Count')
plt.legend()
plt.grid(which='major', color='gray', linestyle='-')
plt.minorticks_on()
plt.grid(which='minor', color='lightgray', linestyle='--')
#plt.ylim(0,100)
#plt.show()
#plt.savefig('normalizedAxialGCI.png')

