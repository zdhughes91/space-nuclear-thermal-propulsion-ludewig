""" 
    a script to run all meshes simulaneously
        -this is really only useful for preparing mesh studies

    March.26.2024
    Author: Zach Hughes - zhughes@tamu.edu
"""
import multiprocessing
import subprocess
import time
import shutil
import os

xcells = {'4725':225,
          '4200':200,
          '3150':150,
          '2100':100,
          '1050':50}

def runGenfoam(caseName):
    """ runs genfoam case using a parallel processing module so I can solve
        all genfoam cases simultaneously
    """
    
    print(f'Running GeN-Foam Case {caseName}...')
    # --- 3. running genfoam
    loggenfoam = open(f'log.genfoam.{caseName}','w')
    os.chdir(f'./orificed{caseName}/')
    process = subprocess.run('GeN-Foam',                           # run genfoam
                            stdout=loggenfoam,                       # log genfoam output
                            stderr=loggenfoam, 
                            shell=True)
    #process.wait()   
    loggenfoam.close()
    os.chdir('./../')
    # --- 


def parallelGenfoam():
    """ runs all genfoam cases simulaneously in order to
        solve TH more efficiently
    """
    global xcells
    start = time.time()
    
    processDict = {}
    for i,assembly in enumerate(xcells):
        processDict[assembly] = multiprocessing.Process(target=runGenfoam,args=(assembly,))
        processDict[assembly].start() # start all cases
        
    for a in xcells:
        processDict[a].join() # wait for all to finish
    print('-'*50)
    print(f"    T/H Solved in {(time.time()-start)/60:.3f} minutes")

if __name__ == "__main__":
    parallelGenfoam()

    # for x in xcells: # this deletes power dnesity in fluid region and replaces it
    #     os.remove(f'./orificed{x}/0/fluidRegion/powerDensityNeutronics')
    #     shutil.copy(f'./orificed{x}/0.9/neutroRegion/powerDensity',
    #                 f'./orificed{x}/0/fluidRegion/powerDensityNeutronics')
        
    