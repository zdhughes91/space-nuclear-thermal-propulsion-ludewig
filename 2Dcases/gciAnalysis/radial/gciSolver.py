""" 
    solves GCI using an imported library
    
    Author: Zach Hughes - zhughes@tamu.edu

"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
from convergence import Convergence
import os

print(os.getcwd())
grids = [(1.000000, 0.970500),
         (2.000000, 0.968540),
          (4.000000, 0.961780)]
# convergence = Convergence()
# convergence.add_grids(grids)
# print(convergence)



ycells = {'outerProp':3,
          'coldFrit': 3,
          'fuel':     6,
          'hotFrit':  3,
          'innerProp':6
          }

xcells = {'n':8,
          'two':4,
          'one':2,
          'zero':1}

def findXlen(totCells):
    """ refinding the x len becuase I forgot how I made them"""
    global ycells
    print(f"totCells={totCells}, xlen={totCells/21}")

# findXlen(6300)
# findXlen(4200)
# findXlen(3150)
# findXlen(2100)
# findXlen(1050)


#
def orderOfAccuracy(f3,f2,f1,r):
    """
    """
    #f3,f2,f1 = abs(f3),abs(f2),abs(f1)

    numerator = np.log(abs(f3-f2)/abs(f2-f1))
    denominator = np.log(r)
    return numerator/denominator

def gridConvergenceIndex(p,r,f2,f1):
    """"""
    return (np.abs(f2-f1)/(r**p - 1))


#
def getVal(line):
    keyval = line.strip().split()
    #key = keyval[0][-14:]
    val = keyval[1][:-1]
    #print(f"val={val}")
    return val

resultInfo = {}
imf,omf,otb,ipr,ovl,tmx = [],[],[],[],[],[]
for mesh in xcells: # loop thru meshes
    #if mesh == '3150': continue
    resultInfo[mesh] = {}
    meshpath = f"{mesh}Radial/0.5/uniform/functionObjects/functionObjectProperties"
    print(meshpath)
    with open(meshpath,'r') as file: # open results file
        for line_number, line in enumerate(file,start=1):
            if line_number == 25:
                val = round(float(getVal(line)),5)
                imf.append((xcells[mesh],val))
            if line_number == 32:
                val = round(float(getVal(line)),5)
                omf.append((xcells[mesh],val))
            if line_number == 46:
                val = round(float(getVal(line)),5)
                otb.append((xcells[mesh],val))
            if line_number == 54:
                val = round(float(getVal(line)),5)
                ipr.append((xcells[mesh],val))
            if line_number == 64:
                val = round(float(getVal(line)),5)
                ovl.append((xcells[mesh],val))
            if line_number == 80:
                val = round(float(getVal(line)),5)
                tmx.append((xcells[mesh],val))


resultsDict = {#'inletMFR':imf,
               #'outletMFR':omf,
               'outletTbulk':otb,
               'inletPress':ipr,
               'outletVelo':ovl,
               'Tmax':tmx
               }

convergenceDict = {}

for result in resultsDict:
    convergence = Convergence()
    convergence.add_grids(resultsDict[result])
    convergenceDict[result] = convergence
    # print(convergence.get_resolution(5))
    # print(result+'='*100)
    # print(convergence)

gciDictRadial = {}

for result in resultsDict:
    gciDictRadial[result] = []
    for i in range(2):
        gciDictRadial[result].append(convergenceDict[result][i].fine.gci_coarse*100)
    gciDictRadial[result].append(convergenceDict[result][i].fine.gci_fine*100)


def percent_formatter(x,_):
    return f"{x :.1f}%"

cullahs = {
    'outletTbulk':   'r',
    'inletPress':'b',
    'outletVelo':'g',
    #'outletMFR':'m',
    'Tmax':     'c'
}

plt.figure(1,dpi=200)
for result in cullahs:
    plt.plot([1,2,4],gciDictRadial[result],label=result,c=cullahs[result],marker='o')
#     #plt.plot([150],gcipctb[result][1],label=result,c=cullahs[result],marker='o')
#     # plt.scatter(50,gcipct[result][0],label=result,c=cullahs[result])
#     # plt.scatter(100,gcipct[result][1],label=result,c=cullahs[result])
# #plt.ylim(0,5)
plt.gca().yaxis.set_major_formatter(FuncFormatter(percent_formatter))
#plt.title('Normalized GCI in 2D Wedge')
plt.ylabel('Normalized GCI')
plt.xlabel('Radial Cell Count')
plt.legend()
plt.grid(which='major', color='gray', linestyle='-')
plt.minorticks_on()
plt.grid(which='minor', color='lightgray', linestyle='--')
plt.subplots_adjust(left=0.15) 
#splt.show()
plt.savefig('normalizedRadialGCI.png')