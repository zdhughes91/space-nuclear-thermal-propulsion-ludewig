/*--------------------------------*- C++ -*----------------------------------*\
|       ______          _   __           ______                               |
|      / ____/  ___    / | / /          / ____/  ____   ____ _   ____ ___     |
|     / / __   / _ \  /  |/ /  ______  / /_     / __ \ / __ `/  / __ `__ \    |
|    / /_/ /  /  __/ / /|  /  /_____/ / __/    / /_/ // /_/ /  / / / / / /    |
|    \____/   \___/ /_/ |_/          /_/       \____/ \__,_/  /_/ /_/ /_/     |
|    Copyright (C) 2015 - 2022 EPFL                                           |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      functions;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// This dict is the set of functions to evaluate interesting values with the 
// -postProcess utility. Problems with running these functions while the 
// simulation runs in parallel have been encountered. Using these functions on 
// a reconstructed case works as excpected.

functions
{
    // --- Mass flow
    // --- Mass flow
    mFlowInlet
    {
        type            massFlow;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     true;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      outerPropellantInlet;
        alphaRhoPhiName alphaRhoPhi;
        scaleFactor     120; // we have a 3 degree slice
    }
    mFlowOutlet
    {
        type            massFlow;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     true;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      innerPropellantOutlet;
        alphaRhoPhiName alphaRhoPhi;
        scaleFactor     120; // we have a 3 degree slice
    }

    // --- TBulk
    TInlet
    {
        type            TBulk;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      outerPropellantInlet;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    TOutlet
    {
        type            TBulk;
        libs            ("libfieldFunctionObjects.so");
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        regionName      innerPropellantOutlet;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }

    // --- Surface fields
    vInlet
    {
        type            surfaceFieldValue; // fieldAverage;
        libs            (fieldFunctionObjects);
        fields          (magU p T alphaRhoPhi alphaPhi);
        operation       areaAverage;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        name            outerPropellantInlet;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    vOutlet
    {
        type            surfaceFieldValue; // fieldAverage;
        libs            (fieldFunctionObjects);
        fields          (magU p T alphaRhoPhi alphaPhi);
        operation       areaAverage;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval	$writeInterval;
        region          fluidRegion;
        regionType      patch;
        name            innerPropellantOutlet;
        thermoName      thermophysicalProperties;
        alphaRhoPhiName alphaRhoPhi;
    }
    // --- Field min max
	minMaxTemperature
    {
        type            fieldMinMax;
        libs            (fieldFunctionObjects);
        mode            magnitude;
	    region          fluidRegion;
        fields
        (
            T 
            Tsurface.lumpedNuclearStructure 
            T.lumpedNuclearStructure 
            Tmax.lumpedNuclearStructure
        );
        enabled         true;
        log             true;
        writeControl    runTime;
        writeInterval   $writeInterval;
    }

    
    // --- Power integral over fluidRegion
    totalPowerFluid
    {
        type            volFieldValue;
        libs            (fieldFunctionObjects);
        fields          (powerDensityNeutronics);
        operation       volIntegrate;
        region          fluidRegion;
        regionType      all;
        log             true;
        writeFields     true;
        writeControl    adjustableRunTime;
        writeInterval   $writeInterval;
        scaleFactor     0.58; // Fuel fraction
    }
    totalPowerNeutro
    {
        type            volFieldValue;
        libs            (fieldFunctionObjects);
        fields          (powerDensity);
        operation       volIntegrate;
        region          neutroRegion;
        regionType      all;
        log             true;
        writeFields     true;
        writeControl    adjustableRunTime;
        writeInterval   $writeInterval;
        scaleFactor     0.58; // Fuel fraction
    }

    densityFieldFunction
    {
        type        exprField;
        libs        (fieldFunctionObjects);
        field       densityField;
        region      fluidRegion;
        readFields  (alphaRhoPhi alphaPhi);
        action      new;

        expression  "alphaRhoPhi / alphaPhi";
        autowrite   true;
        log             true;
        writeFields     false;
        writeControl    adjustableRunTime;
        writeInterval   $writeInterval;
    }

}
// ************************************************************************* //
