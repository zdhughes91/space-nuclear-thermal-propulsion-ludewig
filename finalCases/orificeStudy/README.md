# Orificing the Cold Frit via a Porosity Distribution
## USNC-edited Ludewig SNTP
## Author: Zach Hughes - zhughes@tamu.edu
## Date: Feb.14.2024

### porositySolver.py
This script solves for the porosity distribution in the cold frit for a given number of orifice zones. It uses a polynomial to describe the power distribution in the assembly, which was generated using the powerDensityNeutronics field from the diffusion neutronics solver. 

### Notes:
1. The porosity distribution had a fair amount of success. The max fuel temperature with a uniform cold frit porosity (refered to in the future as the original case) was 7128 and the max fuel temperature with a porosity distribution (that kept the mean porosity equal to that of the uniform porosity) was 4110 K. 
2. Restriction of the flow is neccessary to change the direction of mass flow. This has lead to an increase in pressure drop through the assembly, which is around 2.06MPa compared to the 1.47MPa drop in the original. I have found that if you decrease the mean porosity while keeping the ratios of the porosities to each other equal, you can decrease the pressure loss through the assembly. With this said the reduction is not significant and it is not clear that the Be cold frit could actually be constructed with porosities that low. 
3. The porosity distribution seems to have flattened the temperature profile in the fuel very well in the central three orifice zones, however the temperature at the first and last orifice zones are significantly lower. While it is possible that after burnup these regions become higher power-producing, more work may be necessary to even these regions as well. 

### Future work:
1. A continuous porosity distribution could be created if each cell axially was a different cellZone. This would require a script in salome to partition each of these zones as well as a script to write the phaseProperties + turbulenceProperties files in GeN-Foam. I am not sure how this would impact computational time but I imagine it would increase memory significantly. 
2. Azimuthal power distribution needs to be included. All assemblies except for the central one have large azimuthal power dependence, creating the need for orificing in this direction as well.
3. I think another solution for the porosity distribution is possible solely based on the ratio of the differences of power under each orifice. If the differences in power in all of the regions are normalized to the difference in power in the first and second region, a porosity distribution could be solved for that keeps the mean porosity equal to 0.4. 

### References:
1. Transport Phenomena (Second Edition) by R. Byron Bird, Warren E. Stewart, Edwin N. Lightfoot
2. April Novak's Thesis