"""
    This should be a standalone script that can read the full core serpent output. and produce 
    relevant plots

    current detectors:
        1. hexPowerBoth
         - has power in an axial and hexagonal discretization
        2. hexPlot
         -
        3. compareFuelFlux
         - has flux for 2 energy groups (split at 0.625eV) 

    Author: Zach Hughes - zhughes@tamu.edu in an axial & hexagonal discretization
    Date: Apr.5.2024


% ============ important note ! ============
% NORMALIZATION IS DONE IN THIS SCRIPT!!! (specificallly in sssReturnAxialData()))
% - AT THE TIME OF WRITING THIS, IT IS IN 3Dcases/fullCore/slice/diffusionAnalysis/
% - THE FLUX MUST BE DIVIDED BY THE BIN OF INTEREST.
% hexArea = 72.2cm^2, using 9.131cm as the flat2flat
% cellHeight = 0.6392cm, for 100 cells and a 63.92cm cellHeight
% fuel cross sectional area = pi*(router^2 - rinner^2)
% inletFrac ~= outletFrac ~= (fuel cross sectional area) / hexArea
% THE ABOVE LINE CAN BE SHOWN TO BE TRUE, BUT I MAY NEED TO FIND CORRECT FUEL FRAC USING VOLUME FOR EACH CELL IDK
% ^^ i am doing the above line. see function sssReturnAxialData()
% ==========================================
"""

# --- 
import serpentTools
import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
indexDict = {'a23':[5,6], # i learned this from trial and errror using the hexPlot 
             'a22':[5,7],
             'a15':[4,8],
             'a16':[4,7],
             'a17':[4,6],
             'a18':[4,5] 
             }
# both of below used to normalize serpent detector output
coreXSarea = np.pi*(30.91**2)               #cm
coreRadialSurfaceArea = 2*np.pi*30.91*63.92 #cm
# 

# --------------------------------------------------------- taken from ./scripts/preprocess.py
class flowDomain: # i need this to find volume of fuel region in each cell from serpent
    def __init__(self,innerInletRadius: float,
                      innerOutletRadius: float,
                      outerInletRadius: float,
                      outerOutletRadius: float,
                      axialCellCount: int=100    # number of axial cells in serpent
                ):
        self.innerRadii = [innerInletRadius,innerOutletRadius] # keeping cm
        self.outerRadii = [outerInletRadius,outerOutletRadius] # keeping cm
        self.coreHeight = 63.92 # cm
        self.axialCellCount = axialCellCount
    def getCircularArea(self,innerRadius: float,outerRadius: float) -> float:
        return (3.14159*(outerRadius**2 - innerRadius**2))
    def getCircularPerimeter(self,innerRadius: float,outerRadius: float) -> float:
        return (2*3.14159*(innerRadius+outerRadius))
    def outerRadiusFunction(self,x: float) -> float:
        """ function for the outer radius as a function of height: to be integrated"
            x is the height"""
        m = (self.outerRadii[1]-self.outerRadii[0])/(self.coreHeight)
        return (m*x)+self.outerRadii[0]
    def innerRadiusFunction(self,x: float) -> float:
        """ function for the inner radius as a function of height: to be integrated"
            x is the height"""
        m = (self.innerRadii[1]-self.innerRadii[0])/(self.coreHeight)
        return (m*x)+self.innerRadii[0] 
    def xsAreaFunction(self,x:float) -> float:
        """ a function to make one eqn for the XS area of the annulus as a function of h"""
        return 3.14159*(self.outerRadiusFunction(x)**2-self.innerRadiusFunction(x)**2)
    def assignFuelFrac(self) -> None: 
        """ THIS IS NEEDED TO NORMALIZE THE SERPENT DATA. SEE HEADER"""
        self.fuelFrac = []
        hexArea = 72.2 #cm, with hex flat2flat of 9.131cm
        hexCellHeight = self.coreHeight / self.axialCellCount
        hexVol = hexArea*hexCellHeight
        for i in range(1,self.axialCellCount+1):
            iFuelVol = integrate.quad(self.xsAreaFunction,
                                      ((i-1)/self.axialCellCount)*self.coreHeight,
                                      ((i)/self.axialCellCount)*self.coreHeight)[0]
            iFuelFrac = iFuelVol / hexVol
            self.fuelFrac.append(iFuelFrac)

# ---------------------------------------------------------
def sssInit(fname:str="./settings_det0.m") -> tuple:
    """ convinient when importing this .py
    """
    indexDict = {'a23':[5,6], # i learned this from trial and errror using the hexPlot 
             'a22':[5,7],
             'a15':[4,8],
             'a16':[4,7],
             'a17':[4,6],
             'a18':[4,5] 
             }
    
    # --- reading the .m file
    data = serpentTools.read(fname)

    return indexDict,data

def sssFindAllPeakingFactors(det: dict) -> dict:
    """
    *** this function assumes the detector names as written in the header

    """
    global indexDict

    sumPower = 0
    axialAssemblyPowerDict = {}
    assemblyPowerPeakingFactorDict = {}
    for a in indexDict:
        i0,i1 = indexDict[a][0],indexDict[a][1]

        aPower = det.detectors['hexPlot'].tallies[0,i0,i1]
        axialAssemblyPowerDict[a] = det.detectors['hexPowerBoth'].tallies[0,:,i0,i1]

        sumPower += aPower*1e-6
        ppf = np.max(axialAssemblyPowerDict[a]) / np.mean(axialAssemblyPowerDict[a])

        assemblyPowerPeakingFactorDict[a] = ppf
        print(f'Assembly {a} Results: sss')
        print(f'   Power = {aPower*1e-6:0.3f} MW')
        print(f'   Power peaking factor = {ppf:0.4f}')

    print('Full six assembly results: sss')
    print(f'   Total six assembly power = {sumPower:0.4f} MW')
    print(f'   Max-to-average power = {aPower/27.0707e6:0.4f}') #this is a18/knownAvgPower
    
    return assemblyPowerPeakingFactorDict

def plotHexActiveCore(det:dict) -> None:
    """ using serpentTools to create color map plot of active core power
    """

    det.detectors['hexPlot'].pitch = 9.131
    det.detectors['hexPlot'].hexType = 3
    print(det.detectors['hexPlot'].indexes)
    # notActiveCoreHex.tallies[0,4,5] = 0
    det.detectors['hexPlot'].hexPlot(fixed={'reaction':0},
                            thresh = 1e2,cbarLabel='Power (W)')
    plt.title('Active Core Assembly Power')
    plt.show()

def sssPlotAxialPower(det:dict,assembly:str,plotAll: bool=False) -> None:
    """
    """
    global indexDict
    if not plotAll:
        i0,i1 = indexDict[assembly][0], indexDict[assembly][1]
        plt.title('Axial Assembly Power')
        plt.scatter(np.linspace(0,63.92,100),det.detectors['hexPowerBoth'].tallies[0,:,i0,i1],label=assembly)
        plt.ylabel('Axial Power (W)')
        plt.xlabel('Height (cm)')
        plt.grid(which='major', color='gray', linestyle='-')
        plt.minorticks_on()
        plt.grid(which='minor', color='lightgray', linestyle='--')
        plt.legend()
        #plt.savefig(f'sss_{assembly}axial.png')
        plt.show()
    else:
        for a in indexDict:
            i0,i1 = indexDict[a][0], indexDict[a][1]
            plt.title('Axial Assembly Power')
            plt.scatter(np.linspace(0,63.92,100),det.detectors['hexPowerBoth'].tallies[0,:,i0,i1],label=a)
        plt.ylabel('Axial Power (W)')
        plt.xlabel('Height (cm)')
        plt.grid(which='major', color='gray', linestyle='-')
        plt.minorticks_on()
        plt.grid(which='minor', color='lightgray', linestyle='--')
        plt.legend()
        plt.show()

def sssPlotAxialFlux(det:dict,assembly:str,fast: bool=False,plotAll: bool=False) -> None:
    """
    """
    speedLabel = 'Thermal'
    if fast: speedLabel='Fast'
    
    global indexDict
    if not plotAll:
        i0,i1 = indexDict[assembly][0], indexDict[assembly][1]
        plt.title(f'Axial Assembly{assembly[1:]} {speedLabel} Flux')
        plt.scatter(np.linspace(0,63.92,100),det.detectors['compareFuelFlux'].tallies[0,:,i0,i1],label=assembly)
        plt.ylabel(r'$Flux (\frac{n}{cm^{2} \cdot s})$')
        plt.xlabel('Height (cm)')
        plt.grid(which='major', color='gray', linestyle='-')
        plt.minorticks_on()
        plt.grid(which='minor', color='lightgray', linestyle='--')
        plt.legend()
        plt.show()
    else:
        for a in indexDict:
            i0,i1 = indexDict[a][0], indexDict[a][1]
            plt.title('Axial Assembly Power')
            plt.scatter(np.linspace(0,63.92,100),det.detectors['hexPowerBoth'].tallies[0,:,i0,i1],label=a)
        plt.ylabel('Axial Power (W)')
        plt.xlabel('Height (cm)')
        plt.grid(which='major', color='gray', linestyle='-')
        plt.minorticks_on()
        plt.grid(which='minor', color='lightgray', linestyle='--')
        plt.legend()
        plt.show()

def sssReturnAxialData(det: dict,
                       assembly:str,   # a18,a17,etc
                       var:str         # can be power/therm/fast
                       ) -> tuple:
    """ 

    """
    global indexDict
    i0,i1 = indexDict[assembly][0],indexDict[assembly][1] # grabbing specific assembly indices

    if var == "power":
        ydata = det.detectors['hexPowerBoth'].tallies[0,:,i0,i1]
        xdata = np.linspace(0,63.92,len(ydata))
        return xdata,ydata
    elif var == "therm": # if grabbing flux, normalization is needed
        detname = "compareFuelFlux"
        nrg = 0 # thermal is zero, see det#.m file for confirmation
    elif var == "fast":
        detname = "compareFuelFlux"
        nrg = 1 # fast is one, see det#.m file for confirmation
    
    hexArea = 72.2 # cm^2,using flat2flat of 9.131cm
    hexHeight = 0.6392 # cm, assuming 100 axial bins and height of 63.92
    hexVol = hexArea * hexHeight

    # -- attempting exact normalization
    fuelRegion = flowDomain(1.265,2.429,2.182,3.044)
    fuelRegion.assignFuelFrac() 
    # --

    # ydata = det.detectors[detname].tallies[nrg,:,i0,i1] / hexVol / 0.142 # 0.142 comes from the average fuel frac in hex. see header!
    ydata = det.detectors[detname].tallies[nrg,:,i0,i1] / hexVol / fuelRegion.fuelFrac 
    xdata = np.linspace(0,63.92,len(ydata))

    return xdata,ydata



if __name__ == "__main__":
    indexDict,data = sssInit("./settings_det0.m")

    # --- finding peaking factors
    # sssFindAllPeakingFactors(data)

    # --- plot hex active core
    # plotHexActiveCore(data)

    # --- plot axial dist
    # sssPlotAxialPower(data,'all',plotAll=True)
    # sssPlotAxialFlux(data,'a18',fast=True)

    # --- axial data retrieval example
    x,y = sssReturnAxialData(data,
                             'a18',
                             'fast')
    # plt.scatter(x,y)
    # plt.show()



    # finding bc based on surface flux results
    # [thermal,fast]
    radialWallFlux = [6.25208E+19,1.29722E+20]
    topFlux = [2.23897E+18,1.04855E+19]
    botFlux = [1.80913E+18,1.15940E+19]

    radialWallFlux = 1e4*np.asarray(radialWallFlux) / coreRadialSurfaceArea # convert cm^2 to m^2 and then normalize
    topFlux,botFlux = 1e4*np.asarray(topFlux)/coreXSarea, 1e4*np.asarray(botFlux)/coreXSarea # convert cm^2 to m^2 and then normalize

    print(f'radialTherm={radialWallFlux[0]:0.4e},radialFast={radialWallFlux[1]:0.4e}')
    print(f'topTherm={topFlux[0]:0.4e},topFast={topFlux[1]:0.4e}')
    print(f'botTherm={botFlux[0]:0.4e},botFast={botFlux[1]:0.4e}')
