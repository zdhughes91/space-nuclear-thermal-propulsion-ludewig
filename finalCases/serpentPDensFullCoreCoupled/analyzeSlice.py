"""

    Author: Zach Hughes - zhughes@tamu.edu
    Date: Apr.1.2024
"""

# ---
import numpy as np
import matplotlib.pyplot as plt
import subprocess
from analyzeSerpent import *
from azimuthallyDiscretize import *
from porositySolver import *

# ---



def getData(casename: str) -> list:
    """ a function to read powerdensity output file from neutronics AND the cell volume
        file. vol.csv is the volume file for the 6300 cell 2D wedge
        
        THIS CURRENTLY ONLY SAVES DATA FROM THE FUEL REGION
    """
    
    x = np.genfromtxt(f'{casename}/0/fluidRegion/Cx',skip_header=23,skip_footer=107994-81926)
    y = np.genfromtxt(f'{casename}/0/fluidRegion/Cy',skip_header=23,skip_footer=108853-81926)
    z = np.genfromtxt(f'{casename}/0/fluidRegion/Cz',skip_header=23,skip_footer=108853-81926)
    v = np.genfromtxt(f'{casename}/0/fluidRegion/V',skip_header=23,skip_footer=82152-81926)  # 
    pdensTMH = np.genfromtxt(f'{casename}/0/thermoMechanicalRegion/powerDensityNeutronics',skip_header=23,skip_footer=41837-41615)  # 
    vTMH = np.genfromtxt(f'{casename}/0/thermoMechanicalRegion/V',skip_header=23,skip_footer=41670-41614)  # 
    T    = np.genfromtxt(f'{casename}/0.8/fluidRegion/T',skip_header=23,skip_footer=93964-81926)
    Tmax = np.genfromtxt(f'{casename}/0.8/fluidRegion/Tmax.lumpedNuclearStructure',skip_header=23,skip_footer=82108-81926) 
    pdensData = np.genfromtxt(f'./{casename}/0/fluidRegion/powerDensityNeutronics',skip_header=23,skip_footer=82080-81926)
    # print(len(vTMH),len(pdensTMH))
    # # print(pdensTMH)
    # print(T)
    print(len(x),len(y),len(z),len(v),len(pdensData),len(T),len(Tmax))
    thmSum = 0
    for i in range(len(pdensTMH)):
        thmSum += pdensTMH[i] * vTMH[i]
    print(f'thm = {thmSum}')


    celltoregion = np.genfromtxt('./constant/cellToRegion',skip_header=21,skip_footer=25223-25221)
    cellZoneOrder = ['outerPropellant', 'o5', 'o3', 'o1', 'o2', 'hotFrit', 'o4', 'innerPropellant', 'fuel', 'moderVol']
    fuelLoc = np.where(np.array(cellZoneOrder) == 'fuel')[0][0]
    fuelIndices = np.where(celltoregion == fuelLoc)[0]

    for fueli in fuelIndices: # correcting fuel region for power. 
        pdensData[int(fueli)] = pdensData[int(fueli)] * 0.58

    regions = {'a15':np.genfromtxt(f'{casename}/constant/fluidRegion/polyMesh/sets/region0',skip_header=21,skip_footer=12625-12623,dtype=int),
            'a16':np.genfromtxt(f'{casename}/constant/fluidRegion/polyMesh/sets/region1',skip_header=21,skip_footer=12625-12623,dtype=int),
            'a17':np.genfromtxt(f'{casename}/constant/fluidRegion/polyMesh/sets/region2',skip_header=21,skip_footer=12625-12623,dtype=int),
            'a18':np.genfromtxt(f'{casename}/constant/fluidRegion/polyMesh/sets/region3',skip_header=21,skip_footer=6325-6323,dtype=int),
            'a22':np.genfromtxt(f'{casename}/constant/fluidRegion/polyMesh/sets/region4',skip_header=21,skip_footer=12625-12623,dtype=int),
            'a23':np.genfromtxt(f'{casename}/constant/fluidRegion/polyMesh/sets/region5',skip_header=21,skip_footer=12625-12623,dtype=int)
            }

    volumeMultiplier = {'a15':2, # needed because i have half assemblies/twelfth
                        'a16':2,
                        'a17':2,
                        'a18':12,
                        'a22': 1,
                        'a23':2
                        }
    data = {}
    pdens,rx,ry,rz,rv,rpow = {},{},{},{},{},{}
    for a in regions:
        data[a] = {}
        data[a]['pdens'] = pdensData[regions[a]]
        data[a]['x'] = x[regions[a]] 
        data[a]['y'] = y[regions[a]]
        data[a]['z'] = z[regions[a]]
        data[a]['v'] = v[regions[a]] * volumeMultiplier[a]

        data[a]['T'] = T[regions[a]]
        data[a]['Tmax'] = Tmax[regions[a]]

        data[a]['pow'] = data[a]['pdens'] * data[a]['v']
        data[a]['totPower'] = np.sum(data[a]["pow"])
    #print(data[a]['pow'])
    return data


def flattenData(data: list,
                xyz: list,
                index: int,
                var:str='pow'
                ) -> list:
    """ a function to flatten the data to one variable. i.e. given data 
        with an x,y, and z location, it flattens it to only z

        returns data with 100 bins in the dimension chosen

    data : a list of the data 
    xyz  : a list of the xyz data in a list of lists; xyz = [[x0,x1,...],[y0,y1,...],[z0,...]]
    index: the index of xyz you want to exist at the end. (0,1,or 2)
    """
    l = len(data) 
    xbins = np.linspace(0,63.92,100)
    y = np.asarray(data)
    #
    bin_indices = np.digitize(xyz[index], xbins)
    sums = np.zeros(100)

    for i in range(1, 100 + 1):
        if var == 'pow':       # this line is important !!!! i find average flux but i do not find average power !!!!!!! please note
            sums[i - 1] = np.sum(y[(bin_indices == i)])
        else:
            sums[i - 1] = np.sum(y[(bin_indices == i)]) / len(y[(bin_indices == i)])
    
    return sums,xbins

def returnAxialData(d: dict,
                    assembly:str,
                    var:str='power') -> tuple:
    """ 
    """
    if var == "power":
        ivar = 'pow'
    elif var == "therm" or var == "fast":
        ivar = var
    
    xyz = [d[assembly]['x']*1e2,d[assembly]['y']*1e2,d[assembly]['z']*1e2]
    y,x = flattenData(d[assembly][ivar],xyz,0,ivar)
    y = removeOutlier(y)
    return y,x


def findInAssemblyPeakingFactor(d:dict,regionname:str) -> float:
    """
    
    pinfo: this should be the list of powers in the fuel region (durrently d[4])
    """
    xyz = [d[regionname]['x']*1e2,d[regionname]['y']*1e2,d[regionname]['z']*1e2]
    y,x = flattenData(d[regionname]['pow'],xyz,0)
    y = removeOutlier(y)
    avg,max = np.mean(y), np.max(y)
    #print(f"       avg={avg:0.3e},max={max:0.3e}")
    return max/avg

def findAllPeakingFactors(d: dict) -> dict:
    """ one command to find all in-assembly and in-core peaking factors
    
    """
    sumPower = 0
    aPowerList = []
    ppfDict = {}
    for a in d:
        aPowerList.append(d[a]['totPower'])
        ppfDict[a] = findInAssemblyPeakingFactor(d,a)
        print(f'Assembly {a} Results: Diffusion')
        print(f'   Power = {np.sum(d[a]["totPower"])*1e-6:0.3f} MW')
        print(f'   Power peaking factor = {ppfDict[a]:0.4f}')
        sumPower += np.sum(d[a]["pow"])*1e-6
        
    print('Full slice results: Diffusion')
    print(f'   Total six assembly power = {sumPower:0.3f} MW')
    print(f'   Max-to-average assembly power peaking factor = {np.max(aPowerList)/27.0707e6:0.4f}') #27.1e6 is 1e9/37
    return ppfDict

def percentPPFchange(deterministicPPF,sssPPF) -> None:
    """ the percent change in power peaking factor between determinisic and serpent2"""
    for a in sssPPF:
        pctChange = ((deterministicPPF[a]-sssPPF[a]) / (sssPPF[a])) * 100
        print(f"Assembly {a} pct change = {pctChange:0.1f}")

def removeOutlier(y):
    """ the data consistentlly has one outlier, this removes it
    """
    maxloc = np.where(y==np.max(y))[0]
    y[maxloc] = (y[maxloc-1]+y[maxloc+1])/2
    return y
    
def plotAxialPower(d:dict,regionname:str,plotAll: bool=False) -> None:
    """ builds a plot of the axial power distribution
    
    d: what the getData() function returns
    """
    
    if not plotAll:
        xyz = [d[regionname]['x'],d[regionname]['y'],d[regionname]['z']]
        sums, xbins = flattenData(d[regionname]['pow'],xyz,0)
        sums = removeOutlier(sums)
        plt.figure(6)
        plt.scatter(xbins,sums)
        #plt.ylim(2e5,3e5)
        plt.title(f'Assembly {regionname} Axial Power Distribution: GeN-Foam Diffusion')
        plt.ylabel('Power (W)')
        plt.xlabel('Height (m)')
        plt.grid(which='major', color='gray', linestyle='-')
        plt.minorticks_on()
        plt.grid(which='minor', color='lightgray', linestyle='--')
        #plt.legend()
        plt.savefig(f'{regionname}.png')
        plt.show()
    else:
        for a in d:
            xyz = [d[a]['x'],d[a]['y'],d[a]['z']]
            sums, xbins = flattenData(d[a]['pow'],xyz,0)
            sums = removeOutlier(sums)
            plt.scatter(xbins,sums,label=a)
        plt.title('Axial Power Distribution for All Assemblies')
        plt.ylabel('Power (W)')
        plt.xlabel('Height (m)')
        plt.grid(which='major', color='gray', linestyle='-')
        plt.minorticks_on()
        plt.grid(which='minor', color='lightgray', linestyle='--')
        plt.legend()
        plt.savefig('allaxial.png')
        plt.show()

     
def findMFR(power:float=27.0707e6,Cp: float=16e3,deltaT: float=3100):
    """ find mass flow rate 
    power  : W    , power in assembly
    Cp     : J/kgK, specific heat
    deltaT : K    , change in coolant temperature inlet->outlet
    """
    return power/(Cp*deltaT)

def idealGas(pressure,density):
    """ pressure in MPA, temperature in K"""
    pressure = pressure * 9.869 # MPa to atm conversion
    return (pressure * 2.01568)/ (0.0821 * density) # g/mol to kg/mol, then using L to m^3

def inletVelocity(power:float=(27.0707e6),temperature: float=200,Ipressure: float=9.1):
    """finds the desired inlet velocity based on serpent power

    Args:
        power       (float): W, power in assembly
        temperature (float): K, inlet temperature 
        Ipressure   (float): MPa, inlet pressure
    """
    iR, oR = 2.382e-2, 2.682e-2 # innerRadius, outerRadius - from Cristian's SNTP ppt
    inletArea = 3.14159 * (oR**2 - iR**2)
    rho = idealGas(Ipressure,temperature) # 8.1MPa is roughly the inlet pressure I am seeing, g/cc -> kg/m^3
    massFlowrate = findMFR(power)         # using 18e3 as known avg Cp with goal of 2800K outletT
    #print(massFlowRate,'aaa')
    inletVelocity = massFlowrate / (rho*inletArea) # 10.82519kg/m3 says genfoam
    return inletVelocity

def updateVelocity(assemblyNumber: int,power: float=27.0707e6) -> None:
    """ updates the inlet velocity of the assembly based on power
    
    """
    mfr = findMFR(power) 
    # exe = ['foamDictionary','./0/fluidRegion/U','-entry',
    #         f'boundaryField.outerPropellantInlet{assemblyNumber}.value','-set',
    #         f"uniform (-{Unew:0.4f} 0 0)"]
    exec2 = ['foamDictionary','./0/fluidRegion/U','-entry',
             f'boundaryField.outerPropellantInlet{assemblyNumber}.massFlowRate','-set',f"constant {mfr:0.6e}"]
    print(f'Assembly {assemblyNumber} mfr = {mfr:0.6f}')
    # try:
    #     result2 = subprocess.run(exec2, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    #     print(f"Inlet MFR successfully updated to {mfr:0.2f} m/s")
    # except Exception as e:
    #     print("!!!!!! The inlet MFR did not get updated !!!!!!")   
    
def updateAllVelocity(powerList: list) -> None:
    """ Loops through all assemblies and updates their input velocities to match
        the pressure
    powerList (list): the output list from runSerpent()
    """
    global thDirectory
    assemblyList = ['a15','a16','a17','a18','a22','a23']
    w = [6,6,6,1,12,6]
    weightedTotalPower = np.sum([w[i]*powerList[i] for i in range(len(powerList))])
    powerList = np.asarray(powerList) * 1e9 / weightedTotalPower # you need to keep mean power constant to keep temperatures down
    for i,assembly in enumerate(assemblyList):
        #print('blah+'+assembly)
        updateVelocity(assembly,powerList[i])

def plotComparison(sssData,fvData,abData, # serpent data, fixedValue data, albedo data
                   assembly: str,         # the assembly of interest
                   param: str,            # power/fast flux/ thermal flux
                   show: bool=False
                   ) -> None:
    """
    """
    # - get diffusion w/ fixed value BC data
    y_fvD,x_fvD = returnAxialData(fvData,assembly,param)
    # - 
    y_abD,x_abD = returnAxialData(abData,assembly,param)
    # - get sss data
    x_sss,y_sss = sssReturnAxialData(sssData,assembly,param)
    
    # - plot
    if param == 'power':
        title = f'Assembly {assembly[1:]} Axial Power:'
        ytitle = 'Power (W)'
    else:
        title = f'Assembly {assembly[1:]} {param} flux:'
        ytitle = r'Flux ($\frac{n}{cm^{2} \cdot s}$)'

    plt.figure()
    plt.plot(x_fvD,y_fvD,label='Diff. w/ Fixed Value BC',c='b')
    plt.plot(x_abD,y_abD,label='Diff. w/ Albedo BC',c='m')
    plt.scatter(x_sss,y_sss,label='Serpent',marker='x',c='r')
    plt.title(title)
    plt.ylabel(ytitle)
    plt.grid(which='major', color='gray', linestyle='-')
    plt.minorticks_on()
    plt.grid(which='minor', color='lightgray', linestyle='--')
    plt.xlabel('Height (cm)')
    plt.legend()
    plt.savefig(f'./images/assembly{assembly[1:]}_{param}.png')
    if show: plt.show()

def axiallyDiscretize(d:dict,a:str):
    """ this is designed to find power under each axial zone (5 rn) 
        to be used in proportionalPowerMethod() from porositySolver.py
    
    d(dict): dictionary returned by getData()
    a(str): assembly name
    """
    y,x = returnAxialData(d,a)
    l = len(y)
    
    pDict = {}
    orfNum = 6
    for i in range(5):
        orfNum -= 1
        #pDict[f'o{orfNum+1}'] = np.sum(y[int(i*l/5):int((i+1)*l/5)])
        
        pDict[f'o{orfNum}'] = np.sum(y[int(i*l/5):int((i+1)*l/5)])

    return pDict

def findAzimuthalPowerPeaking(d:dict,a:str) -> float:
    """ 
    """
    sums, aziBins, xBins = azimuthallyDiscretize(d[a],a)
    aziSums = []
    for aziBin in sums: # loop thru azimuthal bins
        aziSums.append(np.sum(aziBin))
    
    return np.max(aziSums)/np.mean(aziSums)



if __name__ == "__main__":
    # --- reading data
    # - diffusion, fixedValue BC
    fv = getData('.')
    
    pList = []
    for i,assembly in enumerate(fv):
        pList.append(fv[assembly]['totPower'])
    #updateAllVelocity(pList)

    # y,x = {},{}
    # for a in fv:
    #     y[a],x[a] = returnAxialData(fv,a)
    #     loc = np.where(y[a] == np.max(y[a][:-3]))
    #     print(x[a][loc])
    #     plt.scatter(x[a],y[a],label=a)
    #     plt.axvline(x=x[a][loc],c='r',linestyle='dashed')
    #     plt.legend()
    #     plt.show()

    pdrop = np.genfromtxt("outerpropellantPressureDrop.csv",delimiter=',',skip_header=1)[:,:2] #usecols=1,dtype=float)
    m = np.min(pdrop[:,0])

    # plt.figure(99)
    # plt.plot(pdrop[:,1],pdrop[:,0]/m)
    # # plt.ylim(7e6,7.2e6)
    # # plt.xlim(0,0.6392)
    # plt.show()
    
    # pList = []
    # aziPP = {}
    # for i,assembly in enumerate(fv):
    #     pList.append(fv[assembly]['totPower'])
    #     aziPP[assembly] = findAzimuthalPowerPeaking(fv,assembly)
    #     print(f'Assembly {assembly} azi power peaking = {aziPP[assembly]}')

    # #updateAllVelocity(pList)

    for a in fv:
        print(f'{a} ---')
        print(f'  maxT    = {np.max(fv[a]["T"])}')
        print(f'  maxTmax = {np.max(fv[a]["Tmax"])}')

    axialPowerDict = axiallyDiscretize(fv,'a15') # finding total power in each axial zone

    updateCaseDict = proportionalPowerMethod(axialPowerDict)

    home = os.getcwd()
    if False:
        for orfNum in updateCaseDict: # loop through each orifice
                os.chdir(home)
                updatePhaseProperties(orfNum,
                                    '.',
                                    updateCaseDict[orfNum][0],
                                    updateCaseDict[orfNum][1])




