/*--------------------------------*- C++ -*----------------------------------*\
|       ______          _   __           ______                               |
|      / ____/  ___    / | / /          / ____/  ____   ____ _   ____ ___     |
|     / / __   / _ \  /  |/ /  ______  / /_     / __ \ / __ `/  / __ `__ \    |
|    / /_/ /  /  __/ / /|  /  /_____/ / __/    / /_/ // /_/ /  / / / / / /    |
|    \____/   \___/ /_/ |_/          /_/       \____/ \__,_/  /_/ /_/ /_/     |
|    Copyright (C) 2015 - 2022 EPFL                                           |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      thermoMechanicalProperties;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

model               linearElasticThermoMechanics;	//only model available now

// constants needed to calculate deformation of fuel, control rods AND structure
// (1D expansion for fuel and CR, displacement-based solver for structures)

planeStress     	no;

linkedFuel      	false; 		// true if there is contact between fuel and
								// cladding (expansion driven by cladding)
solveDisplacement false;

fuelOrientation   	(0 0 1);

TStructRef			200; 		//ref temperature for structures

zones
(
    moderVol
    {
        //for structures
        rho             74.56442; //density
        E               1e9;  // Young modulus
        nu              0.3; // Poisson ratio
        C               81.8; // cp
        k               200; // conductivity
        alpha           1.8e-5; // linear expansion coeff (1/K)
    }

);

// ************************************************************************* //
