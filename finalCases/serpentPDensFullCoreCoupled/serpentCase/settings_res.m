
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   8]) = 'settings' ;
WORKING_DIRECTORY         (idx, [1:  65]) = '/home/grads/z/zhughes/usncAssembly/montecarlo/axialReflector/bare' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Fri May 31 20:52:38 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Sat Jun  1 03:58:30 2024' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 1000 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1717206758065 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 15 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:  15]) = [  1.22123E+00  9.65427E-01  1.00764E+00  9.85246E-01  9.99867E-01  9.95099E-01  9.96180E-01  9.65015E-01  9.70006E-01  9.96940E-01  9.70700E-01  9.87591E-01  9.70580E-01  9.87449E-01  9.81023E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 6.6E-10  5.00000E-02 1.1E-09 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  2.83606E-01 0.00019  4.06941E-01 6.8E-05 ];
DT_FRAC                   (idx, [1:   4]) = [  7.16394E-01 7.5E-05  5.93059E-01 4.7E-05 ];
DT_EFF                    (idx, [1:   4]) = [  2.79012E-01 9.5E-05  2.59781E-01 3.0E-05 ];
IFC_COL_EFF               (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  7.64610E-01 0.00011  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_SAMPLING_EFF          (idx, [1:   2]) = [  2.60659E-01 0.00024 ];
TOT_COL_EFF               (idx, [1:   4]) = [  2.94841E-01 0.00024  3.23382E-01 3.2E-05 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  7.84202E+00 0.00025  4.90267E+00 5.1E-05  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.37593E-01 7.1E-06  5.77994E-02 0.00011  4.60753E-03 0.00012  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TMS_FAIL_STAT             (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.10336E+01 0.00015  2.03139E+01 0.00011 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.07314E+01 0.00015  1.93206E+01 0.00011 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.08363E+01 0.00034  4.04247E+01 0.00011 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  2.48868E+01 0.00018  3.29391E+01 0.00016 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 1000 ;
SIMULATED_HISTORIES       (idx, 1)        = 100001557 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00002E+05 0.00018 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00002E+05 0.00018 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  6.33911E+03 ;
RUNNING_TIME              (idx, 1)        =  4.25878E+02 ;
INIT_TIME                 (idx, [1:   2]) = [  4.57733E-01  4.57733E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  8.61667E-03  8.61667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  4.25411E+02  4.25411E+02  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  4.25862E+02  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 14.88480 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  1.48959E+01 0.00015 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.91922E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.11 ;
ALLOC_MEMSIZE             (idx, 1)        = 6697.73 ;
MEMSIZE                   (idx, 1)        = 6558.57 ;
XS_MEMSIZE                (idx, 1)        = 4769.26 ;
MAT_MEMSIZE               (idx, 1)        = 635.38 ;
RES_MEMSIZE               (idx, 1)        = 301.49 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 852.44 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 139.16 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 57 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 819727 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 8081 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 23 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 85 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 67 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 18 ;
TOT_REA_CHANNELS          (idx, 1)        = 1692 ;
TOT_TRANSMU_REA           (idx, 1)        = 103 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 2 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  5.30428E+14 0.00012  5.30428E+14 0.00012 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 94247357 9.65754E+07 4.48750E+06 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 9368048 9.60239E+06 4.90681E+06 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 313164843 3.20943E+08 1.11570E+07 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 359437053 3.68734E+08 4.05927E+08 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 776217301 7.95854E+08 4.26479E+08 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 676881338 6.93810E+08 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 99335961 1.02044E+08 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 2 2.01076E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 776217301 7.95854E+08 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 8.94070E-06 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  5.41268E+19 0.00013 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  1.06377E+12 0.70676 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  3.65466E+20 9.6E-05 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  2.54667E+18 0.00046 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  4.22140E+20 9.1E-05 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  1.05148E+21 9.9E-05 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  5.15594E+21 0.00010 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  2.60926E+07 0.00010 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  7.21045E-10 5.2E-05 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.35415E-03 0.00215 ];
U235_FISS                 (idx, [1:   4]) = [  2.27815E+19 0.00011  9.99880E-01 1.6E-06 ];
U238_FISS                 (idx, [1:   4]) = [  2.72386E+15 0.01373  1.19546E-04 0.01373 ];
U235_CAPT                 (idx, [1:   4]) = [  4.49004E+18 0.00034  2.93527E-01 0.00029 ];
U238_CAPT                 (idx, [1:   4]) = [  1.18256E+17 0.00216  7.73075E-03 0.00215 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100001557 1.00000E+08 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.44216E+06 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100001557 1.03442E+08 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 27918873 2.88389E+07 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 41869936 4.29548E+07 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 30212748 3.16485E+07 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100001557 1.03442E+08 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 3.41237E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  7.00000E+08 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  7.99087E+01 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  5.55470E+19 9.2E-06 ];
TOT_FISSRATE              (idx, [1:   2]) = [  2.27808E+19 9.3E-06 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  1.52948E+19 0.00013 ];
TOT_ABSRATE               (idx, [1:   2]) = [  3.80755E+19 5.1E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  5.30428E+19 0.00012 ];
TOT_FLUX                  (idx, [1:   2]) = [  3.71988E+21 0.00012 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  1.95589E+20 1.4E-05  1.95614E+20 8.5E-05 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.67874E+19 0.00024 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  5.48630E+19 9.1E-05 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  2.13172E+21 0.00012 ];
INI_FMASS                 (idx, 1)        =  8.76000E+00 ;
TOT_FMASS                 (idx, 1)        =  8.76000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06672E+00 6.3E-05 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.47585E-01 7.6E-05 ];
SIX_FF_P                  (idx, [1:   2]) = [  9.03610E-01 5.7E-05 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.09758E+00 5.0E-05 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.36404E-01 6.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.28179E-01 3.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.53235E+00 0.00010 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04738E+00 0.00012 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43833E+00 2.1E-07 ];
FISSE                     (idx, [1:   2]) = [  1.91787E+02 9.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04734E+00 0.00012  1.03956E+00 0.00012  7.82194E-03 0.00186 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.04733E+00 9.2E-05 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04723E+00 0.00012 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.04733E+00 9.2E-05 ];
ABS_KINF                  (idx, [1:   2]) = [  1.53234E+00 5.5E-05 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.93289E+01 2.1E-05 ];
IMP_ALF                   (idx, [1:   2]) = [  1.93284E+01 9.5E-06 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.06575E-08 0.00041 ];
IMP_EALF                  (idx, [1:   2]) = [  8.06886E-08 0.00018 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.56246E-02 0.00211 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.55733E-02 0.00027 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.52359E-03 0.00135  2.26229E-04 0.00669  1.17723E-03 0.00295  1.12448E-03 0.00318  2.52780E-03 0.00211  1.03559E-03 0.00322  4.32255E-04 0.00501 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.68459E-01 0.00187  1.33360E-02 1.6E-06  3.27387E-02 1.7E-06  1.20781E-01 1.1E-06  3.02787E-01 2.6E-06  8.49516E-01 4.2E-06  2.85307E+00 5.8E-06 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.46993E-03 0.00185  2.61505E-04 0.00945  1.35297E-03 0.00426  1.29782E-03 0.00443  2.87654E-03 0.00299  1.18785E-03 0.00457  4.93248E-04 0.00734 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  4.67519E-01 0.00275  1.33361E-02 4.6E-06  3.27388E-02 2.8E-06  1.20781E-01 1.4E-06  3.02787E-01 3.6E-06  8.49520E-01 7.3E-06  2.85313E+00 1.5E-05 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  8.07883E-05 0.00027  8.07943E-05 0.00027  8.00021E-05 0.00275 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  8.46119E-05 0.00024  8.46182E-05 0.00024  8.37883E-05 0.00275 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.45970E-03 0.00193  2.61197E-04 0.00968  1.35146E-03 0.00424  1.30000E-03 0.00439  2.87287E-03 0.00294  1.18293E-03 0.00458  4.91233E-04 0.00726 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.66673E-01 0.00268  1.33361E-02 3.4E-06  3.27388E-02 2.0E-06  1.20781E-01 1.5E-06  3.02787E-01 4.0E-06  8.49524E-01 7.5E-06  2.85312E+00 1.3E-05 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  7.99340E-05 0.00233  7.99351E-05 0.00233  7.96622E-05 0.00742 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  8.37159E-05 0.00232  8.37171E-05 0.00232  8.34309E-05 0.00742 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.49584E-03 0.00643  2.60566E-04 0.03364  1.36369E-03 0.01423  1.27754E-03 0.01422  2.91003E-03 0.01023  1.19269E-03 0.01508  4.91326E-04 0.02337 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  4.66402E-01 0.00881  1.33360E-02 1.5E-09  3.27388E-02 6.9E-06  1.20780E-01 1.8E-06  3.02789E-01 1.7E-05  8.49511E-01 1.4E-05  2.85300E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.50746E-03 0.00632  2.62416E-04 0.03222  1.36066E-03 0.01362  1.28914E-03 0.01366  2.90380E-03 0.00999  1.19881E-03 0.01477  4.92637E-04 0.02273 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  4.66883E-01 0.00857  1.33360E-02 1.5E-09  3.27388E-02 7.1E-06  1.20780E-01 2.2E-06  3.02790E-01 1.7E-05  8.49510E-01 1.3E-05  2.85300E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -9.38026E+01 0.00605 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  8.06044E-05 0.00017 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  8.44193E-05 0.00011 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.51756E-03 0.00111 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -9.32666E+01 0.00111 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.03584E-06 0.00013 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  4.23416E-06 0.00011  4.23511E-06 0.00011  4.10700E-06 0.00128 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.03589E-04 0.00016  1.03600E-04 0.00016  1.02107E-04 0.00173 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.70629E-01 7.6E-05  6.70041E-01 7.9E-05  7.61579E-01 0.00202 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.10296E+01 0.00294 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  8.22831E+00 5.9E-05  4.46839E+01 9.6E-05 ];

