/*--------------------------------*- C++ -*----------------------------------*\
|       ______          _   __           ______                               |
|      / ____/  ___    / | / /          / ____/  ____   ____ _   ____ ___     |
|     / / __   / _ \  /  |/ /  ______  / /_     / __ \ / __ `/  / __ `__ \    |
|    / /_/ /  /  __/ / /|  /  /_____/ / __/    / /_/ // /_/ /  / / / / / /    |
|    \____/   \___/ /_/ |_/          /_/       \____/ \__,_/  /_/ /_/ /_/     |
|    Copyright (C) 2015 - 2022 EPFL                                           |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       volVectorField;
    object      U;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

dimensions      [0 1 -1 0 0 0 0];

internalField   uniform (0 0 0);

boundaryField
{
    "outerPropellantInlet" // inlet
    {
        type                flowRateInletVelocity;
        massFlowRate        constant ${{0.568/2}};
        rho                 "thermo:rho";
        value               uniform (1 0 0);
    }
    "innerPropellantInlet|hotFritInlet|fuelInlet|coldFritInlet" // inlet side closed
    {
        type            fixedValue;
        value           uniform (0 0 0);
    }
    "hotFritOutlet|fuelOutlet|coldFritOutlet|outerPropellantOutlet|outerPropellantOR" // outlet side closed + outer radius
    {
        type            fixedValue;
        value           uniform (0 0 0);
    }
    "innerPropellantOutlet*" // outlet
    {
        type            zeroGradient;
    }
    ".*Right.*|.*Left.*"
    {
        type            symmetry;
    }
}

// ************************************************************************* //
