"""
    This is a script for generating a materials file from an OpenFOAM polyMesh folder. 
    The materials file relates each cell to a specific material. This is necessary for the
    Serpent2 coupling because Serpent needs to know whih material to put in each cell
    when reading the unstructured mesh.

    Author: Zach Hughes - zhughes@tamu.edu

    To prepare for this script:
        1. create the polyMesh folder with all cellZones representing different materials
        2. type: splitMeshRegions -overwrite -cellZones
        3. This code will need the 'cellToRegion' file as well as the order of cellZones, 
           which can be found at the top of the regions' polyMesh folder in the cellZones 
           file

Link to definition of materials file:
    https://serpent.vtt.fi/mediawiki/index.php/Unstructured_mesh_based_input#Material_file

"""
import numpy as np

# --- user entered information

cellZoneOrder = ['o1','o2','fuel','outerPropellant','o3','hotFrit','innerPropellant','o4','o5']

# ---


celltoregion = np.genfromtxt('cellToRegion',skip_header=21,skip_footer=25223-25221)
#print(celltoregion,len(celltoregion))

# --- write the materials file for the serpent interface
# for m in range(37): # looping through each assembly
#     with open(f'meshes/m{m}/materials','w') as file:
#         for i,region in enumerate(celltoregion):
#             if i == 0: file.write(f"{len(celltoregion)} "+'\n')

#             file.write(f"{cellZoneOrder[int(region)]}mat{m}"+'\n')
# ---

# --- write the mat cards to define different material for each assembly
fA = ['mix', 'fuelmat']
fB = """
rgb 252 90 80
m9z 0.41999993
m7z 0.28777637
m8z 0.29222371
\n"""

oA = ['mat', 'o1mat', '-0.925']
oB = """
rgb 23 190 210
m10     0.6
cfHydro 0.4
\n"""

opA = ['mat', 'outerPropellantmat', '-0.0004922' ]
opB = """
rgb 214 39 40
H-1.00c 0.00029402
H-2.00c 4.41097e-08
\n"""

hA = ['mat', 'hotFritmat', '-3.365' ]
hB = """
rgb 255 127 17
m5      0.3
hfHydro 0.7
\n"""

ipA = ['mat', 'innerPropellantmat', '-0.0004922' ]
ipB = """
rgb 31 119 180
H-1.00c 0.00029402
H-2.00c 4.41097e-08
\n"""

# for m in range(37):
#     with open(f'coupledMaterials','a') as file:
#         for i in range(5):
#             if i == 0: # fuel mat
#                 file.write(f"mix fuelmat{m}")
#                 file.write(fB)
#             if i == 1: # innerPropellantMat
#                 file.write(f"mat innerPropellantmat{m} -0.0004922")
#                 file.write(ipB)
#             if i == 2: # outerPropellantMat
#                 file.write(f"mat outerPropellantmat{m} -0.0004922")
#                 file.write(opB)
#             if i == 3: # hot frit
#                 file.write(f"mix hotFritmat{m}")
#                 file.write(hB)
#             if i == 4: # cold frit
#                 for j in range(1,6):
#                     file.write(f"mix o{j}mat{m}")
#                     file.write(oB)

        #if i == 0: file.write(f"{len(celltoregion)} "+'\n')

        #file.write(f"{cellZoneOrder[int(region)]}mat{m}"+'\n')
# ---

# --- now generating ifc inputs for all assemblies
# 9 u18 b18
# 1 ./meshes/m18/powerDensityNeutronics
# -3.7 600
# 5 3 2 2 2
# ./meshes/m18/points
# ./meshes/m18/faces
# ./meshes/m18/owner
# ./meshes/m18/neighbour
# ./meshes/m18/materials
# ./meshes/m18/density 1
# ./meshes/m18/T 1    
# -1
for m in range(37):
    with open(f"meshes/m{m}/{m}.ifc",'w') as file:
        file.write(f"9 u{m} b{m} \n")
        file.write(f"1 ./meshes/m{m}/powerDensityNeutronics\n")
        file.write('-3.7 600\n')
        file.write('5 3 2 2 2\n')
        file.write(f'./meshes/m{m}/points\n')
        file.write(f'./meshes/m{m}/faces\n')
        file.write(f'./meshes/m{m}/owner\n')
        file.write(f'./meshes/m{m}/neighbour\n')
        file.write(f'./meshes/m{m}/materials\n')
        file.write(f'./meshes/m{m}/density 1\n')
        file.write(f'./meshes/m{m}/T 1\n')
        file.write(f'-1')
# --- 
