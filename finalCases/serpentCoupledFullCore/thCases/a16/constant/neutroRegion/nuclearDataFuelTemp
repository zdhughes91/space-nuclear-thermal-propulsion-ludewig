/*--------------------------------*- C++ -*----------------------------------*\
|       ______          _   __           ______                               |
|      / ____/  ___    / | / /          / ____/  ____   ____ _   ____ ___     |
|     / / __   / _ \  /  |/ /  ______  / /_     / __ \ / __ `/  / __ `__ \    |
|    / /_/ /  /  __/ / /|  /  /_____/ / __/    / /_/ // /_/ /  / / / / / /    |
|    \____/   \___/ /_/ |_/          /_/       \____/ \__,_/  /_/ /_/ /_/     |
|    Copyright (C) 2015 - 2022 EPFL                                           |
\*---------------------------------------------------------------------------*/
FoamFile
{
	version     2.0;
	format      ascii;
	class       dictionary;
	location    "constant/neutroRegion";
	object      nuclearDataFuelTemp;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

energyGroups    2; // [0.000000e+00, 6.250000e-01, 2.000000e+07] eV

precGroups      6;

TfuelRef		2222; // K

TfuelPerturbed 	2444.2; // K

//- Diffusion data
zones
(
	// fuel  // OpenMC: cCore
	// {
	// 	fuelFraction       6.721789e-01;
	// 	IV                 nonuniform List<scalar> 2 (3.762460e-06 1.558214e-04);
	// 	D                  nonuniform List<scalar> 2 (1.370438e-02 8.077060e-03);
	// 	nuSigmaEff         nonuniform List<scalar> 2 (6.688228e-01 1.513674e+01);
	// 	sigmaPow           nonuniform List<scalar> 2 (8.472365e-12 1.924905e-10);
	// 	scatteringMatrixP0 2 2 (
	// 		(2.764347e+01 1.508265e-01)
	// 		(1.584362e+00 3.765516e+01)
	// 	);
	// 	scatteringMatrixP1 2 2 (
	// 		(3.973684e+00 1.157917e-02)
	// 		(-2.944177e-02 4.983370e+00)
	// 	);
	// 	scatteringMatrixP2 2 2 (
	// 		(1.249216e+00 -7.527065e-03)
	// 		(-3.423846e-02 1.338787e+00)
	// 	);
	// 	scatteringMatrixP3 2 2 (
	// 		(1.240760e-01 -8.133838e-03)
	// 		(-2.386908e-02 4.433813e-01)
	// 	);
	// 	scatteringMatrixP4 2 2 (
	// 		(-8.086484e-02 -3.458567e-03)
	// 		(-2.123116e-02 1.166135e-01)
	// 	);
	// 	scatteringMatrixP5 2 2 (
	// 		(5.399665e-03 -7.781042e-04)
	// 		(-1.745023e-02 5.445468e-02)
	// 	);
	// 	sigmaDisapp        nonuniform List<scalar> 2 (6.520404e-01 9.174256e+00);
	// 	chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
	// 	chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
	// 	Beta               nonuniform List<scalar> 6 (2.280958e-04 1.177757e-03 1.124615e-03 2.522367e-03 1.035067e-03 4.335542e-04);
	// 	lambda             nonuniform List<scalar> 6 (1.333617e-02 3.273769e-02 1.207829e-01 3.028110e-01 8.496245e-01 2.853452e+00);
	// 	discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
	// 	integralFlux       nonuniform List<scalar> 2 (1.888963e-01 6.729844e-01);
	// }

	// ".*"  // OpenMC: uCentralUnloadedFuelElement
	// {
	// 	fuelFraction       0.000000e+00;
	// 	IV                 nonuniform List<scalar> 2 (4.041540e-06 1.651318e-04);
	// 	D                  nonuniform List<scalar> 2 (1.046541e-02 6.411494e-03);
	// 	nuSigmaEff         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
	// 	sigmaPow           nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
	// 	scatteringMatrixP0 2 2 (
	// 		(4.707714e+01 6.052266e-01)
	// 		(1.508341e+00 7.481583e+01)
	// 	);
	// 	scatteringMatrixP1 2 2 (
	// 		(1.602026e+01 1.628976e-01)
	// 		(4.562988e-01 1.901585e+01)
	// 	);
	// 	scatteringMatrixP2 2 2 (
	// 		(6.303390e+00 -3.680804e-02)
	// 		(1.423630e-01 6.229642e+00)
	// 	);
	// 	scatteringMatrixP3 2 2 (
	// 		(3.528496e-01 -5.093122e-02)
	// 		(2.444002e-03 2.133765e+00)
	// 	);
	// 	scatteringMatrixP4 2 2 (
	// 		(-6.894942e-01 -2.151375e-02)
	// 		(-4.980062e-02 4.971499e-01)
	// 	);
	// 	scatteringMatrixP5 2 2 (
	// 		(-2.225821e-02 -4.518436e-03)
	// 		(-5.283403e-02 2.168930e-01)
	// 	);
	// 	sigmaDisapp        nonuniform List<scalar> 2 (8.051124e-01 2.500499e+00);
	// 	chiPrompt          nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
	// 	chiDelayed         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
	// 	Beta               nonuniform List<scalar> 6 (0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00);
	// 	lambda             nonuniform List<scalar> 6 (1.333617e-02 3.273769e-02 1.207829e-01 3.028110e-01 8.496245e-01 2.853452e+00);
	// 	discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
	// 	integralFlux       nonuniform List<scalar> 2 (3.441485e-02 9.431295e-02);
	// }
	// //*/
);

// ************************************************************************* //
