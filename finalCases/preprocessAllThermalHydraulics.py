"""
    Author: Zach Hughes - zhughes@tamu.edu

citations:
    1. April Novak's Thesis
    2. SPACE NUCLEAR THERMAL PROPULSION PROGRAM FINAL REPORT
        - R.A. Haslett 
    3. DESIGN OF PARTICLE BED REACTORS FOR THE SPACE NUCLEAR THERMAL PROPULSION PROGRAM 
        - H. Ludewig
    4. Performance of (U, Zr)C-Graphite (Composite) and of (U,Zr)C (Carbide) Fuel Elements in
        the Nuclear Furnace 1 Test Reactor
        - L. Lyon
    5. Synthesis, thermal conductivity, and hydrogen compatibility of a high melt
        point solid solution uranium carbide
        - E. Kardoulaki 
    6. Research, Development, and Production of Substoichiometric Zirconium Carbide for
        High-Temperature Insulation
        - P. Wagner
    7. Analytical expressions for thermophysical properties of solid and liquid
        beryllium relevant for fusion applications
        - P. Tolias
    8. Development of materials and fabrication of porous and
        pebble bed beryllium multipliers
        - D.A. Davydov
    9. Investigation of local Heat Transfer Phenomena in a Pebble Bed HTGR Core
        - NRC 2009
    10. Convection in Porous Media
        - Donald A. Nield
    11. AN INTERIM STUDY OF SINGLE PHASE HEAT TRANSFER CORRELATIONS
        USING HYDROGEN 
        - G. R. Thomas
    12. Heat and Flow Characteristics of Packed Beds 
        - Achenbach

"""
from scipy import integrate

class flowDomain:
    def __init__(self,innerInletRadius: float,
                      innerOutletRadius: float,
                      outerInletRadius: float,
                      outerOutletRadius: float
                ):
        
        self.innerRadii = [innerInletRadius*1e-2,innerOutletRadius*1e-2] # 
        self.outerRadii = [outerInletRadius*1e-2,outerOutletRadius*1e-2] # 
        self.coreHeight = 0.6392 # m

    def getCircularArea(self,innerRadius: float,outerRadius: float) -> float:
        return (3.14159*(outerRadius**2 - innerRadius**2))

    def getCircularPerimeter(self,innerRadius: float,outerRadius: float) -> float:
        return (2*3.14159*(innerRadius+outerRadius))

    def outerRadiusFunction(self,x: float) -> float:
        """ function for the outer radius as a function of height: to be integrated"
            x is the height"""
        m = (self.outerRadii[1]-self.outerRadii[0])/(self.coreHeight)
        return (m*x)+self.outerRadii[0]
    
    def innerRadiusFunction(self,x: float) -> float:
        """ function for the inner radius as a function of height: to be integrated"
            x is the height"""
        m = (self.innerRadii[1]-self.innerRadii[0])/(self.coreHeight)
        return (m*x)+self.innerRadii[0] 
    
    def xsAreaFunction(self,x:float) -> float:
        """ a function to make one eqn for the XS area of the annulus as a function of h"""
        return 3.14159*(self.outerRadiusFunction(x)-self.innerRadiusFunction(x))**2
    
    def wAreaFunction(self,x:float) -> float:
        """ a function to make one eqn for the wetted area of the annulus as a function of h"""
        return 2*3.14159*(self.outerRadiusFunction(x)+self.innerRadiusFunction(x))

    def assignVolume(self) -> None:
        """ integrates areaFunction over the core height to get volume """
        self.volume = integrate.quad(self.xsAreaFunction,0,self.coreHeight)
    
    def assignWettedArea(self) -> None:
        """ integrates wAreaFunction over core h """
        self.wArea = integrate.quad(self.wAreaFunction,0,self.coreHeight)
    
    def assignConicalDh(self) -> None:
        """ assigns Dh using April Novak Thesis eqn 2.90"""
        self.Dh = 4 * self.volume[0] / (self.wArea[0])

    def assignPorousDh(self) -> None:
        """ assigns Dh using eqn 2.92 in April Novak's thesis
            epsilon = 0.4 usuallly [1]"""
        self.Dh = self.epsilon*self.pebbleDiameter/(1-self.epsilon)

class turbulentBoundaryConditions:
    def __init__(self,Dh: float,U: float,T: float):
        self.Dh = Dh     # hydraulic diameter
        self.U = U       # inlet velocity
        self.T = T       # inlet temperature
    def assignRho(self):
        """ assigns hydrogen desnity at temperature T """
        T = self.T
        c = [2.6312690E+01,-1.5654483E-01,3.4832321E-04,-3.7432963E-07,
             2.1564136E-10,-6.8137583E-14,1.1124061E-17,-7.3322603E-22]
        rho=c[0]+c[1]*T+c[2]*T**2+c[3]**3+c[4]*T**4+c[5]*T**5+c[6]*T**6+c[7]*T**7
        self.rho = rho
    def assignMu(self):
        """ assigns hydrogen viscosity at temperature T"""
        T = self.T
        c = [2.6957516E-06,2.1373381E-08,-1.2715441E-12,-5.1005087E-15,
	        4.4678334E-18,-1.6867110E-21,3.0456404E-25,-2.1428674E-29]
        mu=c[0]+c[1]*T+c[2]*T**2+c[3]**3+c[4]*T**4+c[5]*T**5+c[6]*T**6+c[7]*T**7
        self.mu = mu
    def assignRe(self):
        """ assigns reynolds number """
        self.Re = self.rho*self.U*self.Dh/self.mu
    def assignCharacteristicLength(self):
        """"""
        self.l = 0.07 * self.Dh
    def assignIntensity(self):
        """cfd-online turbulence intensity eqn"""
        self.I = (0.16*self.Re**(-1/8))
    def assignKineticEnergy(self):
        """ turbulent kinetic energy """
        self.k = (3/2) * (self.I*self.U)**2
    def assignDissapationEnergy(self):
        """ turbulent dissapation energy """
        self.epsilon = (0.09**(0.75)) * (self.k**1.5) / self.l
    def assignAll(self):
        self.assignRho()
        self.assignMu()
        self.assignRe()
        self.assignCharacteristicLength()
        self.assignIntensity()
        self.assignKineticEnergy()
        self.assignDissapationEnergy()
    def assignMassFlowrate(self,area: float):
        """ to calculate mass flowrate """
        self.area = area
        self.mfr = area * self.rho * self.U

def getHeatConductance(specificPower,t1,t2):
    return specificPower/(t2-t1)

def getFrictionFactorTerms(magV: float,epsilon: float,dp: float) -> tuple:
    """ Eqn. 2.95 in [1]]. 
        - this function takes porosity, velocity magnitude
          and particle diameter and returns the A and B coefs
          for the reynoldsPower drag model 
          A is coefficient, B is constant (added term)"""
    A_ = 150 # Eqn 2.96a in [1], for Re < 1e4
    B_ = 1.75 # Eqn 2.96b in [1], for Re < 1e4
    A = A_ * (1-epsilon)**2 * magV / dp
    B = B_ * (1-epsilon) * magV / dp
    return A,B

def getNusseltCorrelationNield(porosity):
    """ Eqn. 2.16 in [10]
    """
    return 0.255/porosity

def getNusseltCorrelationAchenbach(o):
    """ Eqn. 34 in [12]
    o is a flowDomiain object
    """
    d = o.pebbleDiameter
    h = o.coreHeight/2
    area = o.xsAreaFunction(h)
    D = (area/3.14159)**(1/2)
    #D = 2*(o.outerRadiusFunction(h) - o.innerRadiusFunction(h))
    return (1-(d/D))

def findMFR(power:float=27.0707e6,Cp: float=16e3,deltaT: float=3100):
    """ find mass flow rate 
    power  : W    , power in assembly
    Cp     : J/kgK, specific heat
    deltaT : K    , change in coolant temperature inlet->outlet
    """
    return power/(Cp*deltaT)

def idealGas(pressure,density):
    """ pressure in MPA, temperature in K"""
    pressure = pressure * 9.869 # MPa to atm conversion
    return (pressure * 2.01568)/ (0.0821 * density) # g/mol to kg/mol, then using L to m^3

def inletVelocity(power:float=(27.0707e6),temperature: float=200):
    """finds the desired inlet velocity based on serpent power

    Args:
        power       (float): W, power in assembly
        temperature (float): K, inlet temperature 
    """
    iR, oR = 2.382e-2, 2.682e-2 # innerRadius, outerRadius - from Cristian's SNTP ppt
    inletArea = 3.14159 * (oR**2 - iR**2)
    rho = idealGas(9.25,temperature) # 8.1MPa is roughly the inlet pressure I am seeing, g/cc -> kg/m^3
    massFlowrate = findMFR(power)         # using 18e3 as known avg Cp with goal of 2800K outletT
    #print(massFlowRate,'aaa')
    inletVelocity = massFlowrate / (rho*inletArea) # 10.82519kg/m3 says genfoam
    return inletVelocity


if __name__ == "__main__":

    """
    ASSUMPTION IN HOT + COLD FRIT:
        For the hot and cold frits, I am not sure how to calc Dh for the porous region
        without eqn2.92 in [1]. So, I have assumed 1mm as the particleDiameter (
        even though we don't have particles) in order to find Dh of the frits. 
    """

    pp = { # i convert these values from cm -> m during construction
        'innerPropellant':flowDomain(0,0,0.965,2.129),# r6 to r1
        'hotFrit':flowDomain(0.965,2.129,1.265,2.429),# r6 to r1 and r7 to r2
        'fuelBed':flowDomain(1.265,2.429,2.182,3.044),# r7 to r2 and r8 to r3
        'coldFrit':flowDomain(2.182,3.044,2.382,3.244),# r8 to r3 and r9 to r4
        'outerPropellant':flowDomain(2.382,3.244,2.682,3.544)# r9 to r4 and r10 to r5
        }

    # need to assing pebble diameters to porous regions for Dh calculation
    pp['hotFrit'].pebbleDiameter = 1e-3 # 1mm hot frit pebble size, see above assumption
    pp['coldFrit'].pebbleDiameter = 1e-3 # 1mm cold frit pebble size, see above assumption
    pp['fuelBed'].pebbleDiameter = (475 + (60*2))*1e-6 # fuel diam + 2*ZrC thickness
    pp['hotFrit'].epsilon = 0.7 # epsilon of porousZrC = 0.7 [6],tableII
    pp['coldFrit'].epsilon = 0.4 # epsilon of porousBe = 0.2 [8],fig2
    pp['fuelBed'].epsilon = 1.0-0.58

    totalVolume = 0 # total volume of the fuelAssembly/porous region
    for zone in pp: # solving volume and wetted area of each zone
        pp[zone].assignVolume()
        totalVolume += pp[zone].volume[0]
        pp[zone].assignWettedArea()
        if zone == 'innerPropellant' or zone == 'outerPropellant': # not porous zones
            pp[zone].assignConicalDh()
        else:
            pp[zone].assignPorousDh() # porous Dh calculated differently[1]

        print(f"-- {zone} " + '-'*(25-len(zone)))
        print(f"Volume = {pp[zone].volume[0]:0.3e} m^3")
        print(f"wArea  = {pp[zone].wArea[0]:0.3e} m^2")
        print(f"Dh  = {pp[zone].Dh:0.3e} m") # i currently believe the Dh values are correct, but should be checked
        # 
    print('tttttttt',totalVolume)
    # now solving particle heat conductances --- 

    # begin with define values
    tp = {} # thermophysical properties
    tp['UZrC'] = {
                'rho':3.61e3,           # kg/m3 -- [4],table1
                'k':30,                 # W/(mK) @ 2000K --  [4],fig12  :: note [5],fig12 disagrees with this value
                'Cp':550                # J/(kgK) @ 2750K -- [5],fig9
                }
    tp['ZrC'] = {
                'rho':6.73e3,           # kg/m3 - this is theoretical density...
                'k':43,                 # W/(mK) @ 2000K -- [5],fig12
                'Cp':650                # J/(kgK) @ 2750K -- [5],fig9
                }
    tp['porousZrC'] = { #[6],page11 for nuclear furnace 2. 30pct theoretical = 70pct porosity
                    'rho':6.73e3*0.3,   # kg/m3 - [6],figA-1 :: depends on C/Zr ratio
                    'k':5.2,            # W/(mK) @ 3000K -- [6],fig14
                    'Cp':365            # J/(kgK) - [6],figA-2 :: depends on C/Zr ratio
                    }
    tp['porousBe'] = {
                'rho':1.75e3,           # kg/m3 @ 1500K - [7],fig6 :: melts above 1500 K
                'k':75,                 # W/(mK) @ 1500K -- [7],fig5 :: melts above 1500 K
                'Cp':3661.784           # J/(kgK) @ 1500K -- [7],fig2
                }
    
    # print(tp['porousBe'])

    R_kernel = (475e-6)/2 # m, radius fuel kernel
    t_clad = 60e-6 # m, thickness clad
    R_particle = R_kernel + t_clad
    print(f'rrrrrrrrrrrr{R_particle}')
    rho_uzrc = 3.61 # g/cc -- [4],table1   
    k_uzrc = 30 # W/(mK) @ 2000K --  [4],fig12  :: note [5],fig12 disagrees with this value
    cp_uzrc = 550 # J/(kgK) @ 2750K -- [5],fig9

    rho_zrc = 6.73 # g/cc -- wikipedia
    k_zrc = 43 # W/(mK) @ 2000K -- [5],fig12
    cp_zrc = 650 # J/(kgK) @ 2750K -- [5],fig9

    totalPower = 1e9 # W, SNTP ppt
    powerFuelBed = totalPower / 37
    powerDensityFuelBed = powerFuelBed / pp['fuelBed'].volume[0] # this divided by 
    print(f"vvvol {pp['fuelBed'].volume[0]}")
    print(f"{powerFuelBed/totalVolume:0.3e}")
    print('-- Power Density Calculations: --')
    print("PowerDensityFuelRegion = {:0.3e} W/m^3".format(powerDensityFuelBed))
    # the fuel bed in this case refers to the whole volume up to the moderator
    # m^3 to L conversion is x10^3. W to MW conversion is x10^6
    print("PowerDensityFuelBed    = {:0.3f} MW/L".format((powerFuelBed*1e-9)/totalVolume))

    # finding total wetted area in fuel bed
    pebbleVolume = ((4/3)*3.14159*R_particle**3) 
    pebbleSurfaceArea = 4 * 3.14159 * R_particle**2
    N_particles = pp['fuelBed'].volume[0] * 0.58 / pebbleVolume  # number of particles in single fuel bed
    print('blah!',N_particles)
    kernelPowerDensity = powerFuelBed / (N_particles*(((4/3)*3.14159*R_kernel**3)))
    print(f"kernelPowerDensity {kernelPowerDensity:0.4e} W")
    totalPebbleSurfaceArea = N_particles * pebbleSurfaceArea
    totalFuelBedWettedArea = totalPebbleSurfaceArea #+ (pp['fuelBed'].wArea[0])

    # finding volumetric area in two separate ways. The proved to be identical 
    print('-- Volumetric Area Calculation: --')
    print(f"volumetricArea1 = {totalFuelBedWettedArea/(pp['fuelBed'].volume[0]):0.4f} m^-1")

    vA_gFHR = (3*0.58) / R_particle
    print(f"volumetricArea2 = {vA_gFHR:0.4f} m^-1")
    #print(N_particles)
    N_particles = 1
    
   # print(f"{(308.5-302)/(((4/3)*3.14159*R_kernel**3)):0.3e}")
    #kernelVal = heatConductance(R_kernel,t_clad,pebblePower,k_uzrc)
    try:
        # Open and read the content of heatConductance.py
        with open("heatConductance/heatConductance.py", 'r') as file:
            script_content = file.read()
            # Execute the code from heatConductance.py
            exec(script_content)
    except FileNotFoundError:
        print(f"Error: heatConductance/heatConductance.py not found.")
    except Exception as e:
        print(f"An error occurred: {e}")

    print('-- RhoCp Calculation: --')
    print('node1(fuel) = {:0.3e}'.format(tp['UZrC']['rho']*tp['UZrC']['Cp']))
    print('node2(clad) = {:0.3e}'.format(tp['ZrC']['rho']*tp['ZrC']['Cp']))

    # calculating volume fraction of each structure
    kernelVolumeFraction = (((4/3)*3.14159*R_kernel**3))/pebbleVolume
    casingVolumeFraction = 1 - kernelVolumeFraction
    print('-- Volume Fraction: -- ')
    print(f"kernelFraction = {kernelVolumeFraction:0.5f}")
    print(f"casingFraction = {casingVolumeFraction:0.5f}")
    #print(f"h {2.1/(casingVolumeFraction*pebbleVolume):0.3e}")

    """
    ASSUMPTION IN INLET CONDITIONS:
        No inlet conditions are given in the papers or SNTP ppt. Inlet velocity
        and inlet temperature will have to be tweaked until desired outlet conditions
        are acheived. Initially, an inlet velocity of 50 m/s and temperature of 300K
        will be assumed. Inlet density and viscosity are also a function of this 
        temperature and contribute to Reynolds number. 

        Also, the turbulent bounday conditions could be different at each of the inlets. 
        This is because the hydraulic diameter is different in each of the cellZones. Initially 
        I will assume a single turbulent boundary condition for the entire inlet, and this
        will include assuming the Dh at the inlet is the radius of the entire fuel bed. 
    """
    tbc = turbulentBoundaryConditions(2*.03544,50,300)
    tbc.assignAll()
    print('-- Turbulent Boundary Conditions: --')
    print(f"l       = {tbc.l:0.6f} ")
    print(f"I       = {tbc.I:0.6f} ")
    print(f"k       = {tbc.k:0.6f} ")
    print(f"epsilon = {tbc.epsilon:0.4f} ")


    """
    ASSUMPTION IN TURBULENCE PROPERTIES
        I am not sure if I should specify DStruct in each cellZone so to begin
        I will have all cellZones use the same DStruct, which will be the diameter
        of the entire fuel bed 
    """
    dstruct = (.03544+0.02682)/2
    print('-- turbulenceProperties: DStruct: --')
    print(f"DStruct = {dstruct} m")


    # now solving for courant number
    N_zcells = 400
    coreHeight = 0.6392 # m
    inletU = 50 # m/s
    timeStep = 1.85e-5 # s
    zcellHeight = coreHeight / N_zcells
    courant = (2*inletU) * timeStep / zcellHeight
    print('-- Initial Required Courant Number: --')
    print(f"Co = {courant}")

    print('-- Porous Friction Factor Terms: --')
    for zone in pp:
        if zone == 'innerPropellant' or zone == 'outerPropellant': continue
        print(f"Zone: {zone}")
        A,B = getFrictionFactorTerms(50.0,pp[zone].epsilon,pp[zone].pebbleDiameter)
        print(f"   Coef,Const = {A:0.4e},{B:0.4e}")

    print('-- Porous Nusselt Number Coefs (Nield): --')
    print(f"fuel:     coeff   {getNusseltCorrelationNield(pp['fuelBed'].epsilon):0.4f} ")
    print(f"hotfrit:  coeff   {getNusseltCorrelationNield(pp['hotFrit'].epsilon):0.4f} ")
    print(f"coldfrit: coeff   {getNusseltCorrelationNield(pp['coldFrit'].epsilon):0.4f} ")
    print('-- Porous Nusselt Number Coefs (Achen): --')
    print(f"fuel:     coeff   {getNusseltCorrelationAchenbach(pp['fuelBed']):0.4f} ")
    print(f"hotfrit:  coeff   {getNusseltCorrelationAchenbach(pp['hotFrit']):0.4f} ")
    print(f"coldfrit: coeff   {getNusseltCorrelationAchenbach(pp['coldFrit']):0.4f} ")

    print('-- Mass Flow Rate: --')
    mfr = findMFR(powerFuelBed,18000,3000)
    print(f"Mass flowrate (power) = {mfr}")
    outletV = mfr/(0.6*3.14159*(.02129**2))
    #print(outletV)

    reactorThrust = 1.96e5 # Newtons, 40k lbs of thrust = 178k Newtons
    #exitBulkVelocity = 376 # m/s, exit velocity
    numAssemblies = 37
    specificImpulse = 950  # s
    g = 9.81               # m/s^2
    massFlowRate = reactorThrust / (specificImpulse * g * numAssemblies)
    print(f" Mass flowrate (thrust)= {massFlowRate:0.3f} kg/s")
    print(f"Core mass flow rate (thrust) = {massFlowRate*numAssemblies} kg/s")

    print('-- Ideal Gas Inlet Temperature: --')
    # i calculated 2.38 density using perfect gas and mass flowrate(0.57), velocity(50m/s), area(4.77e-4)
    rho_outIdeal = idealGas(9.1,200)
    print(f"rhoOutletIdeal = {rho_outIdeal}")

    print(f"inlet velocity = {inletVelocity()}")
    # import numpy as np
    # zCoords = np.linspace(0,pp['coldFrit'].coreHeight ,6)
    # for z in zCoords:
    #     if z == 0 or z == 0.6392:
    #         continue
    #     inner = pp['coldFrit'].innerRadiusFunction(z)
    #     outer = pp['coldFrit'].outerRadiusFunction(z)
    #     print(f"z = {0.6392-z}")
    #     print(f"p1 = {inner:0.8f}")
    #     print(f"p2 = {outer:0.8f}")

    # now i want to add hydrogen to the cold and hot frit, because at the moment i do not have any
    # packingFractionHotFrit = 0.3
    # packingFractionColdFrit = 0.6



